#!/bin/bash

#This file is part of Firres.

#Copyright (C) 2020  Inria

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.

#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

rm -rf 20200410_formal_cpe_cvss_v3
mkdir 20200410_formal_cpe_cvss_v3

cp 20200331_formal_cpe_cvss_v3/config.toml 20200410_formal_cpe_cvss_v3/config.toml

jq -cs '{evaluation_results: (.[0].evaluation_results + .[1].evaluation_results + .[2].evaluation_results + .[3].evaluation_results)}' 20200331_formal_cpe_cvss_v3/evaluation.truncated.json 20200404_formal_cpe_cvss_v3_january_to_april_2019/evaluation.json 20200404_formal_cpe_cvss_v3_may_to_august_2019/evaluation.json 20200404_formal_cpe_cvss_v3_september_to_december_2019/evaluation.json > 20200410_formal_cpe_cvss_v3/evaluation.json

echo "Please now execute firres on the newly created experiment to generate additional files and metadata. Be sure to have proper time properties in your firres.toml!"