#!/bin/bash

#This file is part of Firres.

#Copyright (C) 2020  Inria

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.

#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

# $1 = name of an experiment
# $2 = field name
# $3 = field value #1
# $4 = field value #2
# $5 = field value #3

echo -e "a\\p\t======= "$2" ======="
echo -e "\t"$3"\t"$4"\t"$5
echo -ne $3"\t"
./count_cvss_field_prediction.sh $1 $2 $3 $3
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $3 $4
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $3 $5
echo
echo -ne $4"\t"
./count_cvss_field_prediction.sh $1 $2 $4 $3
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $4 $4
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $4 $5
echo
echo -ne $5"\t"
./count_cvss_field_prediction.sh $1 $2 $5 $3
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $5 $4
echo -ne "\t"
./count_cvss_field_prediction.sh $1 $2 $5 $5
echo

echo