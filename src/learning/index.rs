/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use cleanutils;
use std::collections::BTreeMap;
use std::fmt::Write;

const CONSECUTIVE_WORDS: usize = 1; // In another variant of this codebase, this used to be a configurable parameter, but it makes no sense for CVSS prediction

#[derive(Debug, Clone)]
pub struct WordIndex {
    index: BTreeMap<String, WordInIndexEntry>,
    document_total: u64,
    whitelist: Option<Box<WordIndex>>,
}

impl WordIndex {
    pub fn new(whitelist: Option<Box<WordIndex>>) -> WordIndex {
        WordIndex {
            index: BTreeMap::new(),
            document_total: 0,
            whitelist,
        }
    }

    pub fn compute_reverse_index(&self) -> Vec<String> {
        println!("Compute reverse index...");
        let mut result = vec![String::new(); self.index.len()];

        for word in self.index.keys() {
            let entry = self.index.get(word).unwrap();

            result[entry.unique_index] = word.clone();
        }

        println!("Reverse index computed.");

        result
    }

    pub fn publish_document(&mut self, document: &String) -> Vec<WordInDocEntry> {
        let mut index_document: BTreeMap<String, WordInDocEntry> = BTreeMap::new();

        let cleaned_document = cleanutils::clean_document(document);

        let mut stacked_words = Vec::new();

        for uncleaned_word in cleaned_document.split_whitespace() {
            stacked_words.push(uncleaned_word.to_string());

            if stacked_words.len() > CONSECUTIVE_WORDS {
                stacked_words.remove(0);
            }

            let uncleaned_assembled_words = self.assemble_words(&stacked_words);

            for uncleaned_assembled_word in uncleaned_assembled_words {
                let is_first_word_capitalized = {
                    let first_word = uncleaned_assembled_word.split_whitespace().next().unwrap();
                    first_word != first_word.to_lowercase()
                };

                let assembled_word = cleanutils::clean_word(uncleaned_assembled_word);
                let accept_word = self.accept_word(&assembled_word);

                if accept_word {
                    self.publish_word(
                        assembled_word,
                        is_first_word_capitalized,
                        &mut index_document,
                    );
                }
            }
        }

        let mut indexed_document: Vec<WordInDocEntry> = index_document.values().cloned().collect();

        indexed_document.sort_by_key(|k| k.word.clone());

        self.publish_indexed_document(&indexed_document);

        indexed_document
    }

    fn accept_word(&self, word: &String) -> bool {
        match &self.whitelist {
            None => true,
            Some(whitelist_index) => whitelist_index.index.contains_key(word),
        }
    }

    /**
        Example : if input is ["microsoft", "word", "2007"]
        then input is ["microsoft word 2007", "word 2007", "2007"]

    */
    fn assemble_words(&mut self, stacked_words: &Vec<String>) -> Vec<String> {
        let mut assembled_words = Vec::new();

        for i in 0..stacked_words.len() {
            let mut assembled_word = String::new();
            for j in i..stacked_words.len() {
                write!(assembled_word, "{} ", stacked_words[j]).unwrap();
            }
            assembled_word = assembled_word.trim().to_string();
            assembled_words.push(assembled_word);
        }

        assembled_words
    }

    fn publish_word(
        &mut self,
        word: String,
        is_word_capitalized: bool,
        index_document: &mut BTreeMap<String, WordInDocEntry>,
    ) {
        // This is not the best thing performance-wise (2 or 3 traversals are needed on the B-Tree every time while we could theoretically go with one), but it is the most readable thing I've found within Rust memory model.
        if !index_document.contains_key(&word) {
            index_document.insert(
                word.clone(),
                WordInDocEntry {
                    word: word.clone(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 0,
                },
            );
        }

        let word_doc_entry = index_document.get_mut(&word).unwrap(); // We just made sure there was a value in any case

        word_doc_entry.term_frequency += 1;
        word_doc_entry.at_least_one_occurence_was_capitalized =
            word_doc_entry.at_least_one_occurence_was_capitalized || is_word_capitalized;
    }

    fn publish_indexed_document(&mut self, indexed_document: &Vec<WordInDocEntry>) {
        self.document_total += 1;

        for word_doc_entry in indexed_document {
            let word = &word_doc_entry.word;

            if !self.index.contains_key(word) {
                let unique_index = self.index.len();

                self.index.insert(
                    word.clone(),
                    WordInIndexEntry {
                        word: word.clone(),
                        document_with_word_count: 0,
                        unique_index,
                    },
                );
            }

            let word_index_entry = self.index.get_mut(word).unwrap(); // We just made sure there was a value in any case

            word_index_entry.document_with_word_count += 1
        }
    }

    pub fn get_unique_index(&self, word_entry: &WordInDocEntry) -> usize {
        self.index[&word_entry.word].unique_index
    }

    pub fn len(&self) -> usize {
        self.index.len()
    }
}

#[derive(Debug, Clone)]
pub struct WordInIndexEntry {
    pub word: String,
    pub document_with_word_count: u64,
    pub unique_index: usize,
}

#[derive(Clone, PartialEq, Debug)]
pub struct WordInDocEntry {
    pub word: String,
    pub at_least_one_occurence_was_capitalized: bool,
    pub term_frequency: u8,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_whitelist() {
        let mut whitelist = WordIndex::new(None);

        whitelist.publish_document(&"The Lord of the Rings".to_string());
        whitelist.publish_document(&"The Lord of the Flies".to_string());
        whitelist.publish_document(&"The Ace of Spades".to_string());

        let mut index = WordIndex::new(Some(Box::new(whitelist)));

        let entry4 = index.publish_document(&"The Lord of the Ragondins".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry4
        );

        for i in 0..entry4.len() {
            assert_eq!(i, index.get_unique_index(&entry4[i]));
        }
    }

    #[test]
    fn test_index() {
        let mut index = WordIndex::new(None);

        let entry1 = index.publish_document(&"The Lord of the Rings".to_string());
        let entry2 = index.publish_document(&"The Lord of the Flies".to_string());
        let entry3 = index.publish_document(&"The Ace of liBerty".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "rings".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry1
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "flies".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry2
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "ace".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "liberty".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
            ],
            entry3
        );

        for i in 0..entry1.len() {
            assert_eq!(i, index.get_unique_index(&entry1[i]));
        }
    }
}
