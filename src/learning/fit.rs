/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use rand::Rng;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::process::Command;
use time::timeframe::TimeFrame;
use vulnerability::cve::CVEDictionaryEntry;

// This might be *a bit* too much abstracted / generized for its own sake but YOLO
pub fn fit_and_predict_cvss<
    'a,
    Cvss,
    CveToCvss,
    CveToInput,
    CvssToOutput,
    OutputToCvss,
    ReduceInput,
>(
    unfiltered_training_vulnerabilities: &[&'a CVEDictionaryEntry],
    unfiltered_test_vulnerabilities: &[&'a CVEDictionaryEntry],
    time_frame: &TimeFrame,
    cve_to_cvss: CveToCvss,
    cve_to_input: CveToInput,
    cvss_to_output: CvssToOutput,
    cvss_headers: &Vec<&str>,
    output_to_cvss: OutputToCvss,
    reduce_input: ReduceInput,
    verbose_explicability: bool,
) -> Vec<(&'a CVEDictionaryEntry, Cvss, Cvss)>
where
    CveToCvss: Fn(&CVEDictionaryEntry) -> Option<Cvss>,
    CveToInput: Fn(&CVEDictionaryEntry) -> Vec<f64>,
    CvssToOutput: Fn(&Cvss) -> Vec<f64>,
    OutputToCvss: Fn(&Vec<f64>) -> Cvss,
    ReduceInput:
        Fn(&Vec<Vec<f64>>, &Vec<Vec<f64>>, &Vec<f64>, &TimeFrame) -> (Vec<Vec<f64>>, Vec<Vec<f64>>),
{
    let unfiltered_training_sample_size = unfiltered_training_vulnerabilities.len();
    let unfiltered_test_sample_size = unfiltered_test_vulnerabilities.len();
    let input_length = cve_to_input(unfiltered_training_vulnerabilities[0]).len();

    let filtered_training_vulnerabilities = unfiltered_training_vulnerabilities
        .iter()
        .copied()
        .filter(|x| cve_to_cvss(x).is_some())
        .collect::<Vec<&CVEDictionaryEntry>>();
    let filtered_training_sample_size = filtered_training_vulnerabilities.len();
    let filtered_test_vulnerabilities = unfiltered_test_vulnerabilities
        .iter()
        .copied()
        .filter(|x| cve_to_cvss(x).is_some())
        .collect::<Vec<&CVEDictionaryEntry>>();
    let filtered_test_sample_size = filtered_test_vulnerabilities.len();
    let output_length =
        cvss_to_output(&cve_to_cvss(filtered_training_vulnerabilities[0]).unwrap()).len();

    println!("Starting to fit vulnerabilities. Size of training sample : {} ({} before filtering). Size of test sample : {} ({} before filtering). Number of inputs : {}. Number of outputs : {}", filtered_training_sample_size, unfiltered_training_sample_size, filtered_test_sample_size, unfiltered_test_sample_size, input_length, output_length);

    if filtered_test_sample_size > 0 {
        let training_input = filtered_training_vulnerabilities
            .iter()
            .map(|x| cve_to_input(x))
            .collect::<Vec<Vec<f64>>>();
        let test_input = filtered_test_vulnerabilities
            .iter()
            .map(|x| cve_to_input(x))
            .collect::<Vec<Vec<f64>>>();

        // Did not managed to do this in iterative style.
        let mut training_outputs = vec![vec![0.0; filtered_training_sample_size]; output_length];

        for i in 0..filtered_training_vulnerabilities.len() {
            let vuln = filtered_training_vulnerabilities[i];
            let cvss_vector = cve_to_cvss(vuln).unwrap();
            let cvss_outputs = cvss_to_output(&cvss_vector);

            for j in 0..cvss_outputs.len() {
                training_outputs[j][i] = cvss_outputs[j];
            }
        }

        let mut test_outputs = vec![Vec::new(); filtered_test_vulnerabilities.len()];

        for (count, training_output) in training_outputs.iter().enumerate() {
            println!("Fitting CVSS field {}", cvss_headers[count]);
            let (reduced_training_input, reduced_test_input) =
                reduce_input(&training_input, &test_input, &training_output, &time_frame);

            let (test_output, coefficients) = fit_and_predict_f64(
                &reduced_training_input,
                &training_output,
                &reduced_test_input,
            );
            for i in 0..test_outputs.len() {
                test_outputs[i].push(test_output[i]);
            }
            if verbose_explicability {
                let all_weighted_words = coefficients
                    .iter()
                    .zip(time_frame.reverse_index.iter())
                    .map(|(weight, word)| (word.clone(), *weight))
                    .collect::<Vec<_>>();

                for vulnerability in &filtered_test_vulnerabilities {
                    let mut weighted_words = all_weighted_words
                        .iter()
                        .zip(cve_to_input(vulnerability))
                        .filter_map(|(weighted_word, count)| {
                            if count > 0.0 {
                                Some(weighted_word)
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<_>>();

                    weighted_words.sort_by(|a, b| b.1.abs().partial_cmp(&a.1.abs()).unwrap());

                    println!("-------------------");
                    println!("{}", vulnerability.cve_id);
                    println!("{}", vulnerability.description);
                    println!("{:?}", weighted_words);
                }
            }
        }

        filtered_test_vulnerabilities
            .iter()
            .zip(test_outputs.iter())
            .map(|(x, y)| (*x, cve_to_cvss(x).unwrap(), output_to_cvss(y)))
            .collect()
    } else {
        println!("No testable vulnerabilities were disclosed that day, skipping.");
        Vec::new()
    }
}

fn fit_and_predict_f64(
    training_input: &Vec<Vec<f64>>,
    training_output: &Vec<f64>,
    test_input: &Vec<Vec<f64>>,
) -> (Vec<f64>, Vec<f64>) {
    if training_input.len() == 0 {
        panic!("There should be a non-zero number of training inputs !")
    }

    if test_input.len() == 0 {
        panic!("There should be a non-zero number of test inputs !")
    }

    if training_input.len() != training_output.len() {
        panic!(
            "Training input and output vectors should have the same length ! {} vs {}",
            training_input.len(),
            training_output.len()
        )
    }

    let input_length = training_input[0].len();
    for input in training_input {
        if input.len() != input_length {
            panic!("All input vectors should be the same size !")
        }
    }
    for input in test_input {
        if input.len() != input_length {
            panic!("All input vectors should be the same size !")
        }
    }

    let unique_id = provide_unique_id();

    println!("Dumping data into files...");
    create_files(
        unique_id.as_str(),
        training_input,
        training_output,
        test_input,
    );

    println!("Calling python / scikit learn...");
    let result = Command::new("python3")
        .arg("python/linear_fit.py")
        .arg(format!("/tmp/{}.train.input", unique_id))
        .arg(format!("/tmp/{}.train.output", unique_id))
        .arg(format!("/tmp/{}.test.input", unique_id))
        .arg(format!("/tmp/{}.test.output", unique_id))
        .arg(format!("/tmp/{}.coefficients", unique_id))
        .output()
        .unwrap();

    println!("Back to Rust.");

    let errors = String::from_utf8(result.stderr).unwrap();
    if !errors.trim().is_empty() {
        panic!(
            "Errors happend while invoking python / scikit learn script : {}",
            errors
        );
    }

    let output_f = File::open(format!("/tmp/{}.test.output", unique_id)).unwrap();
    let output_file = BufReader::new(output_f);

    let mut result = Vec::new();

    for l in output_file.lines() {
        result.push(l.unwrap().parse().unwrap());
    }

    if result.len() != test_input.len() {
        panic!("Number of output differs from number of input test vectors !");
    }

    let coefficients_f = File::open(format!("/tmp/{}.coefficients", unique_id)).unwrap();
    let coefficients_file = BufReader::new(coefficients_f);

    let coefficients_as_string = coefficients_file.lines().next().unwrap().unwrap();
    let coefficients = coefficients_as_string
        .split(',')
        .map(|x| x.parse::<f64>().unwrap())
        .collect::<Vec<_>>();

    if coefficients.len() != test_input[0].len() {
        panic!("Number of coefficients differs from input number of dimensions !");
    }

    remove_files(unique_id.as_str());

    (result, coefficients)
}

fn create_files(
    unique_id: &str,
    training_input: &Vec<Vec<f64>>,
    training_output: &Vec<f64>,
    test_input: &Vec<Vec<f64>>,
) {
    create_file(
        format!("/tmp/{}.train.input", unique_id),
        &mut training_input.iter().map(format_f64_array_to_line),
    );

    create_file(
        format!("/tmp/{}.train.output", unique_id),
        &mut training_output.iter().map(|x| x.to_string()),
    );
    create_file(
        format!("/tmp/{}.test.input", unique_id),
        &mut test_input.iter().map(format_f64_array_to_line),
    );
}

fn remove_files(unique_id: &str) {
    std::fs::remove_file(format!("/tmp/{}.train.input", unique_id)).unwrap();
    std::fs::remove_file(format!("/tmp/{}.train.output", unique_id)).unwrap();
    std::fs::remove_file(format!("/tmp/{}.test.input", unique_id)).unwrap();
    std::fs::remove_file(format!("/tmp/{}.test.output", unique_id)).unwrap();
    std::fs::remove_file(format!("/tmp/{}.coefficients", unique_id)).unwrap();
}

fn create_file(path: String, data: &mut dyn Iterator<Item = String>) {
    let mut f = File::create(&path).unwrap();

    // No need for a buffered writer : we write every line on disk directly as they can be massive: we need to get them out of memory before working on the next one
    for line in data {
        writeln!(f, "{}", line).unwrap();
    }
}

fn format_f64_array_to_line(array: &Vec<f64>) -> String {
    array
        .iter()
        .map(|y| y.to_string())
        .collect::<Vec<String>>()
        .join(",")
}

fn provide_unique_id() -> String {
    rand::thread_rng().gen_ascii_chars().take(10).collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use config::FirresConfig;
    use time::timeatlas::TimeAtlas;
    use time::timeframe::TimeFrame;

    #[test]
    fn test_simple_fit() {
        let mut firres_config = FirresConfig::new();

        // Only load NVD 2007 file in this test to make it go faster
        firres_config.cvss.suffix = "2007.json".to_string();

        // Make sure the config is correct *for the test*
        firres_config.time.nb_iterations = 365;
        firres_config.time.days_per_iteration = 1;
        firres_config.time.metadata_delay = 7;

        let time_atlas = TimeAtlas::new(&firres_config);

        let time_frame =
            time_atlas.fetch_frame(10, &firres_config, &firres_config.experiments.cvss);

        let all_vulnerabilities = &time_frame.cve_dictionary.entries;

        let training_vulnerabilities = &all_vulnerabilities[0..10].iter().collect::<Vec<_>>();
        let test_vulnerabilities = &all_vulnerabilities[10..20].iter().collect::<Vec<_>>();

        let result = fit_and_predict_cvss(
            &training_vulnerabilities,
            test_vulnerabilities,
            &time_frame,
            |x| cve_to_cvss(&time_atlas, x),
            |x| cve_to_input(&time_frame, x),
            cvss_to_output,
            &cvss_headers(),
            output_to_cvss,
            dont_reduce_input,
            firres_config.verbose_explicability,
        );
        assert_eq!(10, result.len());
    }

    fn cvss_headers() -> Vec<&'static str> {
        vec!["AV", "AC", "Au", "C", "I", "A"]
    }

    fn cve_to_cvss(time_atlas: &TimeAtlas, vulnerability: &CVEDictionaryEntry) -> Option<f64> {
        match time_atlas
            .get_cvss_v2_map()
            .get(vulnerability.cve_id.as_str())
        {
            None => None,
            Some(x) => Some(x.base.access_vector.numerical_value()),
        }
    }

    fn cve_to_input(time_frame: &TimeFrame, vulnerability: &CVEDictionaryEntry) -> Vec<f64> {
        let mut vector = vec![0.0; time_frame.cve_index.len()];

        for word_in_doc in &vulnerability.words {
            let index = time_frame.cve_index.get_unique_index(word_in_doc);
            vector[index] = word_in_doc.term_frequency as f64;
        }

        vector
    }

    fn cvss_to_output(cvss: &f64) -> Vec<f64> {
        vec![*cvss]
    }

    fn output_to_cvss(output: &Vec<f64>) -> f64 {
        assert_eq!(1, output.len());
        output[0]
    }

    fn dont_reduce_input(
        training_input: &Vec<Vec<f64>>,
        test_input: &Vec<Vec<f64>>,
        _training_output: &Vec<f64>,
        _time_frame: &TimeFrame,
    ) -> (Vec<Vec<f64>>, Vec<Vec<f64>>) {
        (training_input.clone(), test_input.clone())
    }
}
