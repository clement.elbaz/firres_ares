/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
extern crate crossbeam;
extern crate rand;
#[macro_use]
extern crate lazy_static;
extern crate regex;

#[macro_use]
extern crate serde_derive;

extern crate bincode;
extern crate chrono;
extern crate config as config_crate;
extern crate edit_distance;
extern crate libcvss;
extern crate serde;
extern crate serde_json;
extern crate toml;

mod config;
use config::FirresConfig;

mod learning;

mod vulnerability;

mod cleanutils;

mod integrity;

mod experiments;

mod time;

use time::timeatlas::TimeAtlas;

fn main() {
    let firres_config = FirresConfig::new();

    let time_atlas = TimeAtlas::new(&firres_config);

    experiments::launch_cvss_prediction_experiment(&firres_config, &time_atlas);
}
