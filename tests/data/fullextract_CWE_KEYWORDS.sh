#!/bin/bash
# $1 = path to zipped CWE file
pubdate_v2=$(cat $1 | gunzip | xpath -q -e 'string(/Weakness_Catalog/@Catalog_Date)')
pubdate_v3=$(cat $1 | gunzip | xpath -q -e 'string(/Weakness_Catalog/@Date)')

[ -n "$pubdate_v2" ] && pubdate=$pubdate_v2 || pubdate=$pubdate_v3
./extract_CWE_KEYWORDS.sh $1 > CWE_KEYWORDS/$pubdate