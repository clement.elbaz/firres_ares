#!/bin/bash
# $1 = path to yearly CVE file from NIST
tmpfile=$(mktemp)
./extract_publication_dates.sh $1 | while read line; do cat CPE_URL_KEYWORDS/$line > ${tmpfile} 2>/dev/null; ./extract_CPE_URL_KEYWORDS.sh $1 $line ${tmpfile} > CPE_URL_KEYWORDS/$line; done