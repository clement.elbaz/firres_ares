#!/bin/bash
# $1 = path to yearly CVE file from NIST
jq -r '.CVE_Items [] | .publishedDate ' $1 | cut -c 1-10 | sort | uniq