CVE-2009-4835
The (1) htk_read_header, (2) alaw_init, (3) ulaw_init, (4) pcm_init, (5) float32_init, and (6) sds_read_header functions in libsndfile 1.0.20 allow context-dependent attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted audio file.
CVE-2009-4836
Eval injection vulnerability in system/services/init.php in Movie PHP Script 2.0 allows remote attackers to execute arbitrary PHP code via the anticode parameter.
CVE-2009-4837
Multiple cross-site scripting (XSS) vulnerabilities in Basic Analysis and Security Engine (BASE) before 1.4.3.1 allow remote attackers to inject arbitrary web script or HTML via the (1) sig[1] parameter to base/base_qry_main.php, or the time[0][1] parameter to (2) base/base_stat_alerts.php or (3) base/base_stat_uaddr.php.  NOTE: some of these details are obtained from third party information.
CVE-2009-4838
SQL injection vulnerability in base_ag_common.php in Basic Analysis and Security Engine (BASE) before 1.4.3.1 allows remote attackers to execute arbitrary SQL commands via unspecified parameters.  NOTE: some of these details are obtained from third party information.
CVE-2009-4839
Multiple cross-site scripting (XSS) vulnerabilities in Basic Analysis and Security Engine (BASE), possibly 1.4.4 and earlier, allow remote attackers to inject arbitrary web script or HTML via unspecified parameters to (1) admin/base_roleadmin.php, (2) admin/base_useradmin.php, (3) base_conf_contents.php, (4) base_qry_sqlcalls.php, and (5) base_ag_main.php.
CVE-2009-4840
Heap-based buffer overflow in the IAManager ActiveX control in IAManager.dll in Roxio CinePlayer 3.2 allows remote attackers to execute arbitrary code via a long argument to the SetIAPlayerName method.
CVE-2009-4841
Heap-based buffer overflow in the SonicMediaPlayer ActiveX control in SonicMediaPlayer.dll in Roxio CinePlayer 3.2 allows remote attackers to execute arbitrary code via a long argument to the DiskType method. NOTE: this might overlap CVE-2007-1559.
CVE-2010-0995
Stack-based buffer overflow in Internet Download Manager (IDM) before 5.19 allows remote attackers to execute arbitrary code via a crafted FTP URI that causes unspecified "test sequences" to be sent from client to server.
CVE-2010-1438
Web Application Finger Printer (WAFP) 0.01-26c3 uses fixed pathnames under /tmp for temporary files and directories, which (1) allows local users to cause a denial of service (application outage) by creating a file with a pathname that the product expects is available for its own internal use, (2) allows local users to overwrite arbitrary files via symlink attacks on certain files in /tmp, (3) might allow local users to delete arbitrary files and directories via a symlink attack on a directory under /tmp, and (4) might make it easier for local users to obtain sensitive information by reading files in a directory under /tmp, related to (a) lib/wafp_pidify.rb, (b) utils/generate_wafp_fingerprint.sh, (c) utils/online_update.sh, and (d) utils/extract_from_db.sh.
CVE-2010-1583
SQL injection vulnerability in the loadByKey function in the TznDbConnection class in tzn_mysql.php in Tirzen (aka TZN) Framework 1.5, as used in TaskFreak! before 0.6.3, allows remote attackers to execute arbitrary SQL commands via the username field in a login action.
CVE-2010-1681
Buffer overflow in VISIODWG.DLL before 10.0.6880.4 in Microsoft Office Visio allows user-assisted remote attackers to execute arbitrary code via a crafted DXF file, a different vulnerability than CVE-2010-0254 and CVE-2010-0256.
CVE-2010-1724
Multiple cross-site scripting (XSS) vulnerabilities in Zikula Application Framework 1.2.2, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via the (1) func parameter to index.php, or the (2) lang parameter to index.php, which is not properly handled by ZLanguage.php.
CVE-2010-1725
SQL injection vulnerability in offers_buy.php in Alibaba Clone Platinum allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-1726
SQL injection vulnerability in offers_buy.php in EC21 Clone 3.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-1727
SQL injection vulnerability in type.asp in JobPost 1.0 allows remote attackers to execute arbitrary SQL commands via the iType parameter. NOTE: some of these details are obtained from third party information.
CVE-2010-1728
Opera before 10.53 on Windows and Mac OS X does not properly handle a series of document modifications that occur asynchronously, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via JavaScript that writes <marquee> sequences in an infinite loop, leading to attempted use of uninitialized memory.  NOTE: this might overlap CVE-2006-6955.
CVE-2010-1729
WebKit.dll in WebKit, as used in Safari.exe 4.531.9.1 in Apple Safari, allows remote attackers to cause a denial of service (application crash) via JavaScript that writes <marquee> sequences in an infinite loop.
CVE-2010-1730
Dolphin Browser 2.5.0 on the HTC Hero allows remote attackers to cause a denial of service (application crash) via JavaScript that writes <marquee> sequences in an infinite loop.
CVE-2010-1731
Google Chrome on the HTC Hero allows remote attackers to cause a denial of service (application crash) via JavaScript that writes <marquee> sequences in an infinite loop.
CVE-2010-1732
Cross-site request forgery (CSRF) vulnerability in the users module in Zikula Application Framework before 1.2.3 allows remote attackers to hijack the authentication of administrators for requests that change the administrator email address (updateemail action).
CVE-2010-1733
Multiple SQL injection vulnerabilities in OCS Inventory NG before 1.02.3 allow remote attackers to execute arbitrary SQL commands via (1) multiple inventory fields to the search form, reachable through index.php; or (2) the "Software name" field to the "All softwares" search form, reachable through index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-1734
The SfnINSTRING function in win32k.sys in the kernel in Microsoft Windows 2000, XP, and Server 2003 allows local users to cause a denial of service (system crash) via a 0x18d value in the second argument (aka the Msg argument) of a PostMessage function call for the DDEMLEvent window.
CVE-2010-1735
The SfnLOGONNOTIFY function in win32k.sys in the kernel in Microsoft Windows 2000, XP, and Server 2003 allows local users to cause a denial of service (system crash) via a 0x4c value in the second argument (aka the Msg argument) of a PostMessage function call for the DDEMLEvent window.
CVE-2010-1736
KrM Haber 1.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for d_atabase/Krmdb.mdb.
CVE-2010-1737
PHP remote file inclusion vulnerability in core/includes/gfw_smarty.php in Gallo 0.1.0, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary PHP code via a URL in the config[gfwroot] parameter.
CVE-2010-1738
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2010-1448.  Reason: This candidate is a duplicate of CVE-2010-1448.  Notes: All CVE users should reference CVE-2010-1448 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2010-1739
SQL injection vulnerability in the Newsfeeds (com_newsfeeds) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the feedid parameter in a categories action to index.php.
CVE-2010-1740
SQL injection vulnerability in newsletter.php in GuppY 4.5.18 allows remote attackers to execute arbitrary SQL commands via the lng parameter.
CVE-2010-1741
SQL injection vulnerability in request_account.php in Billwerx RC 5.2.2 PL2 allows remote attackers to execute arbitrary SQL commands via the primary_number parameter.
CVE-2010-1742
Cross-site scripting (XSS) vulnerability in projects.php in Scratcher allows remote attackers to inject arbitrary web script or HTML via the show parameter.
CVE-2010-1743
SQL injection vulnerability in projects.php in Scratcher allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-1744
SQL injection vulnerability in product.html in B2B Gold Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2010-1745
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2010-1867.  Reason: This candidate is a duplicate of CVE-2010-1867.  Notes: All CVE users should reference CVE-2010-1867 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2010-1746
Multiple cross-site scripting (XSS) vulnerabilities in the Table JX (com_grid) component for Joomla! allow remote attackers to inject arbitrary web script or HTML via the (1) data_search and (2) rpp parameters to index.php.
