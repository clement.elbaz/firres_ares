CVE-2014-9202
Multiple stack-based buffer overflows in an unspecified DLL file in Advantech WebAccess before 8.0_20150816 allow remote attackers to execute arbitrary code via a crafted file that triggers long string arguments to functions.
CVE-2015-1781
Buffer overflow in the gethostbyname_r and other unspecified NSS functions in the GNU C Library (aka glibc or libc6) before 2.22 allows context-dependent attackers to cause a denial of service (crash) or execute arbitrary code via a crafted DNS response, which triggers a call with a misaligned buffer.
CVE-2015-3203
Unrestricted file upload vulnerability in h5ai before 0.25.0 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in the directory specified by the href parameter.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-3974
EasyIO EasyIO-30P-SF controllers with firmware before 0.5.21 and 2.x before 2.0.5.21, as used in Accutrol, Bar-Tech Automation, Infocon/EasyIO, Honeywell Automation India, Johnson Controls, SyxthSENSE, Transformative Wave Technologies, Tridium Asia Pacific, and Tridium Europe products, have a hardcoded password, which makes it easier for remote attackers to obtain access via unspecified vectors.
CVE-2015-5082
Endian Firewall before 3.0 allows remote attackers to execute arbitrary commands via shell metacharacters in the (1) NEW_PASSWORD_1 or (2) NEW_PASSWORD_2 parameter to cgi-bin/chpasswd.cgi.
CVE-2015-5185
The lookupProviders function in providerMgr.c in sblim-sfcb 1.3.4 and 1.3.18 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via an empty className in a packet.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-5279
Heap-based buffer overflow in the ne2000_receive function in hw/net/ne2000.c in QEMU before 2.4.0.1 allows guest OS users to cause a denial of service (instance crash) or possibly execute arbitrary code via vectors related to receiving packets.
CVE-2015-5372
The SAML 2.0 implementation in AdNovum nevisAuth 4.13.0.0 before 4.18.3.1, when using SAML POST-Binding, does not match all attributes of the X.509 certificate embedded in the assertion against the certificate from the identity provider (IdP), which allows remote attackers to inject arbitrary SAML assertions via a crafted certificate.
CVE-2015-5375
Cross-site scripting (XSS) vulnerability in unspecified dialogs for printing content in the Front End in Open-Xchange Server 6 and OX App Suite before 6.22.8-rev8, 6.22.9 before 6.22.9-rev15m, 7.x before 7.6.1-rev25, and 7.6.2 before 7.6.2-rev20 allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to object properties.
CVE-2015-5400
Squid before 3.5.6 does not properly handle CONNECT method peer responses when configured with cache_peer, which allows remote attackers to bypass intended restrictions and gain access to a backend proxy via a CONNECT request.
CVE-2015-5703
SQL injection vulnerability in the public key discovery API call in Open-Xchange OX Guard before 2.0.0-rev8 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-5957
Buffer overflow in the DumpSysVar function in var.c in Remind before 3.1.15 allows attackers to have unspecified impact via a long name.
CVE-2015-6007
Cross-site request forgery (CSRF) vulnerability in Web Reference Database (aka refbase) through 0.9.6 allows remote attackers to hijack the authentication of arbitrary users.
CVE-2015-6008
install.php in Web Reference Database (aka refbase) through 0.9.6 allows remote attackers to execute arbitrary commands via the adminPassword parameter, a different issue than CVE-2015-7381.
CVE-2015-6009
Multiple SQL injection vulnerabilities in Web Reference Database (aka refbase) through 0.9.6 allow remote attackers to execute arbitrary SQL commands via (1) the where parameter to rss.php or (2) the sqlQuery parameter to search.php, a different issue than CVE-2015-7382.
CVE-2015-6010
Multiple cross-site scripting (XSS) vulnerabilities in Web Reference Database (aka refbase) through 0.9.6 and bleeding-edge before 2015-01-08 allow remote attackers to inject arbitrary web script or HTML via the (1) errorNo or (2) errorMsg parameter to error.php; the (3) viewType parameter to duplicate_manager.php; the (4) queryAction, (5) displayType, (6) citeOrder, (7) sqlQuery, (8) showQuery, (9) showLinks, (10) showRows, or (11) queryID parameter to query_manager.php; the (12) sourceText or (13) sourceIDs parameter to import.php; or the (14) typeName or (15) fileName parameter to modify.php.
CVE-2015-6011
Web Reference Database (aka refbase) through 0.9.6 and bleeding-edge before 2015-01-08 allows remote attackers to conduct XML injection attacks via (1) the id parameter to unapi.php or (2) the stylesheet parameter to sru.php.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-6012
Multiple open redirect vulnerabilities in Web Reference Database (aka refbase) through 0.9.6 and bleeding-edge before 2015-01-08 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via the referrer parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-6278
The IPv6 snooping functionality in the first-hop security subsystem in Cisco IOS 12.2, 15.0, 15.1, 15.2, 15.3, 15.4, and 15.5 and IOS XE 3.2SE, 3.3SE, 3.3XO, 3.4SG, 3.5E, and 3.6E before 3.6.3E; 3.7E before 3.7.2E; 3.9S and 3.10S before 3.10.6S; 3.11S before 3.11.4S; 3.12S and 3.13S before 3.13.3S; and 3.14S before 3.14.2S does not properly implement the Control Plane Protection (aka CPPr) feature, which allows remote attackers to cause a denial of service (device reload) via a flood of ND packets, aka Bug ID CSCus19794.
CVE-2015-6279
The IPv6 snooping functionality in the first-hop security subsystem in Cisco IOS 12.2, 15.0, 15.1, 15.2, 15.3, 15.4, and 15.5 and IOS XE 3.2SE, 3.3SE, 3.3XO, 3.4SG, 3.5E, and 3.6E before 3.6.3E; 3.7E before 3.7.2E; 3.9S and 3.10S before 3.10.6S; 3.11S before 3.11.4S; 3.12S and 3.13S before 3.13.3S; and 3.14S before 3.14.2S allows remote attackers to cause a denial of service (device reload) via a malformed ND packet with the Cryptographically Generated Address (CGA) option, aka Bug ID CSCuo04400.
CVE-2015-6280
The SSHv2 functionality in Cisco IOS 15.2, 15.3, 15.4, and 15.5 and IOS XE 3.6E before 3.6.3E, 3.7E before 3.7.1E, 3.10S before 3.10.6S, 3.11S before 3.11.4S, 3.12S before 3.12.3S, 3.13S before 3.13.3S, and 3.14S before 3.14.1S does not properly implement RSA authentication, which allows remote attackers to obtain login access by leveraging knowledge of a username and the associated public key, aka Bug ID CSCus73013.
CVE-2015-6307
Cisco FirePOWER (formerly Sourcefire) 7000 and 8000 devices with software 5.4.0.1 allow remote attackers to cause a denial of service (inspection-engine outage) via crafted packets, aka Bug ID CSCuu10871.
CVE-2015-6463
CodeWrights HART Comm DTM components, as used with Endress+Hauser FieldCare, allow remote attackers to read arbitrary files, send HTTP requests to intranet servers, or cause a denial of service (CPU and memory consumption) via a longtag XML schema containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-6806
The MScrollV function in ansi.c in GNU screen 4.3.1 and earlier does not properly limit recursion, which allows remote attackers to cause a denial of service (stack consumption) via an escape sequence with a large repeat count value.
CVE-2015-6927
vzctl before 4.9.4 determines the virtual environment (VE) layout based on the presence of root.hdd/DiskDescriptor.xml in the VE private directory, which allows local simfs container (CT) root users to change the root password for arbitrary ploop containers, as demonstrated by a symlink attack on the ploop container root.hdd file and then access a control panel.
CVE-2015-6928
classes/admin.class.php in CubeCart 5.2.12 through 5.2.16 and 6.x before 6.0.7 does not properly validate that a password reset request was made, which allows remote attackers to change the administrator password via a recovery request with a space character in the validate parameter and the administrator email in the email parameter.
CVE-2015-7381
Multiple PHP remote file inclusion vulnerabilities in install.php in Web Reference Database (aka refbase) through 0.9.6 allow remote attackers to execute arbitrary PHP code via the (1) pathToMYSQL or (2) databaseStructureFile parameter, a different issue than CVE-2015-6008.
CVE-2015-7382
SQL injection vulnerability in install.php in Web Reference Database (aka refbase) through 0.9.6 allows remote attackers to execute arbitrary SQL commands via the defaultCharacterSet parameter, a different issue than CVE-2015-6009.
CVE-2015-7383
Multiple cross-site scripting (XSS) vulnerabilities in Web Reference Database (aka refbase) through 0.9.6 and bleeding-edge through 2015-04-28 allow remote attackers to inject arbitrary web script or HTML via the (1) adminUserName, (2) pathToMYSQL, (3) databaseStructureFile, or (4) pathToBibutils parameter to install.php or the (5) adminUserName parameter to update.php.
CVE-2015-7386
Multiple cross-site scripting (XSS) vulnerabilities in includes/metaboxes.php in the Gallery - Photo Albums - Portfolio plugin 1.3.47 for WordPress allow remote authenticated users to inject arbitrary web script or HTML via the (1) Media Title or (2) Media Subtitle fields.
CVE-2015-7387
ZOHO ManageEngine EventLog Analyzer 10.6 build 10060 and earlier allows remote attackers to bypass intended restrictions and execute arbitrary SQL commands via an allowed query followed by a disallowed one in the query parameter to event/runQuery.do, as demonstrated by "SELECT 1;INSERT INTO."
