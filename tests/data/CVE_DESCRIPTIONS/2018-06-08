CVE-2011-3172
A vulnerability in pam_modules of SUSE Linux Enterprise allows attackers to log into accounts that should have been disabled. Affected releases are SUSE Linux Enterprise: versions prior to 12.
CVE-2011-4190
The kdump implementation is missing the host key verification in the kdump and mkdumprd OpenSSH integration of kdump prior to version 2012-01-20. This is similar to CVE-2011-3588, but different in that the kdump implementation is specific to SUSE. A remote malicious kdump server could use this flaw to impersonate the correct kdump server to obtain security sensitive information (kdump core files).
CVE-2012-0433
The install-chef-suse.sh script shipped with crowbar before 2012-10-02 is creating files containing confidential data with insecure permissions, allowing local users to read confidential data.
CVE-2013-3703
The controller of the Open Build Service API prior to version 2.4.4 is missing a write permission check, allowing an authenticated attacker to add or remove user roles from packages and/or project meta data.
CVE-2014-0593
The set_version script as shipped with obs-service-set_version is a source validator for the Open Build Service (OBS). In versions prior to 0.5.3-1.1 this script did not properly sanitize the input provided by the user, allowing for code execution on the executing server.
CVE-2014-0594
In the Open Build Service (OBS) before version 2.4.6 the CSRF protection is incorrectly disabled in the web interface, allowing for requests without the user's consent.
CVE-2014-5220
The mdcheck script of the mdadm package for openSUSE 13.2 prior to version 3.3.1-5.14.1 does not properly sanitize device names, which allows local attackers to execute arbitrary commands as root.
CVE-2017-12075
Command injection vulnerability in EZ-Internet in Synology DiskStation Manager (DSM) before 6.2-23739 allows remote authenticated users to execute arbitrary command via the username parameter.
CVE-2017-12078
Command injection vulnerability in EZ-Internet in Synology Router Manager (SRM) before 1.1.6-6931 allows remote authenticated users to execute arbitrary command via the username parameter.
CVE-2017-1405
IBM Security Identity Manager Virtual Appliance 7.0 processes patches, image backups and other updates without sufficiently verifying the origin and integrity of the code. IBM X-Force ID: 127392.
CVE-2018-0225
The Enterprise Console in Cisco AppDynamics App iQ Platform before 4.4.3.10598 (HF4) allows SQL injection, aka the Security Advisory 2089 issue.
CVE-2018-10088
Buffer overflow in XiongMai uc-httpd 1.0.0 has unspecified impact and attack vectors, a different vulnerability than CVE-2017-16725.
CVE-2018-10358
A pool corruption privilege escalation vulnerability in Trend Micro OfficeScan 11.0 SP1 and XG could allow a local attacker to escalate privileges on vulnerable installations due to a flaw within the processing of IOCTL 0x2200B4 in the TMWFP driver. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability.
CVE-2018-10359
A pool corruption privilege escalation vulnerability in Trend Micro OfficeScan 11.0 SP1 and XG could allow a local attacker to escalate privileges on vulnerable installations due to a flaw within the processing of IOCTL 0x220078 in the TMWFP driver. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability.
CVE-2018-10505
A pool corruption privilege escalation vulnerability in Trend Micro OfficeScan 11.0 SP1 and XG could allow a local attacker to escalate privileges on vulnerable installations due to a flaw within the processing of IOCTL 0x220008 in the TMWFP driver. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability.
CVE-2018-10506
A out-of-bounds read information disclosure vulnerability in Trend Micro OfficeScan 11.0 SP1 and XG could allow a local attacker to disclose sensitive information on vulnerable installations due to a flaw within the processing of IOCTL 0x220004 by the TMWFP driver. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability.
CVE-2018-11228
Crestron TSW-1060, TSW-760, TSW-560, TSW-1060-NC, TSW-760-NC, and TSW-560-NC devices before 2.001.0037.001 allow unauthenticated remote code execution via a Bash shell service in Crestron Toolbox Protocol (CTP).
CVE-2018-11229
Crestron TSW-1060, TSW-760, TSW-560, TSW-1060-NC, TSW-760-NC, and TSW-560-NC devices before 2.001.0037.001 allow unauthenticated remote code execution via command injection in Crestron Toolbox Protocol (CTP).
CVE-2018-11409
Splunk through 7.0.1 allows information disclosure by appending __raw/services/server/info/server-info?output_mode=json to a query, as demonstrated by discovering a license key.
CVE-2018-12020
mainproc.c in GnuPG before 2.2.8 mishandles the original filename during decryption and verification actions, which allows remote attackers to spoof the output that GnuPG sends on file descriptor 2 to other programs that use the "--status-fd 2" option. For example, the OpenPGP data might represent an original filename that contains line feed characters in conjunction with GOODSIG or VALIDSIG status codes.
CVE-2018-12041
An issue was discovered on the MediaTek AWUS036NH wireless USB adapter through 5.1.25.0. Attackers can remotely deny service by sending specially constructed 802.11 frames.
CVE-2018-12045
DedeCMS through V5.7SP2 allows arbitrary file upload in dede/file_manage_control.php via a dede/file_manage_view.php?fmdo=upload request with an upfile1 parameter, as demonstrated by uploading a .php file.
CVE-2018-12046
DedeCMS through 5.7SP2 allows arbitrary file write in dede/file_manage_control.php via a dede/file_manage_view.php?fmdo=newfile request with name and str parameters, as demonstrated by writing to a new .php file.
CVE-2018-12047
xfind/search in Ximdex 4.0 has XSS via the filter[n][value] parameters for non-negative values of n, as demonstrated by n equal to 0 through 12.
CVE-2018-12048
** DISPUTED ** A remote attacker can bypass the Management Mode on the Canon LBP7110Cw web interface without a PIN for /checkLogin.cgi via vectors involving /portal_top.html to get full access to the device. NOTE: the vendor reportedly responded that this issue occurs when a customer keeps the default settings without using the countermeasures and best practices shown in the documentation.
CVE-2018-12049
** DISPUTED ** A remote attacker can bypass the System Manager Mode on the Canon LBP6030w web interface without a PIN for /checkLogin.cgi via vectors involving /portal_top.html to get full access to the device. NOTE: the vendor reportedly responded that this issue occurs when a customer keeps the default settings without using the countermeasures and best practices shown in the documentation.
CVE-2018-12051
Arbitrary File Upload and Remote Code Execution exist in PHP Scripts Mall Schools Alert Management Script via $_FILE in /webmasterst/general.php, as demonstrated by a .php file with the image/jpeg content type.
CVE-2018-12052
SQL Injection exists in PHP Scripts Mall Schools Alert Management Script via the q Parameter in get_sec.php.
CVE-2018-12053
Arbitrary File Deletion exists in PHP Scripts Mall Schools Alert Management Script via the img parameter in delete_img.php by using directory traversal.
CVE-2018-12054
Arbitrary File Read exists in PHP Scripts Mall Schools Alert Management Script via the f parameter in img.php, aka absolute path traversal.
CVE-2018-12055
Multiple SQL Injections exist in PHP Scripts Mall Schools Alert Management Script via crafted POST data in contact_us.php, faq.php, about.php, photo_gallery.php, privacy.php, and so on.
CVE-2018-12064
tinyexr 0.9.5 has a heap-based buffer over-read via tinyexr::ReadChannelInfo in tinyexr.h.
CVE-2018-12065
A Local File Inclusion vulnerability in /system/WCore/WHelper.php in Creatiwity wityCMS 0.6.2 allows remote attackers to include local PHP files (execute PHP code) or read non-PHP files by replacing a helper.json file.
CVE-2018-12066
BIRD Internet Routing Daemon before 1.6.4 allows local users to cause a denial of service (stack consumption and daemon crash) via BGP mask expressions in birdc.
CVE-2018-1281
The clustered setup of Apache MXNet allows users to specify which IP address and port the scheduler will listen on via the DMLC_PS_ROOT_URI and DMLC_PS_ROOT_PORT env variables. In versions older than 1.0.0, however, the MXNet framework will listen on 0.0.0.0 rather than user specified DMLC_PS_ROOT_URI once a scheduler node is initialized. This exposes the instance running MXNet to any attackers reachable via the interface they didn't expect to be listening on. For example: If a user wants to run a clustered setup locally, they may specify to run on 127.0.0.1. But since MXNet will listen on 0.0.0.0, it makes the port accessible on all network interfaces.
CVE-2018-1453
IBM Security Identity Manager Virtual Appliance 7.0 allows an authenticated attacker to upload or transfer files of dangerous types that can be automatically processed within the environment. IBM X-Force ID: 140055.
CVE-2018-4141
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Intel Graphics Driver" component. It allows attackers to bypass intended memory-read restrictions via a crafted app.
CVE-2018-4159
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Graphics Drivers" component. It allows attackers to bypass intended memory-read restrictions via a crafted app.
CVE-2018-4171
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Bluetooth" component. It allows attackers to obtain sensitive kernel memory-layout information via a crafted app that leverages device properties.
CVE-2018-4184
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Speech" component. It allows attackers to bypass a sandbox protection mechanism to obtain microphone access.
CVE-2018-4187
An issue was discovered in certain Apple products. iOS before 11.3.1 is affected. macOS before 10.13.4 Security Update 2018-001 is affected. The issue involves the "LinkPresentation" component. It allows remote attackers to spoof the UI via a crafted URL in a text message.
CVE-2018-4188
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to spoof the address bar via a crafted web site.
CVE-2018-4190
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to obtain sensitive credential information that is transmitted during a CSS mask-image fetch.
CVE-2018-4192
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code via a crafted web site that leverages a race condition.
CVE-2018-4193
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Windows Server" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2018-4196
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Accessibility Framework" component. It allows attackers to execute arbitrary code in a privileged context or obtain sensitive information via a crafted app.
CVE-2018-4198
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "UIKit" component. It allows remote attackers to cause a denial of service via a crafted text file.
CVE-2018-4199
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (buffer overflow and application crash) via a crafted web site.
CVE-2018-4200
An issue was discovered in certain Apple products. iOS before 11.3.1 is affected. Safari before 11.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site that triggers a WebCore::jsElementScrollHeightGetter use-after-free.
CVE-2018-4201
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2018-4202
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. The issue involves the "iBooks" component. It allows man-in-the-middle attackers to spoof a password prompt.
CVE-2018-4204
An issue was discovered in certain Apple products. iOS before 11.4 is affected. iOS before 11.3.1 is affected. Safari before 11.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2018-4205
An issue was discovered in certain Apple products. Safari before 11.1.1 is affected. The issue involves the "Safari" component. It allows remote attackers to spoof the address bar via a crafted web site.
CVE-2018-4206
An issue was discovered in certain Apple products. iOS before 11.3.1 is affected. macOS before 10.13.4 Security Update 2018-001 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Crash Reporter" component. It allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted app that replaces a privileged port name.
CVE-2018-4211
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "FontParser" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted font file.
CVE-2018-4214
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to cause a denial of service (memory corruption and Safari crash) or possibly have unspecified other impact via a crafted web site.
CVE-2018-4215
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Bluetooth" component. It allows attackers to gain privileges or cause a denial of service (buffer overflow) via a crafted app.
CVE-2018-4218
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site that triggers an @generatorState use-after-free.
CVE-2018-4219
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "ATS" component. It allows attackers to gain privileges via a crafted app that leverages type confusion.
CVE-2018-4220
An issue was discovered in certain Apple products. Swift before 4.1.1 Security Update 2018-001 is affected. The issue involves the "Swift for Ubuntu" component. It allows attackers to execute arbitrary code in a privileged context because write and execute permissions are enabled during library loading.
CVE-2018-4221
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. The issue involves the "Security" component. It allows web sites to track users by leveraging the transmission of S/MIME client certificates.
CVE-2018-4222
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code via a crafted web site that leverages a getWasmBufferFromValue out-of-bounds read during WebAssembly compilation.
CVE-2018-4223
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Security" component. It allows local users to bypass intended restrictions on the reading of a persistent account identifier.
CVE-2018-4224
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Security" component. It allows local users to bypass intended restrictions on the reading of a persistent device identifier.
CVE-2018-4225
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. watchOS before 4.3.1 is affected. The issue involves the "Security" component. It allows local users to bypass intended restrictions on Keychain state modifications.
CVE-2018-4226
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. watchOS before 4.3.1 is affected. The issue involves the "Security" component. It allows local users to bypass intended restrictions on the reading of sensitive user information.
CVE-2018-4227
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. The issue involves the "Mail" component. It allows remote attackers to read the cleartext content of S/MIME encrypted messages via direct exfiltration.
CVE-2018-4228
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "IOFireWireAVC" component. It allows attackers to execute arbitrary code in a privileged context via a crafted app that leverages a race condition.
CVE-2018-4229
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Grand Central Dispatch" component. It allows attackers to bypass a sandbox protection mechanism by leveraging the misparsing of entitlement plists.
CVE-2018-4230
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "NVIDIA Graphics Drivers" component. It allows attackers to execute arbitrary code in a privileged context via a crafted app that triggers a SetAppSupportBits use-after-free because of a race condition.
CVE-2018-4232
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. The issue involves the "WebKit" component. It allows remote attackers to overwrite cookies via a crafted web site.
CVE-2018-4233
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2018-4234
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "IOHIDFamily" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2018-4235
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Messages" component. It allows local users to perform impersonation attacks via an unspecified injection.
CVE-2018-4236
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "IOGraphics" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2018-4237
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "libxpc" component. It allows attackers to gain privileges via a crafted app that leverages a logic error.
CVE-2018-4238
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Siri" component. It allows physically proximate attackers to bypass the lock-screen protection mechanism and enable Siri.
CVE-2018-4239
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Magnifier" component. It allows physically proximate attackers to bypass the lock-screen protection mechanism and see the most recent Magnifier image.
CVE-2018-4240
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Messages" component. It allows remote attackers to cause a denial of service via a crafted message.
CVE-2018-4241
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Kernel" component. A buffer overflow in mptcp_usr_connectx allows attackers to execute arbitrary code in a privileged context via a crafted app.
CVE-2018-4242
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Hypervisor" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2018-4243
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "Kernel" component. A buffer overflow in getvolattrlist allows attackers to execute arbitrary code in a privileged context via a crafted app.
CVE-2018-4244
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Siri Contacts" component. It allows physically proximate attackers to discover private contact information via Siri.
CVE-2018-4246
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. iCloud before 7.5 on Windows is affected. iTunes before 12.7.5 on Windows is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code via a crafted web site that leverages type confusion.
CVE-2018-4247
An issue was discovered in certain Apple products. iOS before 11.4 is affected. Safari before 11.1.1 is affected. The issue involves the "Safari" component. It allows remote attackers to cause a denial of service (persistent Safari outage) via a crafted web site.
CVE-2018-4249
An issue was discovered in certain Apple products. iOS before 11.4 is affected. macOS before 10.13.5 is affected. tvOS before 11.4 is affected. watchOS before 4.3.1 is affected. The issue involves pktmnglr_ipfilter_input in com.apple.packet-mangler in the "Kernel" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (integer overflow and stack-based buffer overflow) via a crafted app.
CVE-2018-4250
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Messages" component. It allows remote attackers to cause a denial of service via a crafted message.
CVE-2018-4251
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "Firmware" component. It allows attackers to modify the EFI flash-memory region that a crafted app that has root access.
CVE-2018-4252
An issue was discovered in certain Apple products. iOS before 11.4 is affected. The issue involves the "Siri" component. It allows physically proximate attackers to bypass the lock-screen protection mechanism and obtain private notification content via Siri.
CVE-2018-4253
An issue was discovered in certain Apple products. macOS before 10.13.5 is affected. The issue involves the "AMD" component. It allows local users to bypass intended memory-read restrictions or cause a denial of service (out-of-bounds read of kernel memory) via a crafted app.
CVE-2018-8916
Unverified password change vulnerability in Change Password in Synology DiskStation Manager (DSM) before 6.2-23739 allows remote authenticated users to reset password without verification.
CVE-2018-8925
Cross-site request forgery (CSRF) vulnerability in admin/user.php in Synology Photo Station before 6.8.5-3471 and before 6.3-2975 allows remote attackers to hijack the authentication of administrators via the (1) username, (2) password, (3) admin, (4) action, (5) uid, or (6) modify_admin parameter.
CVE-2018-8926
Permissive regular expression vulnerability in synophoto_dsm_user in Synology Photo Station before 6.8.5-3471 and before 6.3-2975 allows remote authenticated users to conduct privilege escalation attacks via the fullname parameter.
CVE-2018-9177
Twonky Server before 8.5.1 has XSS via a folder name on the Shared Folders screen.
CVE-2018-9182
Twonky Server before 8.5.1 has XSS via a modified "language" parameter in the Language section.
CVE-2018-9246
The PGObject::Util::DBAdmin module before 0.120.0 for Perl, as used in LedgerSMB through 1.5.x, insufficiently sanitizes or escapes variable values used as part of shell command execution, resulting in shell code injection via the create(), run_file(), backup(), or restore() function. The vulnerability allows unauthorized users to execute code with the same privileges as the running application.
