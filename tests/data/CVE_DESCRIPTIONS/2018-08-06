CVE-2016-4391
A remote code execution security vulnerability has been identified in all versions of the HP ArcSight WINC Connector prior to v7.3.0.
CVE-2016-4392
A remote cross site scripting vulnerability has been identified in HP Business Service Management software v9.1x, v9.20 - v9.25IP1.
CVE-2016-4397
A local code execution security vulnerability was identified in HP Network Node Manager i (NNMi) v10.00, v10.10 and v10.20 Software.
CVE-2016-4398
A remote arbitrary code execution vulnerability was identified in HP Network Node Manager i (NNMi) Software 10.00, 10.01 (patch1), 10.01 (patch 2), 10.10 using Java Deserialization.
CVE-2016-4399
A security vulnerability was identified in HP Network Node Manager i (NNMi) Software 10.00, 10.01 (patch1), 10.01 (patch 2), 10.10. The vulnerability could result in cross-site scripting (XSS).
CVE-2016-4400
A security vulnerability was identified in HP Network Node Manager i (NNMi) Software 10.00, 10.01 (patch1), 10.01 (patch 2), 10.10. The vulnerability could result in cross-site scripting (XSS).
CVE-2016-4402
A security vulnerability was identified in the Filter SDK component of HP KeyView earlier than v11.2. The vulnerability could be exploited remotely to allow code execution via buffer overflow.
CVE-2016-4403
A security vulnerability was identified in the Filter SDK component of HP KeyView earlier than v11.2. The vulnerability could be exploited remotely to allow code execution via memory corruption.
CVE-2016-4404
A security vulnerability was identified in the Filter SDK component of HP KeyView earlier than v11.2. The vulnerability could be exploited remotely to allow code execution via a memory allocation issue.
CVE-2016-4405
A remote code execution vulnerability was identified in HP Business Service Management (BSM) using Apache Commons Collection Java Deserialization versions v9.20-v9.26
CVE-2016-4406
A remote cross site scripting vulnerability was identified in HPE iLO 3 all version prior to v1.88 and HPE iLO 4 all versions prior to v2.44.
CVE-2016-8526
Aruba Airwave all versions up to, but not including, 8.2.3.1 is vulnerable to an XML external entities (XXE). XXEs are a way to permit XML parsers to access storage that exist on external systems. If an unprivileged user is permitted to control the contents of XML files, XXE can be used as an attack vector. Because the XML parser has access to the local filesystem and runs with the permissions of the web server, it can access any file that is readable by the web server and copy it to an external system of the attacker's choosing. This could include files that contain passwords, which could then lead to privilege escalation.
CVE-2016-8527
Aruba Airwave all versions up to, but not including, 8.2.3.1 is vulnerable to a reflected cross-site scripting (XSS). The vulnerability is present in the VisualRF component of AirWave. By exploiting this vulnerability, an attacker who can trick a logged-in AirWave administrative user into clicking a link could obtain sensitive information, such as session cookies or passwords. The vulnerability requires that an administrative users click on the malicious link while currently logged into AirWave in the same browser.
CVE-2017-12614
It was noticed an XSS in certain 404 pages that could be exploited to perform an XSS attack. Chrome will detect this as a reflected XSS attempt and prevent the page from loading. Firefox and other browsers don't, and are vulnerable to this attack. Mitigation: The fix for this is to upgrade to Apache Airflow 1.9.0 or above.
CVE-2017-1366
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 uses weaker than expected cryptographic algorithms that could allow an attacker to decrypt highly sensitive information. IBM X-Force ID: 126859.
CVE-2017-1368
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 does not set the secure attribute on authorization tokens or session cookies. Attackers may be able to get the cookie values by sending a http:// link to a user or by planting this link in a site the user goes to. The cookie will be sent to the insecure link and the attacker can then obtain the cookie value by snooping the traffic. IBM X-Force ID: 126861.
CVE-2017-1396
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 specifies permissions for a security-critical resource in a way that allows that resource to be read or modified by unintended actors. IBM X-Force ID: 127342.
CVE-2017-1409
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 127396.
CVE-2017-1411
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 does not require that users should have strong passwords by default, which makes it easier for attackers to compromise user accounts. IBM X-Force ID: 127399.
CVE-2017-1412
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 generates an error message that includes sensitive information about its environment, users, or associated data. IBM X-Force ID: 127400.
CVE-2017-14447
An exploitable buffer overflow vulnerability exists in the PubNub message handler for the 'ad' channel of Insteon Hub running firmware version 1012. Specially crafted commands sent through the PubNub service can cause a stack-based buffer overflow overwriting arbitrary data. An attacker should send an authenticated HTTP request to trigger this vulnerability.
CVE-2017-16252
Specially crafted commands sent through the PubNub service in Insteon Hub 2245-222 with firmware version 1012 can cause a stack-based buffer overflow overwriting arbitrary data. An attacker should send an authenticated HTTP request to trigger this vulnerability.At 0x9d014cc0 the value for the cmd key is copied using strcpy to the buffer at $sp+0x11c. This buffer is 20 bytes large, sending anything longer will cause a buffer overflow.
CVE-2017-16653
An issue was discovered in Symfony before 2.7.38, 2.8.31, 3.2.14, 3.3.13, 3.4-BETA5, and 4.0-BETA5. The current implementation of CSRF protection in Symfony (Version >=2) does not use different tokens for HTTP and HTTPS; therefore the token is subject to MITM attacks on HTTP and can then be used in an HTTPS context to do CSRF attacks.
CVE-2017-16654
An issue was discovered in Symfony before 2.7.38, 2.8.31, 3.2.14, 3.3.13, 3.4-BETA5, and 4.0-BETA5. The Intl component includes various bundle readers that are used to read resource bundles from the local filesystem. The read() methods of these classes use a path and a locale to determine the language bundle to retrieve. The locale argument value is commonly retrieved from untrusted user input (like a URL parameter). An attacker can use this argument to navigate to arbitrary directories via the dot-dot-slash attack, aka Directory Traversal.
CVE-2017-16790
An issue was discovered in Symfony before 2.7.38, 2.8.31, 3.2.14, 3.3.13, 3.4-BETA5, and 4.0-BETA5. When a form is submitted by the user, the request handler classes of the Form component merge POST data and uploaded files data into one array. This big array forms the data that are then bound to the form. At this stage there is no difference anymore between submitted POST data and uploaded files. A user can send a crafted HTTP request where the value of a "FileType" is sent as normal POST data that could be interpreted as a local file path on the server-side (for example, "file:///etc/passwd"). If the application did not perform any additional checks about the value submitted to the "FileType", the contents of the given file on the server could have been exposed to the attacker.
CVE-2017-1755
IBM Security Identity Governance Virtual Appliance 5.2 through 5.2.3.2 could allow a local attacker to inject commands into malicious files that could be executed by the administrator. IBM X-Force ID: 135855.
CVE-2017-2654
jenkins-email-ext before version 2.57.1 is vulnerable to an Information Exposure. The Email Extension Plugins is able to send emails to a dynamically created list of users based on the changelogs, like authors of SCM changes since the last successful build. This could in some cases result in emails being sent to people who have no user account in Jenkins, and in rare cases even people who were not involved in whatever project was being built, due to some mapping based on the local-part of email addresses.
CVE-2017-6920
Drupal core 8 before versions 8.3.4 allows remote attackers to execute arbitrary code due to the PECL YAML parser not handling PHP objects safely during certain operations.
CVE-2017-8968
A remote execution of arbitrary code vulnerability has been identified in HPE RESTful Interface Tool 1.5, 2.0 (hprest-1.5-79.x86_64.rpm, ilorest-2.0-403.x86_64.rpm). The issue is resolved in iLOREST v2.1 or subsequent versions.
CVE-2017-8987
A Unauthenticated Remote Denial of Service vulnerability was identified in HPE Integrated Lights-Out 3 (iLO 3) version v1.88 only. The vulnerability is resolved in iLO3 v1.89 or subsequent versions.
CVE-2017-8988
A Remote Bypass of Security Restrictions vulnerability was identified in HPE XP Command View Advanced Edition Software Earlier than 8.5.3-00. The vulnerability impacts DevMgr Earlier than 8.5.3-00 (for Windows, Linux), RepMgr earlier than 8.5.3-00 (for Windows, Linux) and HDLM earlier than 8.5.3-00 (for Windows, Linux, Solaris, AIX).
CVE-2017-8989
A security vulnerability in HPE IceWall SSO Dfw 10.0 and 11.0 on RHEL, HP-UX, and Windows could be exploited remotely to allow URL Redirection.
CVE-2017-8990
A remote code execution vulnerability was identified in HPE Intelligent Management Center (iMC) Wireless Service Manager (WSM) Software earlier than version WSM 7.3 (E0506). This issue was resolved in HPE IMC Wireless Services Manager Software IMC WSM 7.3 E0506P01 or subsequent version.
CVE-2017-8991
HPE has identified a cross site scripting (XSS) vulnerability in HPE CentralView Fraud Risk Management earlier than version CV 6.1. This issue is resolved in HF16 for HPE CV 6.1 or subsequent version.
CVE-2017-8992
HPE has identified a remote privilege escalation vulnerability in HPE CentralView Fraud Risk Management earlier than version CV 6.1. This issue is resolved in HF16 for HPE CV 6.1 or subsequent version.
CVE-2017-9000
ArubaOS, all versions prior to 6.3.1.25, 6.4 prior to 6.4.4.16, 6.5.x prior to 6.5.1.9, 6.5.2, 6.5.3 prior to 6.5.3.3, 6.5.4 prior to 6.5.4.2, 8.x prior to 8.1.0.4 FIPS and non-FIPS versions of software are both affected equally is vulnerable to unauthenticated arbitrary file access. An unauthenticated user with network access to an Aruba mobility controller on TCP port 8080 or 8081 may be able to access arbitrary files stored on the mobility controller. Ports 8080 and 8081 are used for captive portal functionality and are listening, by default, on all IP interfaces of the mobility controller, including captive portal interfaces. The attacker could access files which could contain passwords, keys, and other sensitive information that could lead to full system compromise.
CVE-2017-9001
Aruba ClearPass 6.6.3 and later includes a feature called "SSH Lockout", which causes ClearPass to lock accounts with too many login failures through SSH. When this feature is enabled, an unauthenticated remote command execution vulnerability is present which could allow an unauthenticated user to execute arbitrary commands on the underlying operating system with "root" privilege level. This vulnerability is only present when a specific feature has been enabled. The SSH Lockout feature is not enabled by default, so only systems which have enabled this feature are vulnerable.
CVE-2017-9002
All versions of Aruba ClearPass prior to 6.6.8 contain reflected cross-site scripting vulnerabilities. By exploiting this vulnerability, an attacker who can trick a logged-in ClearPass administrative user into clicking a link could obtain sensitive information, such as session cookies or passwords. The vulnerability requires that an administrative users click on the malicious link while currently logged into ClearPass in the same browser.
CVE-2017-9003
Multiple memory corruption flaws are present in ArubaOS which could allow an unauthenticated user to crash ArubaOS processes. With sufficient time and effort, it is possible these vulnerabilities could lead to the ability to execute arbitrary code - remote code execution has not yet been confirmed.
CVE-2018-13877
The doPayouts() function of the smart contract implementation for MegaCryptoPolis, an Ethereum game, has a Denial of Service vulnerability. If a smart contract that has a fallback function always causing exceptions buys a land, users cannot buy lands near that contract's land, because those purchase attempts will not be completed unless the doPayouts() function successfully sends Ether to certain neighbors.
CVE-2018-1422
IBM Jazz Foundation products (IBM Rational DOORS Next Generation 5.0 through 5.0.2 and 6.0 through 6.0.5) are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 139025.
CVE-2018-14716
A Server Side Template Injection (SSTI) was discovered in the SEOmatic plugin before 3.1.4 for Craft CMS, because requests that don't match any elements incorrectly generate the canonicalUrl, and can lead to execution of Twig code.
CVE-2018-14857
Unrestricted file upload (with remote code execution) in require/mail/NotificationMail.php in Webconsole in OCS Inventory NG OCS Inventory Server through 2.5 allows a privileged user to gain access to the server via a template file containing PHP code, because file extensions other than .html are permitted.
CVE-2018-14869
PHP Template Store Script 3.0.6 allows XSS via the Address line 1, Address Line 2, Bank name, or A/C Holder name field in a profile.
CVE-2018-14960
Xiao5uCompany 1.7 has CSRF via admin/Admin.asp.
CVE-2018-14961
dl/dl_sendmail.php in zzcms 8.3 has SQL Injection via the sql parameter.
CVE-2018-14962
zzcms 8.3 has stored XSS related to the content variable in user/manage.php and zt/show.php.
CVE-2018-14963
zzcms 8.3 has CSRF via the admin/adminadd.php?action=add URI.
CVE-2018-14964
An issue was discovered in EMLsoft 5.4.5. XSS exists via the eml/upload/eml/?action=address&do=edit page.
CVE-2018-14965
An issue was discovered in EMLsoft 5.4.5. The eml/upload/eml/?action=address&do=add page allows CSRF.
CVE-2018-14966
An issue was discovered in EMLsoft 5.4.5. The eml/upload/eml/?action=user&do=add page allows CSRF.
CVE-2018-14967
An issue was discovered in EMLsoft 5.4.5. upload\eml\action\action.user.php has SQL Injection via the numPerPage parameter.
CVE-2018-14968
An issue was discovered in EMLsoft 5.4.5. upload\eml\action\action.address.php has SQL Injection via the numPerPage parameter.
CVE-2018-14969
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/system.php has XSS.
CVE-2018-14970
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/slideshow.php has XSS.
CVE-2018-14971
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/user.php has XSS.
CVE-2018-14972
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/down.php has XSS.
CVE-2018-14973
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/product.php has XSS.
CVE-2018-14974
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/news.php has XSS.
CVE-2018-14975
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/album.php has XSS.
CVE-2018-14976
An issue was discovered in QCMS 3.0.1. upload/System/Controller/backend/category.php has XSS.
CVE-2018-14977
An issue was discovered in QCMS 3.0.1. upload/System/Controller/guest.php has XSS, as demonstrated by the name parameter, a different vulnerability than CVE-2018-8070.
CVE-2018-14978
An issue was discovered in QCMS 3.0.1. CSRF exists via the backend/user/admin/add.html URI.
CVE-2018-1528
IBM Maximo Asset Management 7.6 through 7.6.3 could allow an authenticated user to obtain sensitive information from the WhoAmI API. IBM X-Force ID: 142290.
CVE-2018-1551
IBM WebSphere MQ 8.0.0.2 through 8.0.0.8 and 9.0.0.0 through 9.0.0.3 could allow users to have more authority than they should have if an MQ administrator creates an invalid user group name. IBM X-Force ID: 142888.
CVE-2018-5390
Linux kernel versions 4.9+ can be forced to make very expensive calls to tcp_collapse_ofo_queue() and tcp_prune_ofo_queue() for every incoming packet which can lead to a denial of service.
CVE-2018-7058
Aruba ClearPass, all versions of 6.6.x prior to 6.6.9 are affected by an authentication bypass vulnerability, an attacker can leverage this vulnerability to gain administrator privileges on the system. The vulnerability is exposed only on ClearPass web interfaces, including administrative, guest captive portal, and API. Customers who do not expose ClearPass web interfaces to untrusted users are impacted to a lesser extent.
CVE-2018-7059
Aruba ClearPass prior to 6.6.9 has a vulnerability in the API that helps to coordinate cluster actions. An authenticated user with the "mon" permission could use this vulnerability to obtain cluster credentials which could allow privilege escalation. This vulnerability is only present when authenticated as a user with "mon" permission.
CVE-2018-7060
Aruba ClearPass 6.6.x prior to 6.6.9 and 6.7.x prior to 6.7.1 is vulnerable to CSRF attacks against authenticated users. An attacker could manipulate an authenticated user into performing actions on the web administrative interface.
CVE-2018-7068
HPE has identified a remote HOST header attack vulnerability in HPE CentralView Fraud Risk Management earlier than version CV 6.1. This issue is resolved in HF16 for HPE CV 6.1 or subsequent version.
CVE-2018-7069
HPE has identified a remote unauthenticated access to files vulnerability in HPE CentralView Fraud Risk Management earlier than version CV 6.1. This issue is resolved in HF16 for HPE CV 6.1 or subsequent version.
CVE-2018-7070
HPE has identified a remote disclosure of information vulnerability in HPE CentralView Fraud Risk Management earlier than version CV 6.1. This issue is resolved in HF16 for HPE CV 6.1 or subsequent version.
CVE-2018-7071
HPE has identified a remote access to sensitive information vulnerability in HPE Network Function Virtualization Director (NFVD) 4.2.1 prior to gui patch 3.
CVE-2018-7072
A remote bypass of security restrictions vulnerability was identified in HPE Moonshot Provisioning Manager prior to v1.24.
CVE-2018-7073
A local arbitrary file modification vulnerability was identified in HPE Moonshot Provisioning Manager prior to v1.24.
CVE-2018-7074
A remote code execution vulnerability was identified in HPE Intelligent Management Center (iMC) PLAT 7.3 E0506P07. The vulnerability was resolved in iMC PLAT 7.3 E0605P04 or subsequent version.
CVE-2018-7075
A remote cross-site scripting (XSS) vulnerability was identified in HPE Intelligent Management Center (iMC) PLAT version v7.3 (E0506). The vulnerability is fixed in Intelligent Management Center PLAT 7.3 E0605P04 or subsequent version.
CVE-2018-7078
A remote code execution was identified in HPE Integrated Lights-Out 4 (iLO 4) earlier than version v2.60 and HPE Integrated Lights-Out 5 (iLO 5) earlier than version v1.30.
CVE-2018-7090
HPE XP P9000 Command View Advanced Edition Software (CVAE) has local and remote cross site scripting vulnerability in versions 7.0.0-00 to earlier than 8.60-00 of DevMgr, TSMgr and RepMgr.
CVE-2018-7091
HPE XP P9000 Command View Advanced Edition Software (CVAE) has open URL redirection vulnerability in versions 7.0.0-00 to earlier than 8.60-00 of DevMgr, TSMgr and RepMgr.
CVE-2018-7092
A potential security vulnerability has been identified in HPE Intelligent Management Center Platform (IMC Plat) 7.3 E0506P09. The vulnerability could be remotely exploited to allow for remote directory traversal leading to arbitrary file deletion.
