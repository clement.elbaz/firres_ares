CVE-2009-5016
Integer overflow in the xml_utf8_decode function in ext/xml/xml.c in PHP before 5.2.11 makes it easier for remote attackers to bypass cross-site scripting (XSS) and SQL injection protection mechanisms via a crafted string that uses overlong UTF-8 encoding, a different vulnerability than CVE-2010-3870.
CVE-2009-5017
Mozilla Firefox before 3.6 Beta 3 does not properly handle overlong UTF-8 encoding, which makes it easier for remote attackers to bypass cross-site scripting (XSS) protection mechanisms via a crafted string, a different vulnerability than CVE-2010-1210.
CVE-2010-2637
IBM WebSphere MQ 6.0 before 6.0.2.9 and 7.0 before 7.0.1.1 does not encrypt the username and password in the security parameters field, which allows remote attackers to obtain sensitive information by sniffing the network traffic from a .NET client application.
CVE-2010-3870
The utf8_decode function in PHP before 5.3.4 does not properly handle non-shortest form UTF-8 encoding and ill-formed subsequences in UTF-8 data, which makes it easier for remote attackers to bypass cross-site scripting (XSS) and SQL injection protection mechanisms via a crafted string.
CVE-2010-3890
Cross-site scripting (XSS) vulnerability in IBM OmniFind Enterprise Edition before 9.1 allows remote attackers to inject arbitrary web script or HTML via the command parameter to the administration interface, as demonstrated by the command parameter to ESAdmin/collection.do.
CVE-2010-3891
Cross-site request forgery (CSRF) vulnerability in ESAdmin/security.do in the administrator interface in IBM OmniFind Enterprise Edition before 9.1 allows remote attackers to hijack the authentication of administrators for requests that add an administrative user via a saveNewUser action.
CVE-2010-3892
Session fixation vulnerability in the login form in the administrator interface in IBM OmniFind Enterprise Edition 8.x and 9.x allows remote attackers to hijack web sessions by replaying a session ID (aka SID) value.
Per: http://cwe.mitre.org/data/definitions/384.html

'CWE-384: Session Fixation'
CVE-2010-3893
The administrator interface in IBM OmniFind Enterprise Edition 8.x and 9.x does not restrict use of a session ID (aka SID) value to a single IP address, which allows remote attackers to perform arbitrary administrative actions by leveraging cookie theft, related to a "session impersonation" issue.
CVE-2010-3894
Stack-based buffer overflow in the Java_com_ibm_es_oss_CryptionNative_ESEncrypt function in /opt/IBM/es/lib/libffq.cryptionjni.so in the login form in the administration interface in IBM OmniFind Enterprise Edition before 8.5 FP6 allows remote attackers to execute arbitrary code via a long password.
CVE-2010-3895
esRunCommand in IBM OmniFind Enterprise Edition before 9.1 allows local users to gain privileges by specifying an arbitrary command name as the first argument.
CVE-2010-3896
The ESSearchApplication directory tree in IBM OmniFind Enterprise Edition 8.x and 9.x does not require authentication, which allows remote attackers to modify the server configuration via a request to palette.do.
CVE-2010-3897
ESSearchApplication/palette.do in IBM OmniFind Enterprise Edition 8.x and 9.x includes the administrator password in the HTML source code, which might allow remote attackers to obtain sensitive information by leveraging read access to this file.
CVE-2010-3898
IBM OmniFind Enterprise Edition 8.x and 9.x does not properly restrict the cookie path of administrator (aka ESAdmin) cookies, which might allow remote attackers to bypass authentication by leveraging access to other pages on the web site.
CVE-2010-3899
IBM OmniFind Enterprise Edition 8.x and 9.x performs web crawls with an unlimited recursion depth, which allows remote web servers to cause a denial of service (infinite loop) via a crafted series of documents.
CVE-2010-4236
Untrusted search path vulnerability in estaskwrapper in IBM OmniFind Enterprise Edition before 9.1 allows local users to gain privileges via an ES_LIBRARY_PATH environment variable and a modified PATH environment variable, which is used during execution of the estasklight program, a different vulnerability than CVE-2010-3895.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
