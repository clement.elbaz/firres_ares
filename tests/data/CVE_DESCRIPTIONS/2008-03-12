CVE-2007-6253
Multiple buffer overflows in Adobe Form Designer 5.0 and Form Client 5.0 allow remote attackers to execute arbitrary code via unknown vectors in the (1) Adobe File Dialog Button (FileDlg.dll) and the (2) Adobe Copy to Server Object (SvrCopy.dll) ActiveX controls.
CVE-2008-0643
Cross-site scripting (XSS) vulnerability in Adobe ColdFusion MX 7 and ColdFusion 8 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-0644
Adobe ColdFusion MX 7 and ColdFusion 8 allows remote attackers to bypass the cross-site scripting (XSS) protection mechanism for applications via unspecified vectors related to the setEncoding function.
CVE-2008-0890
Red Hat Directory Server 7.1 before SP4 uses insecure permissions for certain directories, which allows local users to modify JAR files and execute arbitrary code via unknown vectors.
CVE-2008-1202
Cross-site scripting (XSS) vulnerability in the web management interface in Adobe LiveCycle Workflow 6.2 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2008-1203
The administrator interface for Adobe ColdFusion 8 and ColdFusion MX7 does not log failed authentication attempts, which makes it easier for remote attackers to conduct brute force attacks without detection.
CVE-2008-1295
SQL injection vulnerability in archives.php in Gregory Kokanosky (aka Greg's Place) phpMyNewsletter 0.8 beta 5 and earlier allows remote attackers to execute arbitrary SQL commands via the msg_id parameter.
CVE-2008-1296
Multiple cross-site scripting (XSS) vulnerabilities in EncapsGallery 1.11.2 allow remote attackers to inject arbitrary web script or HTML via the file parameter to (1) watermark.php and (2) catalog_watermark.php in core/.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-1297
SQL injection vulnerability in index.php in the eWriting (com_ewriting) 1.2.1 module for Mambo and Joomla! allows remote attackers to execute arbitrary SQL commands via the cat parameter in a selectcat action.
CVE-2008-1298
SQL injection vulnerability in Hadith module for PHP-Nuke allows remote attackers to execute arbitrary SQL commands via the cat parameter in a viewcat action to modules.php.
CVE-2008-1299
Cross-site scripting (XSS) vulnerability in SolutionSearch.do in ManageEngine ServiceDesk Plus 7.0.0 Build 7011 for Windows allows remote attackers to inject arbitrary web script or HTML via the searchText parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-1300
Cross-site scripting (XSS) vulnerability in the Logfile Viewer Settings function in system/workplace/admin/workplace/logfileview/logfileViewSettings.jsp in Alkacon OpenCms 7.0.3 and 7.0.4 allows remote attackers to inject arbitrary web script or HTML via the filePath.0 parameter in a save action, a different vector than CVE-2008-1045.
CVE-2008-1301
Absolute path traversal vulnerability in system/workplace/admin/workplace/logfileview/logfileViewSettings.jsp in Alkacon OpenCms 7.0.3 and 7.0.4 allows remote authenticated administrators to read arbitrary files via a full pathname in the filePath.0 parameter.
CVE-2008-1302
The Perforce service (p4s.exe) in Perforce Server 2007.3/143793 and earlier allows remote attackers to cause a denial of service (daemon crash) via a (1) server-DiffFile or (2) server-ReleaseFile command with a large integer value, which is used in an array initialization calculation, and leads to invalid memory access.
CVE-2008-1303
The Perforce service (p4s.exe) in Perforce Server 2007.3/143793 and earlier allows remote attackers to cause a denial of service (daemon crash) via a missing parameter to the (1) dm-FaultFile, (2) dm-LazyCheck, (3) dm-ResolvedFile, (4) dm-OpenFile, (5) crypto, and possibly unspecified other commands, which triggers a NULL pointer dereference.
CVE-2008-1304
Multiple cross-site scripting (XSS) vulnerabilities in WordPress 2.3.2 allow remote attackers to inject arbitrary web script or HTML via the (1) inviteemail parameter in an invite action to wp-admin/users.php and the (2) to parameter in a sent action to wp-admin/invites.php.
CVE-2008-1305
SQL injection vulnerability in filebase.php in the Filebase mod for phpBB allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-1306
Multiple cross-site scripting (XSS) vulnerabilities in Savvy Content Manager (CM) allow remote attackers to inject arbitrary web script or HTML via the searchterms parameter to (1) searchresults.cfm, (2) search_results.cfm, and (3) search_results/index.cfm.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-1307
Heap-based buffer overflow in the KUpdateObj2 Class ActiveX control in UpdateOcx2.dll in Beijing KingSoft Antivirus Online Update Module 2007.12.29.29 allows remote attackers to execute arbitrary code via a long argument to the SetUninstallName method.
CVE-2008-1308
SQL injection vulnerability in the Sudirman Angriawan NukeC30 3.0 module for PHP-Nuke allows remote attackers to execute arbitrary SQL commands via the id_catg parameter in a ViewCatg action to modules.php.
CVE-2008-1309
The RealAudioObjects.RealAudio ActiveX control in rmoc3260.dll in RealNetworks RealPlayer Enterprise, RealPlayer 10, RealPlayer 10.5 before build 6.0.12.1675, and RealPlayer 11 before 11.0.3 build 6.0.14.806 does not properly manage memory for the (1) Console or (2) Controls property, which allows remote attackers to execute arbitrary code or cause a denial of service (browser crash) via a series of assignments of long string values, which triggers an overwrite of freed heap memory.
CVE-2008-1310
Directory traversal vulnerability in the TFTP server in PacketTrap Networks pt360 Tool Suite 1.1.33.1.0, and other versions before 2.0.3900.0, allows remote attackers to read and overwrite arbitrary files via directory traversal sequences in the pathname.
CVE-2008-1311
The TFTP server in PacketTrap pt360 Tool Suite PRO 2.0.3901.0 and earlier allows remote attackers to cause a denial of service (daemon hang) by uploading a file named (1) '|' (pipe), (2) '"' (quotation mark), or (3) "<>" (less than, greater than); or (4) a file with a long name.  NOTE: the issue for vector 4 might exist because of an incomplete fix for CVE-2008-1312.
CVE-2008-1312
Unspecified vulnerability in the TFTP server in PacketTrap Networks pt360 Tool Suite 1.1.33.1.0, and other versions before 2.0.3900.0, allows remote attackers to cause a denial of service (daemon crash) via a long TFTP packet, a different vulnerability than CVE-2008-1311.
CVE-2008-1313
Multiple SQL injection vulnerabilities in index.php in Bloo 1.00 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) post_id, (2) post_category_id, (3) post_year_month, and (4) static_page_id parameters; and unspecified other vectors.
CVE-2008-1314
SQL injection vulnerability in the Johannes Hass gaestebuch 2.2 module for PHP-Nuke allows remote attackers to execute arbitrary SQL commands via the id parameter in an edit action to modules.php.
