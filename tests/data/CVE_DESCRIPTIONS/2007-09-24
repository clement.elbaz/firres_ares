CVE-2007-3916
The main function in skkdic-expr.c in SKK Tools 1.2 allows local users to overwrite or delete arbitrary files via a symlink attack on a skkdic$PID temporary file.
CVE-2007-4573
The IA32 system call emulation functionality in Linux kernel 2.4.x and 2.6.x before 2.6.22.7, when running on the x86_64 architecture, does not zero extend the eax register after the 32bit entry path to ptrace is used, which might allow local users to gain privileges by triggering an out-of-bounds access to the system call table using the %RAX register.
CVE-2007-4985
ImageMagick before 6.3.5-9 allows context-dependent attackers to cause a denial of service via a crafted image file that triggers (1) an infinite loop in the ReadDCMImage function, related to ReadBlobByte function calls; or (2) an infinite loop in the ReadXCFImage function, related to ReadBlobMSBLong function calls.
CVE-2007-4986
Multiple integer overflows in ImageMagick before 6.3.5-9 allow context-dependent attackers to execute arbitrary code via a crafted (1) .dcm, (2) .dib, (3) .xbm, (4) .xcf, or (5) .xwd image file, which triggers a heap-based buffer overflow.
CVE-2007-4987
Off-by-one error in the ReadBlobString function in blob.c in ImageMagick before 6.3.5-9 allows context-dependent attackers to execute arbitrary code via a crafted image file, which triggers the writing of a '\0' character to an out-of-bounds address.
CVE-2007-4988
Sign extension error in the ReadDIBImage function in ImageMagick before 6.3.5-9 allows context-dependent attackers to execute arbitrary code via a crafted width value in an image file, which triggers an integer overflow and a heap-based buffer overflow.
CVE-2007-5035
** DISPUTED **  PHP remote file inclusion vulnerability in html/modules/extranet_profile/main.php in openEngine 1.9 beta1 allows remote attackers to execute arbitrary PHP code via a URL in the this_module_path parameter.  NOTE: this issue is disputed by CVE because PHP encounters a fatal function-call error on a direct request for the file, before reaching the include statement.
CVE-2007-5036
Multiple buffer overflows in the AirDefense Airsensor M520 with firmware 4.3.1.1 and 4.4.1.4 allow remote authenticated users to cause a denial of service (HTTPS service outage) via a crafted query string in an HTTPS request to (1) adLog.cgi, (2) post.cgi, or (3) ad.cgi, related to the "files filter."
CVE-2007-5037
Buffer overflow in the inotifytools_snprintf function in src/inotifytools.c in the inotify-tools library before 3.11 allows context-dependent attackers to execute arbitrary code via a long filename.
CVE-2007-5038
The offer_account_by_email function in User.pm in the WebService for Bugzilla before 3.0.2, and 3.1.x before 3.1.2, does not check the value of the createemailregexp parameter, which allows remote attackers to bypass intended restrictions on account creation.
CVE-2007-5039
Ghost Security Suite beta 1.110 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the (1) NtCreateKey, (2) NtDeleteValueKey, (3) NtQueryValueKey, (4) NtSetSystemInformation, and (5) NtSetValueKey kernel SSDT hooks.
CVE-2007-5040
Ghost Security Suite alpha 1.200 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the (1) NtCreateKey, (2) NtCreateThread, (3) NtDeleteValueKey, (4) NtQueryValueKey, (5) NtSetSystemInformation, and (6) NtSetValueKey kernel SSDT hooks.
CVE-2007-5041
G DATA InternetSecurity 2007 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the (1) NtCreateKey and (2) NtOpenProcess kernel SSDT hooks.
CVE-2007-5042
Outpost Firewall Pro 4.0.1025.7828 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the (1) NtCreateKey, (2) NtDeleteFile, (3) NtLoadDriver, (4) NtOpenProcess, (5) NtOpenSection, (6) NtOpenThread, and (7) NtUnloadDriver kernel SSDT hooks, a partial regression of CVE-2006-7160.
CVE-2007-5043
Kaspersky Internet Security 7.0.0.125 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to (1) cause a denial of service (crash) and possibly gain privileges via the NtCreateSection kernel SSDT hook or (2) cause a denial of service (avp.exe service outage) via the NtLoadDriver kernel SSDT hook.  NOTE: this issue may partially overlap CVE-2006-3074.
CVE-2007-5044
ZoneAlarm Pro 7.0.362.000 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the (1) NtCreatePort and (2) NtDeleteFile kernel SSDT hooks, a partial regression of CVE-2007-2083.
CVE-2007-5045
Argument injection vulnerability in Apple QuickTime 7.1.5 and earlier, when running on systems with Mozilla Firefox before 2.0.0.7 installed, allows remote attackers to execute arbitrary commands via a QuickTime Media Link (QTL) file with an embed XML element and a qtnext parameter containing the Firefox "-chrome" argument.  NOTE: this is a related issue to CVE-2006-4965 and the result of an incomplete fix for CVE-2007-3670.
CVE-2007-5046
Cross-site scripting (XSS) vulnerability in the Webmail interface for IceWarp Merak Mail Server before 9.0.0 allows remote attackers to inject arbitrary JavaScript via a javascript: URI in an attribute of an element in an email message body, as demonstrated by the onload attribute in a BODY element.
CVE-2007-5047
Norton Internet Security 2008 15.0.0.60 does not properly validate certain parameters to System Service Descriptor Table (SSDT) function handlers, which allows local users to cause a denial of service (crash) and possibly gain privileges via the NtOpenSection kernel SSDT hook.  NOTE: the NtCreateMutant and NtOpenEvent function hooks are already covered by CVE-2007-1793.
CVE-2007-5048
Heap-based buffer overflow in Lhaplus before 1.55 allows remote attackers to execute arbitrary code via a long filename in an ARJ archive.
CVE-2007-5049
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-3387.  Reason: This candidate is a duplicate of CVE-2007-3387.  Notes: All CVE users should reference CVE-2007-3387 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2007-5050
Directory traversal vulnerability in index.php in Neuron News 1.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the q parameter.
CVE-2007-5051
Multiple cross-site scripting (XSS) vulnerabilities in PhpGedView 4.1.1 allow remote attackers to inject arbitrary web script or HTML via the (1) box_width, (2) PEDIGREE_GENERATIONS, and (3) rootid parameters in ancestry.php, and the (4) newpid parameter in timeline.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5052
Multiple cross-site scripting (XSS) vulnerabilities in index.php in Vigile CMS 1.8 allow remote attackers to inject arbitrary web script or HTML via a request to the wiki module with (1) the title parameter or (2) a "title=" sequence in the PATH_INFO, or a request to the download module with (3) the cat parameter or (4) a "cat=" sequence in the PATH_INFO.
CVE-2007-5053
Multiple incomplete blacklist vulnerabilities in iziContents 1 RC6 and earlier allow remote attackers to execute arbitrary PHP code via a URL in (1) the admin_home parameter to modules/poll/poll_summary.php or (2) the rootdp parameter to include/db.php; or a URL in the language_home parameter to (3) search/search.php, (4) poll/inlinepoll.php, (5) poll/showpoll.php, (6) links/showlinks.php, or (7) links/submit_links.php in modules/; related to missing checks in (a) modules/moduleSec.php and (b) include/includeSec.php for inclusion of certain URLs, as demonstrated by an ftps:// URL.
CVE-2007-5054
Multiple PHP remote file inclusion vulnerabilities in iziContents 1 RC6 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the gsLanguage parameter to (1) search/search.php, (2) poll/inlinepoll.php, (3) poll/showpoll.php, (4) links/showlinks.php, or (5) links/submit_links.php in modules/.
CVE-2007-5055
Multiple directory traversal vulnerabilities in iziContents 1 RC6 and earlier allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in (1) the admin_home parameter to modules/poll/poll_summary.php or (2) the rootdp parameter to include/db.php.
CVE-2007-5056
Eval injection vulnerability in adodb-perf-module.inc.php in ADOdb Lite 1.42 and earlier, as used in products including CMS Made Simple, SAPID CMF, Journalness, PacerCMS, and Open-Realty, allows remote attackers to execute arbitrary code via PHP sequences in the last_module parameter.
CVE-2007-5057
NetSupport Manager Client before 10.20.0004 allows remote attackers to bypass the (1) basic and (2) authentication schemes by spoofing the NetSupport Manager.
CVE-2007-5058
Cross-site scripting (XSS) vulnerability in the Web administration interface in Barracuda Spam Firewall before firmware 3.5.10.016 allows remote attackers to inject arbitrary web script or HTML via the username field in a login attempt, which is not properly handled when the Monitor Web Syslog screen is open.
CVE-2007-5059
Multiple cross-site scripting (XSS) vulnerabilities in GreenSQL allow remote attackers to inject arbitrary web script or HTML via several vectors, as demonstrated by the (1) uname and (2) pass parameters in a login form, and (3) an unspecified "url value," leading to storage of XSS sequences in the database and display of these sequences in the alert section of the admin panel.
CVE-2007-5060
Cross-site request forgery (CSRF) vulnerability in the cpass functionality in an admin action in index.php in XCMS allows remote attackers to change arbitrary passwords via certain password_ and rpassword_ parameters, possibly related to timestamp values.
CVE-2007-5061
SQL injection vulnerability in mods/banners/navlist.php in Clansphere 2007.4 allows remote attackers to execute arbitrary SQL commands via the cat_id parameter to index.php in a banners action.
CVE-2007-5062
account.php in Adam Scheinberg Flip 3.0 and earlier allows remote attackers to create administrative accounts via the un parameter in a register action.
CVE-2007-5063
Adam Scheinberg Flip 3.0 and earlier stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a file containing login credentials via a direct request for var/users.txt.
CVE-2007-5064
Buffer overflow in a certain ActiveX control in Xunlei Web Thunder 5.6.9.344, possibly the DapPlayer ActiveX control in DapPlayer_Now.dll, allows remote attackers to execute arbitrary code via a long first argument to the DownURL2 method.  NOTE: some of these details are obtained from third party information.
CVE-2007-5065
PHP remote file inclusion vulnerability in admin.slideshow1.php in the Flash Slide Show (com_slideshow) component for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_live_site parameter.
CVE-2007-5066
Unspecified vulnerability in Webmin before 1.370 on Windows allows remote authenticated users to execute arbitrary commands via a crafted URL.
CVE-2007-5067
Multiple buffer overflows in iMatix Xitami Web Server 2.5c2 allow remote attackers to execute arbitrary code via a long If-Modified-Since header to (1) xigui32.exe or (2) xitami.exe.
CVE-2007-5068
SQL injection vulnerability in index.php in phpFullAnnu (PFA) 6.0 allows remote attackers to execute arbitrary SQL commands via the mod parameter.
CVE-2007-5069
Directory traversal vulnerability in data/compatible.php in the Nuke Mobile Entertainment 1 addon for PHP-Nuke allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the module_name parameter.
CVE-2007-5070
Heap-based buffer overflow in the EasyMailMessagePrinter ActiveX control in emprint.DLL 6.0.1.0 in the Quiksoft EasyMail MessagePrinter Object allows remote attackers to execute arbitrary code via a long string in the first argument to the SetFont method.
CVE-2007-5071
Incomplete blacklist vulnerability in upload_img_cgi.php in Simple PHP Blog before 0.5.1 allows remote attackers to upload dangerous files and execute arbitrary code, as demonstrated by a filename ending in .php. or a .htaccess file, a different vector than CVE-2005-2733. NOTE: the vulnerability was also present in a 0.5.1 download available in the early morning of 20070923.  NOTE: the original 20070920 disclosure provided an incorrect filename, img_upload_cgi.php.
CVE-2007-5072
Multiple cross-site scripting (XSS) vulnerabilities in Simple PHP Blog (SPHPBlog) before 0.5.1, when register_globals is enabled, allow remote attackers to inject arbitrary web script or HTML via certain user_colors array parameters to certain user_style.php files under themes/, as demonstrated by the user_colors[bg_color] parameter.
