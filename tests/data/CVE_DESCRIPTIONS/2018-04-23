CVE-2016-9586
curl before version 7.52.0 is vulnerable to a buffer overflow when doing a large floating point output in libcurl's implementation of the printf() functions. If there are any application that accepts a format string from the outside without necessary input filtering, it could allow remote attacks.
CVE-2016-9594
curl before version 7.52.1 is vulnerable to an uninitialized random in libcurl's internal function that returns a good 32bit random value.  Having a weak or virtually non-existent random value makes the operations that use it vulnerable.
CVE-2017-13073
Cross-site scripting (XSS) vulnerability in QNAP NAS application Photo Station versions 5.2.7, 5.4.3, and their earlier versions could allow remote attackers to inject arbitrary web script or HTML.
CVE-2017-14458
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 8.3.2.25013. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2017-1473
IBM Security Access Manager Appliance 8.0.0 through 8.0.1.6 and 9.0.0 through 9.0.3.1 uses weaker than expected cryptographic algorithms that could allow an attacker to decrypt highly sensitive information. IBM X-Force ID: 128605.
CVE-2017-1486
IBM Cognos Business Intelligence 10.2, 10.2.1, 10.2.1.1, and 10.2.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128624.
CVE-2017-1701
IBM Team Concert (RTC) 5.0, 5.0.1, 5.0.2, 6.0, 6.0.1, 6.0.2, 6.0.3, 6.0.4, and 6.0.5 stores credentials for users using a weak encryption algorithm, which could allow an authenticated user to obtain highly sensitive information. IBM X-Force ID: 134393.
CVE-2017-1764
IBM Cognos Business Intelligence 10.2, 10.2.1, 10.2.1.1, and 10.2.2, under specialized circumstances, could expose plain text credentials to a local user. IBM X-Force ID: 136149.
CVE-2017-17833
OpenSLP releases in the 1.0.2 and 1.1.0 code streams have a heap-related memory corruption issue which may manifest itself as a denial-of-service or a remote code-execution vulnerability.
CVE-2017-1786
IBM WebSphere MQ 8.0 through 8.0.0.8 and 9.0 through 9.0.4 under special circumstances could allow an authenticated user to consume all resources due to a memory leak resulting in service loss. IBM X-Force ID: 136975.
CVE-2017-7893
In SaltStack Salt before 2016.3.6, compromised salt-minions can impersonate the salt-master.
CVE-2018-10233
The User Profile & Membership plugin before 2.0.7 for WordPress has no mitigations implemented against cross site request forgery attacks. This is a structural finding throughout the entire plugin.
CVE-2018-10234
Authenticated Cross site Scripting exists in the User Profile & Membership plugin before 2.0.11 for WordPress via the "Account Deletion Custom Text" input field on the wp-admin/admin.php?page=um_options&section=account page.
CVE-2018-10299
An integer overflow in the batchTransfer function of a smart contract implementation for Beauty Ecosystem Coin (BEC), the Ethereum ERC20 token used in the Beauty Chain economic system, allows attackers to accomplish an unauthorized increase of digital assets by providing two _receivers arguments in conjunction with a large _value argument, as exploited in the wild in April 2018, aka the "batchOverflow" issue.
CVE-2018-10300
Cross-site scripting (XSS) vulnerability in the Web-Dorado Instagram Feed WD plugin before 1.3.1 for WordPress allows remote attackers to inject arbitrary web script or HTML by passing payloads in an Instagram profile's bio.
CVE-2018-10301
Cross-site scripting (XSS) vulnerability in the Web-Dorado Instagram Feed WD plugin before 1.3.1 Premium for WordPress allows remote attackers to inject arbitrary web script or HTML by passing payloads in a comment on an Instagram post.
CVE-2018-10302
A use-after-free in Foxit Reader before 9.1 and PhantomPDF before 9.1 allows remote attackers to execute arbitrary code, aka iDefense ID V-jyb51g3mv9.
CVE-2018-10303
A use-after-free in Foxit Reader before 9.1 and PhantomPDF before 9.1 allows remote attackers to execute arbitrary code, aka iDefense ID V-y0nqfutlf3.
CVE-2018-1106
An authentication bypass flaw has been found in PackageKit before 1.1.10 that allows users without administrator privileges to install signed packages. A local attacker can use this vulnerability to install vulnerable packages to further compromise a system.
CVE-2018-3850
An exploitable use-after-free vulnerability exists in the JavaScript engine Foxit Software Foxit PDF Reader version 9.0.1.1049. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If a browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-4847
A vulnerability has been identified in SIMATIC WinCC OA Operator iOS App (All versions < V1.4). Insufficient protection of sensitive information (e.g. session key for accessing server) in Siemens WinCC OA Operator iOS app could allow an attacker with physical access to the mobile device to read unencrypted data from the app's directory. Siemens provides mitigations to resolve the security issue.
CVE-2018-8781
The udl_fb_mmap function in drivers/gpu/drm/udl/udl_fb.c at the Linux kernel version 3.4 and up to and including 4.15 has an integer-overflow vulnerability allowing local users with access to the udldrmfb driver to obtain full read and write permissions on kernel physical pages, resulting in a code execution in kernel space.
CVE-2018-8880
Lutron Quantum BACnet Integration 2.0 (firmware 3.2.243) doesn't check for correct user authentication before showing the /deviceIP information, which leads to internal network information disclosure.
CVE-2018-9921
In CMS Made Simple 2.2.7, a Directory Traversal issue makes it possible to determine the existence of files and directories outside the web-site installation directory, and determine whether a file has contents matching a specified checksum. The attack uses an admin/checksum.php?__c= request.
