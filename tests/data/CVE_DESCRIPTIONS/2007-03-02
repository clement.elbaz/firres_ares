CVE-2007-0001
The file watch implementation in the audit subsystem (auditctl -w) in the Red Hat Enterprise Linux (RHEL) 4 kernel 2.6.9 allows local users to cause a denial of service (kernel panic) by replacing a watched file, which does not cause the watch on the old inode to be dropped.
Successful exploitation requires that the attacker previously created a watch for a file.
CVE-2007-1005
Heap-based buffer overflow in SW3eng.exe in the eID Engine service in CA (formerly Computer Associates) eTrust Intrusion Detection 3.0.5.57 and earlier allows remote attackers to cause a denial of service (application crash) via a long key length value to the remote administration port (9191/tcp).
CVE-2007-1134
Unspecified vulnerability in Watchtower (WT) before 0.12 has unknown impact and attack vectors, related to "unauthorized accounts."
Watchtower is prone to an unspecified authentication-bypass vulnerability.

An attacker can exploit this issue to gain unauthorized access to the application.

Versions prior to 0.12 are vulnerable. 
 
http://www.securityfocus.com/bid/22721/info
The vendor has released version 0.12 to address this issue.  


Download: http://downloads.sourceforge.net/wtelements/wt0.12.tar.gz?modtime=1171 460836&big_mirror=0

CVE-2007-1135
Multiple SQL injection vulnerabilities in WebMplayer before 0.6.1-Alpha allow remote attackers to execute arbitrary SQL commands via the (1) strid parameter to index.php and the (2) id[0] or other id array index parameter to filecheck.php.
CVE-2007-1136
index.php in WebMplayer before 0.6.1-Alpha allows remote attackers to execute arbitrary code via shell metacharacters in an exec function call.  NOTE: some sources have referred to this as eval injection in the param parameter, but CVE source inspection suggests that this is erroneous.
CVE-2007-1137
putmail.py in Putmail before 1.4 does not detect when a user attempts to use TLS with a server that does not support it, which causes putmail.py to send the username and password in plaintext while the user believes encryption is in use, and allows remote attackers to obtain sensitive information.
CVE-2007-1138
Absolute path traversal vulnerability in list_main_pages.php in Cromosoft Simple Plantilla PHP (SPP) allows remote attackers to list arbitrary directories, and read arbitrary files, via an absolute pathname in the nfolder parameter.
CVE-2007-1139
Unrestricted file upload vulnerability in Cromosoft Simple Plantilla PHP (SPP) allows remote attackers to upload arbitrary scripts via a filename with a double extension.
CVE-2007-1140
Directory traversal vulnerability in edit.php in pheap allows remote attackers to read and modify arbitrary files via a .. (dot dot) in the filename parameter.
CVE-2007-1141
PHP remote file inclusion vulnerability in preview.php in Magic News Plus 1.0.2 allows remote attackers to execute arbitrary PHP code via a URL in the php_script_path parameter.  NOTE: This issue may overlap CVE-2006-0723.
CVE-2007-1142
Cross-site scripting (XSS) vulnerability in Magic News Plus 1.0.2 allows remote attackers to inject arbitrary web script or HTML via the link_parameters parameter in (1) news.php and (2) n_layouts.php.
CVE-2007-1143
Directory traversal vulnerability in pn-menu.php in J-Web Pics Navigator 1.0 allows remote attackers to list arbitrary directories via a .. (dot dot) in the dir parameter.
CVE-2007-1144
Directory traversal vulnerability in jwpn-photos.php in J-Web Pics Navigator 2.0 allows remote attackers to list arbitrary directories via a .. (dot dot) in the dir parameter.
J-Web Pics Navigator is prone to a directory-traversal vulnerability because it fails to properly sanitize user-supplied input. 

An attacker can exploit this vulnerability to retrieve and edit the contents of arbitrary files from the vulnerable system in the context of the affected application.

J-Web Pics Navigator 2.0 is vulnerable to this issue; other versions may also be affected. 

http://www.securityfocus.com/bid/22681
CVE-2007-1145
Multiple cross-site scripting (XSS) vulnerabilities in Kayako SupportSuite - ESupport 3.00.13 and 3.04.10 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to a (1) lostpassword or (2) register action in index.php, (3) unspecified vectors in the Submit form in a submit action in index.php, and (4) the user's name in index.php; and (5) allow remote authenticated users to inject arbitrary web script or HTML via unspecified vectors related to the Admin and Staff Control Panel. NOTE: this might issue overlap CVE-2004-1412, CVE-2005-0487, or CVE-2005-0842.
CVE-2007-1146
PHP remote file inclusion vulnerability in function.php in arabhost allows remote attackers to execute arbitrary PHP code via a URL in the adminfolder parameter.
CVE-2007-1147
PHP remote file inclusion vulnerability in view.php in hbm allows remote attackers to execute arbitrary PHP code via a URL in the hbmpath parameter.
CVE-2007-1148
PHP remote file inclusion vulnerability in install/index.php in LoveCMS 1.4 allows remote attackers to execute arbitrary PHP code via a URL in the step parameter.
CVE-2007-1149
Multiple directory traversal vulnerabilities in LoveCMS 1.4 allow remote attackers to read arbitrary files via a .. (dot dot) in (1) the step parameter to install/index.php or (2) the load parameter to the top-level URI.
CVE-2007-1150
Unrestricted file upload vulnerability in LoveCMS 1.4 allows remote authenticated administrators to upload arbitrary files to /modules/content/pictures/tmp/.
CVE-2007-1151
Cross-site scripting (XSS) vulnerability in LoveCMS 1.4 allows remote attackers to inject arbitrary web script or HTML via the id parameter to the top-level URI, possibly related to a SQL error.
CVE-2007-1152
Multiple directory traversal vulnerabilities in Pyrophobia 2.1.3.1 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) act or (2) pid parameter to the top-level URI (index.php), or the (3) action parameter to admin/index.php.  NOTE: some of these details are obtained from third party information.
CVE-2007-1153
Multiple PHP remote file inclusion vulnerabilities in CutePHP CuteNews 1.3.6 allow remote attackers to execute arbitrary PHP code via unspecified vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information. NOTE: issue might overlap CVE-2004-1660 or CVE-2006-4445.
CVE-2007-1154
SQL injection vulnerability in webSPELL allows remote attackers to execute arbitrary SQL commands via a ws_auth cookie, a different vulnerability than CVE-2006-4782.
Successful exploitation requires that "magic_quotes_gpc" is disabled.
Affected product versions not specified.
CVE-2007-1155
Unrestricted file upload vulnerability in webSPELL allows remote authenticated administrators to upload and execute arbitrary PHP code via the add squad feature.  NOTE: this issue may be an administrative feature, in which case this CVE may be REJECTED.
Affected product versions unspecified.
CVE-2007-1156
JBrowser allows remote attackers to bypass authentication and access certain administrative capabilities via a direct request for _admin/.
CVE-2007-1157
Cross-site request forgery (CSRF) vulnerability in jmx-console/HtmlAdaptor in JBoss allows remote attackers to perform privileged actions as administrators via certain MBean operations, a different vulnerability than CVE-2006-3733.
Affected product versions unspecified.
CVE-2007-1158
Directory traversal vulnerability in index.php in the Pagesetter 6.2.0 through 6.3.0 beta 5 module for PostNuke allows remote attackers to read arbitrary files via a .. (dot dot) in the id parameter.
CVE-2007-1159
Cross-site scripting (XSS) vulnerability in modules/out.php in Pyrophobia 2.1.3.1 allows remote attackers to inject arbitrary web script or HTML via the id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1160
webSPELL 4.0, and possibly later versions, allows remote attackers to bypass authentication via a ws_auth cookie, a different vulnerability than CVE-2006-4782.
This vulnerability may affect more recent versions of the product as well. (WebSPELL, WebSPELL, 4.0 and later)
CVE-2007-1161
Cross-site scripting (XSS) vulnerability in call_entry.php in Call Center Software 0,93 allows remote attackers to inject arbitrary web script or HTML via the problem_desc parameter, as demonstrated by the ONLOAD attribute of a BODY element.
CVE-2007-1162
A certain ActiveX control in the Common Controls Replacement Project (CCRP) CCRP BrowseDialog Server (ccrpbds6.dll) allows remote attackers to cause a denial of service (Internet Explorer 7 crash) via a long (1) IsFolderAvailable or (2) RootFolder property value, different vectors than CVE-2007-0371.
CVE-2007-1163
SQL injection vulnerability in printview.php in webSPELL 4.01.02 and earlier allows remote attackers to execute arbitrary SQL commands via the topic parameter, a different vector than CVE-2007-1019, CVE-2006-5388, and CVE-2006-4783.
CVE-2007-1164
Multiple PHP remote file inclusion vulnerabilities in DBImageGallery 1.2.2 allow remote attackers to execute arbitrary PHP code via a URL in the donsimg_base_path parameter to (1) attributes.php, (2) images.php, or (3) scan.php in admin/; or (4) attributes.php, (5) db_utils.php, (6) images.php, (7) utils.php, or (8) values.php in includes/.
CVE-2007-1165
Multiple PHP remote file inclusion vulnerabilities in DBGuestbook 1.1 allow remote attackers to execute arbitrary PHP code via a URL in the dbs_base_path parameter to (1) utils.php, (2) guestbook.php, or (3) views.php in includes/.
CVE-2007-1166
SQL injection vulnerability in result.php in Nabopoll 1.2 allows remote attackers to execute arbitrary SQL commands via the surv parameter.
CVE-2007-1167
inc/filebrowser/browser.php in deV!L`z Clanportal (DZCP) 1.4.5 and earlier allows remote attackers to obtain MySQL data via the inc/mysql.php value of the file parameter.
This vulnerability is addressed in the following product release:
1.4.6
CVE-2007-1168
Trend Micro ServerProtect for Linux (SPLX) 1.25, 1.3, and 2.5 before 20070216 allows remote attackers to access arbitrary web pages and reconfigure the product via HTTP requests with the splx_2376_info cookie to the web interface port (14942/tcp).
CVE-2007-1169
The web interface in Trend Micro ServerProtect for Linux (SPLX) 1.25, 1.3, and 2.5 before 20070216 accepts logon requests through unencrypted HTTP, which might allow remote attackers to obtain credentials by sniffing the network.
CVE-2007-1170
SimBin GTR - FIA GT Racing Game 1.5.0.0 and earlier, GT Legends 1.1.0.0 and earlier, GTR 2 1.1 and earlier, and RACE - The WTCC Game 1.0 and earlier allow remote attackers to cause a denial of service (client disconnection) via an empty UDP packet to the server port.
CVE-2007-1171
SQL injection vulnerability in includes/nsbypass.php in NukeSentinel 2.5.05, 2.5.11, and other versions before 2.5.12 allows remote attackers to execute arbitrary SQL commands via an admin cookie.
CVE-2007-1172
SQL injection vulnerability in nukesentinel.php in NukeSentinel 2.5.05, and possibly earlier, allows remote attackers to execute arbitrary SQL commands via the Client-IP HTTP header, aka the "File Disclosure Exploit."
CVE-2007-1174
Multiple cross-site scripting (XSS) vulnerabilities in WebAPP before 20070214 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, related to unspecified fields in user Profiles.  NOTE: some of these details are obtained from third party information.
CVE-2007-1175
Cross-site scripting (XSS) vulnerability in an admin feature in WebAPP before 20070209 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-1176
Multiple cross-site scripting (XSS) vulnerabilities in WebAPP before 0.9.9.5 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to (1) Gallery Comments pages, (2) Feedback pages, (3) Search Results pages, and (4) the Statistics Log viewer.
CVE-2007-1177
WebAPP before 0.9.9.5 does not properly filter certain characters in contexts related to (1) the query string, (2) Profiles, (3) the Forum Post icon field, (4) the Edit Profile, and (5) the Gallery, which has unknown impact and remote attack vectors, possibly related to cross-site scripting (XSS).
CVE-2007-1178
WebAPP before 0.9.9.5 does not check access in certain contexts related to (1) Calendar Administration, (2) Instant Messages Administration, and (3) the Image Uploader, which has unknown impact and attack vectors.
CVE-2007-1179
WebAPP before 0.9.9.5 does not properly manage e-mail addresses in certain contexts related to (1) the Recommend feature, Email Article (2) senders and (3) recipients, (4) New User Approval, (5) Edit Profiles, (6) the Newsletter Subscription form, (7) the Recommend form, and (8) sending of articles, which has unknown impact, and remote attack vectors related to spam attacks and possibly other attacks.
CVE-2007-1180
WebAPP before 0.9.9.5 does not check referrers in certain forms, which might facilitate remote cross-site request forgery (CSRF) attacks or have other unknown impact.
CVE-2007-1181
WebAPP before 0.9.9.5 passes (1) Unused Informations and (2) the username through Edit Profile forms, which has unknown impact and attack vectors.
CVE-2007-1182
WebAPP before 0.9.9.5 allows remote Guest users to edit a Guest profile, which has unknown impact.
CVE-2007-1183
WebAPP before 0.9.9.5 allows remote authenticated users to spoof another user's Real Name via whitespace, which has unknown impact and attack vectors.
CVE-2007-1184
The default configuration of WebAPP before 0.9.9.5 has a CAPTCHA setting of "no," which makes it easier for automated programs to submit false data.
CVE-2007-1185
The (1) Search, (2) Edit Profile, (3) Recommend, and (4) User Approval forms in WebAPP before 0.9.9.5 use hidden inputs, which has unknown impact and remote attack vectors.
CVE-2007-1186
WebAPP before 0.9.9.5 does not "censor" the Latest Member real name, which has unknown impact.
CVE-2007-1187
WebAPP before 0.9.9.5 allows remote authenticated users, without admin privileges, to obtain sensitive information via (1) the Forum Archive feature and (2) Recent Searches.
CVE-2007-1188
WebAPP before 0.9.9.5 allows remote attackers to submit Search form input that is not checked for (1) composition or (2) length, which has unknown impact, possibly related to "search form hijacking".
CVE-2007-1189
Integer overflow in the envwrite function in the Alcatel-Lucent Bell Labs Plan 9 kernel allows local users to overwrite certain memory addresses with kernel memory via a large n argument, as demonstrated by (1) modifying the iseve function to gain privileges and (2) making the devpermcheck function grant unrestricted device permissions.
CVE-2007-1190
Unspecified vulnerability in the EmbeddedWB Web Browser ActiveX control allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1191
The Social Bookmarks (del.icio.us) plug-in 8F in Quicksilver writes usernames and passwords in plaintext to the /Library/Logs/Console/UID/Console.log file, which allows local users to obtain sensitive information by reading this file.
CVE-2007-1192
Thomas R. Pasawicz HyperBook Guestbook 1.30 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download an admin password hash via a direct request for data/gbconfiguration.dat.
CVE-2007-1193
Multiple unspecified vulnerabilities in the Login page in OrangeHRM before 20070212 have unknown impact and attack vectors.
Successful exploitation requires that "magic_quotes_gpc" is disabled.
CVE-2007-1194
Norman SandBox Analyzer does not use the proper range for Interrupt Descriptor Table (IDT) entries, which allows local users to determine that the local machine is an emulator, or a similar environment not based on a physical Intel processor, which allows attackers to produce malware that is more difficult to analyze.
CVE-2007-1195
Multiple buffer overflows in XM Easy Personal FTP Server 5.3.0 allow remote attackers to execute arbitrary code via unspecified vectors. NOTE: this issue might overlap CVE-2006-2225, CVE-2006-2226, or CVE-2006-5728.
CVE-2007-1196
Unspecified vulnerability in Citrix Presentation Server Client for Windows before 10.0 allows remote web sites to execute arbitrary code via unspecified vectors, related to the implementation of ICA connectivity through proxy servers.
Upgrade to Citrix Presentation Server Client for Windows version 10.0:
http://www.citrix.com/English/SS/downloads/downloads.asp?dID=2755 

CVE-2007-1197
Multiple unspecified vulnerabilities in Epiware before 4.7.5 have unknown impact and attack vectors, possibly related to cross-site scripting (XSS) and other unspecified issues.
CVE-2007-1198
Cross-site scripting (XSS) vulnerability in TaskFreak! before 0.5.7 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, possibly a variant of CVE-2007-0982.
CVE-2007-1199
Adobe Reader and Acrobat Trial allow remote attackers to read arbitrary files via a file:// URI in a PDF document, as demonstrated with <</URI(file:///C:/)/S/URI>>, a different issue than CVE-2007-0045.
CVE-2007-1217
Buffer overflow in the bufprint function in capiutil.c in libcapi, as used in Linux kernel 2.6.9 to 2.6.20 and isdn4k-utils, allows local users to cause a denial of service (crash) and possibly gain privileges via a crafted CAPI packet.
CVE-2007-1218
Off-by-one buffer overflow in the parse_elements function in the 802.11 printer code (print-802_11.c) for tcpdump 3.9.5 and earlier allows remote attackers to cause a denial of service (crash) via a crafted 802.11 frame.  NOTE: this was originally referred to as heap-based, but it might be stack-based.
CVE-2007-1219
PHP remote file inclusion vulnerability in actions/del.php in Admin Phorum 3.3.1a allows remote attackers to execute arbitrary PHP code via a URL in the include_path parameter.
CVE-2007-1220
The Hypervisor in Microsoft Xbox 360 kernel 4532 and 4548 does not properly verify the parameters passed to the syscall dispatcher, which allows attackers with physical access to bypass code-signing requirements and execute arbitrary code.
CVE-2007-1221
The Hypervisor in Microsoft Xbox 360 kernel 4532 and 4548 allows attackers with physical access to force execution of the hypervisor syscall with a certain register set, which bypasses intended code protection.
CVE-2007-1222
Parallels Desktop for Mac before 20070216 implements Drag and Drop by sharing the entire host filesystem as the .psf share, which allows local users of the guest operating system to write arbitrary files to the host filesystem, and execute arbitrary code via launchd by writing a plist file to a LaunchAgents directory.
CVE-2007-1223
Unspecified vulnerability in Hitachi OSAS/FT/W before 20070223 allows attackers to cause a denial of service (responder control processing halt) by sending "data unexpectedly through the port".
CVE-2007-1224
Grok Developments NetProxy 4.03 allows remote attackers to bypass URL filtering via a request that omits "http://" from the URL and specifies the destination port (:80).
CVE-2007-1225
The connection log file implementation in Grok Developments NetProxy 4.03 does not record requests that omit http:// in a URL, which might allow remote attackers to conduct unauthorized activities and avoid detection.
CVE-2007-1226
McAfee VirusScan for Mac (Virex) before 7.7 patch 1 has weak permissions (0666) for /Library/Application Support/Virex/VShieldExclude.txt, which allows local users to reconfigure Virex to skip scanning of arbitrary files.
CVE-2007-1227
VShieldCheck in McAfee VirusScan for Mac (Virex) before 7.7 patch 1 allow local users to change permissions of arbitrary files via a symlink attack on /Library/Application Support/Virex/VShieldExclude.txt, as demonstrated by symlinking to the root crontab file to execute arbitrary commands.
Apply patch 1 for McAfee Virex version 7.7:
https://mysupport.mcafee.com/eservice_enu/ 

CVE-2007-1228
IBM DB2 UDB 8.2 before Fixpak 7 (aka fixpack 14), and DB2 9 before Fix Pack 2, on UNIX allows the "fenced" user to access certain unauthorized directories.
CVE-2007-1229
Cross-site scripting (XSS) vulnerability in the Nullsoft ShoutcastServer 1.9.7 allows remote attackers to inject arbitrary web script or HTML via the top-level URI on the Incoming interface (port 8001/tcp), which is not properly handled in the administrator interface when viewing the log file.
CVE-2007-1230
Multiple cross-site scripting (XSS) vulnerabilities in wp-includes/functions.php in WordPress before 2.1.2-alpha allow remote attackers to inject arbitrary web script or HTML via (1) the Referer HTTP header or (2) the URI, a different vulnerability than CVE-2007-1049.
