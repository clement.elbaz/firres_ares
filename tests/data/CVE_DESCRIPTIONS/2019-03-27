CVE-2016-10744
In Select2 through 4.0.5, as used in Snipe-IT and other products, rich selectlists allow XSS. This affects use cases with Ajax remote data loading when HTML templates are used to display listbox data.
CVE-2017-18364
phpFK lite has XSS via the faq.php, members.php, or search.php query string or the user.php user parameter.
CVE-2017-2748
A potential security vulnerability caused by the use of insecure (http) transactions during login has been identified with early versions of the Isaac Mizrahi Smartwatch mobile app. HP has no access to customer data as a result of this issue.
CVE-2017-2752
A potential security vulnerability caused by incomplete obfuscation of application configuration information was discovered in Tommy Hilfiger TH24/7 Android app versions 2.0.0.11, 2.0.1.14, 2.1.0.16, and 2.2.0.19. HP has no access to customer data as a result of this issue.
CVE-2017-7655
In Eclipse Mosquitto version from 1.0 to 1.4.15, a Null Dereference vulnerability was found in the Mosquitto library which could lead to crashes for those applications using the library.
CVE-2017-9626
Systems using the Marel Food Processing Systems Pluto platform do not restrict remote access. Marel has created an update for Pluto-based applications. This update will restrict remote access by implementing SSH authentication.
CVE-2018-10934
A cross-site scripting (XSS) vulnerability was found in the JBoss Management Console versions before 7.1.6.CR1, 7.1.6.GA. Users with roles that can create objects in the application can exploit this to attack other privileged users.
CVE-2018-12178
Buffer overflow in network stack for EDK II may allow unprivileged user to potentially enable escalation of privilege and/or denial of service via network.
CVE-2018-12179
Improper configuration in system firmware for EDK II may allow unauthenticated user to potentially enable escalation of privilege, information disclosure and/or denial of service via local access.
CVE-2018-12180
Buffer overflow in BlockIo service for EDK II may allow an unauthenticated user to potentially enable escalation of privilege, information disclosure and/or denial of service via network access.
CVE-2018-12181
Stack overflow in corrupted bmp for EDK II may allow unprivileged user to potentially enable denial of service or elevation of privilege via local access.
CVE-2018-12182
Insufficient memory write check in SMM service for EDK II may allow an authenticated user to potentially enable escalation of privilege, information disclosure and/or denial of service via local access.
CVE-2018-12183
Stack overflow in DxeCore for EDK II may allow an unauthenticated user to potentially enable escalation of privilege, information disclosure and/or denial of service via local access.
CVE-2018-12545
In Eclipse Jetty version 9.3.x and 9.4.x, the server is vulnerable to Denial of Service conditions if a remote client sends either large SETTINGs frames container containing many settings, or many small SETTINGs frames. The vulnerability is due to the additional CPU and memory allocations required to handle changed settings.
CVE-2018-12546
In Eclipse Mosquitto version 1.0 to 1.5.5 (inclusive) when a client publishes a retained message to a topic, then has its access to that topic revoked, the retained message will still be published to clients that subscribe to that topic in the future. In some applications this may result in clients being able cause effects that would otherwise not be allowed.
CVE-2018-12550
When Eclipse Mosquitto version 1.0 to 1.5.5 (inclusive) is configured to use an ACL file, and that ACL file is empty, or contains only comments or blank lines, then Mosquitto will treat this as though no ACL file has been defined and use a default allow policy. The new behaviour is to have an empty ACL file mean that all access is denied, which is not a useful configuration but is not unexpected.
CVE-2018-12551
When Eclipse Mosquitto version 1.0 to 1.5.5 (inclusive) is configured to use a password file for authentication, any malformed data in the password file will be treated as valid. This typically means that the malformed data becomes a username and no password. If this occurs, clients can circumvent authentication and get access to the broker by using the malformed username. In particular, a blank line will be treated as a valid empty username. Other security measures are unaffected. Users who have only used the mosquitto_passwd utility to create and modify their password files are unaffected by this vulnerability.
CVE-2018-14814
WECON Technology PI Studio HMI versions 4.1.9 and prior and PI Studio versions 4.2.34 and prior lacks proper validation of user-supplied data, which may result in a read past the end of an allocated object.
CVE-2018-15585
Cross-Site Scripting (XSS) vulnerability in newwinform.php in GNUBOARD5 before 5.3.1.6 allows remote attackers to inject arbitrary web script or HTML via the popup title parameter.
CVE-2018-16207
PowerAct Pro Master Agent for Windows Version 5.13 and earlier allows authenticated attackers to bypass access restriction to alter or edit unauthorized files via unspecified vectors.
CVE-2018-18994
LCDS Laquis SCADA prior to version 4.1.0.4150 allows an out of bounds read when opening a specially crafted project file, which may cause a system crash or allow data exfiltration.
CVE-2018-19016
Rockwell Automation EtherNet/IP Web Server Modules 1756-EWEB (includes 1756-EWEBK) Version 5.001 and earlier, and CompactLogix 1768-EWEB Version 2.005 and earlier. A remote attacker could send a crafted UDP packet to the SNMP service causing a denial-of-service condition to occur until the affected product is restarted.
CVE-2018-19466
A vulnerability was found in Portainer before 1.20.0. Portainer stores LDAP credentials, corresponding to a master password, in cleartext and allows their retrieval via API calls.
CVE-2018-19641
Unauthenticated remote code execution issue in Micro Focus Solutions Business Manager (SBM) (formerly Serena Business Manager (SBM)) versions prior to 11.5.
CVE-2018-19642
Denial of service issue in Micro Focus Solutions Business Manager (SBM) (formerly Serena Business Manager (SBM)) versions prior to 11.5.
CVE-2018-19643
Information leakage issue in Micro Focus Solutions Business Manager (SBM) (formerly Serena Business Manager (SBM)) versions prior to 11.5.
CVE-2018-19644
Reflected cross site script issue in Micro Focus Solutions Business Manager (SBM) (formerly Serena Business Manager (SBM)) versions prior to 11.5.
CVE-2018-19648
An issue was discovered in ADTRAN PMAA 1.6.2-1, 1.6.3, and 1.6.4. NETCONF Access Management (NACM) allows unprivileged users to create privileged users and execute arbitrary commands via the use of the diagnostic-profile over RESTCONF.
CVE-2018-3613
Logic issue in variable service module for EDK II/UDK2018/UDK2017/UDK2015 may allow an authenticated user to potentially enable escalation of privilege, information disclosure and/or denial of service via local access.
CVE-2018-5923
In HP LaserJet Enterprise, HP PageWide Enterprise, HP LaserJet Managed, and HP OfficeJet Enterprise Printers, solution application signature checking may allow potential execution of arbitrary code.
CVE-2018-5926
A potential vulnerability has been identified in HP Remote Graphics Software?s certificate authentication process version 7.5.0 and earlier.
CVE-2018-5927
HP Support Assistant before 8.7.50.3 allows an unauthorized person with local access to load arbitrary code.
CVE-2019-0160
Buffer overflow in system firmware for EDK II may allow unauthenticated user to potentially enable escalation of privilege and/or denial of service via network access.
CVE-2019-0161
Stack overflow in XHCI for EDK II may allow an unauthenticated user to potentially enable denial of service via local access.
CVE-2019-1000031
A disk space or quota exhaustion issue exists in article2pdf_getfile.php in the article2pdf Wordpress plugin 0.24, 0.25, 0.26, 0.27. Visiting PDF generation link but not following the redirect will leave behind a PDF file on disk which will never be deleted by the plug-in.
CVE-2019-1010257
An Information Disclosure / Data Modification issue exists in article2pdf_getfile.php in the article2pdf Wordpress plugin 0.24, 0.25, 0.26, 0.27. A URL can be constructed which allows overriding the PDF file's path leading to any PDF whose path is known and which is readable to the web server can be downloaded. The file will be deleted after download if the web server has permission to do so. For PHP versions before 5.3, any file can be read by null terminating the string left of the file extension.
CVE-2019-10118
Snipe-IT before 4.6.14 has XSS, as demonstrated by log_meta values and the user's last name in the API.
CVE-2019-10124
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was withdrawn by its CNA. Further investigation showed that it was not a security issue. Notes: none.
CVE-2019-10125
An issue was discovered in aio_poll() in fs/aio.c in the Linux kernel through 5.0.4. A file may be released by aio_poll_wake() if an expected event is triggered immediately (e.g., by the close of a pair of pipes) after the return of vfs_poll(), and this will cause a use-after-free.
CVE-2019-10231
Teclib GLPI before 9.4.1.1 is affected by a PHP type juggling vulnerability allowing bypass of authentication. This occurs in Auth::checkPassword() (inc/auth.class.php).
CVE-2019-10232
Teclib GLPI through 9.3.3 has SQL injection via the "cycle" parameter in /scripts/unlock_tasks.php.
CVE-2019-10233
Teclib GLPI before 9.4.1.1 is affected by a timing attack associated with a cookie.
CVE-2019-10237
S-CMS PHP v1.0 has a CSRF vulnerability to add a new admin user via the 4.edu.php/admin/ajax.php?type=admin&action=add&lang=0 URI, a related issue to CVE-2019-9040.
CVE-2019-10238
Sitemagic CMS v4.4 has XSS in SMFiles/FrmUpload.class.php via the filename parameter.
CVE-2019-1737
A vulnerability in the processing of IP Service Level Agreement (SLA) packets by Cisco IOS Software and Cisco IOS XE software could allow an unauthenticated, remote attacker to cause an interface wedge and an eventual denial of service (DoS) condition on the affected device. The vulnerability is due to improper socket resources handling in the IP SLA responder application code. An attacker could exploit this vulnerability by sending crafted IP SLA packets to an affected device. An exploit could allow the attacker to cause an interface to become wedged, resulting in an eventual denial of service (DoS) condition on the affected device.
CVE-2019-3814
It was discovered that Dovecot before versions 2.2.36.1 and 2.3.4.1 incorrectly handled client certificates. A remote attacker in possession of a valid certificate with an empty username field could possibly use this issue to impersonate other users.
CVE-2019-3817
A use-after-free flaw has been discovered in libcomps before version 0.1.10 in the way ObjMRTrees are merged. An attacker, who is able to make an application read a crafted comps XML file, may be able to crash the application or execute malicious code.
CVE-2019-3821
A flaw was found in the way civetweb frontend was handling requests for ceph RGW server with SSL enabled. An unauthenticated attacker could create multiple connections to ceph RADOS gateway to exhaust file descriptors for ceph-radosgw service resulting in a remote denial of service.
CVE-2019-3828
Ansible fetch module before versions 2.5.15, 2.6.14, 2.7.8 has a path traversal vulnerability which allows copying and overwriting files outside of the specified destination in the local ansible controller host, by not restricting an absolute path.
CVE-2019-3829
A vulnerability was found in gnutls versions from 3.5.8 before 3.6.7. A memory corruption (double free) vulnerability in the certificate verification API. Any client or server application that verifies X.509 certificates with GnuTLS 3.5.8 or later is affected.
CVE-2019-3840
A NULL pointer dereference flaw was discovered in libvirt before version 5.0.0 in the way it gets interface information through the QEMU agent. An attacker in a guest VM can use this flaw to crash libvirtd and cause a denial of service.
CVE-2019-3847
A vulnerability was found in moodle before versions 3.6.3, 3.5.5, 3.4.8 and 3.1.17. Users with the "login as other users" capability (such as administrators/managers) can access other users' Dashboards, but the JavaScript those other users may have added to their Dashboard was not being escaped when being viewed by the user logging in on their behalf.
CVE-2019-3877
A vulnerability was found in mod_auth_mellon before v0.14.2. An open redirect in the logout URL allows requests with backslashes to pass through by assuming that it is a relative URL, while the browsers silently convert backslash characters into forward slashes treating them as an absolute URL. This mismatch allows an attacker to bypass the redirect URL validation logic in apr_uri_parse function.
CVE-2019-5418
There is a File Content Disclosure vulnerability in Action View <5.2.2.1, <5.1.6.2, <5.0.7.2, <4.2.11.1 and v3 where specially crafted accept headers can cause contents of arbitrary files on the target system's filesystem to be exposed.
CVE-2019-5419
There is a possible denial of service vulnerability in Action View (Rails) <5.2.2.1, <5.1.6.2, <5.0.7.2, <4.2.11.1 where specially crafted accept headers can cause action view to consume 100% cpu and make the server unresponsive.
CVE-2019-5420
A remote code execution vulnerability in development mode Rails <5.2.2.1, <6.0.0.beta3 can allow an attacker to guess the automatically generated development mode secret token. This secret token can be used in combination with other Rails internals to escalate to a remote code execution exploit.
CVE-2019-5926
Cross-site scripting vulnerability in KinagaCMS versions prior to 6.5 allows remote authenticated attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-5927
Directory traversal vulnerability in 'an' App for iOS Version 3.2.0 and earlier allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2019-6536
Opening a specially crafted LCDS LAquis SCADA before 4.3.1.71 ELS file may result in a write past the end of an allocated buffer, which may allow an attacker to execute remote code in the context of the current process.
CVE-2019-7167
Zcash, before the Sapling network upgrade (2018-10-28), had a counterfeiting vulnerability. A key-generation process, during evaluation of polynomials related to a to-be-proven statement, produced certain bypass elements. Availability of these elements allowed a cheating prover to bypass a consistency check, and consequently transform the proof of one statement into an ostensibly valid proof of a different statement, thereby breaking the soundness of the proof system. This misled the original Sprout zk-SNARK verifier into accepting the correctness of a transaction.
CVE-2019-9860
Due to unencrypted signal communication and predictability of rolling codes, an attacker can "desynchronize" an ABUS Secvest wireless remote control (FUBE50014 or FUBE50015) relative to its controlled Secvest wireless alarm system FUAA50000 3.01.01, so that sent commands by the remote control are not accepted anymore.
CVE-2019-9862
An issue was discovered on ABUS Secvest wireless alarm system FUAA50000 3.01.01 in conjunction with Secvest remote control FUBE50014 or FUBE50015. Because "encrypted signal transmission" is missing, an attacker is able to eavesdrop sensitive data as cleartext (for instance, the current rolling code state).
CVE-2019-9863
Due to the use of an insecure algorithm for rolling codes in the ABUS Secvest wireless alarm system FUAA50000 3.01.01 and its remote controls FUBE50014 and FUBE50015, an attacker is able to predict valid future rolling codes, and can thus remotely control the alarm system in an unauthorized way.
CVE-2019-9917
ZNC before 1.7.3-rc1 allows an existing remote user to cause a Denial of Service (crash) via invalid encoding.
