CVE-2018-1921
IBM Campaign 9.1.0, 9.1.2, 10.1, and 11.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 152857.
CVE-2018-2021
IBM QRadar SIEM 7.2 and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 155345.
CVE-2018-2022
IBM QRadar SIEM 7.2 and 7.3 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 155346.
CVE-2019-1010083
The Pallets Project Flask before 1.0 is affected by: unexpected memory usage. The impact is: denial of service. The attack vector is: crafted encoded JSON data. The fixed version is: 1. NOTE: this may overlap CVE-2018-1000656.
CVE-2019-1010084
Dancer::Plugin::SimpleCRUD 1.14 and earlier is affected by: Incorrect Access Control. The impact is: Potential for unathorised access to data. The component is: Incorrect calls to _ensure_auth() wrapper result in authentication-checking not being applied to al routes.
CVE-2019-1010091
tinymce 4.7.11, 4.7.12 is affected by: CWE-79: Improper Neutralization of Input During Web Page Generation. The impact is: JavaScript code execution. The component is: Media element. The attack vector is: The victim must paste malicious content to media element's embed tab.
CVE-2019-1010263
Perl Crypt::JWT prior to 0.023 is affected by: Incorrect Access Control. The impact is: allow attackers to bypass authentication by providing a token by crafting with hmac(). The component is: JWT.pm, line 614. The attack vector is: network connectivity. The fixed version is: after commit b98a59b42ded9f9e51b2560410106207c2152d6c.
CVE-2019-1010266
lodash prior to 4.17.11 is affected by: CWE-400: Uncontrolled Resource Consumption. The impact is: Denial of service. The component is: Date handler. The attack vector is: Attacker provides very long strings, which the library attempts to match using a regular expression. The fixed version is: 4.17.11.
CVE-2019-1010275
helm Before 2.7.2 is affected by: CWE-295: Improper Certificate Validation. The impact is: Unauthorized clients could connect to the server because self-signed client certs were aloowed. The component is: helm (many files updated, see https://github.com/helm/helm/pull/3152/files/1096813bf9a425e2aa4ac755b6c991b626dfab50). The attack vector is: A malicious client could connect to the server over the network. The fixed version is: 2.7.2.
CVE-2019-1010283
Univention Corporate Server univention-directory-notifier 12.0.1-3 and earlier is affected by: CWE-213: Intentional Information Exposure. The impact is: Loss of Confidentiality. The component is: function data_on_connection() in src/callback.c. The attack vector is: network connectivity. The fixed version is: 12.0.1-4 and later.
CVE-2019-1010287
Timesheet Next Gen 1.5.3 and earlier is affected by: Cross Site Scripting (XSS). The impact is: Allows an attacker to execute arbitrary HTML and JavaScript code via a "redirect" parameter. The component is: Web login form: login.php, lines 40 and 54. The attack vector is: reflected XSS, victim may click the malicious url.
CVE-2019-10352
A path traversal vulnerability in Jenkins 2.185 and earlier, LTS 2.176.1 and earlier in core/src/main/java/hudson/model/FileParameterValue.java allowed attackers with Job/Configure permission to define a file parameter with a file name outside the intended directory, resulting in an arbitrary file write on the Jenkins master when scheduling a build.
CVE-2019-10353
CSRF tokens in Jenkins 2.185 and earlier, LTS 2.176.1 and earlier did not expire, thereby allowing attackers able to obtain them to bypass CSRF protection.
CVE-2019-10354
A vulnerability in the Stapler web framework used in Jenkins 2.185 and earlier, LTS 2.176.1 and earlier allowed attackers to access view fragments directly, bypassing permission checks and possibly obtain sensitive information.
CVE-2019-11535
Unsanitized user input in the web interface for Linksys WiFi extender products (RE6400 and RE6300 through 1.2.04.022) allows for remote command execution. An attacker can access system OS configurations and commands that are not intended for use beyond the web UI.
CVE-2019-11771
AIX builds of Eclipse OpenJ9 before 0.15.0 contain unused RPATHs which may facilitate code injection and privilege elevation by local users.
CVE-2019-11772
In Eclipse OpenJ9 prior to 0.15, the String.getBytes(int, int, byte[], int) method does not verify that the provided byte array is non-null nor that the provided index is in bounds when compiled by the JIT. This allows arbitrary writes to any 32-bit address or beyond the end of a byte array within Java code run under a SecurityManager.
CVE-2019-12175
In Zeek Network Security Monitor (formerly known as Bro) before 2.6.2, a NULL pointer dereference in the Kerberos (aka KRB) protocol parser leads to DoS because a case-type index is mishandled.
CVE-2019-12475
In MicroStrategy Web before 10.4.6, there is stored XSS in metric due to insufficient input validation.
CVE-2019-12876
Zoho ManageEngine ADManager Plus 6.6.5, ADSelfService Plus 5.7, and DesktopCentral 10.0.380 have Insecure Permissions, leading to Privilege Escalation from low level privileges to System.
CVE-2019-12911
Redbrick Shift through 3.4.3 allows an attacker to extract authentication tokens of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-12912
Redbrick Shift through 3.4.3 allows an attacker to extract emails of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-12913
Redbrick Shift through 3.4.3 allows an attacker to extract emails of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-12914
Redbrick Shift through 3.4.3 allows an attacker to extract authentication tokens of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-13272
In the Linux kernel before 5.1.17, ptrace_link in kernel/ptrace.c mishandles the recording of the credentials of a process that wants to create a ptrace relationship, which allows local users to obtain root access by leveraging certain scenarios with a parent-child process relationship, where a parent drops privileges and calls execve (potentially allowing control by an attacker). One contributing factor is an object lifetime issue (which can also cause a panic). Another contributing factor is incorrect marking of a ptrace relationship as privileged, which is exploitable through (for example) Polkit's pkexec helper with PTRACE_TRACEME. NOTE: SELinux deny_ptrace might be a usable workaround in some environments.
CVE-2019-13346
In MyT 1.5.1, the User[username] parameter has XSS.
CVE-2019-13403
Temenos CWX version 8.9 has an Broken Access Control vulnerability in the module /CWX/Employee/EmployeeEdit2.aspx, leading to the viewing of user information.
CVE-2019-13446
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was withdrawn by its CNA. Further investigation showed that it was not a security issue. Notes: none.
CVE-2019-13447
An issue was discovered in Sertek Xpare 3.67. The login form does not sanitize input data. Because of this, a malicious agent could access the backend database via SQL injection.
CVE-2019-13448
An issue was discovered in Sertek Xpare 3.67. The login form does not sanitize input data. Because of this, a malicious agent could exploit the vulnerable function in order to prepare an XSS payload to send to the product's clients.
CVE-2019-13453
Zipios before 0.1.7 does not properly handle certain malformed zip archives and can go into an infinite loop, causing a denial of service. This is related to zipheadio.h:readUint32() and zipfile.cpp:Zipfile::Zipfile().
CVE-2019-13493
In Sitecore 9.0 rev 171002, Persistent XSS exists in the Media Library and File Manager. An authenticated unprivileged user can modify the uploaded file extension parameter to inject arbitrary JavaScript.
CVE-2019-13573
A SQL injection vulnerability exists in the FolioVision FV Flowplayer Video Player plugin before 7.3.19.727 for WordPress. Successful exploitation of this vulnerability would allow a remote attacker to execute arbitrary SQL commands on the affected system.
CVE-2019-13577
SnmpAdm.exe in MAPLE WBT SNMP Administrator v2.0.195.15 has an Unauthenticated Remote Buffer Overflow via a long string to the CE Remote feature listening on Port 987.
CVE-2019-13584
The remote admin webserver on FANUC Robotics Virtual Robot Controller 8.23 allows Directory Traversal via a forged HTTP request.
CVE-2019-13585
The remote admin webserver on FANUC Robotics Virtual Robot Controller 8.23 has a Buffer Overflow via a forged HTTP request.
CVE-2019-13613
CMD_FTEST_CONFIG in the TP-Link Device Debug protocol in TP-Link Wireless Router Archer Router version 1.0.0 Build 20180502 rel.45702 (EU) and earlier is prone to a stack-based buffer overflow, which allows a remote attacker to achieve code execution or denial of service by sending a crafted payload to the listening server.
CVE-2019-13614
CMD_SET_CONFIG_COUNTRY in the TP-Link Device Debug protocol in TP-Link Archer C1200 1.0.0 Build 20180502 rel.45702 and earlier is prone to a stack-based buffer overflow, which allows a remote attacker to achieve code execution or denial of service by sending a crafted payload to the listening server.
CVE-2019-13619
In Wireshark 3.0.0 to 3.0.2, 2.6.0 to 2.6.9, and 2.4.0 to 2.4.15, the ASN.1 BER dissector and related dissectors could crash. This was addressed in epan/asn1.c by properly restricting buffer increments.
CVE-2019-13623
In NSA Ghidra before 9.1, path traversal can occur in RestoreTask.java (from the package ghidra.app.plugin.core.archive) via an archive with an executable file that has an initial ../ in its filename. This allows attackers to overwrite arbitrary files in scenarios where an intermediate analysis result is archived for sharing with other persons. To achieve arbitrary code execution, one approach is to overwrite some critical Ghidra modules, e.g., the decompile module.
CVE-2019-13624
In ONOS 1.15.0, apps/yang/web/src/main/java/org/onosproject/yang/web/YangWebResource.java mishandles backquote characters within strings that can be used in a shell command.
CVE-2019-13625
NSA Ghidra before 9.0.1 allows XXE when a project is opened or restored, or a tool is imported, as demonstrated by a project.prp file.
CVE-2019-13626
SDL (Simple DirectMedia Layer) 2.x through 2.0.9 has a heap-based buffer over-read in Fill_IMA_ADPCM_block, caused by an integer overflow in IMA_ADPCM_decode() in audio/SDL_wave.c.
CVE-2019-13631
In parse_hid_report_descriptor in drivers/input/tablet/gtco.c in the Linux kernel through 5.2.1, a malicious USB device can send an HID report that triggers an out-of-bounds write during generation of debugging messages.
CVE-2019-13636
In GNU patch through 2.7.6, the following of symlinks is mishandled in certain cases other than input files. This affects inp.c and util.c.
CVE-2019-13637
In LogMeIn join.me before 3.16.0.5505, an attacker could execute arbitrary commands on a targeted system. This vulnerability is due to unsafe search paths used by the application URI that is defined in Windows. An attacker could exploit this vulnerability by convincing a targeted user to follow a malicious link. Successful exploitation could cause the application to load libraries from the directory targeted by the URI link. The attacker could use this behavior to execute arbitrary commands on the system with the privileges of the targeted user if the attacker can place a crafted library in a directory that is accessible to the vulnerable system.
CVE-2019-13640
In qBittorrent before 4.1.7, the function Application::runExternalProgram() located in app/application.cpp allows command injection via shell metacharacters in the torrent name parameter or current tracker parameter, as demonstrated by remote command execution via a crafted name within an RSS feed.
CVE-2019-1917
A vulnerability in the REST API interface of Cisco Vision Dynamic Signage Director could allow an unauthenticated, remote attacker to bypass authentication on an affected system. The vulnerability is due to insufficient validation of HTTP requests. An attacker could exploit this vulnerability by sending a crafted HTTP request to an affected system. A successful exploit could allow the attacker to execute arbitrary actions through the REST API with administrative privileges on the affected system. The REST API is enabled by default and cannot be disabled.
CVE-2019-1919
A vulnerability in the Cisco FindIT Network Management Software virtual machine (VM) images could allow an unauthenticated, local attacker who has access to the VM console to log in to the device with a static account that has root privileges. The vulnerability is due to the presence of an account with static credentials in the underlying Linux operating system. An attacker could exploit this vulnerability by logging in to the command line of the affected VM with the static account. A successful exploit could allow the attacker to log in with root-level privileges. This vulnerability affects only Cisco FindIT Network Manager and Cisco FindIT Network Probe Release 1.1.4 if these products are using Cisco-supplied VM images. No other releases or deployment models are known to be vulnerable.
CVE-2019-1920
A vulnerability in the 802.11r Fast Transition (FT) implementation for Cisco IOS Access Points (APs) Software could allow an unauthenticated, adjacent attacker to cause a denial of service (DoS) condition on an affected interface. The vulnerability is due to a lack of complete error handling condition for client authentication requests sent to a targeted interface configured for FT. An attacker could exploit this vulnerability by sending crafted authentication request traffic to the targeted interface, causing the device to restart unexpectedly.
CVE-2019-1923
A vulnerability in Cisco Small Business SPA500 Series IP Phones could allow a physically proximate attacker to execute arbitrary commands on the device. The vulnerability is due to improper input validation in the device configuration interface. An attacker could exploit this vulnerability by accessing the configuration interface, which may require a password, and then accessing the device's physical interface and inserting a USB storage device. A successful exploit could allow the attacker to execute arbitrary commands on the device in an elevated security context. At the time of publication, this vulnerability affected Cisco Small Business SPA500 Series IP Phones firmware releases 7.6.2SR5 and prior.
CVE-2019-1940
A vulnerability in the Web Services Management Agent (WSMA) feature of Cisco Industrial Network Director (IND) could allow an unauthenticated, remote attacker to gain unauthorized read access to sensitive data using an invalid X.509 certificate. The vulnerability is due to insufficient X.509 certificate validation when establishing a WSMA connection. An attacker could exploit this vulnerability by supplying a crafted X.509 certificate during the WSMA connection setup phase. A successful exploit could allow the attacker to conduct man-in-the-middle attacks to decrypt confidential information on WSMA connections to the affected software. At the time of publication, this vulnerability affected Cisco IND Software releases prior to 1.7.
CVE-2019-1941
A vulnerability in the web-based management interface of Cisco Identity Services Engine (ISE) could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. The vulnerability exists because the web-based management interface does not properly validate user-supplied input. An attacker could exploit this vulnerability by persuading a user to click a malicious link. A successful exploit could allow the attacker to execute arbitrary script code in the context of the affected interface or access sensitive, browser-based information. At the time of publication, this vulnerability affected Cisco ISE running software releases prior to 2.4.0 Patch 9 and 2.6.0.
CVE-2019-1942
A vulnerability in the sponsor portal web interface for Cisco Identity Services Engine (ISE) could allow an authenticated, remote attacker to impact the integrity of an affected system by executing arbitrary SQL queries. The vulnerability is due to insufficient validation of user-supplied input. An attacker could exploit this vulnerability by sending crafted input that includes SQL statements to an affected system. A successful exploit could allow the attacker to modify entries in some database tables, affecting the integrity of the data. At the time of publication, this vulnerability affected Cisco ISE running software releases 2.6.0 and prior.
CVE-2019-1943
A vulnerability in the web interface of Cisco Small Business 200, 300, and 500 Series Switches software could allow an unauthenticated, remote attacker to redirect a user to a malicious web page. The vulnerability is due to improper input validation of the parameters of an HTTP request. An attacker could exploit this vulnerability by intercepting a user's HTTP request and modifying it into a request that causes the web interface to redirect the user to a specific malicious URL. This type of vulnerability is known as an open redirect attack and is used in phishing attacks that get users to unknowingly visit malicious sites.
CVE-2019-3969
Comodo Antivirus versions up to 12.0.0.6810 are vulnerable to Local Privilege Escalation due to CmdAgent's handling of COM clients. A local process can bypass the signature check enforced by CmdAgent via process hollowing which can then allow the process to invoke sensitive COM methods in CmdAgent such as writing to the registry with SYSTEM privileges.
CVE-2019-3970
Comodo Antivirus versions up to 12.0.0.6810 are vulnerable to Arbitrary File Write due to Cavwp.exe handling of Comodo's Antivirus database. Cavwp.exe loads Comodo antivirus definition database in unsecured global section objects, allowing a local low privileged process to modify this data directly and change virus signatures.
CVE-2019-3971
Comodo Antivirus versions up to 12.0.0.6810 are vulnerable to a local Denial of Service affecting CmdVirth.exe via its LPC port "cmdvrtLPCServerPort". A low privileged local process can connect to this port and send an LPC_DATAGRAM, which triggers an Access Violation due to hardcoded NULLs used for Source parameter in a memcpy operation that is called for this handler. This results in CmdVirth.exe and its child svchost.exe instances to terminate.
CVE-2019-3972
Comodo Antivirus versions 12.0.0.6810 and below are vulnerable to Denial of Service affecting CmdAgent.exe via an unprotected section object "<GUID>_CisSharedMemBuff". This section object is exposed by CmdAgent and contains a SharedMemoryDictionary object, which allows a low privileged process to modify the object data causing CmdAgent.exe to crash.
CVE-2019-3973
Comodo Antivirus versions 11.0.0.6582 and below are vulnerable to Denial of Service affecting CmdGuard.sys via its filter port "cmdServicePort". A low privileged process can crash CmdVirth.exe to decrease the port's connection count followed by process hollowing a CmdVirth.exe instance with malicious code to obtain a handle to "cmdServicePort". Once this occurs, a specially crafted message can be sent to "cmdServicePort" using "FilterSendMessage" API. This can trigger an out-of-bounds write if lpOutBuffer parameter in FilterSendMessage API is near the end of specified buffer bounds. The crash occurs when the driver performs a memset operation which uses a size beyond the size of buffer specified, causing kernel crash.
CVE-2019-4054
IBM QRadar SIEM 7.2 and 7.3 could allow a local user to obtain sensitive information when exporting content that could aid an attacker in further attacks against the system. IBM X-Force ID: 156563.
CVE-2019-4194
IBM Jazz for Service Management 1.1.3, 1.1.3.1, and 1.1.3.2 is missing function level access control that could allow a user to delete authorized resources. IBM X-Force ID: 159033.
CVE-2019-4211
IBM QRadar SIEM 7.2 and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 159131.
CVE-2019-4430
IBM Maximo Asset Management 7.6 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system. IBM X-Force ID: 162887.
CVE-2019-5222
There is an information disclosure vulnerability on Secure Input of certain Huawei smartphones in Versions earlier than Tony-AL00B 9.1.0.216(C00E214R2P1). The Secure Input does not properly limit certain system privilege. An attacker tricks the user to install a malicious application and successful exploit could result in information disclosure.
CVE-2019-8931
Redbrick Shift through 3.4.3 allows an attacker to extract emails of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-8932
Redbrick Shift through 3.4.3 allows an attacker to extract authentication tokens of services (such as Gmail, Outlook, etc.) used in the application.
CVE-2019-9848
LibreOffice has a feature where documents can specify that pre-installed scripts can be executed on various document events such as mouse-over, etc. LibreOffice is typically also bundled with LibreLogo, a programmable turtle vector graphics script, which can be manipulated into executing arbitrary python commands. By using the document event feature to trigger LibreLogo to execute python contained within a document a malicious document could be constructed which would execute arbitrary python commands silently without warning. In the fixed versions, LibreLogo cannot be called from a document event handler. This issue affects: Document Foundation LibreOffice versions prior to 6.2.5.
CVE-2019-9849
LibreOffice has a 'stealth mode' in which only documents from locations deemed 'trusted' are allowed to retrieve remote resources. This mode is not the default mode, but can be enabled by users who want to disable LibreOffice's ability to include remote resources within a document. A flaw existed where bullet graphics were omitted from this protection prior to version 6.2.5. This issue affects: Document Foundation LibreOffice versions prior to 6.2.5.
