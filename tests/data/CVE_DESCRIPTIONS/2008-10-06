CVE-2008-3872
Adobe Flash Player 8.0.39.0 and earlier, and 9.x up to 9.0.115.0, allows remote attackers to bypass the allowScriptAccess parameter setting via a crafted SWF file with unspecified "Filter evasion" manipulations.
CVE-2008-4278
VMware VirtualCenter 2.5 before Update 3 build 119838 on Windows displays a user's password in cleartext when the password contains unspecified special characters, which allows physically proximate attackers to steal the password.
CVE-2008-4279
The CPU hardware emulation for 64-bit guest operating systems in VMware Workstation 6.0.x before 6.0.5 build 109488 and 5.x before 5.5.8 build 108000; Player 2.0.x before 2.0.5 build 109488 and 1.x before 1.0.8; Server 1.x before 1.0.7 build 108231; and ESX 2.5.4 through 3.5 allows authenticated guest OS users to gain additional guest OS privileges by triggering an exception that causes the virtual CPU to perform an indirect jump to a non-canonical address.
CVE-2008-4445
The sctp_auth_ep_set_hmacs function in net/sctp/auth.c in the Stream Control Transmission Protocol (sctp) implementation in the Linux kernel before 2.6.26.4, when the SCTP-AUTH extension is enabled, does not verify that the identifier index is within the bounds established by SCTP_AUTH_HMAC_ID_MAX, which allows local users to obtain sensitive information via a crafted SCTP_HMAC_IDENT IOCTL request involving the sctp_getsockopt function, a different vulnerability than CVE-2008-4113.
CVE-2008-4446
Cross-site scripting (XSS) vulnerability in Nucleus EUC-JP 3.31 SP1 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-4447
Cross-site scripting (XSS) vulnerability in actions.php in Positive Software H-Sphere WebShell 4.3.10 allows remote attackers to inject arbitrary web script or HTML via (1) the fn parameter during a dload action, (2) the mask parameter during a search action, and (3) the tab parameter during a sysinfo action.
CVE-2008-4448
Cross-site request forgery (CSRF) vulnerability in actions.php in Positive Software H-Sphere WebShell 4.3.10 allows remote attackers to perform unauthorized actions as an administrator, including file deletion and creation, via a link or IMG tag to the (1) overkill, (2) futils, or (3) edit actions.
CVE-2008-4449
Stack-based buffer overflow in mIRC 6.34 allows remote attackers to execute arbitrary code via a long hostname in a PRIVMSG message.
CVE-2008-4450
Cross-site scripting (XSS) vulnerability in adodb.php in XAMPP for Windows 1.6.8 allows remote attackers to inject arbitrary web script or HTML via the (1) dbserver, (2) host, (3) user, (4) password, (5) database, and (6) table parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4451
The SysInspector AntiStealth driver (esiasdrv.sys) 3.0.65535.0 in ESET System Analyzer Tool 1.1.1.0 allows local users to execute arbitrary code via a certain METHOD_NEITHER IOCTL request to \Device\esiasdrv that overwrites a pointer.
CVE-2008-4452
Buffer overflow in Cambridge Computer Corporation vxFtpSrv 2.0.3 allows remote attackers to cause a denial of service (crash and hang) and possibly execute arbitrary code via a long CWD request.
CVE-2008-4453
The GdPicture (1) Light Imaging Toolkit 4.7.1 GdPicture4S.Imaging ActiveX control (gdpicture4s.ocx) 4.7.0.1 and (2) Pro Imaging SDK 5.7.1 GdPicturePro5S.Imaging ActiveX control (gdpicturepro5s.ocx) 5.7.0.1 allows remote attackers to create, overwrite, and modify arbitrary files via the SaveAsPDF method.  NOTE: this issue might only be exploitable in limited environments or non-default browser settings.  NOTE: this can be leveraged for remote code execution by accessing files using hcp:// URLs.  NOTE: some of these details are obtained from third party information.
CVE-2008-4454
Directory traversal vulnerability in EKINdesigns MySQL Quick Admin 1.5.5 allows remote attackers to read and execute arbitrary files via a .. (dot dot) in the lang parameter to actions.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4455
Directory traversal vulnerability in index.php in EKINdesigns MySQL Quick Admin 1.5.5 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to read and execute arbitrary files via a .. (dot dot) in the language cookie.
CVE-2008-4456
Cross-site scripting (XSS) vulnerability in the command-line client in MySQL 5.0.26 through 5.0.45, and other versions including versions later than 5.0.45, when the --html option is enabled, allows attackers to inject arbitrary web script or HTML by placing it in a database cell, which might be accessed by this client when composing an HTML document.  NOTE: as of 20081031, the issue has not been fixed in MySQL 5.0.67.
