CVE-2015-5304
Red Hat JBoss Enterprise Application Platform (EAP) before 6.4.5 does not properly authorize access to shut down the server, which allows remote authenticated users with the Monitor, Deployer, or Auditor role to cause a denial of service via unspecified vectors.
CVE-2015-6425
The WebApplications Identity Management subsystem in Cisco Unified Communications Manager 10.5(0.98000.88) allows remote attackers to cause a denial of service (subsystem outage) via invalid session tokens, aka Bug ID CSCul83786.
CVE-2015-7201
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-7202
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 43.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-7203
Buffer overflow in the DirectWriteFontInfo::LoadFontFamilyData function in gfx/thebes/gfxDWriteFontList.cpp in Mozilla Firefox before 43.0 might allow remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted font-family name.
CVE-2015-7204
Mozilla Firefox before 43.0 does not properly store the properties of unboxed objects, which allows remote attackers to execute arbitrary code via crafted JavaScript variable assignments.
CVE-2015-7205
Integer underflow in the RTPReceiverVideo::ParseRtpPacket function in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 might allow remote attackers to obtain sensitive information, cause a denial of service, or possibly have unspecified other impact by triggering a crafted WebRTC RTP packet.
CVE-2015-7207
Mozilla Firefox before 43.0 does not properly restrict the availability of IFRAME Resource Timing API times, which allows remote attackers to bypass the Same Origin Policy and obtain sensitive information via crafted JavaScript code that leverages history.back and performance.getEntries calls, a related issue to CVE-2015-1300.
CVE-2015-7208
Mozilla Firefox before 43.0 stores cookies containing vertical tab characters, which allows remote attackers to obtain sensitive information by reading HTTP Cookie headers.
CVE-2015-7210
Use-after-free vulnerability in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 allows remote attackers to execute arbitrary code by triggering attempted use of a data channel that has been closed by a WebRTC function.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-7211
Mozilla Firefox before 43.0 mishandles the # (number sign) character in a data: URI, which allows remote attackers to spoof web sites via unspecified vectors.
CVE-2015-7212
Integer overflow in the mozilla::layers::BufferTextureClient::AllocateForSurface function in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 allows remote attackers to execute arbitrary code by triggering a graphics operation that requires a large texture allocation.
CVE-2015-7213
Integer overflow in the MPEG4Extractor::readMetaData function in MPEG4Extractor.cpp in libstagefright in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 on 64-bit platforms allows remote attackers to execute arbitrary code via a crafted MP4 video file that triggers a buffer overflow.
CVE-2015-7214
Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 allow remote attackers to bypass the Same Origin Policy via data: and view-source: URIs.
CVE-2015-7215
The importScripts function in the Web Workers API implementation in Mozilla Firefox before 43.0 allows remote attackers to bypass the Same Origin Policy by triggering use of the no-cors mode in the fetch API to attempt resource access that throws an exception, leading to information disclosure after a rethrow.
CVE-2015-7216
The gdk-pixbuf configuration in Mozilla Firefox before 43.0 on Linux GNOME platforms incorrectly enables the JasPer decoder, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted JPEG 2000 image.
CVE-2015-7217
The gdk-pixbuf configuration in Mozilla Firefox before 43.0 on Linux GNOME platforms incorrectly enables the TGA decoder, which allows remote attackers to cause a denial of service (heap-based buffer overflow) via a crafted Truevision TGA image.
CVE-2015-7218
The HTTP/2 implementation in Mozilla Firefox before 43.0 allows remote attackers to cause a denial of service (integer underflow, assertion failure, and application exit) via a single-byte header frame that triggers incorrect memory allocation.
CVE-2015-7219
The HTTP/2 implementation in Mozilla Firefox before 43.0 allows remote attackers to cause a denial of service (integer underflow, assertion failure, and application exit) via a malformed PushPromise frame that triggers decompressed-buffer length miscalculation and incorrect memory allocation.
CVE-2015-7220
Buffer overflow in the XDRBuffer::grow function in js/src/vm/Xdr.cpp in Mozilla Firefox before 43.0 might allow remote attackers to cause a denial of service or possibly have unspecified other impact via crafted JavaScript code.
CVE-2015-7221
Buffer overflow in the nsDeque::GrowCapacity function in xpcom/glue/nsDeque.cpp in Mozilla Firefox before 43.0 might allow remote attackers to cause a denial of service or possibly have unspecified other impact by triggering a deque size change.
CVE-2015-7222
Integer underflow in the Metadata::setData function in MetaData.cpp in libstagefright in Mozilla Firefox before 43.0 and Firefox ESR 38.x before 38.5 allows remote attackers to execute arbitrary code or cause a denial of service (incorrect memory allocation and application crash) via an MP4 video file with crafted covr metadata that triggers a buffer overflow.
CVE-2015-7223
The WebExtension APIs in Mozilla Firefox before 43.0 allow remote attackers to gain privileges, and possibly obtain sensitive information or conduct cross-site scripting (XSS) attacks, via a crafted web site.
CVE-2015-8000
db.c in named in ISC BIND 9.x before 9.9.8-P2 and 9.10.x before 9.10.3-P2 allows remote attackers to cause a denial of service (REQUIRE assertion failure and daemon exit) via a malformed class attribute.
CVE-2015-8357
Directory traversal vulnerability in the bitrix.xscan module before 1.0.4 for Bitrix allows remote authenticated users to rename arbitrary files, and consequently obtain sensitive information or cause a denial of service, via a .. (dot dot) in the file parameter to admin/bitrix.xscan_worker.php.
CVE-2015-8358
Directory traversal vulnerability in the bitrix.mpbuilder module before 1.0.12 for Bitrix allows remote administrators to include and execute arbitrary local files via a .. (dot dot) in the element name of the "work" array parameter to admin/bitrix.mpbuilder_step2.php.
CVE-2015-8370
Multiple integer underflows in Grub2 1.98 through 2.02 allow physically proximate attackers to bypass authentication, obtain sensitive information, or cause a denial of service (disk corruption) via backspace characters in the (1) grub_username_get function in grub-core/normal/auth.c or the (2) grub_password_get function in lib/crypto.c, which trigger an "Off-by-two" or "Out of bounds overwrite" memory error.
CVE-2015-8461
Race condition in resolver.c in named in ISC BIND 9.9.8 before 9.9.8-P2 and 9.10.3 before 9.10.3-P2 allows remote attackers to cause a denial of service (INSIST assertion failure and daemon exit) via unspecified vectors.
CVE-2015-8476
Multiple CRLF injection vulnerabilities in PHPMailer before 5.2.14 allow attackers to inject arbitrary SMTP commands via CRLF sequences in an (1) email address to the validateAddress function in class.phpmailer.php or (2) SMTP command to the sendCommand function in class.smtp.php, a different vulnerability than CVE-2012-0796.
CVE-2015-8562
Joomla! 1.5.x, 2.x, and 3.x before 3.4.6 allow remote attackers to conduct PHP object injection attacks and execute arbitrary PHP code via the HTTP User-Agent header, as exploited in the wild in December 2015.
CVE-2015-8563
Cross-site request forgery (CSRF) vulnerability in the com_templates component in Joomla! 3.2.0 through 3.3.x and 3.4.x before 3.4.6 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2015-8564
Directory traversal vulnerability in Joomla! 3.4.x before 3.4.6 allows remote attackers to have unspecified impact via directory traversal sequences in the XML install file in an extension package archive.
CVE-2015-8565
Directory traversal vulnerability in Joomla! 3.2.0 through 3.3.x and 3.4.x before 3.4.6 allows remote attackers to have unspecified impact via unknown vectors.
CVE-2015-8566
The Session package 1.x before 1.3.1 for Joomla! Framework allows remote attackers to execute arbitrary code via unspecified session values.
CVE-2015-8577
The Buffer Overflow Protection (BOP) feature in McAfee VirusScan Enterprise before 8.8 Patch 6 allocates memory with Read, Write, Execute (RWX) permissions at predictable addresses on 32-bit platforms when protecting another application, which allows attackers to bypass the DEP and ASLR protection mechanisms via unspecified vectors.
CVE-2015-8578
AVG Internet Security 2015 allocates memory with Read, Write, Execute (RWX) permissions at predictable addresses when protecting user-mode processes, which allows attackers to bypass the DEP and ASLR protection mechanisms via unspecified vectors.
CVE-2015-8579
Kaspersky Total Security 2015 15.0.2.361 allocates memory with Read, Write, Execute (RWX) permissions at predictable addresses when protecting user-mode processes, which allows attackers to bypass the DEP and ASLR protection mechanisms via unspecified vectors.
CVE-2015-8580
Multiple use-after-free vulnerabilities in the (1) Print method and (2) App object handling in Foxit Reader before 7.2.2 and Foxit PhantomPDF before 7.2.2 allow remote attackers to execute arbitrary code via a crafted PDF document.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-8581
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2016-0779. Reason: This candidate is a duplicate of CVE-2016-0779. Notes: All CVE users should reference CVE-2016-0779 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
