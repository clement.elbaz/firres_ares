CVE-2011-1337
Opera before 11.50 allows remote attackers to cause a denial of service (disk consumption) via invalid URLs that trigger creation of error pages.
CVE-2011-1514
The inet service in HP OpenView Storage Data Protector 6.00 through 6.20 allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via a request containing crafted parameters.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2011-1515
The inet service in HP OpenView Storage Data Protector 6.00 through 6.20 allows remote attackers to cause a denial of service (daemon exit) via a request containing crafted parameters.
CVE-2011-1865
Multiple stack-based buffer overflows in the inet service in HP OpenView Storage Data Protector 6.00 through 6.20 allow remote attackers to execute arbitrary code via a request containing crafted parameters.
CVE-2011-1866
Buffer overflow in omniinet.exe in the inet service in HP OpenView Storage Data Protector 6.00 through 6.20 allows remote attackers to execute arbitrary code via a crafted request, related to the EXEC_CMD functionality.
CVE-2011-2608
ovbbccb.exe 6.20.50.0 and other versions in HP OpenView Performance Agent 4.70 and 5.0; and Operations Agent 11.0, 8.60.005, 8.60.006, 8.60.007, 8.60.008, 8.60.501, and 8.53; allows remote attackers to delete arbitrary files via a full pathname in the File field in a Register command.
CVE-2011-2609
Opera before 11.50 does not properly restrict data: URIs, which makes it easier for remote attackers to conduct cross-site scripting (XSS) attacks via a crafted web site.
CVE-2011-2610
Unspecified vulnerability in Opera before 11.50 has unknown impact and attack vectors, related to a "moderately severe issue."
CVE-2011-2611
Unspecified vulnerability in the printing functionality in Opera before 11.50 allows user-assisted remote attackers to cause a denial of service (application crash) via a crafted web page.
CVE-2011-2612
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by progorod.ru.
CVE-2011-2613
The Array.prototype.join method in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via a non-array object that contains initial holes.
CVE-2011-2614
The SVG implementation in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via vectors involving a path on which many characters are drawn.
CVE-2011-2615
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (application hang) via unknown content on a web page, as demonstrated by domiteca.com.
CVE-2011-2616
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (memory consumption) via unknown content on a web page, as demonstrated by test262.ecmascript.org.
CVE-2011-2617
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via vectors related to selecting a text node, and closed pop-up windows, removed pop-up windows, and IFRAME elements.
CVE-2011-2618
Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via web script that moves a (1) AUDIO element or (2) VIDEO element between windows.
CVE-2011-2619
Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via a gradient with many stops, related to the implementation of CANVAS elements, SVG, and Cascading Style Sheets (CSS).
CVE-2011-2620
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via vectors involving SVG animation.
CVE-2011-2621
Unspecified vulnerability in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via vectors related to form layout.
CVE-2011-2622
Unspecified vulnerability in the Web Workers implementation in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via unknown vectors.
CVE-2011-2623
Unspecified vulnerability in the SVG BiDi implementation in Opera before 11.50 allows remote attackers to cause a denial of service (application crash or hang) via unknown vectors.
CVE-2011-2624
Opera before 11.50 allows user-assisted remote attackers to cause a denial of service (application hang) via a large table, which is not properly handled during a print preview.
CVE-2011-2625
Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via a SELECT element that contains many OPTION elements.
CVE-2011-2626
Opera before 11.50 allows remote attackers to cause a denial of service (application crash) by using "injected script" to set the SRC attribute of an IFRAME element.
CVE-2011-2627
Unspecified vulnerability in the DOM implementation in Opera before 11.50 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by live.com.
CVE-2011-2628
Opera before 11.11 does not properly implement FRAMESET elements, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via vectors related to page unload.
CVE-2011-2629
Unspecified vulnerability in Opera before 11.11 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by www.falk.de.
CVE-2011-2630
Opera before 11.11 allows user-assisted remote attackers to cause a denial of service (application crash) via a crafted web page that is not properly handled during a reload occurring after the opening of a popup of the Easy Sticky Note extension.
CVE-2011-2631
The Cascading Style Sheets (CSS) implementation in Opera before 11.11 does not properly handle the column-count property, which allows remote attackers to cause a denial of service (infinite repaint loop and application hang) via a web page, as demonstrated by an unspecified Wikipedia page.
CVE-2011-2632
Opera before 11.11 does not properly handle destruction of a Silverlight instance, which allows remote attackers to cause a denial of service (application crash) via a web page, as demonstrated by vod.onet.pl.
CVE-2011-2633
Unspecified vulnerability in Opera before 11.11 allows remote attackers to cause a denial of service (application crash) via vectors involving a Certificate Revocation List (CRL) file, as demonstrated by the multicert-ca-02.crl file.
CVE-2011-2634
Opera before 11.10 allows remote attackers to hijack (1) searches and (2) customizations via unspecified third party applications.
CVE-2011-2635
The Cascading Style Sheets (CSS) implementation in Opera before 11.10 allows remote attackers to cause a denial of service (application crash) via vectors involving use of the :hover pseudo-class, in conjunction with transforms, for a floated element.
CVE-2011-2636
Unspecified vulnerability in Opera before 11.10 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by a certain Tomato Firmware page.
CVE-2011-2637
Unspecified vulnerability in Opera before 11.10 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by futura-sciences.com, seoptimise.com, and mitosyfraudes.org.
CVE-2011-2638
Unspecified vulnerability in Opera before 11.10 allows remote attackers to cause a denial of service (application crash) via unknown content on a web page, as demonstrated by games on zylom.com.
CVE-2011-2639
Opera before 11.10 does not properly handle hidden animated GIF images, which allows remote attackers to cause a denial of service (CPU consumption) via an image file that triggers continual repaints.
CVE-2011-2640
Opera before 11.10 allows remote attackers to cause a denial of service (application crash) via an HTML document that has an empty parameter value for an embedded Java applet.
CVE-2011-2641
Opera 11.11 allows remote attackers to cause a denial of service (application crash) by setting the FACE attribute of a FONT element within an IFRAME element after changing the SRC attribute of this IFRAME element to an about:blank value.
