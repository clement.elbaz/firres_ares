CVE-2010-0808
Microsoft Internet Explorer 6 and 7 on Windows XP and Vista does not prevent script from simulating user interaction with the AutoComplete feature, which allows remote attackers to obtain sensitive form information via a crafted web site, aka "AutoComplete Information Disclosure Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-071.mspx

'An attacker who successfully exploited this vulnerability could potentially capture data previously entered into forms in the browser. The AutoComplete feature is disabled by default.'
CVE-2010-1883
Integer overflow in the Embedded OpenType (EOT) Font Engine in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows remote attackers to execute arbitrary code via a crafted table in an embedded font, aka "Embedded OpenType Font Integer Overflow Vulnerability."
CVE-2010-2740
The OpenType Font (OTF) format driver in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 does not properly perform memory allocation during font parsing, which allows local users to gain privileges via a crafted application, aka "OpenType Font Parsing Vulnerability."
CVE-2010-2741
The OpenType Font (OTF) format driver in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 performs an incorrect integer calculation during font processing, which allows local users to gain privileges via a crafted application, aka "OpenType Font Validation Vulnerability."
CVE-2010-2744
The kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 do not properly manage a window class, which allows local users to gain privileges by creating a window, then using (1) the SetWindowLongPtr function to modify the popup menu structure, or (2) the SwitchWndProc function with a switch window information pointer, which is not re-initialized when a WM_NCCREATE message is processed, aka "Win32k Window Class Vulnerability."
CVE-2010-2745
Microsoft Windows Media Player (WMP) 9 through 12 does not properly deallocate objects during a browser reload action, which allows user-assisted remote attackers to execute arbitrary code via crafted media content referenced in an HTML document, aka "Windows Media Player Memory Corruption Vulnerability."
CVE-2010-2746
Heap-based buffer overflow in Comctl32.dll (aka the common control library) in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7, when a third-party SVG viewer is used, allows remote attackers to execute arbitrary code via a crafted HTML document that triggers unspecified messages from this viewer, aka "Comctl32 Heap Overflow Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-081.mspx

'This vulnerability cannot be exploited in the default Windows configuration. In order to be vulnerable to the issue, a third-party application that renders scalable vector graphics (SVG) must be installed on the system.'
CVE-2010-2747
Microsoft Word 2002 SP3 and Office 2004 for Mac do not properly handle an uninitialized pointer during parsing of a Word document, which allows remote attackers to execute arbitrary code via a crafted document that triggers memory corruption, aka "Word Uninitialized Pointer Vulnerability."
CVE-2010-2748
Microsoft Word 2002 SP3 and Office 2004 for Mac do not properly check an unspecified boundary during parsing of a Word document, which allows remote attackers to execute arbitrary code via a crafted document that triggers memory corruption, aka "Word Boundary Check Vulnerability."
CVE-2010-2750
Array index error in Microsoft Word 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via a crafted Word document that triggers memory corruption, aka "Word Index Vulnerability."
CVE-2010-3214
Stack-based buffer overflow in Microsoft Word 2002 SP3, 2003 SP3, 2007 SP2, and 2010; Office 2004 and 2008 for Mac; Open XML File Format Converter for Mac; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2; Word Viewer; Office Web Apps; and Word Web App allows remote attackers to execute arbitrary code via a crafted Word document, aka "Word Stack Overflow Vulnerability."
CVE-2010-3215
Microsoft Word 2002 SP3 and Office 2004 for Mac do not properly handle unspecified return values during parsing of a Word document, which allows remote attackers to execute arbitrary code via a crafted document that triggers memory corruption, aka "Word Return Value Vulnerability."
CVE-2010-3216
Microsoft Word 2002 SP3 and Office 2004 for Mac allow remote attackers to execute arbitrary code via a crafted Word document containing bookmarks that trigger use of an invalid pointer and memory corruption, aka "Word Bookmarks Vulnerability."
CVE-2010-3217
Double free vulnerability in Microsoft Word 2002 SP3 allows remote attackers to execute arbitrary code via a Word document with crafted List Format Override (LFO) records, aka "Word Pointer Vulnerability."
CVE-2010-3218
Heap-based buffer overflow in Microsoft Word 2002 SP3 allows remote attackers to execute arbitrary code via malformed records in a Word document, aka "Word Heap Overflow Vulnerability."
CVE-2010-3219
Array index vulnerability in Microsoft Word 2002 SP3 allows remote attackers to execute arbitrary code via a crafted Word document that triggers memory corruption, aka "Word Index Parsing Vulnerability."
CVE-2010-3220
Unspecified vulnerability in Microsoft Word 2002 SP3 and Office 2004 for Mac allows remote attackers to execute arbitrary code via a crafted Word document that triggers memory corruption, aka "Word Parsing Vulnerability."
CVE-2010-3221
Microsoft Word 2002 SP3 and 2003 SP3, Office 2004 for Mac, and Word Viewer do not properly handle a malformed record during parsing of a Word document, which allows remote attackers to execute arbitrary code via a crafted document that triggers memory corruption, aka "Word Parsing Vulnerability."
CVE-2010-3222
Stack-based buffer overflow in the Remote Procedure Call Subsystem (RPCSS) in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 allows local users to gain privileges via a crafted LPC message that requests an LRPC connection from an LPC server to a client, aka "LPC Message Buffer Overrun Vulnerability."
CVE-2010-3223
The user interface in Microsoft Cluster Service (MSCS) in Microsoft Windows Server 2008 R2 does not properly set administrative-share permissions for new cluster disks that are shared as part of a failover cluster, which allows remote attackers to read or modify data on these disks via requests to the associated share, aka "Permissions on New Cluster Disks Vulnerability."
CVE-2010-3225
Use-after-free vulnerability in the Media Player Network Sharing Service in Microsoft Windows Vista SP1 and SP2 and Windows 7 allows remote attackers to execute arbitrary code via a crafted Real Time Streaming Protocol (RTSP) packet, aka "RTSP Use After Free Vulnerability."
CVE-2010-3228
The JIT compiler in Microsoft .NET Framework 4.0 on 64-bit platforms does not properly perform optimizations, which allows remote attackers to execute arbitrary code via a crafted .NET application that triggers memory corruption, aka ".NET Framework x64 JIT Compiler Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-077.mspx

'This vulnerability only affects the x64 and Itanium architectures.'

CVE-2010-3229
The Secure Channel (aka SChannel) security package in Microsoft Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7, when IIS 7.x is used, does not properly process client certificates during SSL and TLS handshakes, which allows remote attackers to cause a denial of service (LSASS outage and reboot) via a crafted packet, aka "TLSv1 Denial of Service Vulnerability."
CVE-2010-3230
Integer overflow in Microsoft Excel 2002 SP3 allows remote attackers to execute arbitrary code via an Excel document with crafted record information, aka "Excel Record Parsing Integer Overflow Vulnerability."
CVE-2010-3231
Microsoft Excel 2002 SP3, Office 2004 and 2008 for Mac, and Open XML File Format Converter for Mac do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Excel Record Parsing Memory Corruption Vulnerability."
CVE-2010-3232
Microsoft Excel 2003 SP3 and 2007 SP2; Office 2004 and 2008 for Mac; Open XML File Format Converter for Mac; Excel Viewer SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2 do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Excel File Format Parsing Vulnerability."
CVE-2010-3233
Microsoft Excel 2002 SP3 and 2003 SP3 does not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted .wk3 (aka Lotus 1-2-3 workbook) file, aka "Lotus 1-2-3 Workbook Parsing Vulnerability."
CVE-2010-3234
Microsoft Excel 2002 SP3 does not properly validate formula information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Formula Substream Memory Corruption Vulnerability."
CVE-2010-3235
Microsoft Excel 2002 SP3 does not properly validate formula information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Formula Biff Record Vulnerability."
CVE-2010-3236
Microsoft Excel 2002 SP3 and 2003 SP3, Office 2004 and 2008 for Mac, and Open XML File Format Converter for Mac do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Out Of Bounds Array Vulnerability."
CVE-2010-3237
Microsoft Excel 2002 SP3 and Office 2004 for Mac do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Merge Cell Record Pointer Vulnerability."
CVE-2010-3238
Microsoft Excel 2002 SP3 and 2003 SP3, and Office 2004 for Mac, does not properly validate binary file-format information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Negative Future Function Vulnerability."
CVE-2010-3239
Microsoft Excel 2002 SP3 does not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Extra Out of Boundary Record Parsing Vulnerability."
CVE-2010-3240
Microsoft Excel 2002 SP3 and 2007 SP2; Excel Viewer SP2; and Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2 do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Real Time Data Array Record Vulnerability."
CVE-2010-3241
Microsoft Excel 2002 SP3, Office 2004 and 2008 for Mac, and Open XML File Format Converter for Mac do not properly validate binary file-format information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Out-of-Bounds Memory Write in Parsing Vulnerability."
CVE-2010-3242
Microsoft Excel 2002 SP3, Office 2004 and 2008 for Mac, and Open XML File Format Converter for Mac do not properly validate record information, which allows remote attackers to execute arbitrary code via a crafted Excel document, aka "Ghost Record Type Parsing Vulnerability."
CVE-2010-3243
Cross-site scripting (XSS) vulnerability in the toStaticHTML function in Microsoft Internet Explorer 8, and the SafeHTML function in Microsoft Windows SharePoint Services 3.0 SP2 and Office SharePoint Server 2007 SP2, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka "HTML Sanitization Vulnerability."
CVE-2010-3325
Microsoft Internet Explorer 6 through 8 does not properly handle unspecified special characters in Cascading Style Sheets (CSS) documents, which allows remote attackers to obtain sensitive information from a different (1) domain or (2) zone via a crafted web site, aka "CSS Special Character Information Disclosure Vulnerability."
CVE-2010-3326
Microsoft Internet Explorer 6 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2010-3327
The implementation of HTML content creation in Microsoft Internet Explorer 6 through 8 does not remove the Anchor element during pasting and editing, which might allow remote attackers to obtain sensitive deleted information by visiting a web page, aka "Anchor Element Information Disclosure Vulnerability."
CVE-2010-3328
Use-after-free vulnerability in the CAttrArray::PrivateFind function in mshtml.dll in Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code by setting an unspecified property of a stylesheet object, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2010-3329
mshtmled.dll in Microsoft Internet Explorer 7 and 8 allows remote attackers to execute arbitrary code via a crafted Microsoft Office document that causes the HtmlDlgHelper class destructor to access uninitialized memory, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2010-3330
Microsoft Internet Explorer 6 through 8 does not properly restrict script access to content from a different (1) domain or (2) zone, which allows remote attackers to obtain sensitive information via a crafted web site, aka "Cross-Domain Information Disclosure Vulnerability."
CVE-2010-3331
Microsoft Internet Explorer 6 through 8 does not properly handle objects in memory in certain circumstances involving use of Microsoft Word to read Word documents, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
