CVE-2007-2927
Unspecified vulnerability in Atheros 802.11 a/b/g wireless adapter drivers before 5.3.0.35, and 6.x before 6.0.3.67, on Windows allows remote attackers to cause a denial of service via a crafted 802.11 management frame.
CVE-2007-3108
The BN_from_montgomery function in crypto/bn/bn_mont.c in OpenSSL 0.9.8e and earlier does not properly perform Montgomery multiplication, which might allow local users to conduct a side-channel attack and retrieve RSA private keys.
CVE-2007-3384
Multiple cross-site scripting (XSS) vulnerabilities in examples/servlet/CookieExample in Apache Tomcat 3.3 through 3.3.2 allow remote attackers to inject arbitrary web script or HTML via the (1) Name or (2) Value field, related to error messages.
CVE-2007-3844
Mozilla Firefox 2.0.0.5, Thunderbird 2.0.0.5 and before 1.5.0.13, and SeaMonkey 1.1.3 allows remote attackers to conduct cross-site scripting (XSS) attacks with chrome privileges via an addon that inserts a (1) javascript: or (2) data: link into an about:blank document loaded by chrome via (a) the window.open function or (b) a content.location assignment, aka "Cross Context Scripting." NOTE: this issue is caused by a CVE-2007-3089 regression.
CVE-2007-3845
Mozilla Firefox before 2.0.0.6, Thunderbird before 1.5.0.13 and 2.x before 2.0.0.6, and SeaMonkey before 1.1.4 allow remote attackers to execute arbitrary commands via certain vectors associated with launching "a file handling program based on the file extension at the end of the URI," a variant of CVE-2007-4041.  NOTE: the vendor states that "it is still possible to launch a filetype handler based on extension rather than the registered protocol handler."
CVE-2007-4175
Multiple cross-site scripting (XSS) vulnerabilities in index.php in OpenRat CMS 0.8-beta1 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) subaction and (2) action parameters.
CVE-2007-4176
Multiple unspecified vulnerabilities in EQDKP Plus before 0.4.4.5 have unknown impact and attack vectors.
CVE-2007-4177
Multiple cross-site scripting (XSS) vulnerabilities in Interact before 2.4 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this might overlap CVE-2007-3328.
CVE-2007-4178
Cross-site scripting (XSS) vulnerability in index.php in WebDirector 2.2 and earlier allows remote attackers to inject arbitrary web script or HTML via the deslocal parameter.
CVE-2007-4179
Unspecified vulnerability in the Address and Routing Parameter Area (ARPA) transport functionality in HP-UX B.11.11 and B.11.23 allows local users to cause an unspecified denial of service via unknown vectors.  NOTE: this is probably different from CVE-2007-0916, but this is not certain due to lack of vendor details.
CVE-2007-4180
** DISPUTED **  Directory traversal vulnerability in data/inc/theme.php in Pluck 4.3, when register_globals is enabled, allows remote attackers to read arbitrary local files via a .. (dot dot) in the file parameter.  NOTE: CVE and a reliable third party dispute this vulnerability because the code uses a fixed argument when invoking fputs, which cannot be used to read files.
CVE-2007-4181
** DISPUTED **  PHP remote file inclusion vulnerability in data/inc/theme.php in Pluck 4.3, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the dir parameter.  NOTE: A reliable third party disputes this vulnerability because the applicable include is within a function that does not receive the dir parameter from an HTTP request.
CVE-2007-4182
Unrestricted file upload vulnerability in index.php in WikiWebWeaver 1.1 and earlier allows remote attackers to upload and execute arbitrary PHP code via an upload action specifying a filename with a double extension such as .gif.php, which is accessible from data/documents/.
CVE-2007-4183
SQL injection vulnerability in main.php in paBugs 2.0 Beta 3 and earlier allows remote attackers to execute arbitrary SQL commands via the cid parameter to index.php.
CVE-2007-4184
SQL injection vulnerability in administrator/popups/pollwindow.php in Joomla! 1.0.12 allows remote attackers to execute arbitrary SQL commands via the pollid parameter.
CVE-2007-4185
Joomla! 1.0.12 allows remote attackers to obtain sensitive information via a direct request for (1) Stat.php (2) OutputFilter.php, (3) OutputCache.php, (4) Modifier.php, (5) Reader.php, and (6) TemplateCache.php in includes/patTemplate/patTemplate/; (7) includes/Cache/Lite/Output.php; and other unspecified components, which reveal the path in various error messages.
CVE-2007-4186
PHP remote file inclusion vulnerability in admin.tour_toto.php in the Tour de France Pool (com_tour_toto) 1.0.1 module for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.
CVE-2007-4187
Multiple eval injection vulnerabilities in the com_search component in Joomla! 1.5 beta before RC1 (aka Mapya) allow remote attackers to execute arbitrary PHP code via PHP sequences in the searchword parameter, related to default_results.php in (1) components/com_search/views/search/tmpl/ and (2) templates/beez/html/com_search/search/.
CVE-2007-4188
Session fixation vulnerability in Joomla! before 1.0.13 (aka Sunglow) allows remote attackers to hijack administrative web sessions via unspecified vectors.
CVE-2007-4189
Multiple cross-site scripting (XSS) vulnerabilities in Joomla! before 1.0.13 (aka Sunglow) allow remote attackers to inject arbitrary web script or HTML via unspecified vectors in the (1) com_search, (2) com_content, and (3) mod_login components.  NOTE: some of these details are obtained from third party information.
CVE-2007-4190
CRLF injection vulnerability in Joomla! before 1.0.13 (aka Sunglow) allows remote attackers to inject arbitrary HTTP headers and probably conduct HTTP response splitting attacks via CRLF sequences in the url parameter.  NOTE: this can be leveraged for cross-site scripting (XSS) attacks.  NOTE: some of these details are obtained from third party information.
CVE-2007-4191
Panda Antivirus 2008 stores service executables under the product's installation directory with weak permissions, which allows local users to obtain LocalSystem privileges by modifying PAVSRV51.EXE or other unspecified files, a related issue to CVE-2006-4657.
CVE-2007-4192
Multiple cross-site scripting (XSS) vulnerabilities in IDE Group DVD Rental System (DRS) 5.1 before 20070801 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: it is not clear whether IDE Group updates all DRS installations in its role as an application service provider. If so, then this issue should not be included in CVE.
CVE-2007-4193
Multiple cross-site request forgery (CSRF) vulnerabilities in index.php in IDE Group DVD Rental System (DRS) 5.1 before 20070801 allow remote attackers to perform certain actions as arbitrary users, as demonstrated by (1) modifying data or (2) canceling a subscription. NOTE: it is not clear whether IDE Group updates all DRS installations in its role as an application service provider. If so, then this issue should not be included in CVE.
CVE-2007-4194
Guidance Software EnCase 5.0 allows user-assisted remote attackers to cause a denial of service (stack memory consumption) and possibly have other unspecified impact via a malformed file, related to "EnCase's file system parsing." NOTE: this information is based upon a vague pre-advisory. It might overlap CVE-2007-4036.
CVE-2007-4195
Use-after-free vulnerability in ext2fs.c in Brian Carrier The Sleuth Kit (TSK) before 2.09 allows user-assisted remote attackers to cause a denial of service (application crash) and prevent examination of certain ext2fs files via a malformed ext2fs image.
CVE-2007-4196
icat in Brian Carrier The Sleuth Kit (TSK) before 2.09 misinterprets a certain memory location as the holder of a loop iteration count, which allows user-assisted remote attackers to cause a denial of service (long loop) and prevent examination of certain NTFS files via a malformed NTFS image.
CVE-2007-4197
icat in Brian Carrier The Sleuth Kit (TSK) before 2.09 omits NULL pointer checks in certain code paths, which allows user-assisted remote attackers to cause a denial of service (NULL dereference and application crash) and prevent examination of certain NTFS files via a malformed NTFS image.
CVE-2007-4198
The fs_data_put_str function in ntfs.c in fls in Brian Carrier The Sleuth Kit (TSK) before 2.09 does not validate a certain length value, which allows user-assisted remote attackers to cause a denial of service (application crash) and prevent examination of certain NTFS files via a malformed NTFS image, which triggers a buffer over-read.
CVE-2007-4199
Brian Carrier The Sleuth Kit (TSK) before 2.09 allows user-assisted remote attackers to cause a denial of service (application crash) and prevent examination of certain NTFS files via a malformed NTFS image that triggers (1) dereference of a certain integer value by ntfs_dent.c in fls, or (2) dereference of a certain other integer value by ntfs.c in fsstat.
CVE-2007-4200
ntfs.c in fsstat in Brian Carrier The Sleuth Kit (TSK) before 2.09 interprets a certain variable as a byte count rather than a count of 32-bit integers, which allows user-assisted remote attackers to cause a denial of service (application crash) and prevent examination of certain NTFS files via a malformed NTFS image.
CVE-2007-4201
Guidance Software EnCase 6.2 and 6.5 does not properly handle a volume with more than 25 partitions, which might allow remote attackers to prevent examination of certain data, a related issue to CVE-2007-4035.
CVE-2007-4202
Guidance Software EnCase Enterprise Edition (EEE) 6 does not properly verify the identity of the acquisition target during communication with the EnCase Servlet (EEE servlet), which might allow remote attackers to spoof the disk image.
CVE-2007-4203
Session fixation vulnerability in Mambo 4.6.2 CMS allows remote attackers to hijack web sessions by setting the Cookie parameter.
CVE-2007-4204
Hitachi Groupmax Collaboration - Schedule, as used in Groupmax Collaboration Portal 07-32 through 07-32-/B, uCosminexus Collaboration Portal 06-32 through 06-32-/B, and Groupmax Collaboration Web Client - Mail/Schedule 07-32 through 07-32-/A, can assign schedule data to the wrong user under unspecified conditions, which might allow remote authenticated users to obtain sensitive information.
CVE-2007-4205
XHA (Linux-HA) on the BlueCat Networks Adonis DNS/DHCP Appliance 5.0.2.8 allows remote attackers to cause a denial of service (heartbeat control process crash) via a UDP packet to port 694.  NOTE: this may be the same as CVE-2006-3121.
CVE-2007-4206
Kaspersky Anti-Spam 3.0 MP1 before Critical Fix 2 (3.0.278.4) sets incorrect permissions for application files in certain upgrade scenarios, which might allow local users to gain privileges.
CVE-2007-4207
SQL injection vulnerability in admin_console/index.asp in Gallery In A Box allows remote attackers to execute arbitrary SQL commands via the (1) Username or (2) Password field.  NOTE: these fields might be associated with the txtUsername and txtPassword parameters.
CVE-2007-4208
SQL injection vulnerability in default.asp in Next Gen Portfolio Manager allows remote attackers to execute arbitrary SQL commands via the (1) Users_Email or (2) Users_Password parameter in an ExecuteTheLogin action.
CVE-2007-4209
SQL injection vulnerability in Recherche.php in Aceboard forum allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2007-4210
Multiple SQL injection vulnerabilities in module.php in LANAI (la-nai) CMS 1.2.14 allow remote attackers to execute arbitrary SQL commands via (1) the mid parameter in an faqviewgroup action in the FAQ Modules, (2) the cid parameter in the EZSHOPINGCART Modules, or (3) the gid parameter in a view action in the GALLERY Modules.
CVE-2007-4211
The ACL plugin in Dovecot before 1.0.3 allows remote authenticated users with the insert right to save certain flags via a (1) COPY or (2) APPEND command.
CVE-2007-4212
Multiple cross-site scripting (XSS) vulnerabilities in the Search Module in PHP-Nuke allow remote attackers to inject arbitrary web script or HTML via a trailing "<" instead of a ">" in (1) the onerror attribute of an IMG element, (2) the onload attribute of an IFRAME element, or (3) redirect users to other sites via the META tag.
CVE-2007-4224
KDE Konqueror 3.5.7 allows remote attackers to spoof the URL address bar by calling setInterval with a small interval and changing the window.location property.
CVE-2007-4225
Visual truncation vulnerability in KDE Konqueror 3.5.7 allows remote attackers to spoof the URL address bar via an http URI with a large amount of whitespace in the user/password portion.
CVE-2007-4226
Directory traversal vulnerability in the BlueCat Networks Proteus IPAM appliance 2.0.2.0 (Adonis DNS/DHCP appliance 5.0.2.8) allows remote authenticated administrators, with certain TFTP privileges, to create and overwrite arbitrary files via a .. (dot dot) in a pathname.  NOTE: this can be leveraged for administrative access by overwriting /etc/shadow.
CVE-2007-4227
Microsoft Windows Explorer (explorer.exe) allows user-assisted remote attackers to cause a denial of service via a certain JPG file, as demonstrated by something.jpg.  NOTE: this issue might be related to CVE-2007-3958.
CVE-2007-4228
rmpvc on IBM AIX 4.3 allows local users to cause a denial of service (system crash) via long port logical name (-l) argument.
CVE-2007-4229
Unspecified vulnerability in KDE Konqueror 3.5.7 and earlier allows remote attackers to cause a denial of service (failed assertion and application crash) via certain malformed HTML, as demonstrated by a document containing TEXTAREA, BUTTON, BR, BDO, PRE, FRAMESET, and A tags.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-4230
** DISPUTED **  BellaBiblio allows remote attackers to gain administrative privileges via a bellabiblio cookie with the value "administrator." NOTE: this issue is disputed by CVE and multiple third parties because the cookie value must be an MD5 hash.
CVE-2007-4231
PHP remote file inclusion vulnerability in order/login.php in IDevSpot PhpHostBot 1.06 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the svr_rootscript parameter, a different vector than CVE-2007-4094 and CVE-2006-3776.
CVE-2007-4232
PHP remote file inclusion vulnerability in admin/inc/change_action.php in Andreas Robertz PHPNews 0.93 allows remote attackers to execute arbitrary PHP code via a URL in the format_menue parameter.
CVE-2007-4233
Multiple unspecified vulnerabilities in Camera Life before 2.6 allow attackers to cause a denial of service via unknown vectors.
CVE-2007-4234
Unspecified vulnerability in Camera Life before 2.6 allows remote attackers to download private photos via unspecified vectors associated with the names of the photos.  NOTE: some of these details are obtained from third party information.
CVE-2007-4235
Multiple PHP remote file inclusion vulnerabilities in VietPHP allow remote attackers to execute arbitrary PHP code via a URL in (1) the dirpath parameter to (a) _functions.php, or (2) the language parameter to (b) admin/index.php or (c) index.php.
CVE-2007-4236
Buffer overflow in lpd in bos.rte.printers in AIX 5.2 and 5.3 allows local users with printq group privileges to gain root privileges.
CVE-2007-4237
Buffer overflow in the atm subset in arp in devices.common.IBM.atm.rte in AIX 5.2 and 5.3 allows local users to gain root privileges.
CVE-2007-4238
AIX 5.2 and 5.3 install pioinit with user and group ownership of bin, which allows local users with bin or possibly printq privileges to gain root privileges by modifying pioinit.
CVE-2007-4239
Cross-site scripting (XSS) vulnerability in user/forgotPassStep2.jsp in the admin interface in C-SAM oneWallet 210_07062007;1.0 allows remote attackers to inject arbitrary web script or HTML via the loginID parameter.
CVE-2007-4240
The check_logout function in class/auth.php in Help Center Live (hcl) 2.1.3a sends a redirect to the web browser but does not exit when administrative credentials are missing, which allows remote attackers to delete administrative users and have other unspecified impact via certain requests to (1) admin/departments.php, (2) admin/operators.php, and other unspecified scripts.  NOTE: some of these details are obtained from third party information.
CVE-2007-4241
Buffer overflow in ldcconn in Hewlett-Packard (HP) Controller for Cisco Local Director on HP-UX 11.11i allows remote attackers to execute arbitrary code via a long string to TCP port 17781.
CVE-2007-4242
The pop3 Proxy in Astaro Security Gateway (ASG) 7 does not perform virus scanning of attachments that exceed the maximum attachment size, and passes these attachments, which allows remote attackers to bypass this scanning via a large attachment.
CVE-2007-4243
Unspecified vulnerability in pfilter-reporter.pl in Astaro Security Gateway (ASG) 7 allows remote attackers to cause a denial of service (CPU consumption) via certain network traffic, as demonstrated by P2P and iTunes applications that download large amounts of data.
CVE-2007-4244
PHP remote file inclusion vulnerability in langset.php in J! Reactions (com_jreactions) 1.8.1 and earlier, a Joomla! component, allows remote attackers to execute arbitrary PHP code via a URL in the comPath parameter.
CVE-2007-4245
Cross-site scripting (XSS) vulnerability in Search.php in DiMeMa CONTENTdm (CDM) allows remote attackers to inject arbitrary web script or HTML via a search, probably related to the CISOBOX1 parameter to results.php in CDM 4.2.
CVE-2007-4246
Unspecified vulnerability, possibly a buffer overflow, in Justsystem Ichitaro 2007 and earlier allows remote attackers to execute arbitrary code via a modified document, as actively exploited in August 2007 by malware such as Tarodrop.D (Tarodrop.Q), a different vulnerability than CVE-2006-4326, CVE-2006-5424, CVE-2006-6400, and CVE-2007-1938.
CVE-2007-4247
Windows Calendar on Microsoft Windows Vista allows remote attackers to cause a denial of service (NULL dereference and persistent application crash) via a malformed ICS file.
CVE-2007-4248
The CallCmd function in toolbar_gaming.dll in the Toolbar Gaming toolbar for Internet Explorer allows remote attackers to cause a denial of service (NULL dereference and browser crash) via unspecified vectors.
CVE-2007-4249
The isChecked function in Toolbar.DLL in the ExportNation toolbar for Internet Explorer allows remote attackers to cause a denial of service (NULL dereference and browser crash) via unspecified vectors.
CVE-2007-4250
The isChecked function in Toolbar.DLL in Advanced Searchbar before 3.33 allows remote attackers to cause a denial of service (NULL dereference and browser crash) via unspecified vectors.
CVE-2007-4251
OpenOffice.org (OOo) 2.2 does not properly handle files with multiple extensions, which allows user-assisted remote attackers to cause a denial of service.
CVE-2007-4252
Absolute path traversal vulnerability in a certain ActiveX control in CkString.dll 1.1 and earlier in CHILKAT ASP String allows remote attackers to create or overwrite arbitrary files via a full pathname in the first argument to the SaveToFile method, a different vulnerability than CVE-2007-3633.
More detailed information listed here: http://www.securityfocus.com/bid/25205/info
CVE-2007-4253
SQL injection vulnerability in the News module in modules.php in Envolution 1.1.0 and earlier allows remote attackers to execute arbitrary SQL commands via the topic parameter, a different vector than CVE-2005-4263.
CVE-2007-4254
Stack-based buffer overflow in a certain ActiveX control in VDT70.DLL in Microsoft Visual Database Tools Database Designer 7.0 for Microsoft Visual Studio 6 allows remote attackers to execute arbitrary code via a long argument to the NotSafe method.  NOTE: this may overlap CVE-2007-2885 or CVE-2005-2127.
CVE-2007-4255
Buffer overflow in the mSQL extension in PHP 5.2.3 allows context-dependent attackers to execute arbitrary code via a long first argument to the msql_connect function.
CVE-2007-4256
Directory traversal vulnerability in showpage.cgi in YNP Portal System 2.2.0 allows remote attackers to read arbitrary files via a .. (dot dot) in the p parameter.
CVE-2007-4257
Multiple buffer overflows in Live for Speed (LFS) S1 and S2 allow user-assisted remote attackers to execute arbitrary code via (1) a .spr file (single player replay file) containing a long user name or (2) a .ply file containing a long number plate string, different vectors than CVE-2007-4140.
CVE-2007-4258
SQL injection vulnerability in directory.php in Prozilla Pub Site Directory allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2007-4259
EZPhotoSales 1.9.3 and earlier allows remote attackers to download arbitrary image files via (1) a direct request for a URL under OnlineViewing/galleries/ or (2) navigation of the gallery user interface with JavaScript disabled.
CVE-2007-4260
EZPhotoSales 1.9.3 and earlier has a default "admin" account for galleries, which allows remote attackers to access arbitrary galleries by specifying this username.
CVE-2007-4261
EZPhotoSales 1.9.3 and earlier stores sensitive information under the web root with insufficient access control, which allows remote attackers to download (1) a file containing cleartext passwords via a direct request for OnlineViewing/data/galleries.txt, or (2) a file containing username hashes and password hashes via a direct request for OnlineViewing/configuration/config.dat/.  NOTE: vector 2 can be leveraged for administrative access because authentication does not require knowledge of cleartext values, but instead uses the username hash in the ConfigLogin parameter and the password hash in the ConfigPassword parameter.
CVE-2007-4262
Unrestricted file upload vulnerability in EZPhotoSales 1.9.3 and earlier allows remote authenticated administrators to upload and execute arbitrary PHP code under OnlineViewing/galleries/.
CVE-2007-4263
Unspecified vulnerability in the server side of the Secure Copy (SCP) implementation in Cisco 12.2-based IOS allows remote authenticated users to read, write or overwrite any file on the device's filesystem via unknown vectors.
