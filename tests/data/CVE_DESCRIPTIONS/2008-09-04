CVE-2007-6716
fs/direct-io.c in the dio subsystem in the Linux kernel before 2.6.23 does not properly zero out the dio struct, which allows local users to cause a denial of service (OOPS), as demonstrated by a certain fio test.
CVE-2008-1389
libclamav/chmunpack.c in the chm-parser in ClamAV before 0.94 allows remote attackers to cause a denial of service (application crash) via a malformed CHM file, related to an "invalid memory access."
CVE-2008-2441
Cisco Secure ACS 3.x before 3.3(4) Build 12 patch 7, 4.0.x, 4.1.x before 4.1(4) Build 13 Patch 11, and 4.2.x before 4.2(0) Build 124 Patch 4 does not properly handle an EAP Response packet in which the value of the length field exceeds the actual packet length, which allows remote authenticated users to cause a denial of service (CSRadius and CSAuth service crash) or possibly execute arbitrary code via a crafted RADIUS (1) EAP-Response/Identity, (2) EAP-Response/MD5, or (3) EAP-Response/TLS Message Attribute packet.
CVE-2008-2732
Multiple unspecified vulnerabilities in the SIP inspection functionality in Cisco PIX and Adaptive Security Appliance (ASA) 5500 devices 7.0 before 7.0(7)16, 7.1 before 7.1(2)71, 7.2 before 7.2(4)7, 8.0 before 8.0(3)20, and 8.1 before 8.1(1)8 allow remote attackers to cause a denial of service (device reload) via unknown vectors, aka Bug IDs CSCsq07867, CSCsq57091, CSCsk60581, and CSCsq39315.
CVE-2008-2733
Cisco PIX and Adaptive Security Appliance (ASA) 5500 devices 7.2 before 7.2(4)2, 8.0 before 8.0(3)14, and 8.1 before 8.1(1)4, when configured as a client VPN endpoint, do not properly process IPSec client authentication, which allows remote attackers to cause a denial of service (device reload) via a crafted authentication attempt, aka Bug ID CSCso69942.
CVE-2008-2734
Memory leak in the crypto functionality in Cisco Adaptive Security Appliance (ASA) 5500 devices 7.2 before 7.2(4)2, 8.0 before 8.0(3)14, and 8.1 before 8.1(1)4, when configured as a clientless SSL VPN endpoint, allows remote attackers to cause a denial of service (memory consumption and VPN hang) via a crafted SSL or HTTP packet, aka Bug ID CSCso66472.
CVE-2008-2735
The HTTP server in Cisco Adaptive Security Appliance (ASA) 5500 devices 8.0 before 8.0(3)15 and 8.1 before 8.1(1)5, when configured as a clientless SSL VPN endpoint, does not properly process URIs, which allows remote attackers to cause a denial of service (device reload) via a URI in a crafted SSL or HTTP packet, aka Bug ID CSCsq19369.
CVE-2008-2736
Unspecified vulnerability in Cisco Adaptive Security Appliance (ASA) 5500 devices 8.0(3)15, 8.0(3)16, 8.1(1)4, and 8.1(1)5, when configured as a clientless SSL VPN endpoint, allows remote attackers to obtain usernames and passwords via unknown vectors, aka Bug ID CSCsq45636.
CVE-2008-3903
Asterisk Open Source 1.2.x before 1.2.32, 1.4.x before 1.4.24.1, and 1.6.0.x before 1.6.0.8; Asterisk Business Edition A.x.x, B.x.x before B.2.5.8, C.1.x.x before C.1.10.5, and C.2.x.x before C.2.3.3; s800i 1.3.x before 1.3.0.2; and Trixbox PBX 2.6.1, when Digest authentication and authalwaysreject are enabled, generates different responses depending on whether a SIP username is valid, which allows remote attackers to enumerate valid usernames.
Additional details can be found here: http://www.voipsa.org/pipermail/voipsec_voipsa.org/2006-May/001628.html
CVE-2008-3904
src/main-win.c in GPicView 0.1.9 in Lightweight X11 Desktop Environment (LXDE) allows context-dependent attackers to execute arbitrary commands via shell metacharacters in a filename.
CVE-2008-3905
resolv.rb in Ruby 1.8.5 and earlier, 1.8.6 before 1.8.6-p287, 1.8.7 before 1.8.7-p72, and 1.9 r18423 and earlier uses sequential transaction IDs and constant source ports for DNS requests, which makes it easier for remote attackers to spoof DNS responses, a different vulnerability than CVE-2008-1447.
CVE-2008-3906
CRLF injection vulnerability in Sys.Web in Mono 2.0 and earlier allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in the query string.
CVE-2008-3907
The open-in-browser command in newsbeuter before 1.1 allows remote attackers to execute arbitrary commands via shell metacharacters in a feed URL.
http://www.openwall.com/lists/oss-security/2008/09/01/4

"The previous version allowed to execute arbitrary code by a 
crafted feed URL that is passed as a command line parameter 
if the URL is opened by an external browser."
CVE-2008-3908
Multiple buffer overflows in Princeton WordNet (wn) 3.0 allow context-dependent attackers to execute arbitrary code via (1) a long argument on the command line; a long (2) WNSEARCHDIR, (3) WNHOME, or (4) WNDBVERSION environment variable; or (5) a user-supplied dictionary (aka data file).  NOTE: since WordNet itself does not run with special privileges, this issue only crosses privilege boundaries when WordNet is invoked as a third party component.
CVE-2008-3909
The administration application in Django 0.91, 0.95, and 0.96 stores unauthenticated HTTP POST requests and processes them after successful authentication occurs, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks and delete or modify data via unspecified requests.
CVE-2008-3910
dns2tcp before 0.4.1 does not properly handle negative values in a certain length field in the input argument to the (1) dns_simple_decode or (2) dns_decode function, which allows remote attackers to overwrite a buffer and have unspecified other impact.
CVE-2008-3911
The proc_do_xprt function in net/sunrpc/sysctl.c in the Linux kernel 2.6.26.3 does not check the length of a certain buffer obtained from userspace, which allows local users to overflow a stack-based buffer and have unspecified other impact via a crafted read system call for the /proc/sys/sunrpc/transports file.
CVE-2008-3916
Heap-based buffer overflow in the strip_escapes function in signal.c in GNU ed before 1.0 allows context-dependent or user-assisted attackers to execute arbitrary code via a long filename.  NOTE: since ed itself does not typically run with special privileges, this issue only crosses privilege boundaries when ed is invoked as a third-party component.
http://xforce.iss.net/xforce/xfdb/44643

"GNU ed is vulnerable to a heap-based buffer overflow, caused by improper bounds checking by the strip_escapes() function. By persuading a victim to open a specially-crafted file, a remote attacker could overflow a buffer and execute arbitrary code on the system."
CVE-2008-3917
Cross-site scripting (XSS) vulnerability in index.php in Ovidentia 6.6.5 allows remote attackers to inject arbitrary web script or HTML via the field parameter in a search action.
CVE-2008-3918
SQL injection vulnerability in index.php in Ovidentia 6.6.5 allows remote attackers to execute arbitrary SQL commands via the field parameter in a search action.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3919
Unspecified vulnerability in multiple JustSystems Ichitaro products allows remote attackers to execute arbitrary code via a crafted JTD document, as exploited in the wild in August 2008.
CVE-2008-3920
Unspecified vulnerability in BitlBee before 1.2.2 allows remote attackers to "recreate" and "hijack" existing accounts via unspecified vectors.
CVE-2008-3921
Multiple cross-site scripting (XSS) vulnerabilities in AWStats Totals 1.0 through 1.14 allow remote attackers to inject arbitrary web script or HTML via the (1) month and (2) year parameter.
CVE-2008-3922
awstatstotals.php in AWStats Totals 1.0 through 1.14 allows remote attackers to execute arbitrary code via PHP sequences in the sort parameter, which is used by the multisort function when dynamically creating an anonymous PHP function.
CVE-2008-3923
Multiple cross-site scripting (XSS) vulnerabilities in statistics.php in Content Management Made Easy (CMME) 1.12 allow remote attackers to inject arbitrary web script or HTML via the (1) page and (2) year parameters in an hstat_year action.
CVE-2008-3924
The "Make a backup" functionality in Content Management Made Easy (CMME) 1.12 stores sensitive information under the web root with insufficient access control, which allows remote attackers to discover (1) account names and (2) password hashes via a direct request for (a) backup/cmme_data.zip or (b) backup/cmme_cmme.zip.  NOTE: it was later reported that vector a also affects CMME 1.19.
CVE-2008-3925
Cross-site request forgery (CSRF) vulnerability in admin.php in Content Management Made Easy (CMME) 1.12 allows remote attackers to trigger the logout of an administrative user via a logout action.
CVE-2008-3926
Multiple directory traversal vulnerabilities in Content Management Made Easy (CMME) 1.12 allow remote attackers to (1) read arbitrary files via a .. (dot dot) in the env parameter in a weblog action to index.php, or (2) create arbitrary directories via a .. (dot dot) in the env parameter in a login action to admin.php.
CVE-2008-3927
genmsgidx in Tiger 3.2.2 allows local users to overwrite or delete arbitrary files via a symlink attack on temporary files.
CVE-2008-3928
test.sh in Honeyd 1.5c might allow local users to overwrite arbitrary files via a symlink attack on a temporary file.
CVE-2008-3929
gather-messages.sh in Ampache 3.4.1 allows local users to overwrite arbitrary files via a symlink attack on the /tmp/filelist temporary file.
CVE-2008-3930
migrate_aliases.sh in Citadel Server 7.37 allows local users to overwrite arbitrary files via a symlink attack on a temporary file.
CVE-2008-3931
javareconf in R 2.7.2 allows local users to overwrite arbitrary files via a symlink attack on temporary files.
CVE-2008-3932
Wireshark (formerly Ethereal) 0.9.7 through 1.0.2 allows attackers to cause a denial of service (hang) via a crafted NCP packet that triggers an infinite loop.
CVE-2008-3933
Wireshark (formerly Ethereal) 0.10.14 through 1.0.2 allows attackers to cause a denial of service (crash) via a packet with crafted zlib-compressed data that triggers an invalid read in the tvb_uncompress function.
CVE-2008-3934
Unspecified vulnerability in Wireshark (formerly Ethereal) 0.99.6 through 1.0.2 allows attackers to cause a denial of service (crash) via a crafted Tektronix .rf5 file.
