CVE-2013-0500
IBM Storwize V7000 Unified 1.3.x and 1.4.x before 1.4.2.0 does not properly handle device files that are created with the NFS protocol but accessed with a non-NFS protocol, which allows remote authenticated users to obtain sensitive information, modify programs or files, or cause a denial of service (device crash) via a (1) CIFS, (2) HTTPS, (3) SCP, or (4) SFTP operation.
CVE-2013-2190
The translate_hierarchy_event function in x11/clutter-device-manager-xi2.c in Clutter, when resuming the system, does not properly handle XIQueryDevice errors when a device has "disappeared," which causes the gnome-shell to crash and allows physically proximate attackers to access the previous gnome-shell session via unspecified vectors.
CVE-2013-2254
The deepGetOrCreateNode function in impl/operations/AbstractCreateOperation.java in org.apache.sling.servlets.post.bundle 2.2.0 and 2.3.0 in Apache Sling does not properly handle a NULL value that returned when the session does not have permissions to the root node, which allows remote attackers to cause a denial of service (infinite loop) via unspecified vectors.
CVE-2013-3025
Multiple cross-site scripting (XSS) vulnerabilities in IBM Rational Focal Point 6.5.x and 6.6.x before 6.6.0.1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-4287
Algorithmic complexity vulnerability in Gem::Version::VERSION_PATTERN in lib/rubygems/version.rb in RubyGems before 1.8.23.1, 1.8.24 through 1.8.25, 2.0.x before 2.0.8, and 2.1.x before 2.1.0, as used in Ruby 1.9.0 through 2.0.0p247, allows remote attackers to cause a denial of service (CPU consumption) via a crafted gem version that triggers a large amount of backtracking in a regular expression.
CVE-2013-4363
Algorithmic complexity vulnerability in Gem::Version::ANCHORED_VERSION_PATTERN in lib/rubygems/version.rb in RubyGems before 1.8.23.2, 1.8.24 through 1.8.26, 2.0.x before 2.0.10, and 2.1.x before 2.1.5, as used in Ruby 1.9.0 through 2.0.0p247, allows remote attackers to cause a denial of service (CPU consumption) via a crafted gem version that triggers a large amount of backtracking in a regular expression.  NOTE: this issue is due to an incomplete fix for CVE-2013-4287.
CVE-2013-4365
Heap-based buffer overflow in the fcgid_header_bucket_read function in fcgid_bucket.c in the mod_fcgid module before 2.3.9 for the Apache HTTP Server allows remote attackers to have an unspecified impact via unknown vectors.
CVE-2013-4368
The outs instruction emulation in Xen 3.1.x, 4.2.x, 4.3.x, and earlier, when using FS: or GS: segment override, uses an uninitialized variable as a segment base, which allows local 64-bit PV guests to obtain sensitive information (hypervisor stack content) via unspecified vectors related to stale data in a segment register.
CVE-2013-4369
The xlu_vif_parse_rate function in the libxlu library in Xen 4.2.x and 4.3.x allows local users to cause a denial of service (NULL pointer dereference) by using the "@" character as the VIF rate configuration.
CWE-476: NULL Pointer Dereference

Per http://cwe.mitre.org/data/definitions/476.html
CVE-2013-4370
The ocaml binding for the xc_vcpu_getaffinity function in Xen 4.2.x and 4.3.x frees certain memory that may still be intended for use, which allows local users to cause a denial of service (heap corruption and crash) and possibly execute arbitrary code via unspecified vectors that trigger a (1) use-after-free or (2) double free.
CVE-2013-4371
Use-after-free vulnerability in the libxl_list_cpupool function in the libxl toolstack library in Xen 4.2.x and 4.3.x, when running "under memory pressure," returns the original pointer when the realloc function fails, which allows local users to cause a denial of service (heap corruption and crash) and possibly execute arbitrary code via unspecified vectors.
CVE-2013-4389
Multiple format string vulnerabilities in log_subscriber.rb files in the log subscriber component in Action Mailer in Ruby on Rails 3.x before 3.2.15 allow remote attackers to cause a denial of service via a crafted e-mail address that is improperly handled during construction of a log message.
CVE-2013-4397
Multiple integer overflows in the th_read function in lib/block.c in libtar before 1.2.20 allow remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long (1) name or (2) link in an archive, which triggers a heap-based buffer overflow.
CVE-2013-4689
J-Web in Juniper Junos before 10.4R13, 11.4 before 11.4R7, 12.1R before 12.1R6, 12.1X44 before 12.1X44-D15, 12.1x45 before 12.1X45-D10, 12.2 before 12.2R3, 12.3 before 12.3R2, and 13.1 before 13.1R3 allow remote attackers to bypass the cross-site request forgery (CSRF) protection mechanism and hijack the authentication of administrators for requests that (1) create new administrator accounts or (2) have other unspecified impacts.
CVE-2013-5376
Cross-site scripting (XSS) vulnerability in IBM Storwize V7000 Unified 1.3.x and 1.4.x before 1.4.2.0 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors, related to a "cross frame scripting" attack against an administrative user.
CVE-2013-6013
Buffer overflow in the flow daemon (flowd) in Juniper Junos 10.4 before 10.4S14, 11.4 before 11.4R7-S2, 12.1.X44 before 12.1X44-D15, 12.1X45 before 12.1X45-D10 on SRX devices, when using telnet pass-through authentication on the firewall, might allow remote attackers to execute arbitrary code via a crafted telnet message.
CVE-2013-6015
Juniper Junos before 10.4S14, 11.4 before 11.4R5-S2, 12.1R before 12.1R3, 12.1X44 before 12.1X44-D20, and 12.1X45 before 12.1X45-D15 on SRX Series services gateways, when a plugin using TCP proxy is configured, allows remote attackers to cause a denial of service (flow daemon crash) via an unspecified sequence of TCP packets.
CVE-2013-6169
The TLS driver in ejabberd before 2.1.12 supports (1) SSLv2 and (2) weak SSL ciphers, which makes it easier for remote attackers to obtain sensitive information via a brute-force attack.
CVE-2013-6170
Juniper Junos 10.0 before 10.0S28, 10.4 before 10.4R7, 11.1 before 11.1R5, 11.2 before 11.2R2, and 11.4 before 11.4R1, when in a Next-Generation Multicast VPN (NGEN MVPN) environment, allows remote attackers to cause a denial of service (RPD routing daemon crash) via a large number of crafted PIM (S,G) join requests.
