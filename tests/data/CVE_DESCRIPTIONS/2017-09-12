CVE-2014-9624
CAPTCHA bypass vulnerability in MantisBT before 1.2.19.
CVE-2014-9634
Jenkins before 1.586 does not set the secure flag on session cookies when run on Tomcat 7.0.41 or later, which makes it easier for remote attackers to capture cookies by intercepting their transmission within an HTTP session.
CVE-2014-9635
Jenkins before 1.586 does not set the HttpOnly flag in a Set-Cookie header for session cookies when run on Tomcat 7.0.41 or later, which makes it easier for remote attackers to obtain potentially sensitive information via script access to cookies.
CVE-2015-9228
In post-new.php in the Photocrati NextGEN Gallery plugin 2.1.10 for WordPress, unrestricted file upload is available via the name parameter, if a file extension is changed from .jpg to .php.
CVE-2015-9229
In the nggallery-manage-gallery page in the Photocrati NextGEN Gallery plugin 2.1.15 for WordPress, XSS is possible for remote authenticated administrators via the images[1][alttext] parameter.
CVE-2015-9230
In the admin/db-backup-security/db-backup-security.php page in the BulletProof Security plugin before .52.5 for WordPress, XSS is possible for remote authenticated administrators via the DBTablePrefix parameter.
CVE-2017-1000250
All versions of the SDP server in BlueZ 5.46 and earlier are vulnerable to an information disclosure vulnerability which allows remote attackers to obtain sensitive information from the bluetoothd process memory. This vulnerability lies in the processing of SDP search attribute requests.
CVE-2017-1000251
The native Bluetooth stack in the Linux Kernel (BlueZ), starting at the Linux kernel version 2.6.32 and up to and including 4.13.1, are vulnerable to a stack overflow vulnerability in the processing of L2CAP configuration responses resulting in Remote code execution in kernel space.
CVE-2017-1162
IBM QRadar 7.2 and 7.3 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 122957.
CVE-2017-1352
IBM Maximo Asset Management 7.5 and 7.6 could allow an authenticated user to inject commands into work orders that could be executed by another user that downloads the affected file. IBM X-Force ID: 126538.
CVE-2017-14266
tcprewrite in Tcpreplay 3.4.4 has a Heap-Based Buffer Overflow vulnerability triggered by a crafted PCAP file, a related issue to CVE-2016-6160.
CVE-2017-14313
The shibboleth_login_form function in shibboleth.php in the Shibboleth plugin before 1.8 for WordPress is prone to an XSS vulnerability due to improper use of add_query_arg().
CVE-2017-14314
Off-by-one error in the DrawImage function in magick/render.c in GraphicsMagick 1.3.26 allows remote attackers to cause a denial of service (DrawDashPolygon heap-based buffer over-read and application crash) via a crafted file.
CVE-2017-14315
In Apple iOS 7 through 9, due to a BlueBorne flaw in the implementation of LEAP (Low Energy Audio Protocol), a large audio command can be sent to a targeted device and lead to a heap overflow with attacker-controlled data. Since the audio commands sent via LEAP are not properly validated, an attacker can use this overflow to gain full control of the device through the relatively high privileges of the Bluetooth stack in iOS. The attack bypasses Bluetooth access control; however, the default "Bluetooth On" value must be present in Settings.
CVE-2017-14316
A parameter verification issue was discovered in Xen through 4.9.x. The function `alloc_heap_pages` allows callers to specify the first NUMA node that should be used for allocations through the `memflags` parameter; the node is extracted using the `MEMF_get_node` macro. While the function checks to see if the special constant `NUMA_NO_NODE` is specified, it otherwise does not handle the case where `node >= MAX_NUMNODES`. This allows an out-of-bounds access to an internal array.
CVE-2017-14317
A domain cleanup issue was discovered in the C xenstore daemon (aka cxenstored) in Xen through 4.9.x. When shutting down a VM with a stubdomain, a race in cxenstored may cause a double-free. The xenstored daemon may crash, resulting in a DoS of any parts of the system relying on it (including domain creation / destruction, ballooning, device changes, etc.).
CVE-2017-14318
An issue was discovered in Xen 4.5.x through 4.9.x. The function `__gnttab_cache_flush` handles GNTTABOP_cache_flush grant table operations. It checks to see if the calling domain is the owner of the page that is to be operated on. If it is not, the owner's grant table is checked to see if a grant mapping to the calling domain exists for the page in question. However, the function does not check to see if the owning domain actually has a grant table or not. Some special domains, such as `DOMID_XEN`, `DOMID_IO` and `DOMID_COW` are created without grant tables. Hence, if __gnttab_cache_flush operates on a page owned by these special domains, it will attempt to dereference a NULL pointer in the domain struct.
CVE-2017-14319
A grant unmapping issue was discovered in Xen through 4.9.x. When removing or replacing a grant mapping, the x86 PV specific path needs to make sure page table entries remain in sync with other accounting done. Although the identity of the page frame was validated correctly, neither the presence of the mapping nor page writability were taken into account.
CVE-2017-14324
In ImageMagick 7.0.7-1 Q16, a memory leak vulnerability was found in the function ReadMPCImage in coders/mpc.c, which allows attackers to cause a denial of service via a crafted file.
CVE-2017-14325
In ImageMagick 7.0.7-1 Q16, a memory leak vulnerability was found in the function PersistPixelCache in magick/cache.c, which allows attackers to cause a denial of service (memory consumption in ReadMPCImage in coders/mpc.c) via a crafted file.
CVE-2017-14326
In ImageMagick 7.0.7-1 Q16, a memory leak vulnerability was found in the function ReadMATImage in coders/mat.c, which allows attackers to cause a denial of service via a crafted file.
CVE-2017-14333
The process_version_sections function in readelf.c in GNU Binutils 2.29 allows attackers to cause a denial of service (Integer Overflow, and hang because of a time-consuming loop) or possibly have unspecified other impact via a crafted binary file with invalid values of ent.vn_next, during "readelf -a" execution.
CVE-2017-14335
On Beijing Hanbang Hanbanggaoke devices, because user-controlled input is not sufficiently sanitized, sending a PUT request to /ISAPI/Security/users/1 allows an admin password change.
CVE-2017-14337
When MISP before 2.4.80 is configured with X.509 certificate authentication (CertAuth) in conjunction with a non-MISP external user management ReST API, if an external user provides X.509 certificate authentication and this API returns an empty value, the unauthenticated user can be granted access as an arbitrary user.
CVE-2017-1434
IBM DB2 for Linux, UNIX and Windows 11.1 (includes DB2 Connect Server) under unusual circumstances, could expose highly sensitive information in the error log to a local user.
CVE-2017-14341
ImageMagick 7.0.6-6 has a large loop vulnerability in ReadWPGImage in coders/wpg.c, causing CPU exhaustion via a crafted wpg image file.
CVE-2017-14342
ImageMagick 7.0.6-6 has a memory exhaustion vulnerability in ReadWPGImage in coders/wpg.c via a crafted wpg image file.
CVE-2017-14343
ImageMagick 7.0.6-6 has a memory leak vulnerability in ReadXCFImage in coders/xcf.c via a crafted xcf image file.
CVE-2017-14344
This vulnerability allows local attackers to escalate privileges on Jungo WinDriver 12.4.0 and earlier. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability. The specific flaw exists within the processing of IOCTL 0x95382673 by the windrvr1240 kernel driver. The issue lies in the failure to properly validate user-supplied data which can result in a kernel pool overflow. An attacker can leverage this vulnerability to execute arbitrary code under the context of kernel.
CVE-2017-14345
SQL Injection exists in tianchoy/blog through 2017-09-12 via the id parameter to view.php.
CVE-2017-14346
upload.php in tianchoy/blog through 2017-09-12 allows unrestricted file upload and PHP code execution by using the image/jpeg, image/pjpeg, image/png, or image/gif content type for a .php file.
CVE-2017-14347
NexusPHP 1.5.beta5.20120707 has XSS in the returnto parameter to fun.php in a delete action.
CVE-2017-14348
LibRaw before 0.18.4 has a heap-based Buffer Overflow in the processCanonCameraInfo function via a crafted file.
CVE-2017-1438
IBM DB2 for Linux, UNIX and Windows 9.7, 10.1, 10.5, and 11.1 (includes DB2 Connect Server) could allow a local user with DB2 instance owner privileges to obtain root access. IBM X-Force ID: 128057.
CVE-2017-1439
IBM DB2 for Linux, UNIX and Windows 9.7, 10,1, 10.5, and 11.1 (includes DB2 Connect Server) could allow a local user with DB2 instance owner privileges to obtain root access. IBM X-Force ID: 128058.
CVE-2017-14396
In osTicket before 1.10.1, SQL injection is possible by constructing an array via use of square brackets at the end of a parameter name, as demonstrated by the key parameter to file.php.
CVE-2017-14397
AnyDesk before 3.6.1 on Windows has a DLL injection vulnerability.
CVE-2017-14399
In BlackCat CMS 1.2.2, unrestricted file upload is possible in backend\media\ajax_rename.php via the extension parameter, as demonstrated by changing the extension from .jpg to .php.
CVE-2017-14400
In ImageMagick 7.0.7-1 Q16, the PersistPixelCache function in magick/cache.c mishandles the pixel cache nexus, which allows remote attackers to cause a denial of service (NULL pointer dereference in the function GetVirtualPixels in MagickCore/cache.c) via a crafted file.
CVE-2017-1451
IBM DB2 for Linux, UNIX and Windows 9.7, 10,1, 10.5, and 11.1 (includes DB2 Connect Server) could allow a local user with DB2 instance owner privileges to obtain root access. IBM X-Force ID: 128178.
CVE-2017-1452
IBM DB2 for Linux, UNIX and Windows 9.7, 10,1, 10.5, and 11.1 (includes DB2 Connect Server) could allow a local user to obtain elevated privilege and overwrite DB2 files. IBM X-Force ID: 128180.
CVE-2017-1519
IBM DB2 10.5 and 11.1 contains a denial of service vulnerability. A remote user can cause disruption of service for DB2 Connect Server setup with a particular configuration. IBM X-Force ID: 129829.
CVE-2017-1520
IBM DB2 9.7, 10,1, 10.5, and 11.1 is vulnerable to an unauthorized command that allows the database to be activated when authentication type is CLIENT. IBM X-Force ID: 129830.
CVE-2017-3131
A Cross-Site Scripting vulnerability in Fortinet FortiOS versions 5.4.0 through 5.4.4 and 5.6.0 allows attackers to execute unauthorized code or commands via the filter input in "Applications" under FortiView.
CVE-2017-3132
A Cross-Site Scripting vulnerability in Fortinet FortiOS versions 5.6.0 and earlier allows attackers to Execute unauthorized code or commands via the action input during the activation of a FortiToken.
CVE-2017-3133
A Cross-Site Scripting vulnerability in Fortinet FortiOS versions 5.6.0 and earlier allows attackers to execute unauthorized code or commands via the Replacement Message HTML for SSL-VPN.
CVE-2017-7734
A Cross-Site Scripting vulnerability in Fortinet FortiOS versions 5.4.0 through 5.4.4 allows attackers to execute unauthorized code or commands via 'Comments' while saving Config Revisions.
CVE-2017-7735
A Cross-Site Scripting vulnerability in Fortinet FortiOS versions 5.2.0 through 5.2.11 and 5.4.0 through 5.4.4 allows attackers to execute unauthorized code or commands via the "Groups" input while creating or editing User Groups.
CVE-2017-8015
EMC AppSync (all versions prior to 3.5) contains a SQL injection vulnerability that could potentially be exploited by malicious users to compromise the affected system.
CVE-2017-8918
XXE in Dive Assistant - Template Builder in Blackwave Dive Assistant - Desktop Edition 8.0 allows attackers to remotely view local files via a crafted template.xml file.
