CVE-2011-2485
The gdk_pixbuf__gif_image_load function in gdk-pixbuf/io-gif.c in gdk-pixbuf before 2.23.5 does not properly handle certain return values, which allows remote attackers to cause a denial of service (memory consumption) via a crafted GIF image file.
CVE-2011-2716
The DHCP client (udhcpc) in BusyBox before 1.20.0 allows remote DHCP servers to execute arbitrary commands via shell metacharacters in the (1) HOST_NAME, (2) DOMAIN_NAME, (3) NIS_DOMAIN, and (4) TFTP_SERVER_NAME host name options.
CVE-2011-4028
The LockServer function in os/utils.c in X.Org xserver before 1.11.2 allows local users to determine the existence of arbitrary files via a symlink attack on a temporary lock file, which is handled differently if the file exists.
CVE-2011-4029
The LockServer function in os/utils.c in X.Org xserver before 1.11.2 allows local users to change the permissions of arbitrary files to 444, read those files, and possibly cause a denial of service (removed execution permission) via a symlink attack on a temporary lock file.
CVE-2011-4086
The journal_unmap_buffer function in fs/jbd2/transaction.c in the Linux kernel before 3.3.1 does not properly handle the _Delay and _Unwritten buffer head states, which allows local users to cause a denial of service (system crash) by leveraging the presence of an ext4 filesystem that was mounted with a journal.
CVE-2011-4127
The Linux kernel before 3.2.2 does not properly restrict SG_IO ioctl calls, which allows local users to bypass intended restrictions on disk read and write operations by sending a SCSI command to (1) a partition block device or (2) an LVM volume.
CVE-2011-5096
Stack-based buffer overflow in cstore.exe in the Media Application Server (MAS) in Avaya Aura Application Server 5300 (formerly Nortel Media Application Server) 1.x before 1.0.2 and 2.0 before Patch Bundle 10 allows remote attackers to execute arbitrary code via a crafted cs_anams parameter in a CONTENT_STORE_ADMIN_REQ packet.
CVE-2012-0045
The em_syscall function in arch/x86/kvm/emulate.c in the KVM implementation in the Linux kernel before 3.2.14 does not properly handle the 0f05 (aka syscall) opcode, which allows guest OS users to cause a denial of service (guest OS crash) via a crafted application, as demonstrated by an NASM file.
CVE-2012-0833
The acllas__handle_group_entry function in servers/plugins/acl/acllas.c in 389 Directory Server before 1.2.10 does not properly handled access control instructions (ACIs) that use certificate groups, which allows remote authenticated LDAP users with a certificate group to cause a denial of service (infinite loop and CPU consumption) by binding to the server.
CVE-2012-0876
The XML parser (xmlparse.c) in expat before 2.1.0 computes hash values without restricting the ability to trigger hash collisions predictably, which allows context-dependent attackers to cause a denial of service (CPU consumption) via an XML file with many identifiers with the same value.
CVE-2012-1106
The C handler plug-in in Automatic Bug Reporting Tool (ABRT), possibly 2.0.8 and earlier, does not properly set the group (GID) permissions on core dump files for setuid programs when the sysctl fs.suid_dumpable option is set to 2, which allows local users to obtain sensitive information.
CVE-2012-1147
readfilemap.c in expat before 2.1.0 allows context-dependent attackers to cause a denial of service (file descriptor consumption) via a large number of crafted XML files.
CVE-2012-1148
Memory leak in the poolGrow function in expat/lib/xmlparse.c in expat before 2.1.0 allows context-dependent attackers to cause a denial of service (memory consumption) via a large number of crafted XML files that cause improperly-handled reallocation failures when expanding entities.
CVE-2012-2100
The ext4_fill_flex_info function in fs/ext4/super.c in the Linux kernel before 3.2.2, on the x86 platform and unspecified other platforms, allows user-assisted remote attackers to trigger inconsistent filesystem-groups data and possibly cause a denial of service via a malformed ext4 filesystem containing a super block with a large FLEX_BG group size (aka s_log_groups_per_flex value).  NOTE: this vulnerability exists because of an incomplete fix for CVE-2009-4307.
CVE-2012-2133
Use-after-free vulnerability in the Linux kernel before 3.3.6, when huge pages are enabled, allows local users to cause a denial of service (system crash) or possibly gain privileges by interacting with a hugetlbfs filesystem, as demonstrated by a umount operation that triggers improper handling of quota data.
CVE-2012-2181
Directory traversal vulnerability in the Dojo module in IBM WebSphere Portal 7.0.0.1 and 7.0.0.2 before CF14, and 8.0, allows remote attackers to read arbitrary files via a crafted URL.
CVE-2012-2214
proxy.c in libpurple in Pidgin before 2.10.4 does not properly handle canceled SOCKS5 connection attempts, which allows user-assisted remote authenticated users to cause a denial of service (application crash) via a sequence of XMPP file-transfer requests.
CVE-2012-2314
The bootloader configuration module (pyanaconda/bootloader.py) in Anaconda uses 755 permissions for /etc/grub.d, which allows local users to obtain password hashes and conduct brute force password guessing attacks.
CVE-2012-2318
msg.c in the MSN protocol plugin in libpurple in Pidgin before 2.10.4 does not properly handle crafted characters, which allows remote servers to cause a denial of service (application crash) by placing these characters in a text/plain message.
CVE-2012-2678
389 Directory Server before 1.2.11.6 (aka Red Hat Directory Server before 8.2.10-3), after the password for a LDAP user has been changed and before the server has been reset, allows remote attackers to read the plaintext password via the unhashed#user#password attribute.
CVE-2012-2746
389 Directory Server before 1.2.11.6 (aka Red Hat Directory Server before 8.2.10-3), when the password of a LDAP user has been changed and audit logging is enabled, saves the new password to the log in plain text, which allows remote authenticated users to read the password.
CVE-2012-2747
Unspecified vulnerability in Joomla! 2.5.x before 2.5.5 allows remote attackers to gain privileges via unknown attack vectors related to "Inadequate checking."
CVE-2012-2748
Unspecified vulnerability in Joomla! 2.5.x before 2.5.5 allows remote attackers to obtain sensitive information via vectors related to "Inadequate filtering" and a "SQL error."
CVE-2012-3366
The Trigger plugin in bcfg2 1.2.x before 1.2.3 allows remote attackers with root access to the client to execute arbitrary commands via shell metacharacters in the UUID field to the server process (bcfg2-server).
CVE-2012-3368
Integer signedness error in attach.c in dtach 0.8 allows remote attackers to obtain sensitive information from daemon stack memory in opportunistic circumstances by reading application data after an improper connection-close request, as demonstrated by running an IRC client in dtach.
CVE-2012-3811
Unrestricted file upload vulnerability in ImageUpload.ashx in the Wallboard application in Avaya IP Office Customer Call Reporter 7.0 before 7.0.5.8 Q1 2012 Maintenance Release and 8.0 before 8.0.9.13 Q1 2012 Maintenance Release allows remote attackers to execute arbitrary code by uploading an executable file and then accessing it via a direct request.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2012-3828
Cross-site scripting (XSS) vulnerability in Joomla! 2.5.3 allows remote attackers to inject arbitrary web script or HTML via the Host HTTP Header.
CVE-2012-3829
Joomla! 2.5.3 allows remote attackers to obtain the installation path via the Host HTTP Header.
CVE-2012-3830
Cross-site scripting (XSS) vulnerability in decoda/templates/video.php in Decoda before 3.3.3 allows remote attackers to inject arbitrary web script or HTML via the video directive.
CVE-2012-3831
Cross-site scripting (XSS) vulnerability in decoda/templates/video.php in Decoda before 3.3.1 allows remote attackers to inject arbitrary web script or HTML via multiple URLs in an img tag.
CVE-2012-3832
Cross-site scripting (XSS) vulnerability in decoda/Decoda.php in Decoda before 3.2 allows remote attackers to inject arbitrary web script or HTML via vectors related to (1) b or (2) div tags.
CVE-2012-3833
Cross-site scripting (XSS) vulnerability in the default index page in admin/ in Quick.CMS 4.0 allows remote attackers to inject arbitrary web script or HTML via the p parameter.
CVE-2012-3834
SQL injection vulnerability in forensics/base_qry_main.php in AlienVault Open Source Security Information Management (OSSIM) 3.1 allows remote authenticated users to execute arbitrary SQL commands via the time[0][0] parameter.
CVE-2012-3835
Multiple cross-site scripting (XSS) vulnerabilities in AlienVault Open Source Security Information Management (OSSIM) 3.1 allow remote attackers to inject arbitrary web script or HTML via the (1) url parameter to top.php or (2) time[0][0] parameter to forensics/base_qry_main.php, which is not properly handled in an error page.
CVE-2012-3836
Multiple cross-site scripting (XSS) vulnerabilities in Baby Gekko before 1.2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) groupname parameter in a savecategory in the users module; (2) virtual_filename, (3) branch, (4) contact_person, (5) street, (6) city, (7) province, (8) postal, (9) country, (10) tollfree, (11) phone, (12) fax, or (13) mobile parameter in a saveitem action in the contacts module; (14) title parameter in a savecategory action in the menus module; (15) firstname or (16) lastname in a saveitem action in the users module; (17) meta_key or (18) meta_description in a saveitem action in the blog module; or (19) the PATH_INFO to admin/index.php.
CVE-2012-3837
Multiple cross-site scripting (XSS) vulnerabilities in apps/users/registration.template.php in Baby Gekko 1.2.0 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) username, (2) email_address, (3) password, (4) password_verify, (5) firstname, (6) lastname, or (7) verification_code parameter to users/action/register.  NOTE: some of these details are obtained from third party information.
CVE-2012-3838
Gekko before 1.2.0 allows remote attackers to obtain the installation path via a direct request to (1) admin/templates/babygekko/index.php or (2) templates/html5demo/index.php.
CVE-2012-3839
Multiple SQL injection vulnerabilities in application/core/MY_Model.php in MyClientBase 0.12 allow remote attackers to execute arbitrary SQL commands via the (1) invoice_number or (2) tags parameter to index.php/invoice_search.
CVE-2012-3840
Multiple cross-site scripting (XSS) vulnerabilities in index.php/users/form/user_id in MyClientBase 0.12 allow remote attackers to inject arbitrary web script or HTML via the (1) first_name or (2) last_name parameters.
CVE-2012-3841
Untrusted search path vulnerability in KMPlayer 3.2.0.19 allows local users to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse ehtrace.dll that is located in the current working directory.
Per indicated reference links 1182120 and 1182121, the attack can be leveraged remotely has been scored as such pending clarification of CVE description.
CVE-2012-3842
Multiple cross-site scripting (XSS) vulnerabilities in CMD_DOMAIN in JBMC Software DirectAdmin 1.403 allow remote authenticated users with certain privileges to inject arbitrary web script or HTML via the (1) select0 or (2) select8 parameters.
CVE-2012-3843
Cross-site scripting (XSS) vulnerability in the registration page in e107, probably 1.0.1, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-3844
Cross-site scripting (XSS) vulnerability in vBulletin 4.1.12 allows remote attackers to inject arbitrary web script or HTML via a long string in the subject parameter when creating a post.
CVE-2012-3845
Buffer overflow in LAN Messenger 1.2.28 and earlier allows remote attackers to cause a denial of service (crash) via a long string in an initiation request.
CVE-2012-3846
Cross-site scripting (XSS) vulnerability in index.php in PHP-pastebin 2.1 allows remote attackers to inject arbitrary web script or HTML via the title parameter.
