CVE-2008-1104
Stack-based buffer overflow in Foxit Reader before 2.3 build 2912 allows user-assisted remote attackers to execute arbitrary code via a crafted PDF file, related to the util.printf JavaScript function and floating point specifiers in format strings.
CVE-2008-1660
Unspecified vulnerability in useradd on HP-UX B.11.11, B.11.23, and B.11.31 allows local users to access arbitrary files and directories via unspecified vectors.
CVE-2008-1948
The _gnutls_server_name_recv_params function in lib/ext_server_name.c in libgnutls in gnutls-serv in GnuTLS before 2.2.4 does not properly calculate the number of Server Names in a TLS 1.0 Client Hello message during extension handling, which allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a zero value for the length of Server Names, which leads to a buffer overflow in session resumption data in the pack_security_parameters function, aka GNUTLS-SA-2008-1-1.
CVE-2008-1949
The _gnutls_recv_client_kx_message function in lib/gnutls_kx.c in libgnutls in gnutls-serv in GnuTLS before 2.2.4 continues to process Client Hello messages within a TLS message after one has already been processed, which allows remote attackers to cause a denial of service (NULL dereference and crash) via a TLS message containing multiple Client Hello messages, aka GNUTLS-SA-2008-1-2.
CVE-2008-1950
Integer signedness error in the _gnutls_ciphertext2compressed function in lib/gnutls_cipher.c in libgnutls in GnuTLS before 2.2.4 allows remote attackers to cause a denial of service (buffer over-read and crash) via a certain integer value in the Random field in an encrypted Client Hello message within a TLS record with an invalid Record Length, which leads to an invalid cipher padding length, aka GNUTLS-SA-2008-1-3.
The vendor has released a statement regarding this issue:

http://lists.gnupg.org/pipermail/gnutls-dev/2006-September/001208.html
CVE-2008-2241
Directory traversal vulnerability in caloggerd in CA BrightStor ARCServe Backup 11.0, 11.1, and 11.5 allows remote attackers to append arbitrary data to arbitrary files via directory traversal sequences in unspecified input fields, which are used in log messages.  NOTE: this can be leveraged for code execution in many installation environments by writing to a startup file or configuration file.
CVE-2008-2242
Multiple buffer overflows in xdr functions in the server in CA BrightStor ARCServe Backup 11.0, 11.1, and 11.5 allow remote attackers to execute arbitrary code, as demonstrated by a stack-based buffer overflow via a long parameter to the xdr_rwsstring function.
CVE-2008-2357
Stack-based buffer overflow in the split_redraw function in split.c in mtr before 0.73, when invoked with the -p (aka --split) option, allows remote attackers to execute arbitrary code via a crafted DNS PTR record.  NOTE: it could be argued that this is a vulnerability in the ns_name_ntop function in resolv/ns_name.c in glibc and the proper fix should be in glibc; if so, then this should not be treated as a vulnerability in mtr.
CVE-2008-2390
Hpufunction.dll 4.0.0.1 in HP Software Update exposes the unsafe (1) ExecuteAsync and (2) Execute methods, which allows remote attackers to execute arbitrary code via an absolute pathname in the first argument.
CVE-2008-2391
SubSonic allows remote attackers to bypass pagesize limits and cause a denial of service (CPU consumption) via a pageindex (aka data page number) of -1.
CVE-2008-2392
Unrestricted file upload vulnerability in WordPress 2.5.1 and earlier might allow remote authenticated administrators to upload and execute arbitrary PHP files via the Upload section in the Write Tabs area of the dashboard.
CVE-2008-2393
SQL injection vulnerability in play.php in EntertainmentScript 1.4.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-2394
Multiple SQL injection vulnerabilities in TAGWORX.CMS 3.00.02 allow remote attackers to execute arbitrary SQL commands via the (1) cid parameter to contact.php and the (2) nid parameter to news.php.
CVE-2008-2395
SQL injection vulnerability in thread.php in AlkalinePHP 0.80.00 beta and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-2396
PHP remote file inclusion vulnerability in index.php in Wajox Software microSSys CMS 1.5 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in an arbitrary element of the PAGES array parameter.
CVE-2008-2397
Cross-site scripting (XSS) vulnerability in search-results.dot in dotCMS 1.x allows remote attackers to inject arbitrary web script or HTML via the search_query parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-2398
Cross-site scripting (XSS) vulnerability in index.php in AppServ Open Project 2.5.10 and earlier allows remote attackers to inject arbitrary web script or HTML via the appservlang parameter.
