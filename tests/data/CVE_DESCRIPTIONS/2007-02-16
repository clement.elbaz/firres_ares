CVE-2007-0451
Apache SpamAssassin before 3.1.8 allows remote attackers to cause a denial of service via long URLs in malformed HTML, which triggers "massive memory usage."
Upgrade to SpamAssassin version 3.1.8
CVE-2007-0710
The Bonjour functionality in iChat in Apple Mac OS X 10.3.9 allows remote attackers to cause a denial of service (persistent application crash) via unspecified vectors, possibly related to CVE-2007-0614.
CVE-2007-0859
The Find feature in Palm OS Treo smart phones operates despite the system password lock, which allows attackers with physical access to obtain sensitive information (memory contents) by doing (1) text searches or (2) paste operations after pressing certain keyboard shortcut keys.
CVE-2007-0897
Clam AntiVirus ClamAV before 0.90 does not close open file descriptors under certain conditions, which allows remote attackers to cause a denial of service (file descriptor consumption and failed scans) via CAB archives with a cabinet header record length of zero, which causes a function to return without closing a file descriptor.
This vulnerability is addressed in the following product release:
Clam AntiVirus, ClamAV, 0.90 Stable
CVE-2007-0898
Directory traversal vulnerability in clamd in Clam AntiVirus ClamAV before 0.90 allows remote attackers to overwrite arbitrary files via a .. (dot dot) in the id MIME header parameter in a multi-part message.
This vulnerability is addressed in the following product release:
Clam Anti-Virus, ClamAV, 0.90
CVE-2007-0959
Cisco PIX 500 and ASA 5500 Series Security Appliances 7.2.2, when configured to inspect certain TCP-based protocols, allows remote attackers to cause a denial of service (device reboot) via malformed TCP packets.
CVE-2007-0960
Unspecified vulnerability in Cisco PIX 500 and ASA 5500 Series Security Appliances 7.2.2, when configured to use the LOCAL authentication method, allows remote authenticated users to gain privileges via unspecified vectors.
CVE-2007-0961
Cisco PIX 500 and ASA 5500 Series Security Appliances 6.x before 6.3(5.115), 7.0 before 7.0(5.2), and 7.1 before 7.1(2.5), and the FWSM 3.x before 3.1(3.24), when the "inspect sip" option is enabled, allows remote attackers to cause a denial of service (device reboot) via malformed SIP packets.
CVE-2007-0962
Cisco PIX 500 and ASA 5500 Series Security Appliances 7.0 before 7.0(4.14) and 7.1 before 7.1(2.1), and the FWSM 2.x before 2.3(4.12) and 3.x before 3.1(3.24), when "inspect http" is enabled, allows remote attackers to cause a denial of service (device reboot) via malformed HTTP traffic.
CVE-2007-0963
Unspecified vulnerability in Cisco Firewall Services Module (FWSM) 3.x before 3.1(3.3), when set to log at the "debug" level, allows remote attackers to cause a denial of service (device reboot) by sending packets that are not of a particular protocol such as TCP or UDP, which triggers the reboot during generation of Syslog message 710006.
CVE-2007-0964
Cisco FWSM 3.x before 3.1(3.18), when authentication is configured to use "aaa authentication match" or "aaa authentication include", allows remote attackers to cause a denial of service (device reboot) via a malformed HTTPS request.
CVE-2007-0965
Cisco FWSM 3.x before 3.1(3.2), when authentication is configured to use "aaa authentication match" or "aaa authentication include", allows remote attackers to cause a denial of service (device reboot) via a long HTTP request.
CVE-2007-0966
Cisco Firewall Services Module (FWSM) 3.x before 3.1(3.11), when the HTTPS server is enabled, allows remote attackers to cause a denial of service (device reboot) via certain HTTPS traffic.
CVE-2007-0967
Cisco Firewall Services Module (FWSM) 3.x before 3.1(3.1) allows remote attackers to cause a denial of service (device reboot) via malformed SNMP requests.
CVE-2007-0968
Unspecified vulnerability in Cisco Firewall Services Module (FWSM) before 2.3(4.7) and 3.x before 3.1(3.1) causes the access control entries (ACE) in an ACL to be improperly evaluated, which allows remote authenticated users to bypass intended certain ACL protections.
CVE-2007-0969
Multiple cross-site scripting (XSS) vulnerabilities in WebTester 5.0.20060927 and earlier allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to POST parameters to multiple files.
CVE-2007-0970
Multiple SQL injection vulnerabilities in WebTester 5.0.20060927 and earlier allow remote attackers to execute arbitrary SQL commands via the testID parameter to directions.php, and unspecified parameters to other files that accept GET or POST input.
CVE-2007-0971
Multiple SQL injection vulnerabilities in Jupiter CMS 1.1.5 allow remote attackers to execute arbitrary SQL commands via the Client-IP HTTP header and certain other HTTP headers, which set the ip variable that is used in SQL queries performed by index.php and certain other PHP scripts.  NOTE: the attack vector might involve _SERVER.
CVE-2007-0972
Unrestricted file upload vulnerability in modules/emoticons.php in Jupiter CMS 1.1.5 allows remote attackers to upload arbitrary files by modifying the HTTP request to send an image content type, and to omit is_guest and is_user parameters.  NOTE: this issue might be related to CVE-2006-4875.
CVE-2007-0973
Multiple cross-site scripting (XSS) vulnerabilities in index.php in Jupiter CMS 1.1.5 allow remote attackers to inject arbitrary web script or HTML via the Referer HTTP header and certain other HTTP headers, which are displayed without proper sanitization when an administrator performs a Logged Guest action.
CVE-2007-0974
Multiple unspecified vulnerabilities in Ian Bezanson DropBox before 0.0.4 beta have unknown impact and attack vectors, possibly related to a variable extraction vulnerability.
CVE-2007-0975
Variable extraction vulnerability in Ian Bezanson Apache Stats before 0.0.3 beta allows attackers to overwrite critical variables, with unknown impact, when the extract function is used on the _REQUEST superglobal array.
CVE-2007-0976
Buffer overflow in the ActSoft DVD-Tools ActiveX control (dvdtools.ocx) allows remote attackers to execute arbitrary code via a long DVD_TOOLS.OpenDVD property value.
CVE-2007-0977
IBM Lotus Domino R5 and R6 WebMail, with "Generate HTML for all fields" enabled, stores HTTPPassword hashes from names.nsf in a manner accessible through Readviewentries and OpenDocument requests to the defaultview view, a different vector than CVE-2005-2428.
"Generate HTML for all fields" must be enabled for successful exploitation.
CVE-2007-0978
Buffer overflow in swcons in IBM AIX 5.3 allows local users to gain privileges via long input data.
CVE-2007-0979
Unspecified vulnerability in LifeType before 1.1.6, and 1.2 before 1.2-beta2, allows remote attackers to obtain sensitive information (file contents) via a "crafted URL."
CVE-2007-0980
Unspecified vulnerability in HP Serviceguard for Linux; packaged for SuSE SLES8 and United Linux 1.0 before SG A.11.15.07, SuSE SLES9 and SLES10 before SG A.11.16.10, and Red Hat Enterprise Linux (RHEL) before SG A.11.16.10; allows remote attackers to obtain unauthorized access via unspecified vectors.
CVE-2007-0981
Mozilla based browsers, including Firefox before 1.5.0.10 and 2.x before 2.0.0.2, and SeaMonkey before 1.0.8, allow remote attackers to bypass the same origin policy, steal cookies, and conduct other attacks by writing a URI with a null byte to the hostname (location.hostname) DOM property, due to interactions with DNS resolver code.
CVE-2007-0982
Cross-site scripting (XSS) vulnerability in error.php in TaskFreak! 0.5.5 allows remote attackers to inject arbitrary web script or HTML via the tznMessage parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0983
PHP remote file inclusion vulnerability in _admin/nav.php in AT Contenator 1.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the Root_To_Script parameter.
CVE-2007-0984
SQL injection vulnerability in admin_poll.asp in PollMentor 2.0 allows remote attackers to execute arbitrary SQL commands via the id parameter to pollmentorres.asp.
CVE-2007-0985
SQL injection vulnerability in nickpage.php in phpCC 4.2 beta and earlier allows remote attackers to execute arbitrary SQL commands via the npid parameter in a sign_gb action.
CVE-2007-0986
PHP remote file inclusion vulnerability in index.php in Jupiter CMS 1.1.5, when PHP 5.0.0 or later is used, allows remote attackers to execute arbitrary PHP code via an ftp URL in the n parameter.
Successful exploitation requires that "magic_quotes_gpc" is disabled and that "allow_url_fopen" is enabled.
This vulnerability requires that Jupiter CMS 1.1.5 is used with PHP 5.0.0 or later.
CVE-2007-0987
Directory traversal vulnerability in index.php in Jupiter CMS 1.1.5 allows remote attackers to include and execute arbitrary local files via a .. (dot dot), or an absolute pathname, in the n parameter.
