CVE-2011-5320
scanf and related functions in glibc before 2.15 allow local users to cause a denial of service (segmentation fault) via a large string of 0s.
CVE-2014-3164
cmds/servicemanager/service_manager.c in Android before commit 7d42a3c31ba78a418f9bdde0e0ab951469f321b5 allows attackers to cause a denial of service (NULL pointer dereference, or out-of-bounds write) via vectors related to binder passed lengths.
CVE-2014-3531
Multiple cross-site scripting (XSS) vulnerabilities in Foreman before 1.5.2 allow remote authenticated users to inject arbitrary web script or HTML via the operating system (1) name or (2) description.
CVE-2014-3706
ovirt-engine, as used in Red Hat MRG 3, allows man-in-the-middle attackers to spoof servers by leveraging failure to verify key attributes in vdsm X.509 certificates.
CVE-2014-3709
The org.keycloak.services.resources.SocialResource.callback method in JBoss KeyCloak before 1.0.3.Final allows remote attackers to conduct cross-site request forgery (CSRF) attacks by leveraging lack of CSRF protection.
CVE-2014-7242
The SumaHo application 3.0.0 and earlier for Android and the SumaHo "driving capability" diagnosis result transmission application 1.2.2 and earlier for Android allow man-in-the-middle attackers to spoof servers and obtain sensitive information by leveraging failure to verify SSL/TLS server certificates.
CVE-2014-7813
Red Hat CloudForms 3 Management Engine (CFME) allows remote authenticated users to cause a denial of service (resource consumption) via vectors involving calls to the .to_sym rails function and lack of garbage collection of inserted symbols.
CVE-2014-8491
The Grand Flagallery plugin before 4.25 for WordPress allows remote attackers to obtain the installation path via a request to (1) flagallery-skins/banner_widget_default/gallery.php or (2) flash-album-gallery/skins/banner_widget_default/gallery.php.
CVE-2015-1239
Double free vulnerability in the j2k_read_ppm_v3 function in OpenJPEG before r2997, as used in PDFium in Google Chrome, allows remote attackers to cause a denial of service (process crash) via a crafted PDF.
CVE-2015-2156
Netty before 3.9.8.Final, 3.10.x before 3.10.3.Final, 4.0.x before 4.0.28.Final, and 4.1.x before 4.1.0.Beta5 and Play Framework 2.x before 2.3.9 might allow remote attackers to bypass the httpOnly flag on cookies and obtain sensitive information by leveraging improper validation of cookie name and value characters.
CVE-2015-3400
sharenfs 0.6.4, when built with commits bcdd594 and 7d08880 from the zfs repository, provides world readable access to the shared zfs file system, which might allow remote authenticated users to obtain sensitive information by reading shared files.
CVE-2015-5164
The Qpid server on Red Hat Satellite 6 does not properly restrict message types, which allows remote authenticated users with administrative access on a managed content host to execute arbitrary code via a crafted message, related to a pickle processing problem in pulp.
CVE-2015-5227
The Landing Pages plugin before 1.9.2 for WordPress allows remote attackers to execute arbitrary code via the url parameter.
CVE-2015-5376
SQL injection vulnerability in the login form in GSI WiNPAT Portal 3.2.0.1001 through 3.6.1.0 allows remote attackers to execute arbitrary SQL commands via the username field.
CVE-2015-5739
The net/http library in net/textproto/reader.go in Go before 1.4.3 does not properly parse HTTP header keys, which allows remote attackers to conduct HTTP request smuggling attacks via a space instead of a hyphen, as demonstrated by "Content Length" instead of "Content-Length."
CVE-2015-5740
The net/http library in net/http/transfer.go in Go before 1.4.3 does not properly parse HTTP headers, which allows remote attackers to conduct HTTP request smuggling attacks via a request with two Content-length headers.
CVE-2015-6961
Open redirect vulnerability in gluon/tools.py in Web2py 2.9.11 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the _next parameter to user/logout.
CVE-2015-7714
Multiple SQL injection vulnerabilities in the Realtyna RPL (com_rpl) component before 8.9.5 for Joomla! allow remote administrators to execute arbitrary SQL commands via the (1) id, (2) copy_field in a data_copy action, (3) pshow in an update_field action, (4) css, (5) tip, (6) cat_id, (7) text_search, (8) plisting, or (9) pwizard parameter to administrator/index.php.
CVE-2015-7715
Cross-site request forgery (CSRF) vulnerability in the Realtyna RPL (com_rpl) component before 8.9.5 for Joomla! allows remote attackers to hijack the authentication of administrators for requests that add a user via an add_user action to administrator/index.php.
CVE-2015-7943
Open redirect vulnerability in the Overlay module in Drupal 7.x before 7.41, the jQuery Update module 7.x-2.x before 7.x-2.7 for Drupal, and the LABjs module 7.x-1.x before 7.x-1.8 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-3233.
CVE-2016-10515
In Redmine before 3.2.3, there are stored XSS vulnerabilities affecting Textile and Markdown text formatting, and project homepages.
CVE-2016-5714
Puppet Enterprise 2015.3.3 and 2016.x before 2016.4.0, and Puppet Agent 1.3.6 through 1.7.0 allow remote attackers to bypass a host whitelist protection mechanism and execute arbitrary code on Puppet nodes via vectors related to command validation, aka "Puppet Execution Protocol (PXP) Command Whitelist Validation Vulnerability."
CVE-2017-13083
Akeo Consulting Rufus prior to version 2.17.1187 does not adequately validate the integrity of updates downloaded over HTTP, allowing an attacker to easily convince a user to execute arbitrary code
CVE-2017-14322
The function in charge to check whether the user is already logged in init.php in Interspire Email Marketer (IEM) prior to 6.1.6 allows remote attackers to bypass authentication and obtain administrative access by using the IEM_CookieLogin cookie with a specially crafted value.
CVE-2017-14956
AlienVault USM v5.4.2 and earlier offers authenticated users the functionality of exporting generated reports via the "/ossim/report/wizard_email.php" script. Besides offering an export via a local download, the script also offers the possibility to send out any report via email to a given address (either in PDF or XLS format). Since there is no anti-CSRF token protecting this functionality, it is vulnerable to Cross-Site Request Forgery attacks.
CVE-2017-15359
In the 3CX Phone System 15.5.3554.1, the Management Console typically listens to port 5001 and is prone to a directory traversal attack: "/api/RecordingList/DownloadRecord?file=" and "/api/SupportInfo?file=" are the vulnerable parameters. An attacker must be authenticated to exploit this issue to access sensitive information to aid in subsequent attacks.
CVE-2017-15568
In Redmine before 3.2.8, 3.3.x before 3.3.5, and 3.4.x before 3.4.3, XSS exists in app/helpers/application_helper.rb via a multi-value field with a crafted value that is mishandled during rendering of issue history.
CVE-2017-15569
In Redmine before 3.2.8, 3.3.x before 3.3.5, and 3.4.x before 3.4.3, XSS exists in app/helpers/queries_helper.rb via a multi-value field with a crafted value that is mishandled during rendering of an issue list.
CVE-2017-15570
In Redmine before 3.2.8, 3.3.x before 3.3.5, and 3.4.x before 3.4.3, XSS exists in app/views/timelog/_list.html.erb via crafted column data.
CVE-2017-15571
In Redmine before 3.2.8, 3.3.x before 3.3.5, and 3.4.x before 3.4.3, XSS exists in app/views/issues/_list.html.erb via crafted column data.
CVE-2017-15572
In Redmine before 3.2.6 and 3.3.x before 3.3.3, remote attackers can obtain sensitive information (password reset tokens) by reading a Referer log, because account/lost_password does not use a redirect.
CVE-2017-15573
In Redmine before 3.2.6 and 3.3.x before 3.3.3, XSS exists because markup is mishandled in wiki content.
CVE-2017-15574
In Redmine before 3.2.6 and 3.3.x before 3.3.3, stored XSS is possible by using an SVG document as an attachment.
CVE-2017-15575
In Redmine before 3.2.6 and 3.3.x before 3.3.3, Redmine.pm lacks a check for whether the Repository module is enabled in a project's settings, which might allow remote attackers to obtain sensitive differences information or possibly have unspecified other impact.
CVE-2017-15576
Redmine before 3.2.6 and 3.3.x before 3.3.3 mishandles Time Entry rendering in activity views, which allows remote attackers to obtain sensitive information.
CVE-2017-15577
Redmine before 3.2.6 and 3.3.x before 3.3.3 mishandles the rendering of wiki links, which allows remote attackers to obtain sensitive information.
CVE-2017-15578
In PHPSUGAR PHP Melody before 2.7.3, SQL Injection exists via the image parameter to admin/edit_category.php.
CVE-2017-15579
In PHPSUGAR PHP Melody before 2.7.3, SQL Injection exists via an aa_pages_per_page cookie in a playlist action to watch.php.
CVE-2017-15583
The embedded web server on ABB Fox515T 1.0 devices is vulnerable to Local File Inclusion. It accepts a parameter that specifies a file for display or for use as a template. The filename is not validated; an attacker could retrieve any file.
CVE-2017-15587
An integer overflow was discovered in pdf_read_new_xref_section in pdf/pdf-xref.c in Artifex MuPDF 1.11.
CVE-2017-15588
An issue was discovered in Xen through 4.9.x allowing x86 PV guest OS users to execute arbitrary code on the host OS because of a race condition that can cause a stale TLB entry.
CVE-2017-15589
An issue was discovered in Xen through 4.9.x allowing x86 HVM guest OS users to obtain sensitive information from the host OS (or an arbitrary guest OS) because intercepted I/O operations can cause a write of data from uninitialized hypervisor stack memory.
CVE-2017-15590
An issue was discovered in Xen through 4.9.x allowing x86 guest OS users to cause a denial of service (hypervisor crash) or possibly gain privileges because MSI mapping was mishandled.
CVE-2017-15591
An issue was discovered in Xen 4.5.x through 4.9.x allowing attackers (who control a stub domain kernel or tool stack) to cause a denial of service (host OS crash) because of a missing comparison (of range start to range end) within the DMOP map/unmap implementation.
CVE-2017-15592
An issue was discovered in Xen through 4.9.x allowing x86 HVM guest OS users to cause a denial of service (hypervisor crash) or possibly gain privileges because self-linear shadow mappings are mishandled for translated guests.
CVE-2017-15593
An issue was discovered in Xen through 4.9.x allowing x86 PV guest OS users to cause a denial of service (memory leak) because reference counts are mishandled.
CVE-2017-15594
An issue was discovered in Xen through 4.9.x allowing x86 SVM PV guest OS users to cause a denial of service (hypervisor crash) or gain privileges because IDT settings are mishandled during CPU hotplugging.
CVE-2017-15595
An issue was discovered in Xen through 4.9.x allowing x86 PV guest OS users to cause a denial of service (unbounded recursion, stack consumption, and hypervisor crash) or possibly gain privileges via crafted page-table stacking.
CVE-2017-15596
An issue was discovered in Xen 4.4.x through 4.9.x allowing ARM guest OS users to cause a denial of service (prevent physical CPU usage) because of lock mishandling upon detection of an add-to-physmap error.
CVE-2017-15600
In GNU Libextractor 1.4, there is a NULL Pointer Dereference in the EXTRACTOR_nsf_extract_method function of plugins/nsf_extractor.c.
CVE-2017-15601
In GNU Libextractor 1.4, there is a heap-based buffer overflow in the EXTRACTOR_png_extract_method function in plugins/png_extractor.c, related to processiTXt and stndup.
CVE-2017-15602
In GNU Libextractor 1.4, there is an integer signedness error for the chunk size in the EXTRACTOR_nsfe_extract_method function in plugins/nsfe_extractor.c, leading to an infinite loop for a crafted size.
CVE-2017-8022
An issue was discovered in EMC NetWorker (prior to 8.2.4.9, all supported 9.0.x versions, prior to 9.1.1.3, prior to 9.2.0.4). The Server service (nsrd) is affected by a buffer overflow vulnerability. A remote unauthenticated attacker may potentially exploit this vulnerability to execute arbitrary code on vulnerable installations of the software, or cause a denial of service, depending on the target system's platform.
CVE-2017-8024
EMC Isilon OneFS (versions prior to 8.1.0.1, versions prior to 8.0.1.2, versions prior to 8.0.0.6, version 7.2.1.x) is impacted by a reflected cross-site scripting vulnerability that may potentially be exploited by malicious users to compromise the affected system.
