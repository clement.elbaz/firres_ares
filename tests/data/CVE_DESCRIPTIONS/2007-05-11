CVE-2007-0244
pptpgre.c in PoPToP Point to Point Tunneling Server (pptpd) before 1.3.4 allows remote attackers to cause a denial of service (PPTP connection tear-down) via (1) GRE packets with out-of-order sequence numbers or (2) certain GRE packets that are processed using a wrong pointer and improperly dequeued.
CVE-2007-1262
Multiple cross-site scripting (XSS) vulnerabilities in the HTML filter in SquirrelMail 1.4.0 through 1.4.9a allow remote attackers to inject arbitrary web script or HTML via the (1) data: URI in an HTML e-mail attachment or (2) various non-ASCII character sets that are not properly filtered when viewed with Microsoft Internet Explorer.
CVE-2007-2522
Stack-based buffer overflow in the inoweb Console Server in CA Anti-Virus for the Enterprise r8, Threat Manager r8, Anti-Spyware for the Enterprise r8, and Protection Suites r3 allows remote attackers to execute arbitrary code via a long (1) username or (2) password.
CVE-2007-2523
CA Anti-Virus for the Enterprise r8 and Threat Manager r8 before 20070510 use weak permissions (NULL security descriptor) for the Task Service shared file mapping, which allows local users to modify this mapping and gain privileges by triggering a stack-based buffer overflow in InoCore.dll before 8.0.448.0.
The Computer Associates Integrated Threat Manager product is only vulnerable if it is release 8.0 before 2007-05-10.
CVE-2007-2589
Cross-site request forgery (CSRF) vulnerability in compose.php in SquirrelMail 1.4.0 through 1.4.9a allows remote attackers to send e-mails from arbitrary users via certain data in the SRC attribute of an IMG element.
CVE-2007-2590
Nokia Intellisync Mobile Suite 6.4.31.2, 6.6.0.107, and 6.6.2.2, possibly involving Novell Groupwise Mobile Server and Nokia Intellisync Wireless Email Express, allows remote attackers to obtain user names and other sensitive information via a direct request to (1) usrmgr/userList.asp or (2) usrmgr/userStatusList.asp.
CVE-2007-2591
usrmgr/userList.asp in Nokia Intellisync Mobile Suite 6.4.31.2, 6.6.0.107, and 6.6.2.2, possibly involving Novell Groupwise Mobile Server and Nokia Intellisync Wireless Email Express, allows remote attackers to modify user account details and cause a denial of service (account deactivation) via the userid parameter in an update action.
CVE-2007-2592
Multiple cross-site scripting (XSS) vulnerabilities in Nokia Intellisync Mobile Suite 6.4.31.2, 6.6.0.107, and 6.6.2.2, possibly involving Novell Groupwise Mobile Server and Nokia Intellisync Wireless Email Express, allow remote attackers to inject arbitrary web script or HTML via the (1) username parameter to de/pda/dev_logon.asp and (2) multiple unspecified vectors in (a) usrmgr/registerAccount.asp, (b) de/create_account.asp, and other files.
CVE-2007-2593
The Terminal Server in Microsoft Windows 2003 Server, when using TLS, allows remote attackers to bypass SSL and self-signed certificate requirements, downgrade the server security, and possibly conduct man-in-the-middle attacks via unspecified vectors, as demonstrated using the Remote Desktop Protocol (RDP) 6.0 client.  NOTE: a third party claims that the vendor may have fixed this in approximately 2006.
CVE-2007-2594
PHP remote file inclusion vulnerability in inc/articles.inc.php in phpMyPortal 3.0.0 RC3 allows remote attackers to execute arbitrary PHP code via a URL in the GLOBALS[CHEMINMODULES] parameter.
CVE-2007-2595
RSAuction 2.73.1.3 allows remote authenticated users to move their own account status from Suspended to Active via a direct request for the activation URL that is provided at the time of account registration. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2596
PHP remote file inclusion vulnerability in common/func.php in aForum 1.32 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the CommonAbsDir parameter.
CVE-2007-2597
Multiple PHP remote file inclusion vulnerabilities in telltarget CMS 1.3.3 allow remote attackers to execute arbitrary PHP code via a URL in the (1) ordnertiefe parameter to site_conf.php; or the (2) tt_docroot parameter to (a) class.csv.php, (b) produkte_nach_serie.php, or (c) ref_kd_rubrik.php in functionen/; (d) hg_referenz_jobgalerie.php, (e) surfer_anmeldung_NWL.php, (f) produkte_nach_serie_alle.php, (g) surfer_aendern.php, (h) ref_kd_rubrik.php, or (i) referenz.php in module/; or (j) 1/lay.php or (k) 3/lay.php in standard/.
CVE-2007-2598
SQL injection vulnerability in print.php in SimpleNews 1.0.0 FINAL allows remote attackers to execute arbitrary SQL commands via the news_id parameter.
CVE-2007-2599
Multiple SQL injection vulnerabilities in TutorialCMS (aka Photoshop Tutorials) 1.00 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) catFile parameter to (a) browseCat.php or (b) browseSubCat.php; the (2) id parameter to (c) openTutorial.php, (d) topFrame.php, or (e) admin/editListing.php; or (3) the search parameter to search.php.
CVE-2007-2600
Multiple cross-site scripting (XSS) vulnerabilities in TutorialCMS (aka Photoshop Tutorials) 1.00 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) catFile parameter to (a) browseCat.php or (b) browseSubCat.php; the (2) id parameter to (c) openTutorial.php, (d) topFrame.php, or (e) admin/editListing.php; or the (3) search parameter to search.php.
CVE-2007-2601
Buffer overflow in a certain ActiveX control in the GDivX Zenith Player AviFixer class in fix.dll 1.0.0.1 allows remote attackers to execute arbitrary code via a long SetInputFile property value.
CVE-2007-2602
Buffer overflow in MIBEXTRA.EXE in Ipswitch WhatsUp Gold 11 allows attackers to cause a denial of service (application crash) or execute arbitrary code via a long MIB filename argument.  NOTE: If there is not a common scenario under which MIBEXTRA.EXE is called with attacker-controlled command line arguments, then perhaps this issue should not be included in CVE.
CVE-2007-2603
Unspecified vulnerability in the Init function in the Audio CD Ripper OCX (AudioCDRipperOCX.ocx) 1.0 ActiveX control allows remote attackers to cause a denial of service (NULL dereference and Internet Explorer crash) via unspecified vectors.
CVE-2007-2604
Unspecified vulnerability in the FlexLabel ActiveX control allows remote attackers to cause a denial of service (unstable behavior) via an improper initialization, as demonstrated by a certain value of the Caption property.
CVE-2007-2605
Unspecified vulnerability in the GetPropertyById function in ISoftomateObj in SoftomateLib in BRUJULA4.NET.DLL in the Brujula Toolbar (Brujula.net toolbar) allows attackers to cause a denial of service (NULL dereference and browser crash) via certain arguments.
CVE-2007-2606
Multiple buffer overflows in Firebird 2.1 allow attackers to trigger memory corruption and possibly have other unspecified impact via certain input processed by (1) config\ConfigFile.cpp or (2) msgs\check_msgs.epp.  NOTE: if ConfigFile.cpp reads a configuration file with restrictive permissions, then the ConfigFile.cpp vector may not cross privilege boundaries and perhaps should not be included in CVE.
CVE-2007-2607
PHP remote file inclusion vulnerability in views/print/printbar.php in LaVague 0.3 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the views_path parameter.
CVE-2007-2608
PHP remote file inclusion vulnerability in lib/smarty/SmartyFU.class.php in Miplex2 Alpha 1 allows remote attackers to execute arbitrary PHP code via a URL in the system[smarty][dir] parameter.
CVE-2007-2609
Multiple PHP remote file inclusion vulnerabilities in gnuedu 1.3b2 allow remote attackers to execute arbitrary PHP code via a URL in the (a) ETCDIR parameter to (1) libs/lom.php; (2) lom_update.php, (3) check-lom.php, and (4) weigh_keywords.php in scripts/; the (b) LIBSDIR parameter to (5) logout.php, (6) help.php, (7) index.php, (8) login.php; and the ETCDIR parameter to (9) web/lom.php.
CVE-2007-2610
Cross-site scripting (XSS) vulnerability in OpenLD before 1.1.9, and 1.1-modified before 1.1-modified3, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors in the Search feature, possibly the term parameter.
CVE-2007-2611
Multiple PHP remote file inclusion vulnerabilities in CGX 20050314 allow remote attackers to execute arbitrary PHP code via a URL in the pathCGX parameter to (1) mtdialogo.php, (2) ltdialogo.php, (3) login.php, and (4) logingecon.php in inc/; and multiple unspecified files in frm/, sql/, and cns/.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-2612
SQL injection vulnerability in libs/Wakka.class.php in WikkaWiki (Wikka Wiki) before 1.1.6.3 allows remote attackers to execute arbitrary SQL commands via the limit parameter.  NOTE: this issue only applies to a "modified installation."
CVE-2007-2613
WikkaWiki (Wikka Wiki) before 1.1.6.3 allows attackers in a shared virtual host server environment to upload and execute an arbitrary configuration file by modifying the WAKKA_CONFIG environment variable.
The vendor has addressed this issue through a product update:
http://www.wikkawiki.org/downloads/
CVE-2007-2614
PHP remote file inclusion vulnerability in examples/widget8.php in phpHtmlLib 2.4.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the phphtmllib parameter.
CVE-2007-2615
Multiple PHP remote file inclusion vulnerabilities in Crie seu PHPLojaFacil 0.1.5 allow remote attackers to execute arbitrary PHP code via a URL in the path_local parameter to (1) ftp.php, (2) libs/db.php, and (3) libs/ftp.php.
CVE-2007-2616
Stack-based buffer overflow in the SSL version of the NMDMC.EXE service in Novell NetMail 3.52e FTF2 and probably earlier allows remote attackers to execute arbitrary code via a crafted request.
CVE-2007-2617
srsexec in Sun Remote Services (SRS) Net Connect Software Proxy Core package in Sun Solaris 10 does not enforce file permissions when opening files, which allows local users to read the first line of arbitrary files via the -d and -v options.
CVE-2007-2618
CRLF injection vulnerability in index.php in Drake CMS 0.4.0 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in the lang parameter. NOTE: Drake CMS has only a beta version available, and the vendor has previously stated "We do not consider security reports valid until the first official release of Drake CMS."
In order to make the exploit work, the attacker would have to craft a webpage (hosted on the same domain as the Drake CMS website) and have the Drake CMS user make a post from that page.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-2619
Symantec pcAnywhere 11.5.x and 12.0.x retains unencrypted login credentials for the most recent login within process memory, which allows local administrators to obtain the credentials by reading process memory, a different vulnerability than CVE-2006-3785.
CVE-2007-2620
PHP remote file inclusion vulnerability in inc/config.inc.php in Jakub Steiner (aka jimmac) original 0.11 allows remote attackers to execute arbitrary PHP code via a URL in the x[1] parameter.
CVE-2007-2621
SQL injection vulnerability in event_view.php in Thyme Calendar 1.3 allows remote attackers to execute arbitrary SQL commands via the eid parameter.
CVE-2007-2622
Multiple SQL injection vulnerabilities in TaskDriver 1.2 and earlier allow remote attackers to execute arbitrary SQL commands via (1) the username parameter to login.php or (2) the taskid parameter to notes.php.
CVE-2007-2623
Multiple buffer overflows in RControl.dll in Remote Display Dev kit 1.2.1.0 allow remote attackers to cause a denial of service (Internet Explorer 7 crash) via (1) a long first argument to the connect function or (2) a long InternalServer property value, possibly involving ntdll.dll.
CVE-2007-2624
Dynamic variable evaluation vulnerability in shared/config/cp_config.php in All In One Control Panel (AIOCP) before 1.3.016 allows remote attackers to conduct cross-site scripting (XSS) and possibly other attacks via the SERVER superglobal array.  NOTE: some of these details are obtained from third party information.
CVE-2007-2625
Cross-site scripting (XSS) vulnerability in shared/code/cp_authorization.php in All In One Control Panel (AIOCP) before 1.3.016 allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.  NOTE: some of these details are obtained from third party information.
CVE-2007-2626
** DISPUTED **  SQL injection vulnerability in admin.php in SchoolBoard allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters.  NOTE: CVE disputes this issue, because 'username' does not exist, and the password is not used in any queries.
CVE-2007-2627
Cross-site scripting (XSS) vulnerability in sidebar.php in WordPress, when custom 404 pages that call get_sidebar are used, allows remote attackers to inject arbitrary web script or HTML via the query string (PHP_SELF), a different vulnerability than CVE-2007-1622.
CVE-2007-2628
PHP remote file inclusion vulnerability in include/logout.php in Justin Koivisto SecurityAdmin for PHP (aka PHPSecurityAdmin, PSA) 4.0.2 allows remote attackers to execute arbitrary PHP code via a URL in the PSA_PATH parameter.
CVE-2007-2629
Bradford CampusManager Network Control Application Server 3.1(6) allows remote attackers to obtain sensitive information (backup, log, and configuration files) via direct request for certain files in (1) /runTime/ or (2) /remediationReports/.
CVE-2007-2630
Incomplete blacklist vulnerability in filemanager/browser/default/connectors/php/config.php in the FCKeditor module, as used in ActiveCampaign 1-2-All (aka 12All) 4.50 through 4.53.13, and possibly other products, allows remote authenticated administrators to upload and possibly execute .php4 and .php5 files via unspecified vectors.  NOTE: this issue is reachable through filemanager/browser/default/browser.html.
