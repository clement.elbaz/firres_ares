CVE-2012-4230
The bbcode plugin in TinyMCE 3.5.8 does not properly enforce the TinyMCE security policy for the (1) encoding directive and (2) valid_elements attribute, which allows attackers to conduct cross-site scripting (XSS) attacks via application-specific vectors, as demonstrated using a textarea element.
CVE-2013-2025
Cross-site scripting (XSS) vulnerability in Ushahidi Platform 2.5.x through 2.6.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-3069
Multiple cross-site scripting (XSS) vulnerabilities in NETGEAR WNDR4700 with firmware 1.0.0.34 allow remote authenticated users to inject arbitrary web script or HTML via the (1) UserName or (2) Password to the NAS User Setup page, (3) deviceName to USB_advanced.htm, or (4) Network Key to the Wireless Setup page.
CVE-2013-4565
Heap-based buffer overflow in the __OLEdecode function in ppthtml 0.5.1 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted .ppt file.
CVE-2013-4722
Multiple cross-site scripting (XSS) vulnerabilities in Admin/login/default.asp in DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions allow remote attackers to inject arbitrary web script or HTML via the (1) username, (2) url, (3) qstr parameter.
CVE-2013-4723
Open redirect vulnerability in DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the l parameter to track.aspx.
CVE-2013-4726
Cross-site request forgery (CSRF) vulnerability in DDSN Interactive cm3 Acora CMS 6.0.6/1a, 6.0.2/1a, 5.5.7/12b, 5.5.0/1b-p1, and possibly other versions, allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2013-5660
Buffer overflow in Power Software WinArchiver 3.2 allows remote attackers to execute arbitrary code via a crafted .zip file.
CVE-2013-5954
Multiple cross-site request forgery (CSRF) vulnerabilities in OpenX 2.8.11 and earlier allow remote attackers to hijack the authentication of administrators for requests that delete (1) users via admin/agency-user-unlink.php, (2) advertisers via admin/advertiser-delete.php, (3) banners via admin/banner-delete.php, (4) campaigns via admin/campaign-delete.php, (5) channels via admin/channel-delete.php, (6) affiliate websites via admin/affiliate-delete.php, or (7) zones via admin/zone-delete.php.
CVE-2013-5956
Cross-site scripting (XSS) vulnerability in includes/flvthumbnail.php in the Youtube Gallery (com_youtubegallery) component 3.4.0 for Joomla! allows remote attackers to inject arbitrary web script or HTML via the videofile parameter.
CVE-2014-0760
The Festo CECX-X-C1 Modular Master Controller with CoDeSys and CECX-X-M1 Modular Controller with CoDeSys and SoftMotion provide an undocumented access method involving the FTP protocol, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.
CVE-2014-0769
The Festo CECX-X-C1 Modular Master Controller with CoDeSys and CECX-X-M1 Modular Controller with CoDeSys and SoftMotion do not require authentication for connections to certain TCP ports, which allows remote attackers to (1) modify the configuration via a request to the debug service on port 4000 or (2) delete log entries via a request to the log service on port 4001.
CVE-2014-0780
Directory traversal vulnerability in NTWebServer in InduSoft Web Studio 7.1 before SP2 Patch 4 allows remote attackers to read administrative passwords in APP files, and consequently execute arbitrary code, via unspecified web requests.
CVE-2014-2579
Multiple cross-site request forgery (CSRF) vulnerabilities in XCloner Standalone 3.5 and earlier allow remote attackers to hijack the authentication of administrators for requests that (1) change the administrator password via the config task to index2.php or (2) when the enable_db_backup and sql_mem options are enabled, access the database backup functionality via the dbbackup_comp parameter in the generate action to index2.php.  NOTE: vector 2 might be a duplicate of CVE-2014-2340, which is for the XCloner Wordpress plugin.  NOTE: remote attackers can leverage CVE-2014-2996 with vector 2 to execute arbitrary commands.
CVE-2014-2729
Cross-site scripting (XSS) vulnerability in content.aspx in Ektron CMS 8.7 before 8.7.0.055 allows remote authenticated users to inject arbitrary web script or HTML via the category0 parameter, which is not properly handled when displaying the Subjects tab in the View Properties menu option.
CVE-2014-2908
Cross-site scripting (XSS) vulnerability in the integrated web server on Siemens SIMATIC S7-1200 CPU devices 2.x and 3.x allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-2909
CRLF injection vulnerability in the integrated web server on Siemens SIMATIC S7-1200 CPU devices 2.x and 3.x allows remote attackers to inject arbitrary HTTP headers via unspecified vectors.
CVE-2014-2984
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2014-2650.  Reason: This candidate is a reservation duplicate of CVE-2014-2650.  Notes: All CVE users should reference CVE-2014-2650 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2014-2996
XCloner Standalone 3.5 and earlier, when enable_db_backup and sql_mem are enabled, allows remote authenticated administrators to execute arbitrary commands via shell metacharacters in the dbbackup_comp parameter in a generate action to index2.php.  NOTE: it is not clear whether this issue crosses privilege boundaries, since administrators might already have the privileges to execute code.  NOTE: this can be leveraged by remote attackers using CVE-2014-2579.
