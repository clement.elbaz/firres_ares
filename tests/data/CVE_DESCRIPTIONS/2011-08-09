CVE-2008-7292
Bugzilla 2.20.x before 2.20.5, 2.22.x before 2.22.3, and 3.0.x before 3.0.3 on Windows does not delete the temporary files associated with uploaded attachments, which allows local users to obtain sensitive information by reading these files, a different vulnerability than CVE-2011-2977.
CVE-2008-7293
Mozilla Firefox before 4 cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2008-7294
Google Chrome before 4.0.211.0 cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2008-7295
Microsoft Internet Explorer cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2008-7296
Apple Safari cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2008-7297
Opera cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2008-7298
The Android browser in Android cannot properly restrict modifications to cookies established in HTTPS sessions, which allows man-in-the-middle attackers to overwrite or delete arbitrary cookies via a Set-Cookie header in an HTTP response, related to lack of the HTTP Strict Transport Security (HSTS) includeSubDomains feature, aka a "cookie forcing" issue.
CVE-2011-2221
The Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 allows remote attackers to bypass WebAdmin authentication and obtain sensitive GroupWise information via unspecified vectors.
CVE-2011-2222
Session fixation vulnerability in WebAdmin in the Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html
'CWE-384: Session Fixation'
CVE-2011-2223
The Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 sends the Admin LDAP password in cleartext, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2011-2224
The Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 does not include the HTTPOnly flag in a Set-Cookie header, which makes it easier for remote attackers to conduct cross-site scripting (XSS) attacks via unspecified vectors.
CVE-2011-2379
Cross-site scripting (XSS) vulnerability in Bugzilla 2.4 through 2.22.7, 3.0.x through 3.3.x, 3.4.x before 3.4.12, 3.5.x, 3.6.x before 3.6.6, 3.7.x, 4.0.x before 4.0.2, and 4.1.x before 4.1.3, when Internet Explorer before 9 or Safari before 5.0.6 is used for Raw Unified mode, allows remote attackers to inject arbitrary web script or HTML via a crafted patch, related to content sniffing.
CVE-2011-2380
Bugzilla 2.23.3 through 2.22.7, 3.0.x through 3.3.x, 3.4.x before 3.4.12, 3.5.x, 3.6.x before 3.6.6, 3.7.x, 4.0.x before 4.0.2, and 4.1.x before 4.1.3 allows remote attackers to determine the existence of private group names via a crafted parameter during (1) bug creation or (2) bug editing.
CVE-2011-2381
CRLF injection vulnerability in Bugzilla 2.17.1 through 2.22.7, 3.0.x through 3.3.x, 3.4.x before 3.4.12, 3.5.x, 3.6.x before 3.6.6, 3.7.x, 4.0.x before 4.0.2, and 4.1.x before 4.1.3 allows remote attackers to inject arbitrary e-mail headers via an attachment description in a flagmail notification.
CVE-2011-2589
Heap-based buffer overflow in the SendLogAction method in the UUPlayer ActiveX control 6.0.0.1 in UUSee 2010 6.11.0609.2 might allow remote attackers to execute arbitrary code via a long argument.
CVE-2011-2590
The Play method in the UUPlayer ActiveX control 6.0.0.1 in UUSee 2010 6.11.0609.2 allows remote attackers to execute arbitrary programs via a UNC share pathname in the MPlayerPath parameter.
CVE-2011-2976
Cross-site scripting (XSS) vulnerability in Bugzilla 2.16rc1 through 2.22.7, 3.0.x through 3.3.x, and 3.4.x before 3.4.12 allows remote attackers to inject arbitrary web script or HTML via vectors involving a BUGLIST cookie.
CVE-2011-2977
Bugzilla 3.6.x before 3.6.6, 3.7.x, 4.0.x before 4.0.2, and 4.1.x before 4.1.3 on Windows does not delete the temporary files associated with uploaded attachments, which allows local users to obtain sensitive information by reading these files.  NOTE: this issue exists because of a regression in 3.6.
CVE-2011-2978
Bugzilla 2.16rc1 through 2.22.7, 3.0.x through 3.3.x, 3.4.x before 3.4.12, 3.5.x, 3.6.x before 3.6.6, 3.7.x, 4.0.x before 4.0.2, and 4.1.x before 4.1.3 does not prevent changes to the confirmation e-mail address (aka old_email field) for e-mail change notifications, which makes it easier for remote attackers to perform arbitrary address changes by leveraging an unattended workstation.
CVE-2011-2979
Bugzilla 4.1.x before 4.1.3 generates different responses for certain assignee queries depending on whether the group name is valid, which allows remote attackers to determine the existence of private group names via a custom search.  NOTE: this vulnerability exists because of a CVE-2010-2756 regression.
CVE-2011-3012
The ioQuake3 engine, as used in World of Padman 1.2 and earlier, Tremulous 1.1.0, and ioUrbanTerror 2007-12-20, does not check for dangerous file extensions before writing to the quake3 directory, which allows remote attackers to execute arbitrary code via a crafted third-party addon that creates a Trojan horse DLL file, a different vulnerability than CVE-2011-2764.
CVE-2011-3013
WebAdmin in the Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 supports weak SSL ciphers, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2011-3014
The Mobility Pack before 1.2 in Novell Data Synchronizer 1.x through 1.1.2 build 428 does not properly restrict caching of HTTPS responses, which makes it easier for remote attackers to obtain sensitive information by leveraging an unattended workstation.
