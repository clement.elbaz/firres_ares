CVE-2007-3749
The kernel in Apple Mac OS X 10.4 through 10.4.10 does not reset the current Mach Thread Port or Thread Exception Port when executing a setuid program, which allows local users to execute arbitrary code by creating the port before launching the setuid program, then writing to the address space of the setuid process.
CVE-2007-4267
Stack-based buffer overflow in the Networking component in Apple Mac OS X 10.4 through 10.4.10 allows local users to execute arbitrary code via a crafted IOCTL request that adds an AppleTalk zone to a routing table.
"By sending a maliciously crafted ioctl request to an
AppleTalk socket, a local user may cause an unexpected system
shutdown or arbitrary code execution with system privileges."
CVE-2007-4268
Integer signedness error in the Networking component in Apple Mac OS X 10.4 through 10.4.10 allows local users to execute arbitrary code via a crafted AppleTalk message with a negative value, which satisfies a signed comparison during mbuf allocation but is later interpreted as an unsigned value, which triggers a heap-based buffer overflow.
"By sending a maliciously crafted AppleTalk message, a local user may
cause an unexpected system shutdown or arbitrary code execution with
system privileges."
CVE-2007-4269
Integer overflow in the Networking component in Apple Mac OS X 10.4 through 10.4.10 allows local users to execute arbitrary code via a crafted AppleTalk Session Protocol (ASP) message on an AppleTalk socket, which triggers a heap-based buffer overflow.
CVE-2007-4344
Multiple input validation errors in ACD ACDSee Photo Manager 9.0 build 108, Pro Photo Manager 8.1 build 99, and Photo Editor 4.0 build 195 allow user-assisted remote attackers to execute arbitrary code via a long section string in (1) a PSP image to the ID_PSP.apl plug-in or (2) an LHA archive to the AM_LHA.apl plug-in, resulting in a heap-based buffer overflow.
CVE-2007-4678
AppleRAID in Apple Mac OS X 10.3.9 and 10.4 through 10.4.10 allows attackers to cause a denial of service (crash) via a crafted striped disk image, which triggers a NULL pointer dereference when it is mounted.
CVE-2007-4679
CFFTP in CFNetwork for Apple Mac OS X 10.4 through 10.4.10 allows remote FTP servers to force clients to connect to other hosts via crafted responses to FTP PASV commands.
CVE-2007-4680
CFNetwork in Apple Mac OS X 10.3.9 and 10.4 through 10.4.10 does not properly validate certificates, which allows remote attackers to spoof trusted SSL certificates via a man-in-the-middle attack.
CVE-2007-4681
Buffer overflow in CoreFoundation in Apple Mac OS X 10.3.9 and 10.4 through 10.4.10 allows local users to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted directory hierarchy.
CVE-2007-4682
CoreText in Apple Mac OS X 10.4 through 10.4.10 allows attackers to cause a denial of service (application crash) and possibly execute arbitrary code via crafted text content that triggers an access of an uninitialized object pointer.
CVE-2007-4683
Directory traversal vulnerability in the kernel in Apple Mac OS X 10.4 through 10.4.10 allows local users to bypass the chroot mechanism via a relative path when changing the current working directory.
CVE-2007-4684
Integer overflow in the kernel in Apple Mac OS X 10.4 through 10.4.10 allows local users to execute arbitrary code via a large num_sels argument to the i386_set_ldt system call.
CVE-2007-4685
The kernel in Apple Mac OS X 10.4 through 10.4.10 allows local users to gain privileges by executing setuid or setgid programs in which the stdio, stderr, or stdout file descriptors are "in an unexpected state."
CVE-2007-4686
Integer signedness error in the ttioctl function in bsd/kern/tty.c in the xnu kernel in Apple Mac OS X 10.4 through 10.4.10 allows local users to cause a denial of service (system shutdown) or gain privileges via a crafted TIOCSETD ioctl request.
CVE-2007-4687
The remote_cmds component in Apple Mac OS X 10.4 through 10.4.10 contains a symbolic link from the tftpboot private directory to the root directory, which allows tftpd users to escape the private directory and access arbitrary files.
CVE-2007-4688
The Networking component in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to obtain all addresses for a host, including link-local addresses, via a Node Information Query.
CVE-2007-4689
Double free vulnerability in the Networking component in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to cause a denial of service (system shutdown) or execute arbitrary code via crafted IPV6 packets.
CVE-2007-4690
Double free vulnerability in the NFS component in Apple Mac OS X 10.4 through 10.4.10 allows remote authenticated users to execute arbitrary code via a crafted AUTH_UNIX RPC packet.
CVE-2007-4691
The NSURL component in Apple Mac OS X 10.4 through 10.4.10 performs case-sensitive comparisons that allow attackers to bypass intended restrictions for local file system URLs.
CVE-2007-4692
The tabbed browsing feature in Apple Safari 3 before Beta Update 3.0.4 on Windows, and Mac OS X 10.4 through 10.4.10, allows remote attackers to spoof HTTP authentication for other sites and possibly conduct phishing attacks by causing an authentication sheet to be displayed for a tab that is not active, which makes it appear as if it is associated with the active tab.
CVE-2007-4693
The SecurityAgent component in Mac OS X 10.4 through 10.4.10 allows attackers with physical access to bypass the authentication dialog of the screen saver and send keystrokes to a process, related to "handling of keyboard focus between secure text fields."
CVE-2007-4694
Safari in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to access local content via file:// URLs.
CVE-2007-4695
Unspecified "input validation" vulnerability in WebCore in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to modify form field values via unknown vectors related to file uploads.
CVE-2007-4696
Race condition in WebCore in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to obtain information for forms from other sites via unknown vectors related to "page transitions" in Safari.
CVE-2007-4697
Unspecified vulnerability in WebCore in Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to cause a denial of service (application termination) or execute arbitrary code via unknown vectors related to browser history, which triggers memory corruption.
CVE-2007-4698
Apple Safari 3 before Beta Update 3.0.4 on Windows, and Mac OS X 10.4 through 10.4.10, allows remote attackers to conduct cross-site scripting (XSS) attacks by causing JavaScript events to be associated with the wrong frame.
CVE-2007-4699
The default configuration of Safari in Apple Mac OS X 10.4 through 10.4.10 adds a private key to the keychain with permissions that allow other applications to access the key without warning the user, which might allow other applications to bypass intended access restrictions.
CVE-2007-4700
Unspecified vulnerability in WebKit on Apple Mac OS X 10.4 through 10.4.10 allows remote attackers to use Safari as an indirect proxy and send attacker-controlled data to arbitrary TCP ports via unknown vectors.
CVE-2007-4701
WebKit on Apple Mac OS X 10.4 through 10.4.10 does not create temporary files securely when Safari is previewing a PDF file, which allows local users to read the contents of that file.
CVE-2007-4702
The Application Firewall in Apple Mac OS X 10.5, when "Block all incoming connections" is enabled, does not prevent root processes or mDNSResponder from accepting connections, which might allow remote attackers or local root processes to bypass intended access restrictions.
CVE-2007-4703
The Application Firewall in Apple Mac OS X 10.5 does not prevent a root process from accepting incoming connections, even when "Block incoming connections" has been set for its associated executable, which might allow remote attackers or local root processes to bypass intended access restrictions.
CVE-2007-4704
The Application Firewall in Apple Mac OS X 10.5 does not apply changed settings to processes that are started by launchd until the processes are restarted, which might allow attackers to bypass intended access restrictions.
CVE-2007-5501
The tcp_sacktag_write_queue function in net/ipv4/tcp_input.c in Linux kernel 2.6.21 through 2.6.23.7, and 2.6.24-rc through 2.6.24-rc2, allows remote attackers to cause a denial of service (crash) via crafted ACK responses that trigger a NULL pointer dereference.
CVE-2007-5905
Adobe ColdFusion 8 and MX 7 allows remote attackers to hijack sessions via unspecified vectors that trigger establishment of a session to a ColdFusion application in which the (1) CFID or (2) CFTOKEN cookies have empty values, possibly due to a session fixation vulnerability.
CVE-2007-5973
SQL injection vulnerability in articles.php in JPortal 2.3.1 and earlier allows remote attackers to execute arbitrary SQL commands via the topic parameter.
CVE-2007-5974
SQL injection vulnerability in mailer.php in JPortal 2 allows remote attackers to execute arbitrary SQL commands via the to parameter.
CVE-2007-5975
SQL injection vulnerability in index.php in TBSource, as used in (1) TBDev and (2) TorrentStrike 0.4, allows remote authenticated users to execute arbitrary SQL commands via the choice parameter.  NOTE: some of these details are obtained from third party information.
CVE-2007-5976
SQL injection vulnerability in db_create.php in phpMyAdmin before 2.11.2.1 allows remote authenticated users with CREATE DATABASE privileges to execute arbitrary SQL commands via the db parameter.
CVE-2007-5977
Cross-site scripting (XSS) vulnerability in db_create.php in phpMyAdmin before 2.11.2.1 allows remote authenticated users with CREATE DATABASE privileges to inject arbitrary web script or HTML via a hex-encoded IMG element in the db parameter in a POST request, a different vulnerability than CVE-2006-6942.
CVE-2007-5978
SQL injection vulnerability in brokenlink.php in the mylinks module for XOOPS allows remote attackers to execute arbitrary SQL commands via the lid parameter.
CVE-2007-5979
Cross-site scripting (XSS) vulnerability in download_plugin.php3 in F5 Firepass 4100 SSL VPN 5.4 through 5.5.2 and 6.0 through 6.0.1 allows remote attackers to inject arbitrary web script or HTML via the backurl parameter.
CVE-2007-5980
Cross-site scripting (XSS) vulnerability in home/rss.php in eggblog before 3.1.1 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO (PHP_SELF).
CVE-2007-5981
Lantronix SCS3200 does not properly handle public-key requests, which allows remote attackers to cause a denial of service (unresponsive device) via unspecified keyscan requests.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5982
Multiple cross-site scripting (XSS) vulnerabilities in X7 Chat 2.0.4, 2.0.5, and possibly other versions allow remote attackers to inject arbitrary web script or HTML via the (1) room parameter to sources/frame.php, the (2) theme_c parameter to help/index.php, or the (3) INSTALL_X7CHATVERSION parameter to upgradev1.php.
CVE-2007-5983
Cross-site scripting (XSS) vulnerability in index.php in Justin Hagstrom AutoIndex PHP Script before 2.2.3 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO (PHP_SELF).
CVE-2007-5984
classes/Url.php in Justin Hagstrom AutoIndex PHP Script before 2.2.4 allows remote attackers to cause a denial of service (CPU and memory consumption) via a %00 sequence in the dir parameter to index.php, which triggers an erroneous "recursive calculation."
CVE-2007-5985
Multiple cross-site scripting (XSS) vulnerabilities in BtiTracker before 1.4.5 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors to (1) account.php, (2) moresmiles.php, or (3) recover.php; or (4) the "to" parameter to usercp.php.
CVE-2007-5986
SQL injection vulnerability in include/functions.php in BtiTracker before 1.4.5 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2007-5987
details.php in BtiTracker before 1.4.5, when torrent viewing is disabled for guests, allows remote attackers to bypass protection mechanisms via a direct request, as demonstrated by (1) reading the details of an arbitrary torrent and (2) modifying a torrent owned by a guest.
CVE-2007-5988
blocks/shoutbox_block.php in BtiTracker 1.4.4 does not verify user accounts, which allows remote attackers to post shoutbox entries as arbitrary users via a modified nick field.
CVE-2007-5990
Cross-site scripting (XSS) vulnerability in ExoPHPdesk allows remote attackers to inject arbitrary web script or HTML via unspecified vectors in a user profile, possibly the (1) name and (2) website parameters to register.php.
CVE-2007-5991
SQL injection vulnerability in index.php in ExoPHPdesk allows remote attackers to execute arbitrary SQL commands via the user parameter in a profile fn action.
CVE-2007-5992
SQL injection vulnerability in index.php in datecomm Social Networking Script (aka Myspace Clone Script) allows remote attackers to execute arbitrary SQL commands via the seid parameter in a viewcat s action on the forums page.
CVE-2007-5993
Cross-site scripting (XSS) vulnerability in Visionary Technology in Library Solutions (VTLS) vtls.web.gateway before 48.1.1 allows remote attackers to inject arbitrary web script or HTML via the searchtype parameter.
CVE-2007-5994
PHP remote file inclusion vulnerability in check_noimage.php in Fritz Berger yet another php photo album - next generation (yappa-ng) 2.3.2 allows remote attackers to execute arbitrary PHP code via a URL in the config[path_src_include] parameter.
CVE-2007-5995
PHP remote file inclusion vulnerability in examples/patExampleGen/bbcodeSource.php in patBBcode 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the example parameter.
CVE-2007-5996
SQL injection vulnerability in searchresult.php in Softbiz Link Directory Script allows remote attackers to execute arbitrary SQL commands via the sbcat_id parameter, a related issue to CVE-2007-5449.
CVE-2007-5997
SQL injection vulnerability in campaign_stats.php in Softbiz Banner Exchange Network Script 1.0 allows remote authenticated users to execute arbitrary SQL commands via the id parameter.
CVE-2007-5998
SQL injection vulnerability in ads.php in Softbiz Ad Management plus Script 1 allows remote authenticated users to execute arbitrary SQL commands via the package parameter.
CVE-2007-5999
SQL injection vulnerability in product_desc.php in Softbiz Auctions Script allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-6000
KDE Konqueror 3.5.6 and earlier allows remote attackers to cause a denial of service (crash) via large HTTP cookie parameters.
CVE-2007-6001
Multiple cross-site scripting (XSS) vulnerabilities in index.php in Bandersnatch 0.4 allow remote attackers to inject arbitrary web script or HTML via the (1) func or (2) date parameter, or the jid parameter in a (3) log or (4) user action, a different vulnerability than CVE-2007-3910.
CVE-2007-6002
Cross-site scripting (XSS) vulnerability in Fenriru (1) Sleipnir 2.5.17 R2 and earlier and (2) Grani 3.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the Search field in a search for additions to the Favorites section.
CVE-2007-6003
Cross-site scripting (XSS) vulnerability in cgi/b/ic/connect in the Thomson SpeedTouch 716 with firmware 5.4.0.14 allows remote attackers to inject arbitrary web script or HTML via the url parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-6004
Multiple SQL injection vulnerabilities in index.php in Toko Instan 7.6 allow remote attackers to execute arbitrary SQL commands via (1) the id parameter in an artikel action or (2) the katid parameter in a produk action.
CVE-2007-6005
Unspecified vulnerability in the GpcContainer.GpcContainer.1 ActiveX control in WebEx allows remote attackers to cause a denial of service (memory access violation and crash) via (1) an invalid argument to the InitParam method or (2) an unspecified vector involving the SetParam method.
CVE-2007-6006
TestLink before 1.7.1 does not enforce an unspecified authorization mechanism, which has unknown impact and attack vectors.
CVE-2007-6007
Integer overflow in the ID_PSP.apl plug-in for ACD ACDSee Photo Manager 9.0 build 108, Pro Photo Manager 8.1 build 99, and Photo Editor 4.0 build 195 allows user-assisted remote attackers to execute arbitrary code via a crafted PSP image that triggers a heap-based buffer overflow.
CVE-2007-6008
Heap-based buffer overflow in emlsr.dll before 2.0.0.4 in Autonomy (formerly Verity) KeyView Viewer, Filter, and Export SDK allows remote attackers to execute arbitrary code via a long Content-Type header line in an EML file.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-6009
Multiple buffer overflows in ACD products allow user-assisted remote attackers to execute arbitrary code via a long section string in a (1) XBM or (2) XPM file to (a) ID_X.apl or (b) IDE_ACDStd.apl.  NOTE: the PSP and LHA vectors are already covered by CVE-2007-4344 and CVE-2007-6007.  NOTE: these might be integer overflows rather than buffer overflows.
CVE-2007-6010
Unspecified vulnerability in pioneers (formerly gnocatan) 0.11.3 allows remote attackers to cause a denial of service (daemon crash) via unspecified vectors that trigger an assert error.  NOTE: this issue reportedly exists because of an incomplete fix for CVE-2007-5933.
