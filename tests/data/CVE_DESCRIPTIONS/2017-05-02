CVE-2014-9940
The regulator_ena_gpio_free function in drivers/regulator/core.c in the Linux kernel before 3.19 allows local users to gain privileges or cause a denial of service (use-after-free) via a crafted application.
CVE-2015-8257
The devtools.sh script in AXIS network cameras allows remote authenticated users to execute arbitrary commands via shell metacharacters in the app parameter to (1) app_license.shtml, (2) app_license_custom.shtml, (3) app_index.shtml, or (4) app_params.shtml.
CVE-2015-9004
kernel/events/core.c in the Linux kernel before 3.19 mishandles counter grouping, which allows local users to gain privileges via a crafted application, related to the perf_pmu_register and perf_event_open functions.
CVE-2016-10243
TeX Live allows remote attackers to execute arbitrary commands by leveraging inclusion of mpost in shell_escape_commands in the texmf.cnf config file.
CVE-2016-4442
The rack-mini-profiler gem before 0.10.1 for Ruby allows remote attackers to obtain sensitive information about allocated strings and objects by leveraging incorrect ordering of security checks.
CVE-2016-4467
The C client and C-based client bindings in the Apache Qpid Proton library before 0.13.1 on Windows do not properly verify that the server hostname matches a domain name in the subject's Common Name (CN) or subjectAltName field of the X.509 certificate when using the SChannel-based security layer, which allows man-in-the-middle attackers to spoof servers via an arbitrary valid certificate.
CVE-2016-5006
The Cloud Controller in Cloud Foundry before 239 logs user-provided service objects at creation, which allows attackers to obtain sensitive user credential information via unspecified vectors.
CVE-2016-5063
The RSCD agent in BMC Server Automation before 8.6 SP1 Patch 2 and 8.7 before Patch 3 on Windows might allow remote attackers to bypass authorization checks and make an RPC call via unspecified vectors.
CVE-2016-5810
upAdminPg.asp in Advantech WebAccess before 8.1_20160519 allows remote authenticated administrators to obtain sensitive password information via unspecified vectors.
CVE-2017-0331
An elevation of privilege vulnerability in the NVIDIA video driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as Critical due to the possibility of a local permanent device compromise, which may require reflashing the operating system to repair the device. Product: Android. Versions: Kernel 3.10. Android ID: A-34113000. References: N-CVE-2017-0331.
CVE-2017-5689
An unprivileged network attacker could gain system privileges to provisioned Intel manageability SKUs: Intel Active Management Technology (AMT) and Intel Standard Manageability (ISM). An unprivileged local attacker could provision manageability features gaining unprivileged network or local system privileges on Intel manageability SKUs: Intel Active Management Technology (AMT), Intel Standard Manageability (ISM), and Intel Small Business Technology (SBT).
CVE-2017-6551
Pexip Infinity before 14.2 allows remote attackers to cause a denial of service (service restart) or execute arbitrary code via vectors related to Conferencing Nodes.
CVE-2017-7216
The Management Web Interface in Palo Alto Networks PAN-OS before 7.1.9 allows remote authenticated users to obtain sensitive information via unspecified request parameters.
CVE-2017-7440
Kerio Connect 8.0.0 through 9.2.2, and Kerio Connect Client desktop application for Windows and Mac 9.2.0 through 9.2.2, when e-mail preview is enabled, allows remote attackers to conduct clickjacking attacks via a crafted e-mail message.
CVE-2017-7476
Gnulib before 2017-04-26 has a heap-based buffer overflow with the TZ environment variable. The error is in the save_abbr function in time_rz.c.
CVE-2017-7483
Rxvt 2.7.10 is vulnerable to a denial of service attack by passing the value -2^31 inside a terminal escape code, which results in a non-invertible integer that eventually leads to a segfault due to an out of bounds read.
CVE-2017-8086
Memory leak in the v9fs_list_xattr function in hw/9pfs/9p-xattr.c in QEMU (aka Quick Emulator) allows local guest OS privileged users to cause a denial of service (memory consumption) via vectors involving the orig_value variable.
CVE-2017-8112
hw/scsi/vmw_pvscsi.c in QEMU (aka Quick Emulator) allows local guest OS privileged users to cause a denial of service (infinite loop and CPU consumption) via the message ring page count.
CVE-2017-8418
RuboCop 0.48.1 and earlier does not use /tmp in safe way, allowing local users to exploit this to tamper with cache files belonging to other users.
CVE-2017-8419
LAME through 3.99.5 relies on the signed integer data type for values in a WAV or AIFF header, which allows remote attackers to cause a denial of service (stack-based buffer overflow or heap-based buffer overflow) or possibly have unspecified other impact via a crafted file, as demonstrated by mishandling of num_channels.
CVE-2017-8421
The function coff_set_alignment_hook in coffcode.h in Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.28, has a memory leak vulnerability which can cause memory exhaustion in objdump via a crafted PE file. Additional validation in dump_relocs_in_section in objdump.c can resolve this.
