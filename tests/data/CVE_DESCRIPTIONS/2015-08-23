CVE-2015-1992
IBM Systems Director 5.2.x, 6.1.x, 6.2.0.x, 6.2.1.x, 6.3.0.0, 6.3.1.x, 6.3.2.x, 6.3.3.x, 6.3.5.0, and 6.3.6.0 improperly processes events, which allows local users to gain privileges via unspecified vectors.
CVE-2015-2014
Open redirect vulnerability in the web server in IBM Domino 8.5 before 8.5.3 FP6 IF9 and 9.0 before 9.0.1 FP4 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks or cross-site scripting (XSS) attacks via a crafted URL, aka SPR SJAR9DNGDA.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-2015
Cross-site scripting (XSS) vulnerability in pubnames.ntf (aka the Directory template) in the web server in IBM Domino before 9.0.0 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka SPR KLYH8WBPRN.
CVE-2015-2018
IBM Integration Bus 9 and 10 before 10.0.0.1 and WebSphere Message Broker 7 before 7.0.0.8 and 8 before 8.0.0.7 do not ensure that the correct security profile is selected, which allows remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2015-2872
Multiple cross-site scripting (XSS) vulnerabilities in Trend Micro Deep Discovery Inspector (DDI) on Deep Discovery Threat appliances with software before 3.5.1477, 3.6.x before 3.6.1217, 3.7.x before 3.7.1248, 3.8.x before 3.8.1263, and other versions allow remote attackers to inject arbitrary web script or HTML via (1) crafted input to index.php that is processed by certain Internet Explorer 7 configurations or (2) crafted input to the widget feature.
CVE-2015-2873
Trend Micro Deep Discovery Inspector (DDI) on Deep Discovery Threat appliances with software before 3.5.1477, 3.6.x before 3.6.1217, 3.7.x before 3.7.1248, 3.8.x before 3.8.1263, and other versions allows remote attackers to obtain sensitive information or change the configuration via a direct request to the (1) system log URL, (2) whitelist URL, or (3) blacklist URL.
<a href="http://cwe.mitre.org/data/definitions/425.html" rel="nofollow">CWE-425: Direct Request ('Forced Browsing')</a>
CVE-2015-2904
Actiontec GT784WN modems with firmware before NCS01-1.0.13 have hardcoded credentials, which makes it easier for remote attackers to obtain root access by connecting to the web administration interface.
<a href="http://cwe.mitre.org/data/definitions/798.html" target="_blank">CWE-798: Use of Hard-coded Credentials</a>
CVE-2015-2905
Cross-site request forgery (CSRF) vulnerability on Actiontec GT784WN modems with firmware before NCS01-1.0.13 allows remote attackers to hijack the authentication or intranet connectivity of arbitrary users.
CVE-2015-2906
** DISPUTED ** Mobile Devices (aka MDI) C4 OBD-II dongles with firmware 2.x and 3.4.x, as used in Metromile Pulse and other products, store SSH private keys that are the same across different customers' installations, which makes it easier for remote attackers to obtain access by leveraging knowledge of a private key from another installation.  NOTE: the vendor states "This was a flaw for the developer/debugging devices (again not possible in production versions)."
<a href="https://cwe.mitre.org/data/definitions/321.html" target="_blank">CWE-321: Use of Hard-coded Cryptographic Key</a>
CVE-2015-2907
** DISPUTED ** Mobile Devices (aka MDI) C4 OBD-II dongles with firmware 2.x and 3.4.x, as used in Metromile Pulse and other products, have hardcoded SSH credentials, which makes it easier for remote attackers to obtain access by leveraging knowledge of the required username and password.  NOTE: the vendor states "This was a flaw for the developer/debugging devices (again not possible in production versions)."
<a href="http://cwe.mitre.org/data/definitions/798.html" target="_blank">CWE-798: Use of Hard-coded Credentials</a>
CVE-2015-2908
** DISPUTED ** Mobile Devices (aka MDI) C4 OBD-II dongles with firmware 2.x and 3.4.x, as used in Metromile Pulse and other products, do not validate firmware updates, which allows remote attackers to execute arbitrary code by specifying an update server.  NOTE: the vendor states "This was a flaw for the developer/debugging devices, and was fixed in production version about 3 years ago."
CVE-2015-4949
IBM Tivoli Storage Manager for Databases: Data Protection for Microsoft SQL Server 7.1 before 7.1.2, Tivoli Storage Manager for Mail: Data Protection for Microsoft Exchange Server 7.1 before 7.1.2, and Tivoli Storage FlashCopy Manager 4.1 before 4.1.2 place cleartext passwords in exception messages, which allows physically proximate attackers to obtain sensitive information by reading GUI pop-up windows, a different vulnerability than CVE-2015-6557.
CVE-2015-4950
The mailbox-restore feature in IBM Tivoli Storage Manager for Mail: Data Protection for Microsoft Exchange Server 6.1 before 6.1.3.6, 6.3 before 6.3.1.3, 6.4 before 6.4.1.4, and 7.1 before 7.1.0.2; Tivoli Storage FlashCopy Manager: FlashCopy Manager for Microsoft Exchange Server 2.1, 2.2, 3.1 before 3.1.1.5, 3.2 before 3.2.1.7, and 4.1 before 4.1.1; and Tivoli Storage Manager FastBack for Microsoft Exchange 6.1 before 6.1.5.4 does not ensure that the correct mailbox is selected, which allows remote authenticated users to obtain sensitive information via a duplicate alias name.
CVE-2015-6557
IBM Tivoli Storage Manager for Databases: Data Protection for Microsoft SQL Server 5.5 before 5.5.6.1, 6.3 before 6.3.1.5, 6.4 before 6.4.1.7, and 7.1 before 7.1.2; Tivoli Storage Manager for Mail: Data Protection for Microsoft Exchange Server 5.5 before 5.5.1.1, 6.1 before 6.1.3.7, 6.3 before 6.3.1.5, 6.4 before 6.4.1.7, and 7.1 before 7.1.2; and Tivoli Storage FlashCopy Manager 3.1 before 3.1.1.5, 3.2 before 3.2.1.7, and 4.1 before 4.1.2, when application tracing is used, place cleartext passwords in exception messages, which allows physically proximate attackers to obtain sensitive information by reading trace output, a different vulnerability than CVE-2015-4949.
