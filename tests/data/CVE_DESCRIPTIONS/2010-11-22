CVE-2010-3037
goform/websXMLAdminRequestCgi.cgi in Cisco Unified Videoconferencing (UVC) System 5110 and 5115, and possibly Unified Videoconferencing System 3545 and 5230, Unified Videoconferencing 3527 Primary Rate Interface (PRI) Gateway, Unified Videoconferencing 3522 Basic Rate Interfaces (BRI) Gateway, and Unified Videoconferencing 3515 Multipoint Control Unit (MCU), allows remote authenticated administrators to execute arbitrary commands via the username field, related to a "shell command injection vulnerability," aka Bug ID CSCti54059.
CVE-2010-3038
Cisco Unified Videoconferencing (UVC) System 5110 and 5115, when the Linux operating system is used, has a default password for the (1) root, (2) cs, and (3) develop accounts, which makes it easier for remote attackers to obtain access via the (a) FTP or (b) SSH daemon, aka Bug ID CSCti54008.
CVE-2010-3432
The sctp_packet_config function in net/sctp/output.c in the Linux kernel before 2.6.35.6 performs extraneous initializations of packet data structures, which allows remote attackers to cause a denial of service (panic) via a certain sequence of SCTP traffic.
CVE-2010-3618
PGP Desktop 10.0.x before 10.0.3 SP2 and 10.1.0 before 10.1.0 SP1 does not properly implement the "Decrypt/Verify File via Right-Click" functionality for multi-packet OpenPGP messages that represent multi-message input, which allows remote attackers to spoof signed data by concatenating an additional message to the end of a legitimately signed message, related to a "piggy-back" or "unsigned data injection" issue.
CVE-2010-3803
Integer overflow in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted string.
CVE-2010-3804
The JavaScript implementation in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, uses a weak algorithm for generating values of random numbers, which makes it easier for remote attackers to track a user by predicting a value, a related issue to CVE-2008-5913 and CVE-2010-3171.
CVE-2010-3805
Integer underflow in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving WebSockets.  NOTE: this may overlap CVE-2010-3254.
CVE-2010-3808
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly perform a cast of an unspecified variable during processing of editing commands, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3809
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly perform a cast of an unspecified variable during processing of inline styling, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3810
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly handle the History object, which allows remote attackers to spoof the location bar's URL or add URLs to the history via a cross-origin attack.
CVE-2010-3811
Use-after-free vulnerability in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving element attributes.
CVE-2010-3812
Integer overflow in the Text::wholeText method in dom/Text.cpp in WebKit, as used in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4; webkitgtk before 1.2.6; and possibly other products allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving Text objects.
CVE-2010-3813
The WebCore::HTMLLinkElement::process function in WebCore/html/HTMLLinkElement.cpp in WebKit, as used in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4; webkitgtk before 1.2.6; and possibly other products does not verify whether DNS prefetching is enabled when processing an HTML LINK element, which allows remote attackers to bypass intended access restrictions, as demonstrated by an HTML e-mail message that uses a LINK element for X-Confirm-Reading-To functionality.
CVE-2010-3816
Use-after-free vulnerability in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving scrollbars.
CVE-2010-3817
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly perform a cast of an unspecified variable during processing of Cascading Style Sheets (CSS) 3D transforms, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3818
Use-after-free vulnerability in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving inline text boxes.
CVE-2010-3819
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly perform a cast of an unspecified variable during processing of Cascading Style Sheets (CSS) boxes, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3820
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, accesses uninitialized memory during processing of editable elements, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3821
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly handle the :first-letter pseudo-element in a Cascading Style Sheets (CSS) token sequence, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2010-3822
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, accesses an uninitialized pointer during processing of Cascading Style Sheets (CSS) counter styles, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3823
Use-after-free vulnerability in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving Geolocation objects.  NOTE: this might overlap CVE-2010-3415.
CVE-2010-3824
Use-after-free vulnerability in WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving SVG use elements.
CVE-2010-3826
WebKit in Apple Safari before 5.0.3 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1.3 on Mac OS X 10.4, does not properly perform a cast of an unspecified variable during processing of colors in an SVG document, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted web site.
CVE-2010-3872
The fcgid_header_bucket_read function in fcgid_bucket.c in the mod_fcgid module before 2.3.6 for the Apache HTTP Server does not use bytewise pointer arithmetic in certain circumstances, which has unspecified impact and attack vectors related to "untrusted FastCGI applications" and a "stack buffer overwrite."
CVE-2010-4165
The do_tcp_setsockopt function in net/ipv4/tcp.c in the Linux kernel before 2.6.37-rc2 does not properly restrict TCP_MAXSEG (aka MSS) values, which allows local users to cause a denial of service (OOPS) via a setsockopt call that specifies a small value, leading to a divide-by-zero error or incorrect use of a signed integer.
CVE-2010-4167
Untrusted search path vulnerability in configure.c in ImageMagick before 6.6.5-5, when MAGICKCORE_INSTALLED_SUPPORT is defined, allows local users to gain privileges via a Trojan horse configuration file in the current working directory.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-4169
Use-after-free vulnerability in mm/mprotect.c in the Linux kernel before 2.6.37-rc2 allows local users to cause a denial of service via vectors involving an mprotect system call.
CVE-2010-4173
The default configuration of libsdp.conf in libsdp 1.1.104 and earlier creates log files in /tmp, which allows local users to overwrite arbitrary files via a (1) symlink or (2) hard link attack on the libsdp.log.##### temporary file.
CVE-2010-4210
The pfs_getextattr function in FreeBSD 7.x before 7.3-RELEASE and 8.x before 8.0-RC1 unlocks a mutex that was not previously locked, which allows local users to cause a denial of service (kernel panic), overwrite arbitrary memory locations, and possibly execute arbitrary code via vectors related to opening a file on a file system that uses pseudofs.
CVE-2010-4299
Heap-based buffer overflow in ZfHIPCND.exe in Novell Zenworks 7 Handheld Management (ZHM) allows remote attackers to execute arbitrary code via a crafted request to TCP port 2400.
CVE-2010-4302
/opt/rv/Versions/CurrentVersion/Mcu/Config/Mcu.val in Cisco Unified Videoconferencing (UVC) System 5110 and 5115, when the Linux operating system is used, uses a weak hashing algorithm for the (1) administrator and (2) operator passwords, which makes it easier for local users to obtain sensitive information by recovering the cleartext values, aka Bug ID CSCti54010.
CVE-2010-4303
Cisco Unified Videoconferencing (UVC) System 5110 and 5115, when the Linux operating system is used, uses world-readable permissions for the /etc/shadow file, which allows local users to discover encrypted passwords by reading this file, aka Bug ID CSCti54043.
CVE-2010-4304
The web interface in Cisco Unified Videoconferencing (UVC) System 3545, 5110, 5115, and 5230; Unified Videoconferencing 3527 Primary Rate Interface (PRI) Gateway; Unified Videoconferencing 3522 Basic Rate Interfaces (BRI) Gateway; and Unified Videoconferencing 3515 Multipoint Control Unit (MCU) uses predictable session IDs based on time values, which makes it easier for remote attackers to hijack sessions via a brute-force attack, aka Bug ID CSCti54048.
CVE-2010-4305
Cisco Unified Videoconferencing (UVC) System 3545, 5110, 5115, and 5230; Unified Videoconferencing 3527 Primary Rate Interface (PRI) Gateway; Unified Videoconferencing 3522 Basic Rate Interfaces (BRI) Gateway; and Unified Videoconferencing 3515 Multipoint Control Unit (MCU) improperly use cookies for web-interface credentials, which allows remote attackers to obtain sensitive information by reading a (1) cleartext or (2) base64-encoded cleartext cookie, aka Bug ID CSCti54052.
