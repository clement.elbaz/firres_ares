CVE-2013-0334
Bundler before 1.7, when multiple top-level source lines are used, allows remote attackers to install arbitrary gems by creating a gem with the same name as another gem in a different source.
CVE-2014-2334
Multiple cross-site scripting (XSS) vulnerabilities in the Web User Interface in Fortinet FortiAnalyzer before 5.0.7 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2014-2336.
CVE-2014-2335
Multiple cross-site scripting (XSS) vulnerabilities in the Web User Interface in Fortinet FortiManager before 5.0.7 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2014-2336.
CVE-2014-2336
Multiple cross-site scripting (XSS) vulnerabilities in the Web User Interface in Fortinet FortiManager before 5.0.7 and FortiAnalyzer before 5.0.7 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2014-2334 and CVE-2014-2335.
CVE-2014-3366
SQL injection vulnerability in the administrative web interface in Cisco Unified Communications Manager allows remote authenticated users to execute arbitrary SQL commands via a crafted response, aka Bug ID CSCup88089.
CVE-2014-3372
Multiple cross-site scripting (XSS) vulnerabilities in the CCM reports interface in the Server in Cisco Unified Communications Manager allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCuq90589.
CVE-2014-3373
Multiple cross-site scripting (XSS) vulnerabilities in the CCM Dialed Number Analyzer interface in the Server in Cisco Unified Communications Manager allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCup92550.
CVE-2014-3374
Multiple cross-site scripting (XSS) vulnerabilities in the CCM admin interface in the Server in Cisco Unified Communications Manager allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCuq90582.
CVE-2014-3375
Multiple cross-site scripting (XSS) vulnerabilities in the CCM Service interface in the Server in Cisco Unified Communications Manager allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCuq90597.
CVE-2014-3473
Cross-site scripting (XSS) vulnerability in the Orchestration/Stack section in the Horizon Orchestration dashboard in OpenStack Dashboard (Horizon) before 2013.2.4, 2014.1 before 2014.1.2, and Juno before Juno-2, when used with Heat, allows remote Orchestration template owners or catalogs to inject arbitrary web script or HTML via a crafted template.
CVE-2014-3474
Cross-site scripting (XSS) vulnerability in horizon/static/horizon/js/horizon.instances.js in the Launch Instance menu in OpenStack Dashboard (Horizon) before 2013.2.4, 2014.1 before 2014.1.2, and Juno before Juno-2 allows remote authenticated users to inject arbitrary web script or HTML via a network name.
CVE-2014-3475
Cross-site scripting (XSS) vulnerability in the Users panel (admin/users/) in OpenStack Dashboard (Horizon) before 2013.2.4, 2014.1 before 2014.1.2, and Juno before Juno-2 allows remote administrators to inject arbitrary web script or HTML via a user email address, a different vulnerability than CVE-2014-8578.
CVE-2014-3708
OpenStack Compute (Nova) before 2014.1.4 and 2014.2.x before 2014.2.1 allows remote authenticated users to cause a denial of service (CPU consumption) via an IP filter in a list active servers API request.
CVE-2014-6101
Cross-site scripting (XSS) vulnerability in the redirect-login feature in IBM Business Process Manager (BPM) Advanced 7.5 through 8.5.5 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-6148
IBM Tivoli Application Dependency Discovery Manager (TADDM) 7.2.0.0 through 7.2.0.10, 7.2.1.0 through 7.2.1.6, and 7.2.2.0 through 7.2.2.2 does not require TADDM authentication for rptdesign downloads, which allows remote authenticated users to obtain sensitive database information via a crafted URL.
CVE-2014-6150
Cross-site scripting (XSS) vulnerability in IBM Tivoli Application Dependency Discovery Manager (TADDM) 7.2.1.0 through 7.2.1.6 and 7.2.2.0 through 7.2.2.2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-7177
XML External Entity vulnerability in Enalean Tuleap 7.2 and earlier allows remote authenticated users to read arbitrary files via a crafted xml document in a create action to plugins/tracker/.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-7985
Directory traversal vulnerability in EspoCRM before 2.6.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the action parameter to install/index.php.
CVE-2014-7986
install/index.php in EspoCRM before 2.6.0 allows remote attackers to re-install the application via a 1 value in the installProcess parameter.
CVE-2014-7987
Cross-site scripting (XSS) vulnerability in EspoCRM before 2.6.0 allows remote attackers to inject arbitrary web script or HTML via the desc parameter in an errors action to install/index.php.
CVE-2014-8081
lib/execute/execSetResults.php in TestLink before 1.9.13 allows remote attackers to conduct PHP object injection attacks and execute arbitrary PHP code via the filter_result_result parameter.
CVE-2014-8082
lib/functions/database.class.php in TestLink before 1.9.13 allows remote attackers to obtain sensitive information via unspecified vectors, which reveals the installation path in an error message.
CVE-2014-8333
The VMware driver in OpenStack Compute (Nova) before 2014.1.4 allows remote authenticated users to cause a denial of service (disk consumption) by deleting an instance in the resize state.
CVE-2014-8334
The WP-DBManager (aka Database Manager) plugin before 2.7.2 for WordPress allows remote authenticated users to execute arbitrary commands via shell metacharacters in the (1) $backup['filepath'] (aka "Path to Backup:" field) or (2) $backup['mysqldumppath'] variable.
CVE-2014-8399
The default configuration in systemd-shim 8 enables the Abandon debugging clause, which allows local users to cause a denial of service via unspecified vectors.
CVE-2014-8495
Citrix XenMobile MDX Toolkit before 9.0.4, when used to wrap iOS 8 applications, does not properly encrypt cached application data, which allows context-dependent attackers to obtain sensitive information by reading the cache.
CVE-2014-8509
The lazy_bdecode function in BitTorrent bootstrap-dht (aka Bootstrap) allows remote attackers to execute arbitrary code via a crafted packet, which triggers an out-of-bounds read, related to "Improper Indexing."
CVE-2014-8577
Multiple cross-site scripting (XSS) vulnerabilities in Croogo before 2.1.0 allow remote attackers to inject arbitrary web script or HTML via the (1) data[Contact][title] parameter to admin/contacts/contacts/add page; (2) data[Block][title] or (3) data[Block][alias] parameter to admin/blocks/blocks/edit page; (4) data[Region][title] parameter to admin/blocks/regions/add page; (5) data[Menu][title] or (6) data[Menu][alias] parameter to admin/menus/menus/add page; or (7) data[Link][title] parameter to admin/menus/links/add/menu page.
CVE-2014-8578
Cross-site scripting (XSS) vulnerability in the Groups panel in OpenStack Dashboard (Horizon) before 2013.2.4, 2014.1 before 2014.1.2, and Juno before Juno-2 allows remote administrators to inject arbitrary web script or HTML via a user email address, a different vulnerability than CVE-2014-3475.
