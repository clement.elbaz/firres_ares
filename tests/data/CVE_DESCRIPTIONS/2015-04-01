CVE-2012-2808
The PRNG implementation in the DNS resolver in Bionic in Android before 4.1.1 incorrectly uses time and PID information during the generation of random numbers for query ID values and UDP source ports, which makes it easier for remote attackers to spoof DNS responses by guessing these numbers, a related issue to CVE-2015-0800.
<a href="https://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
CVE-2014-9713
The default slapd configuration in the Debian openldap package 2.4.23-3 through 2.4.39-1.1 allows remote authenticated users to modify the user's permissions and other user attributes via unspecified vectors.
CVE-2015-0259
OpenStack Compute (Nova) before 2014.1.4, 2014.2.x before 2014.2.3, and kilo before kilo-3 does not validate the origin of websocket requests, which allows remote attackers to hijack the authentication of users for access to consoles via a crafted webpage.
CVE-2015-0800
The PRNG implementation in the DNS resolver in Mozilla Firefox (aka Fennec) before 37.0 on Android does not properly generate random numbers for query ID values and UDP source ports, which makes it easier for remote attackers to spoof DNS responses by guessing these numbers, a related issue to CVE-2012-2808.
CVE-2015-0801
Mozilla Firefox before 37.0, Firefox ESR 31.x before 31.6, and Thunderbird before 31.6 allow remote attackers to bypass the Same Origin Policy and execute arbitrary JavaScript code with chrome privileges via vectors involving anchor navigation, a similar issue to CVE-2015-0818.
CVE-2015-0802
Mozilla Firefox before 37.0 relies on docshell type information instead of page principal information for Window.webidl access control, which might allow remote attackers to execute arbitrary JavaScript code with chrome privileges via certain content navigation that leverages the reachability of a privileged window with an unintended persistence of access to restricted internal methods.
CVE-2015-0803
The HTMLSourceElement::AfterSetAttr function in Mozilla Firefox before 37.0 does not properly constrain the original data type of a casted value during the setting of a SOURCE element's attributes, which allows remote attackers to execute arbitrary code or cause a denial of service (use-after-free) via a crafted HTML document.
CVE-2015-0804
The HTMLSourceElement::BindToTree function in Mozilla Firefox before 37.0 does not properly constrain a data type after omitting namespace validation during certain tree-binding operations, which allows remote attackers to execute arbitrary code or cause a denial of service (use-after-free) via a crafted HTML document containing a SOURCE element.
CVE-2015-0805
The Off Main Thread Compositing (OMTC) implementation in Mozilla Firefox before 37.0 makes an incorrect memset call during interaction with the mozilla::layers::BufferTextureClient::AllocateForSurface function, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via vectors that trigger rendering of 2D graphics content.
CVE-2015-0806
The Off Main Thread Compositing (OMTC) implementation in Mozilla Firefox before 37.0 attempts to use memset for a memory region of negative length during interaction with the mozilla::layers::BufferTextureClient::AllocateForSurface function, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via vectors that trigger rendering of 2D graphics content.
CVE-2015-0807
The navigator.sendBeacon implementation in Mozilla Firefox before 37.0, Firefox ESR 31.x before 31.6, and Thunderbird before 31.6 processes HTTP 30x status codes for redirects after a preflight request has occurred, which allows remote attackers to bypass intended CORS access-control checks and conduct cross-site request forgery (CSRF) attacks via a crafted web site, a similar issue to CVE-2014-8638.
CVE-2015-0808
The webrtc::VPMContentAnalysis::Release function in the WebRTC implementation in Mozilla Firefox before 37.0 uses incompatible approaches to the deallocation of memory for simple-type arrays, which might allow remote attackers to cause a denial of service (memory corruption) via unspecified vectors.
CVE-2015-0810
Mozilla Firefox before 37.0 on OS X does not ensure that the cursor is visible, which allows remote attackers to conduct clickjacking attacks via a Flash object in conjunction with DIV elements associated with layered presentation, and crafted JavaScript code that interacts with an IMG element.
CVE-2015-0811
The QCMS implementation in Mozilla Firefox before 37.0 allows remote attackers to obtain sensitive information from process heap memory or cause a denial of service (out-of-bounds read) via an image that is improperly handled during transformation.
CVE-2015-0812
Mozilla Firefox before 37.0 does not require an HTTPS session for lightweight theme add-on installations, which allows man-in-the-middle attackers to bypass an intended user-confirmation requirement by deploying a crafted web site and conducting a DNS spoofing attack against a mozilla.org subdomain.
CVE-2015-0813
Use-after-free vulnerability in the AppendElements function in Mozilla Firefox before 37.0, Firefox ESR 31.x before 31.6, and Thunderbird before 31.6 on Linux, when the Fluendo MP3 plugin for GStreamer is used, allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via a crafted MP3 file.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-0814
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 37.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-0815
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 37.0, Firefox ESR 31.x before 31.6, and Thunderbird before 31.6 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-0816
Mozilla Firefox before 37.0, Firefox ESR 31.x before 31.6, and Thunderbird before 31.6 do not properly restrict resource: URLs, which makes it easier for remote attackers to execute arbitrary JavaScript code with chrome privileges by leveraging the ability to bypass the Same Origin Policy, as demonstrated by the resource: URL associated with PDF.js.
CVE-2015-1233
Google Chrome before 41.0.2272.118 does not properly handle the interaction of IPC, the Gamepad API, and Google V8, which allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2015-1234
Race condition in gpu/command_buffer/service/gles2_cmd_decoder.cc in Google Chrome before 41.0.2272.118 allows remote attackers to cause a denial of service (buffer overflow) or possibly have unspecified other impact by manipulating OpenGL ES commands.
CVE-2015-1892
The Multicast DNS (mDNS) responder in IBM Security Access Manager for Web 7.x before 7.0.0 FP12 and 8.x before 8.0.1 FP1 inadvertently responds to unicast queries with source addresses that are not link-local, which allows remote attackers to cause a denial of service (traffic amplification) or obtain potentially sensitive information via port-5353 UDP packets.
CVE-2015-2294
Multiple cross-site scripting (XSS) vulnerabilities in the WebGUI in pfSense before 2.2.1 allow remote attackers to inject arbitrary web script or HTML via the (1) zone parameter to status_captiveportal.php; (2) if or (3) dragtable parameter to firewall_rules.php; (4) queue parameter in an add action to firewall_shaper.php; (5) id parameter in an edit action to services_unbound_acls.php; or (6) filterlogentries_time, (7) filterlogentries_sourceipaddress, (8) filterlogentries_sourceport, (9) filterlogentries_destinationipaddress, (10) filterlogentries_interfaces, (11) filterlogentries_destinationport, (12) filterlogentries_protocolflags, or (13) filterlogentries_qty parameter to diag_logs_filter.php.
CVE-2015-2751
Xen 4.3.x, 4.4.x, and 4.5.x, when using toolstack disaggregation, allows remote domains with partial management control to cause a denial of service (host lock) via unspecified domctl operations.
CVE-2015-2752
The XEN_DOMCTL_memory_mapping hypercall in Xen 3.2.x through 4.5.x, when using a PCI passthrough device, is not preemptible, which allows local x86 HVM domain users to cause a denial of service (host CPU consumption) via a crafted request to the device model (qemu-dm).
CVE-2015-2755
Multiple cross-site request forgery (CSRF) vulnerabilities in the AB Google Map Travel (AB-MAP) plugin before 4.0 for WordPress allow remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via the (1) lat (Latitude), (2) long (Longitude), (3) map_width, (4) map_height, or (5) zoom (Map Zoom) parameter in the ab_map_options page to wp-admin/admin.php.
CVE-2015-2756
QEMU, as used in Xen 3.3.x through 4.5.x, does not properly restrict access to PCI command registers, which might allow local HVM guest users to cause a denial of service (non-maskable interrupt and host crash) by disabling the (1) memory or (2) I/O decoding for a PCI Express device and then accessing the device, which triggers an Unsupported Request (UR) response.
CVE-2015-2808
The RC4 algorithm, as used in the TLS protocol and SSL protocol, does not properly combine state data with key data during the initialization phase, which makes it easier for remote attackers to conduct plaintext-recovery attacks against the initial bytes of a stream by sniffing network traffic that occasionally relies on keys affected by the Invariance Weakness, and then using a brute-force approach involving LSB values, aka the "Bar Mitzvah" issue.
CVE-2015-2809
The Multicast DNS (mDNS) responder in Synology DiskStation Manager (DSM) before 3.1 inadvertently responds to unicast queries with source addresses that are not link-local, which allows remote attackers to cause a denial of service (traffic amplification) or obtain potentially sensitive information via port-5353 UDP packets to the Avahi component.
CVE-2015-2811
XML external entity (XXE) vulnerability in ReportXmlViewer in SAP NetWeaver Portal 7.31.201109172004 allows remote attackers to send requests to intranet servers via crafted XML, aka SAP Security Note 2111939.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-2812
XML external entity (XXE) vulnerability in XMLValidationComponent in SAP NetWeaver Portal 7.31.201109172004 allows remote attackers to send requests to intranet servers via crafted XML, aka SAP Security Note 2093966.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-2813
XML external entity (XXE) vulnerability in SAP Mobile Platform allows remote attackers to send requests to intranet servers via crafted XML, aka SAP Security Note 2125358.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-2814
SAP EMR Unwired (com.sap.mobile.healthcare.emr.v2) and Clinical Task Tracker (com.sap.mobile.healthcare.ctt) does not properly restrict access, which allows remote attackers to change the backendurl, clientid, ssourl, and infopageurl settings via unspecified vectors, aka SAP Security Note 2117079.
CVE-2015-2815
Buffer overflow in the C_SAPGPARAM function in the NetWeaver Dispatcher in SAP KERNEL 7.00 (7000.52.12.34966) and 7.40 (7400.12.21.30308) allows remote authenticated users to cause a denial of service or possibly execute arbitrary code via unspecified vectors, aka SAP Security Note 2063369.
CVE-2015-2816
The XcListener in SAP Afaria 7.0.6001.5 does not properly restrict access, which allows remote attackers to have unspecified impact via a crafted request, aka SAP Security Note 2134905.
CVE-2015-2817
The SAP Management Console in SAP NetWeaver 7.40 allows remote attackers to obtain sensitive information via the ReadProfile parameters, aka SAP Security Note 2091768.
CVE-2015-2818
XML external entity (XXE) vulnerability in SAP Mobile Platform 3 allows remote attackers to send requests to intranet servers via crafted XML, aka SAP Security Note 2125513.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>

CVE-2015-2819
SAP Sybase SQL Anywhere 11 and 16 allows remote attackers to cause a denial of service (crash) via a crafted request, aka SAP Security Note 2108161.
CVE-2015-2820
Buffer overflow in XcListener in SAP Afaria 7.0.6001.5 allows remote attackers to cause a denial of service (process termination) via a crafted request, aka SAP Security Note 2132584.
CVE-2015-2821
TYPO3 Neos 1.1.x before 1.1.3 and 1.2.x before 1.2.3 allows remote editors to access, create, and modify content nodes in the workspace of other editors via unspecified vectors.
