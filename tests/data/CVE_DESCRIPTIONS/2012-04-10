CVE-2012-0146
Open redirect vulnerability in Microsoft Forefront Unified Access Gateway (UAG) 2010 SP1 and SP1 Update 1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a crafted URL, aka "UAG Blind HTTP Redirect Vulnerability."
CVE-2012-0147
Microsoft Forefront Unified Access Gateway (UAG) 2010 SP1 and SP1 Update 1 does not properly configure the default web site, which allows remote attackers to obtain sensitive information via a crafted HTTPS request, aka "Unfiltered Access to UAG Default Website Vulnerability."
CVE-2012-0151
The Authenticode Signature Verification function in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, and Windows 8 Consumer Preview does not properly validate the digest of a signed portable executable (PE) file, which allows user-assisted remote attackers to execute arbitrary code via a modified file with additional content, aka "WinVerifyTrust Signature Validation Vulnerability."
CVE-2012-0158
The (1) ListView, (2) ListView2, (3) TreeView, and (4) TreeView2 ActiveX controls in MSCOMCTL.OCX in the Common Controls in Microsoft Office 2003 SP3, 2007 SP2 and SP3, and 2010 Gold and SP1; Office 2003 Web Components SP3; SQL Server 2000 SP4, 2005 SP4, and 2008 SP2, SP3, and R2; BizTalk Server 2002 SP1; Commerce Server 2002 SP4, 2007 SP2, and 2009 Gold and R2; Visual FoxPro 8.0 SP1 and 9.0 SP2; and Visual Basic 6.0 Runtime allow remote attackers to execute arbitrary code via a crafted (a) web site, (b) Office document, or (c) .rtf file that triggers "system state" corruption, as exploited in the wild in April 2012, aka "MSCOMCTL.OCX RCE Vulnerability."
CVE-2012-0163
Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly validate function parameters, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (aka XBAP), (2) a crafted ASP.NET application, or (3) a crafted .NET Framework application, aka ".NET Framework Parameter Validation Vulnerability."
CVE-2012-0168
Microsoft Internet Explorer 6 through 9 allows user-assisted remote attackers to execute arbitrary code via a crafted HTML document that is not properly handled during a "Print table of links" print operation, aka "Print Feature Remote Code Execution Vulnerability."
CVE-2012-0169
Microsoft Internet Explorer 9 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "JScript9 Remote Code Execution Vulnerability."
CVE-2012-0170
Microsoft Internet Explorer 6 and 7 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "OnReadyStateChange Remote Code Execution Vulnerability."
CVE-2012-0171
Microsoft Internet Explorer 6 through 9 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "SelectAll Remote Code Execution Vulnerability."
CVE-2012-0172
Microsoft Internet Explorer 6 through 8 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "VML Style Remote Code Execution Vulnerability."
CVE-2012-0177
Heap-based buffer overflow in the Office Works File Converter in Microsoft Office 2007 SP2, Works 9, and Works 6-9 File Converter allows remote attackers to execute arbitrary code via a crafted Works (aka .wps) file, aka "Office WPS Converter Heap Overflow Vulnerability."
CVE-2012-0774
Integer overflow in Adobe Reader and Acrobat 9.x before 9.5.1 and 10.x before 10.1.3 allows attackers to execute arbitrary code via a crafted TrueType font.
CVE-2012-0775
The JavaScript implementation in Adobe Reader and Acrobat 9.x before 9.5.1 and 10.x before 10.1.3 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2012-0776
The installer in Adobe Reader 9.x before 9.5.1 and 10.x before 10.1.3 allows attackers to bypass intended access restrictions and execute arbitrary code via unspecified vectors.
CVE-2012-0777
The JavaScript API in Adobe Reader and Acrobat 9.x before 9.5.1 and 10.x before 10.1.3 on Mac OS X and Linux allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
Per: http://www.adobe.com/support/security/bulletins/apsb12-08.html

'These updates resolve a memory corruption in the JavaScript API that could lead to code execution (CVE-2012-0777) (Macintosh and Linux only).'
CVE-2012-1182
The RPC code generator in Samba 3.x before 3.4.16, 3.5.x before 3.5.14, and 3.6.x before 3.6.4 does not implement validation of an array length in a manner consistent with validation of array memory allocation, which allows remote attackers to execute arbitrary code via a crafted RPC call.
