CVE-2007-0437
Multiple cross-site scripting (XSS) vulnerabilities in the sample Cache' Server Page (CSP) scripts in InterSystems Cache' allow remote attackers to inject arbitrary web script or HTML via (1) the TO parameter to loop.csp, (2) the VALUE parameter to cookie.csp, and (3) the PAGE parameter to showsource.csp in csp/samples/; and allow remote authenticated users to inject arbitrary web script or HTML via (4) the ERROR parameter to csp/samples/xmlclasseserror.csp, and unspecified vectors in (5) object.csp and (6) lotteryhistory.csp in csp/samples/.
CVE-2007-4425
Multiple buffer overflows in Live for Speed (LFS) demo, S1, and S2 allow remote authenticated users to (1) cause a denial of service (server crash) and probably execute arbitrary code via an ID 3 packet with a long nickname field, and (2) cause a denial of service (server crash) via an ID 10 packet containing a long string corresponding to an unavailable track.
CVE-2007-4426
Live for Speed (LFS) S1 and S2 allows remote attackers to cause a denial of service (server crash) via (1) a certain 0x00 byte in a pre-login ID 3 packet, which triggers a NULL dereference; or (2) a pre-login ID 5 packet that lacks certain strings, which triggers an invalid pointer dereference.
CVE-2007-4427
Unspecified vulnerability in the login page redirection logic in the Cache' Server Page (CSP) implementation in InterSystems Cache' 2007.1.0.369.0 and 2007.1.1.420.0 allows remote authenticated users to modify data on a server, related to encoding of certain parameter values by this redirection logic, aka MAK2116.
CVE-2007-4428
Lhaz 1.33 allows remote attackers to execute arbitrary code via unknown vectors, as actively exploited in August 2007 by the Exploit-LHAZ.a gzip file, a different issue than CVE-2006-4116.
CVE-2007-4429
Unspecified vulnerability in Skype allows remote attackers to cause a denial of service (server hang) via unknown vectors related to sending long URIs, as claimed to be actively exploited on 20070817 using a "call to a specific number."  NOTE: this identifier is for the en.securitylab.ru disclosure.  According to the vendor, this issue is separate from the "sign-on issues" that reduced Skype service on 20070817, which appears to be a site-specific problem.  As of 20070821, it is not clear whether this issue is simply a symptom of the larger sign-on problem.
CVE-2007-4430
Unspecified vulnerability in Cisco IOS 12.0 through 12.4 allows context-dependent attackers to cause a denial of service (device restart and BGP routing table rebuild) via certain regular expressions in a "show ip bgp regexp" command.  NOTE: unauthenticated remote attacks are possible in environments with anonymous telnet and Looking Glass access.
CVE-2007-4431
Cross-domain vulnerability in Apple Safari for Windows 3.0.3 and earlier allows remote attackers to bypass the Same Origin Policy, with access from local zones to external domains, via a certain body.innerHTML property value, aka "classic JavaScript frame hijacking."
CVE-2007-4432
Untrusted search path vulnerability in the wrapper scripts for the (1) rug, (2) zen-updater, (3) zen-installer, and (4) zen-remover programs on SUSE Linux 10.1 and Enterprise 10 allows local users to gain privileges via modified (a) LD_LIBRARY_PATH and (b) MONO_GAC_PREFIX environment variables.
CVE-2007-4433
Cross-site scripting (XSS) vulnerability in textfilesearch.aspx in the Text File Search ASP.NET edition allows remote attackers to inject arbitrary web script or HTML via the search field.
CVE-2007-4434
Cross-site scripting (XSS) vulnerability in textfilesearch.asp in the Text File Search ASP (Classic) edition allows remote attackers to inject arbitrary web script or HTML via the query parameter.
CVE-2007-4435
Multiple SQL injection vulnerabilities in TorrentTrader before 1.07 allow remote attackers to execute arbitrary SQL commands via unspecified parameters to (1) account-inbox.php, (2) account-settings.php, and possibly (3) backend/functions.php.
CVE-2007-4436
The Drupal Project module before 5.x-1.0, 4.7.x-2.3, and 4.7.x-1.3 and Project issue tracking module before 5.x-1.0, 4.7.x-2.4, and 4.7.x-1.4 do not properly enforce permissions, which allows remote attackers to (1) obtain sensitive via the Tracker Module and the Recent posts page; (2) obtain project names via unspecified vectors; (3) obtain sensitive information via the statistics pages; and (4) read CVS project activity.
CVE-2007-4437
SQL injection vulnerability in albums.php in Ampache before 3.3.3.5 allows remote attackers to execute arbitrary SQL commands via the match parameter.  NOTE: some details are obtained from third party information.
CVE-2007-4438
Session fixation vulnerability in Ampache before 3.3.3.5 allows remote attackers to hijack web sessions via unspecified vectors.
