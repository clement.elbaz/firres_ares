CVE-2016-10726
The XMLUI feature in DSpace before 3.6, 4.x before 4.5, and 5.x before 5.5 allows directory traversal via the themes/ path in an attack with two or more arbitrary characters and a colon before a pathname, as demonstrated by a themes/Reference/aa:etc/passwd URI.
CVE-2017-1729
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 134909.
CVE-2017-1738
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 contains an undisclosed vulnerability that would allow an authenticated user to obtain elevated privileges. IBM X-Force ID: 134919.
CVE-2017-1791
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137036.
CVE-2017-1792
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137037.
CVE-2017-1793
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137038.
CVE-2017-5704
Platform sample code firmware included with 4th Gen Intel Core Processor, 5th Gen Intel Core Processor, 6th Gen Intel Core Processor, and 7th Gen Intel Core Processor potentially exposes password information in memory to a local attacker with administrative privileges.
CVE-2018-10861
A flaw was found in the way ceph mon handles user requests. Any authenticated ceph user having read access to ceph can delete, create ceph storage pools and corrupt snapshot images. Ceph branches master, mimic, luminous and jewel are believed to be affected.
CVE-2018-10872
A flaw was found in the way the Linux kernel handled exceptions delivered after a stack switch operation via Mov SS or Pop SS instructions. During the stack switch operation, processor does not deliver interrupts and exceptions, they are delivered once the first instruction after the stack switch is executed. An unprivileged system user could use this flaw to crash the system kernel resulting in DoS. This CVE-2018-10872 was assigned due to regression of CVE-2018-8897 in Red Hat Enterprise Linux 6.10 GA kernel. No other versions are affected by this CVE.
CVE-2018-10887
A flaw was found in libgit2 before version 0.27.3. It has been discovered that an unexpected sign extension in git_delta_apply function in delta.c file may lead to an integer overflow which in turn leads to an out of bound read, allowing to read before the base object. An attacker may use this flaw to leak memory addresses or cause a Denial of Service.
CVE-2018-10888
A flaw was found in libgit2 before version 0.27.3. A missing check in git_delta_apply function in delta.c file, may lead to an out-of-bound read while reading a binary delta file. An attacker may use this flaw to cause a Denial of Service.
CVE-2018-10889
A flaw was found in moodle before versions 3.5.1, 3.4.4, 3.3.7. No option existed to omit logs from data privacy exports, which may contain details of other users who interacted with the requester.
CVE-2018-10890
A flaw was found in moodle before versions 3.5.1, 3.4.4, 3.3.7, 3.1.13. It was possible for the core_course_get_categories web service to return hidden categories, which should be omitted when fetching course categories.
CVE-2018-10891
A flaw was found in moodle before versions 3.5.1, 3.4.4, 3.3.7, 3.1.13. When a quiz question bank is imported, it was possible for the question preview that is displayed to execute JavaScript that is written into the question bank.
CVE-2018-10943
An issue was discovered on Barco ClickShare CSE-200 and CS-100 Base Units with firmware before 1.6.0.3. Sending an arbitrary unexpected string to TCP port 7100 respecting a certain frequency timing disconnects all clients and results in a crash of the Unit.
CVE-2018-1116
A flaw was found in polkit before version 0.116. The implementation of the polkit_backend_interactive_authority_check_authorization function in polkitd allows to test for authentication and trigger authentication of unrelated processes owned by other users. This may result in a local DoS and information disclosure.
CVE-2018-1128
It was found that cephx authentication protocol did not verify ceph clients correctly and was vulnerable to replay attack. Any attacker having access to ceph cluster network who is able to sniff packets on network can use this vulnerability to authenticate with ceph service and perform actions allowed by ceph service. Ceph branches master, mimic, luminous and jewel are believed to be vulnerable.
CVE-2018-1129
A flaw was found in the way signature calculation was handled by cephx authentication protocol. An attacker having access to ceph cluster network who is able to alter the message payload was able to bypass signature checks done by cephx protocol. Ceph branches master, mimic, luminous and jewel are believed to be vulnerable.
CVE-2018-12230
An wrong logical check identified in the transferFrom function of a smart contract implementation for RemiCoin (RMC), an Ethereum ERC20 token, allows the attacker to steal tokens or conduct resultant integer underflow attacks.
CVE-2018-12461
Fixed issues with NetIQ eDirectory prior to 9.1.1 when checking certificate revocation.
CVE-2018-12462
NetIQ iManager 3.1.1 addresses potential XSS vulnerabilities.
CVE-2018-1331
In Apache Storm 0.10.0 through 0.10.2, 1.0.0 through 1.0.6, 1.1.0 through 1.1.2, and 1.2.0 through 1.2.1, an attacker with access to a secure storm cluster in some cases could execute arbitrary code as a different user.
CVE-2018-1337
In Apache Directory LDAP API before 1.0.2, a bug in the way the SSL Filter was setup made it possible for another thread to use the connection before the TLS layer has been established, if the connection has already been used and put back in a pool of connections, leading to leaking any information contained in this request (including the credentials when sending a BIND request).
CVE-2018-13388
The review attachment resource in Atlassian Fisheye and Crucible before version 4.5.3 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in attached files.
CVE-2018-13389
The attachment resource in Atlassian Confluence before version 6.6.1 allows remote attackers to spoof web content in the Mozilla Firefox Browser through attachments that have a content-type of application/rdf+xml.
CVE-2018-13797
The macaddress module before 0.2.9 for Node.js is prone to an arbitrary command injection flaw, due to allowing unsanitized input to an exec (rather than execFile) call.
CVE-2018-13818
** DISPUTED ** Twig before 2.4.4 allows Server-Side Template Injection (SSTI) via the search search_key parameter. NOTE: the vendor points out that Twig itself is not a web application and states that it is the responsibility of web applications using Twig to properly wrap input to it.
CVE-2018-13833
An issue was discovered in cmft through 2017-09-24. The cmft::rwReadFile function in image.cpp allows remote attackers to cause a denial of service (stack-based buffer overflow and application crash) or possibly have unspecified other impact.
CVE-2018-13843
** DISPUTED ** An issue has been found in HTSlib 1.8. It is a memory leak in bgzf_getline in bgzf.c. NOTE: the software maintainer's position is that the "failure to free memory" can be fixed in applications that use the HTSlib library (such as test/test_bgzf.c in the original report) and is not a library issue.
CVE-2018-13844
An issue has been found in HTSlib 1.8. It is a memory leak in fai_read in faidx.c.
CVE-2018-13845
An issue has been found in HTSlib 1.8. It is a buffer over-read in sam_parse1 in sam.c.
CVE-2018-13846
An issue has been found in Bento4 1.5.1-624. AP4_Mpeg2TsVideoSampleStream::WriteSample in Core/Ap4Mpeg2Ts.cpp has a heap-based buffer over-read after a call from Mp42Ts.cpp, a related issue to CVE-2018-14532.
CVE-2018-13847
An issue has been found in Bento4 1.5.1-624. It is a SEGV in AP4_StcoAtom::AdjustChunkOffsets in Core/Ap4StcoAtom.cpp.
CVE-2018-13848
An issue has been found in Bento4 1.5.1-624. It is a SEGV in AP4_StszAtom::GetSampleSize in Core/Ap4StszAtom.cpp.
CVE-2018-13849
edit_requests.php in yTakkar Instagram-clone through 2018-04-23 has XSS via an onmouseover payload because of an inadequate XSS protection mechanism based on preg_replace.
CVE-2018-13850
The "Firebase Cloud Messaging (FCM) + Advance Admin Panel" component supporting Firebase Push Notification on iOS (through 2017-10-26) allows SQL injection via the /advance_push/public/login username parameter.
CVE-2018-13863
The MongoDB bson JavaScript module (also known as js-bson) versions 0.5.0 to 1.0.x before 1.0.5 is vulnerable to a Regular Expression Denial of Service (ReDoS) in lib/bson/decimal128.js. The flaw is triggered when the Decimal128.fromString() function is called to parse a long untrusted string.
CVE-2018-13865
An issue was discovered in idreamsoft iCMS 7.0.9. XSS exists via the callback parameter in a public/api.php uploadpic request, bypassing the iWAF protection mechanism.
CVE-2018-13866
An issue was discovered in the HDF HDF5 1.8.20 library. There is a stack-based buffer over-read in the function H5F_addr_decode_len in H5Fint.c.
CVE-2018-13867
An issue was discovered in the HDF HDF5 1.8.20 library. There is an out of bounds read in the function H5F__accum_read in H5Faccum.c.
CVE-2018-13868
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer over-read in the function H5O_fill_old_decode in H5Ofill.c.
CVE-2018-13869
An issue was discovered in the HDF HDF5 1.8.20 library. There is a memcpy parameter overlap in the function H5O_link_decode in H5Olink.c.
CVE-2018-13870
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer over-read in the function H5O_link_decode in H5Olink.c.
CVE-2018-13871
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer overflow in the function H5FL_blk_malloc in H5FL.c.
CVE-2018-13872
An issue was discovered in the HDF HDF5 1.8.20 library. There is a heap-based buffer overflow in the function H5G_ent_decode in H5Gent.c.
CVE-2018-13873
An issue was discovered in the HDF HDF5 1.8.20 library. There is a buffer over-read in H5O_chunk_deserialize in H5Ocache.c.
CVE-2018-13874
An issue was discovered in the HDF HDF5 1.8.20 library. There is a stack-based buffer overflow in the function H5FD_sec2_read in H5FDsec2.c, related to HDmemset.
CVE-2018-13875
An issue was discovered in the HDF HDF5 1.8.20 library. There is an out-of-bounds read in the function H5VM_memcpyvv in H5VM.c.
CVE-2018-13876
An issue was discovered in the HDF HDF5 1.8.20 library. There is a stack-based buffer overflow in the function H5FD_sec2_read in H5FDsec2.c, related to HDread.
CVE-2018-1396
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 138429.
CVE-2018-1407
IBM Rational Team Concert 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 138445.
CVE-2018-1408
IBM Rational Team Concert 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 138446.
CVE-2018-1423
IBM Jazz Foundation products could disclose sensitive information to an authenticated attacker that could be used in further attacks against the system. IBM X-Force ID: 139026.
CVE-2018-1458
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 9.7, 10,1, 10.5 and 11.1 could allow a local user to execute arbitrary code and conduct DLL hijacking attacks. IBM X-Force ID: 140209.
CVE-2018-1487
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 9.7, 10.1, 10.5 and 11.1 binaries load shared libraries from an untrusted path potentially giving low privilege users full access to the DB2 instance account by loading a malicious shared library. IBM X-Force ID: 140972.
CVE-2018-1492
IBM Jazz Foundation products could allow a user with physical access to the system to log in as another user due to the server's failure to properly log out from the previous session. IBM X-Force ID: 140977.
CVE-2018-1521
IBM Rational Team Concert 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141802.
CVE-2018-1523
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141804.
CVE-2018-1549
IBM Rational Quality Manager 5.0 through 5.0.2 and 6.0 through 6.0.5 are vulnerable to HTTP response splitting attacks. A remote attacker could exploit this vulnerability using specially-crafted URL to cause the server to return a split response, once the URL is clicked. This would allow the attacker to perform further attacks, such as Web cache poisoning, cross-site scripting, and possibly obtain sensitive information. IBM X-Force ID: 142658.
CVE-2018-1566
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 9.7, 10.1, 10.5, and 11.1 could allow a local user to execute arbitrary code due to a format string error. IBM X-Force ID: 143023.
CVE-2018-2427
SAP BusinessObjects Business Intelligence Suite, versions 4.10 and 4.20, and SAP Crystal Reports (version for Visual Studio .NET, Version 2010) allows an attacker to inject code that can be executed by the application. An attacker could thereby control the behaviour of the application.
CVE-2018-2431
SAP BusinessObjects Business Intelligence Suite, versions 4.10 and 4.20, does not sufficiently encode user controlled inputs, resulting in Cross-Site Scripting (XSS) vulnerability.
CVE-2018-2432
SAP BusinessObjects Business Intelligence (BI Launchpad and Central Management Console) versions 4.10, 4.20 and 4.30 allow an attacker to include invalidated data in the HTTP response header sent to a Web user. Successful exploitation of this vulnerability may lead to advanced attacks, including: cross-site scripting and page hijacking.
CVE-2018-2433
SAP Gateway (SAP KERNEL 32 NUC, SAP KERNEL 32 Unicode, SAP KERNEL 64 NUC, SAP KERNEL 64 Unicode 7.21, 7.21EXT, 7.22 and 7.22EXT; SAP KERNEL 7.21, 7.22, 7.45, 7.49 and 7.53) allows an attacker to prevent legitimate users from accessing a service, either by crashing or flooding the service.
CVE-2018-2434
A content spoofing vulnerability in the following components allows to render html pages containing arbitrary plain text content, which might fool an end user: UI add-on for SAP NetWeaver (UI_Infra, 1.0), SAP UI Implementation for Decoupled Innovations (UI_700, 2.0): SAP NetWeaver 7.00 Implementation, SAP User Interface Technology (SAP_UI 7.4, 7.5, 7.51, 7.52). There is little impact as it is not possible to embed active contents such as JavaScript or hyperlinks.
CVE-2018-2435
SAP NetWeaver Enterprise Portal from 7.0 to 7.02, 7.11, 7.20, 7.30, 7.31, 7.40, 7.50, does not sufficiently encode user controlled inputs, resulting in Cross-Site Scripting (XSS) vulnerability.
CVE-2018-2436
Executing transaction WRCK in SAP R/3 Enterprise Retail (EHP6) does not perform necessary authorization checks for an authenticated user, resulting in escalation of privileges.
CVE-2018-2437
The SAP Internet Graphics Service (IGS), 7.20, 7.20EXT, 7.45, 7.49, 7.53, allows an attacker to externally trigger IGS command executions which can lead to: disclosure of information and malicious file insertion or modification.
CVE-2018-2438
The SAP Internet Graphics Server (IGS), 7.20, 7.20EXT, 7.45, 7.49, 7.53, has several denial-of-service vulnerabilities that allow an attacker to prevent legitimate users from accessing a service, either by crashing or flooding the service.
CVE-2018-2439
The SAP Internet Graphics Server (IGS), 7.20, 7.20EXT, 7.45, 7.49, 7.53, has insufficient request validation (for example, where the request is validated for authenticity and validity) and under certain conditions, will process invalid requests. Several areas of the SAP Internet Graphics Server (IGS) did not require sufficient input validation. Namely, the SAP Internet Graphics Server (IGS) HTTP and RFC listener, SAP Internet Graphics Server (IGS) portwatcher when registering a portwatcher to the multiplexer and the SAP Internet Graphics Server (IGS) multiplexer had insufficient input validation and thus allowing a malformed data packet to cause a crash.
CVE-2018-2440
Under certain circumstances SAP Dynamic Authorization Management (DAM) by NextLabs (Java Policy Controller versions 7.7 and 8.5) exposes sensitive information in the application logs.
CVE-2018-3619
Information disclosure vulnerability in storage media in systems with Intel Optane memory module with Whole Disk Encryption may allow an attacker to recover data via physical access.
CVE-2018-3627
Logic bug in Intel Converged Security Management Engine 11.x may allow an attacker to execute arbitrary code via local privileged access.
CVE-2018-3628
Buffer overflow in HTTP handler in Intel Active Management Technology in Intel Converged Security Manageability Engine Firmware 3.x, 4.x, 5.x, 6.x, 7.x, 8.x, 9.x, 10.x, and 11.x may allow an attacker to execute arbitrary code via the same subnet.
CVE-2018-3629
Buffer overflow in event handler in Intel Active Management Technology in Intel Converged Security Manageability Engine Firmware 3.x, 4.x, 5.x, 6.x, 7.x, 8.x, 9.x, 10.x, and 11.x may allow an attacker to cause a denial of service via the same subnet.
CVE-2018-3632
Memory corruption in Intel Active Management Technology in Intel Converged Security Manageability Engine Firmware 6.x / 7.x / 8.x / 9.x / 10.x / 11.0 / 11.5 / 11.6 / 11.7 / 11.10 / 11.20 could be triggered by an attacker with local administrator permission on the system.
CVE-2018-3652
Existing UEFI setting restrictions for DCI (Direct Connect Interface) in 5th and 6th generation Intel Xeon Processor E3 Family, Intel Xeon Scalable processors, and Intel Xeon Processor D Family allows a limited physical presence attacker to potentially access platform secrets via debug interfaces.
CVE-2018-3667
Installation tool IPDT (Intel Processor Diagnostic Tool) 4.1.0.24 sets permissions of installed files incorrectly, allowing for execution of arbitrary code and potential privilege escalation.
CVE-2018-3668
Unquoted service paths in Intel Processor Diagnostic Tool (IPDT) before version 4.1.0.27 allows a local attacker to potentially execute arbitrary code.
CVE-2018-3682
BMC Firmware in Intel server boards, compute modules, and systems potentially allow an attacker with administrative privileges to make unauthorized read\writes to the SMBUS.
CVE-2018-3683
Unquoted service paths in Intel Quartus Prime in versions 15.1 - 18.0 allow a local attacker to potentially execute arbitrary code.
CVE-2018-3684
Unquoted service paths in Intel Quartus II in versions 11.0 - 15.0 allow a local attacker to potentially execute arbitrary code.
CVE-2018-3687
Unquoted service paths in Intel Quartus II Programmer and Tools in versions 11.0 - 15.0 allow a local attacker to potentially execute arbitrary code.
CVE-2018-3688
Unquoted service paths in Intel Quartus Prime Programmer and Tools in versions 15.1 - 18.0 allow a local attacker to potentially execute arbitrary code.
CVE-2018-3693
Systems with microprocessors utilizing speculative execution and branch prediction may allow unauthorized disclosure of information to an attacker with local user access via a speculative buffer overflow and side-channel analysis.
CVE-2018-5553
The Crestron Console service running on DGE-100, DM-DGE-200-C, and TS-1542-C devices with default configuration and running firmware versions 1.3384.00049.001 and lower are vulnerable to command injection that can be used to gain root-level access.
CVE-2018-9853
Insecure access control in freeSSHd version 1.3.1 allows attackers to obtain the privileges of the freesshd.exe process by leveraging the ability to login to an unprivileged account on the server.
