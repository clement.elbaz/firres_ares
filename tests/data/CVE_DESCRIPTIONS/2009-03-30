CVE-2007-6721
The Legion of the Bouncy Castle Java Cryptography API before release 1.38, as used in Crypto Provider Package before 1.36, has unknown impact and remote attack vectors related to "a Bleichenbacher vulnerability in simple RSA CMS signatures without signed attributes."
CVE-2008-6536
Unspecified vulnerability in 7-zip before 4.5.7 has unknown impact and remote attack vectors, as demonstrated by the PROTOS GENOME test suite for Archive Formats (c10).
CVE-2008-6537
LightNEasy/lightneasy.php in LightNEasy No database version 1.2 allows remote attackers to obtain the hash of the administrator password via the setup "do" action to LightNEasy.php, which is cleared from $_GET but later accessed using $_REQUEST.
CVE-2008-6538
DeStar 0.2.2-5 allows remote attackers to add arbitrary users via a direct request to config/add/CfgOptUser.
CVE-2008-6539
Static code injection vulnerability in user/settings/ in DeStar 0.2.2-5 allows remote authenticated users to add arbitrary administrators and inject arbitrary Python code into destar_cfg.py via a crafted pin parameter.
CVE-2008-6540
DotNetNuke before 4.8.2, during installation or upgrade, does not warn the administrator when the default (1) ValidationKey and (2) DecryptionKey values cannot be modified in the web.config file, which allows remote attackers to bypass intended access restrictions by using the default keys.
CVE-2008-6541
Unrestricted file upload vulnerability in the file manager module in DotNetNuke before 4.8.2 allows remote administrators to upload arbitrary files and gain privileges to the server via unspecified vectors.
CVE-2008-6542
Unspecified vulnerability in the Skin Manager in DotNetNuke before 4.8.2 allows remote authenticated administrators to perform "server-side execution of application logic" by uploading a static file that is converted into a dynamic script via unknown vectors related to HTM or HTML files.
Per vendor advisory: http://www.dotnetnuke.com/News/SecurityBulletins/SecurityBulletinno13/tabid/1149/Default.aspx


Mitigating factors

    * The host user must have added the HTM or HTML file type to the default File Upload Extensions
    * The user must have access to the file manager.
    * By default this issue only affects Admin users.

CVE-2008-6543
Multiple PHP remote file inclusion vulnerabilities in ComScripts TEAM Quick Classifieds 1.0 via the DOCUMENT_ROOT parameter to (1) index.php3, (2) locate.php3, (3) search_results.php3, (4) classifieds/index.php3, and (5) classifieds/view.php3; (6) index.php3, (7) manager.php3, (8) pass.php3, (9) remember.php3 (10) sign-up.php3, (11) update.php3, (12) userSet.php3, and (13) verify.php3 in controlcenter/; (14) alterCats.php3, (15) alterFeatured.php3, (16) alterHomepage.php3, (17) alterNews.php3, (18) alterTheme.php3, (19) color_help.php3, (20) createdb.php3, (21) createFeatured.php3, (22) createHomepage.php3, (23) createL.php3, (24) createM.php3, (25) createNews.php3, (26) createP.php3, (27) createS.php3, (28) createT.php3, (29) index.php3, (30) mailadmin.php3, and (31) setUp.php3 in controlpannel/; (32) include/sendit.php3 and (33) include/sendit2.php3; and possibly (34) include/adminHead.inc, (35) include/usersHead.inc, and (36) style/default.scheme.inc.
CVE-2008-6544
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in Simple Machines Forum (SMF) 1.1.4 allow remote attackers to execute arbitrary PHP code via a URL in the (1) settings[default_theme_dir] parameter to Sources/Subs-Graphics.php and (2) settings[default_theme_dir] parameter to Sources/Themes.php.  NOTE: CVE and multiple third parties dispute this issue because the files contain a protection mechanism against direct request.
CVE-2008-6545
PHP remote file inclusion vulnerability in news/include/createdb.php in Web Server Creator Web Portal 0.1 allows remote attackers to execute arbitrary PHP code via a URL in the langfile parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6546
Unspecified vulnerability in phpns before 2.1.3 has unknown impact and attack vectors related to "activation permissions."
CVE-2008-6547
schema.py in FormEncode for Python (python-formencode) 1.0 does not apply the chained_validators feature, which allows attackers to bypass intended access restrictions via unknown vectors.
CVE-2008-6548
The rst parser (parser/text_rst.py) in MoinMoin 1.6.1 does not check the ACL of an included page, which allows attackers to read unauthorized include files via unknown vectors.
CVE-2008-6549
The password_checker function in config/multiconfig.py in MoinMoin 1.6.1 uses the cracklib and python-crack features even though they are not thread-safe, which allows remote attackers to cause a denial of service (segmentation fault and crash) via unknown vectors.
CVE-2008-6550
Cross-site scripting (XSS) vulnerability in glossaire.php in Glossaire 2.0 allows remote attackers to inject arbitrary web script or HTML via the letter parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6551
Multiple directory traversal vulnerabilities in e-Vision CMS 2.0.2 and earlier, when magic_quotes_gpc is disabled, allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in (1) an adminlang cookie to admin/ind_ex.php; or the module parameter to (2) 3rdparty/adminpart/add3rdparty.php, (3) polling/adminpart/addpolling.php, (4) contact/adminpart/addcontact.php, (5) brandnews/adminpart/addbrandnews.php, (6) newsletter/adminpart/addnewsletter.php, (7) game/adminpart/addgame.php, (8) tour/adminpart/addtour.php, (9) articles/adminpart/addarticles.php, (10) product/adminpart/addproduct.php, or (11) plain/adminpart/addplain.php in modules/.
CVE-2008-6552
Red Hat Cluster Project 2.x allows local users to modify or overwrite arbitrary files via symlink attacks on files in /tmp, involving unspecified components in Resource Group Manager (aka rgmanager) before 2.03.09-1, gfs2-utils before 2.03.09-1, and CMAN - The Cluster Manager before 2.03.09-1 on Fedora 9.
CVE-2008-6553
microcms-admin-home.php in Implied by Design Micro CMS (Micro-CMS) 3.5 (aka 0.3.5) does not require authentication as an administrator, which allows remote attackers to (1) create administrative accounts via an add_admin action, (2) remove administrative accounts via a delete_admin action, and (3) modify administrative passwords via a change_password action.
CVE-2008-6554
cgi-bin/script in Aztech ADSL2/2+ 4-port router 3.7.0 build 070426 allows remote attackers to execute arbitrary commands via shell metacharacters in the query string.
CVE-2008-6555
cgi-bin/webutil.pl in The Puppet Master WebUtil allows remote attackers to execute arbitrary commands via shell metacharacters in the dig command.
CVE-2008-6556
cgi-bin/webutil.pl in The Puppet Master WebUtil 2.3 allows remote attackers to execute arbitrary commands via shell metacharacters in the whois command.
CVE-2008-6557
cgi-bin/webutil.pl in The Puppet Master WebUtil 2.7 allows remote attackers to execute arbitrary commands via shell metacharacters in the details command.
CVE-2008-6558
Untrusted search path vulnerability in (1) hvdisp and (2) rcvm in ReliantHA 1.1.4 in SCO UnixWare 7.1.4 allows local users to gain root privileges by modifying the RELIANT_PATH environment variable to point to a malicious bin/hvenv program.
CVE-2008-6559
Merge mcd in ReliantHA 1.1.4 in SCO UnixWare 7.1.4 allows local users to gain root privileges via a crafted -d argument that contains .. (dot dot) sequences that point to a directory containing a file whose name includes shell metacharacters.
CVE-2009-0115
The Device Mapper multipathing driver (aka multipath-tools or device-mapper-multipath) 0.4.8, as used in SUSE openSUSE, SUSE Linux Enterprise Server (SLES), Fedora, and possibly other operating systems, uses world-writable permissions for the socket file (aka /var/run/multipathd.sock), which allows local users to send arbitrary commands to the multipath daemon.
CVE-2009-1170
Unspecified vulnerability in Sun OpenSolaris snv_100 through snv_101 allows local users, with privileges in a non-global zone, to execute arbitrary code in the global zone when a global-zone user is using mdb on a non-global zone process.
CVE-2009-1171
The TeX filter in Moodle 1.6 before 1.6.9+, 1.7 before 1.7.7+, 1.8 before 1.8.9, and 1.9 before 1.9.5 allows user-assisted attackers to read arbitrary files via an input command in a "$$" sequence, which causes LaTeX to include the contents of the file.
