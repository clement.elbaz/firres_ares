CVE-2017-10803
In Odoo 8.0, Odoo Community Edition 9.0 and 10.0, and Odoo Enterprise Edition 9.0 and 10.0, insecure handling of anonymization data in the Database Anonymization module allows remote authenticated privileged users to execute arbitrary Python code, because unpickle is used.
CVE-2017-10804
In Odoo 8.0, Odoo Community Edition 9.0 and 10.0, and Odoo Enterprise Edition 9.0 and 10.0, remote attackers can bypass authentication under certain circumstances because parameters containing 0x00 characters are truncated before reaching the database layer. This occurs because Psycopg 2.x before 2.6.3 is used.
CVE-2017-10805
In Odoo 8.0, Odoo Community Edition 9.0 and 10.0, and Odoo Enterprise Edition 9.0 and 10.0, incorrect access control on OAuth tokens in the OAuth module allows remote authenticated users to hijack OAuth sessions of other users.
CVE-2017-10807
JabberD 2.x (aka jabberd2) before 2.6.1 allows anyone to authenticate using SASL ANONYMOUS, even when the sasl.anonymous c2s.xml option is not enabled.
CVE-2017-10810
Memory leak in the virtio_gpu_object_create function in drivers/gpu/drm/virtio/virtgpu_object.c in the Linux kernel through 4.11.8 allows attackers to cause a denial of service (memory consumption) by triggering object-initialization failures.
CVE-2017-3865
A vulnerability in the IPsec component of Cisco StarOS for Cisco ASR 5000 Series Routers could allow an unauthenticated, remote attacker to terminate all active IPsec VPN tunnels and prevent new tunnels from establishing, resulting in a denial of service (DoS) condition. Affected Products: ASR 5000 Series Routers, Virtualized Packet Core (VPC) Software. More Information: CSCvc21129. Known Affected Releases: 21.1.0 21.1.M0.65601 21.1.v0. Known Fixed Releases: 21.2.A0.65754 21.1.b0.66164 21.1.V0.66014 21.1.R0.65759 21.1.M0.65749 21.1.0.66030 21.1.0.
CVE-2017-6605
A vulnerability in the web-based management interface of Cisco Identity Services Engine (ISE) could allow an authenticated, remote attacker to conduct a reflective cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. More Information: CSCvc85415. Known Affected Releases: 2.1(0.800).
CVE-2017-6698
A vulnerability in the Cisco Prime Infrastructure (PI) and Evolved Programmable Network Manager (EPNM) SQL database interface could allow an authenticated, remote attacker to impact the confidentiality and integrity of the application by executing arbitrary SQL queries, aka SQL Injection. More Information: CSCvc23892 CSCvc35270 CSCvc35626 CSCvc35630 CSCvc49568. Known Affected Releases: 3.1(1) 2.0(4.0.45B).
CVE-2017-6699
A vulnerability in the web-based management interface of Cisco Prime Infrastructure (PI) and Evolved Programmable Network Manager (EPNM) could allow an unauthenticated, remote attacker to conduct a reflected cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. More Information: CSCvc24616 CSCvc35363 CSCvc49574. Known Affected Releases: 3.1(1) 2.0(4.0.45B).
CVE-2017-6700
A vulnerability in the web-based management interface of Cisco Prime Infrastructure (PI) and Evolved Programmable Network Manager (EPNM) could allow an unauthenticated, remote attacker to conduct a Document Object Model (DOM) based (environment or client-side) cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. More Information: CSCvc24620 CSCvc49586. Known Affected Releases: 3.1(1) 2.0(4.0.45B).
CVE-2017-6701
A vulnerability in the web application interface of the Cisco Identity Services Engine (ISE) portal could allow an unauthenticated, remote attacker to conduct a stored cross-site scripting (XSS) attack against a user of the web interface of an affected system. More Information: CSCvd49141. Known Affected Releases: 2.1(102.101).
CVE-2017-6702
A vulnerability in the web framework of Cisco SocialMiner could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface of an affected system. More Information: CSCve15285. Known Affected Releases: 11.5(1).
CVE-2017-6703
A vulnerability in the web application in the Cisco Prime Collaboration Provisioning tool could allow an unauthenticated, remote attacker to hijack another user's session. More Information: CSCvc90346. Known Affected Releases: 12.1.
CVE-2017-6704
A vulnerability in the web application in the Cisco Prime Collaboration Provisioning tool could allow an authenticated, remote attacker to perform arbitrary file downloads that could allow the attacker to read files from the underlying filesystem. More Information: CSCvc90335. Known Affected Releases: 12.1.
CVE-2017-6705
A vulnerability in the filesystem of the Cisco Prime Collaboration Provisioning tool could allow an authenticated, local attacker to acquire sensitive information. More Information: CSCvc82973. Known Affected Releases: 12.1.
CVE-2017-6706
A vulnerability in the logging subsystem of the Cisco Prime Collaboration Provisioning tool could allow an unauthenticated, local attacker to acquire sensitive information. More Information: CSCvd07260. Known Affected Releases: 12.1.
CVE-2017-6715
A vulnerability in the web framework of Cisco Firepower Management Center could allow an authenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface. Affected Products: Cisco Firepower Management Center Releases 5.4.1.x and prior. More Information: CSCuy88951. Known Affected Releases: 5.4.1.6.
CVE-2017-6716
A vulnerability in the web framework code of Cisco Firepower Management Center could allow an authenticated, remote attacker to conduct a stored cross-site scripting (XSS) attack against a user of the web interface of an affected system. Affected Products: Cisco Firepower Management Center Software Releases prior to 6.0.0.0. More Information: CSCuy88785. Known Affected Releases: 5.4.1.6.
CVE-2017-6717
A vulnerability in the web framework of Cisco Firepower Management Center could allow an authenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface. More Information: CSCvc38801. Known Affected Releases: 6.0.1.3 6.2.1. Known Fixed Releases: 6.2.1.
CVE-2017-6718
A vulnerability in the CLI of Cisco IOS XR Software could allow an authenticated, local attacker to elevate privileges to the root level. More Information: CSCvb99384. Known Affected Releases: 6.2.1.BASE. Known Fixed Releases: 6.2.11.3i.ROUT 6.2.1.29i.ROUT 6.2.1.26i.ROUT.
CVE-2017-6719
A vulnerability in the CLI of Cisco IOS XR Software could allow an authenticated, local attacker to execute arbitrary commands on the host operating system with root privileges, aka Command Injection. More Information: CSCvb99406. Known Affected Releases: 6.2.1.BASE. Known Fixed Releases: 6.2.1.28i.BASE 6.2.1.22i.BASE 6.1.32.8i.BASE 6.1.31.3i.BASE 6.1.3.10i.BASE.
CVE-2017-6721
A vulnerability in the ingress processing of fragmented TCP packets by Cisco Wide Area Application Services (WAAS) could allow an unauthenticated, remote attacker to cause the WAASNET process to restart unexpectedly, causing a denial of service (DoS) condition. More Information: CSCvc57428. Known Affected Releases: 6.3(1). Known Fixed Releases: 6.3(0.143) 6.2(3c)6 6.2(3.22).
CVE-2017-6722
A vulnerability in the Extensible Messaging and Presence Protocol (XMPP) service of Cisco Unified Contact Center Express (UCCx) could allow an unauthenticated, remote attacker to masquerade as a legitimate user, aka a Clear Text Authentication Vulnerability. More Information: CSCuw86638. Known Affected Releases: 10.6(1). Known Fixed Releases: 11.5(1.10000.61).
CVE-2017-6724
A vulnerability in the web framework code of Cisco Prime Infrastructure could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface of an affected system. More Information: CSCuw65843. Known Affected Releases: 3.1(0.0).
CVE-2017-6725
A vulnerability in the web framework code of Cisco Prime Infrastructure could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface of an affected system. More Information: CSCuw65833 CSCuw65837. Known Affected Releases: 2.2(2).
CVE-2017-7276
There is reflected XSS in TOPdesk before 5.7.6 and 6.x and 7.x before 7.03.019.
CVE-2017-7315
An issue was discovered on Humax Digital HG100R 2.0.6 devices. To download the backup file it's not necessary to use credentials, and the router credentials are stored in plaintext inside the backup, aka GatewaySettings.bin.
CVE-2017-7316
An issue was discovered on Humax Digital HG100R 2.0.6 devices. There is XSS on the 404 page.
CVE-2017-7317
An issue was discovered on Humax Digital HG100 2.0.6 devices. The attacker can find the root credentials in the backup file, aka GatewaySettings.bin.
CVE-2017-9313
Multiple Cross-site scripting (XSS) vulnerabilities in Webmin before 1.850 allow remote attackers to inject arbitrary web script or HTML via the sec parameter to view_man.cgi, the referers parameter to change_referers.cgi, or the name parameter to save_user.cgi. NOTE: these issues were not fixed in 1.840.
