CVE-2011-1474
A locally locally exploitable DOS vulnerability was found in pax-linux versions 2.6.32.33-test79.patch, 2.6.38-test3.patch, and 2.6.37.4-test14.patch. A bad bounds check in arch_get_unmapped_area_topdown triggered by programs doing an mmap after a MAP_GROWSDOWN mmap will create an infinite loop condition without releasing the VM semaphore eventually leading to a system crash.
CVE-2012-2736
In NetworkManager 0.9.2.0, when a new wireless network was created with WPA/WPA2 security in AdHoc mode, it created an open/insecure network.
CVE-2012-3462
A flaw was found in SSSD version 1.9.0. The SSSD's access-provider logic causes the result of the HBAC rule processing to be ignored in the event that the access-provider is also handling the setup of the user's SELinux user context.
CVE-2012-4420
An information disclosure flaw was found in the way the Java Virtual Machine (JVM) implementation of Java SE 7 as provided by OpenJDK 7 incorrectly initialized integer arrays after memory allocation (in certain circumstances they had nonzero elements right after the allocation). A remote attacker could use this flaw to obtain potentially sensitive information.
CVE-2013-2011
WordPress W3 Super Cache Plugin before 1.3.2 contains a PHP code-execution vulnerability which could allow remote attackers to inject arbitrary code. This issue exists because of an incomplete fix for CVE-2013-2009.
CVE-2013-3085
An authentication bypass exists in the web management interface in Belkin F5D8236-4 v2.
CVE-2013-3088
Belkin N900 router (F9K1104v1) contains an Authentication Bypass using "Javascript debugging".
CVE-2013-4318
File injection vulnerability in Ruby gem Features 0.3.0 allows remote attackers to inject malicious html in the /tmp directory.
CVE-2015-5290
ircd-ratbox 3.0.9 mishandles the MONITOR command which allows remote attackers to cause a denial of service (system out-of-memory event).
CVE-2018-18288
CrushFTP through 8.3.0 is vulnerable to credentials theft via URL redirection.
CVE-2018-20492
An issue was discovered in GitLab Community and Enterprise Edition before 11.4.13, 11.5.x before 11.5.6, and 11.6.x before 11.6.1. It has Incorrect Access Control (issue 2 of 6).
CVE-2019-15691
TigerVNC version prior to 1.10.1 is vulnerable to stack use-after-return, which occurs due to incorrect usage of stack memory in ZRLEDecoder. If decoding routine would throw an exception, ZRLEDecoder may try to access stack variable, which has been already freed during the process of stack unwinding. Exploitation of this vulnerability could potentially result into remote code execution. This attack appear to be exploitable via network connectivity.
CVE-2019-15692
TigerVNC version prior to 1.10.1 is vulnerable to heap buffer overflow. Vulnerability could be triggered from CopyRectDecoder due to incorrect value checks. Exploitation of this vulnerability could potentially result into remote code execution. This attack appear to be exploitable via network connectivity.
CVE-2019-15693
TigerVNC version prior to 1.10.1 is vulnerable to heap buffer overflow, which occurs in TightDecoder::FilterGradient. Exploitation of this vulnerability could potentially result into remote code execution. This attack appear to be exploitable via network connectivity.
CVE-2019-15694
TigerVNC version prior to 1.10.1 is vulnerable to heap buffer overflow, which could be triggered from DecodeManager::decodeRect. Vulnerability occurs due to the signdness error in processing MemOutStream. Exploitation of this vulnerability could potentially result into remote code execution. This attack appear to be exploitable via network connectivity.
CVE-2019-15695
TigerVNC version prior to 1.10.1 is vulnerable to stack buffer overflow, which could be triggered from CMsgReader::readSetCursor. This vulnerability occurs due to insufficient sanitization of PixelFormat. Since remote attacker can choose offset from start of the buffer to start writing his values, exploitation of this vulnerability could potentially result into remote code execution. This attack appear to be exploitable via network connectivity.
CVE-2019-16326
D-Link DIR-601 B1 2.00NA devices have CSRF because no anti-CSRF token is implemented. A remote attacker could exploit this in conjunction with CVE-2019-16327 to enable remote router management and device compromise. NOTE: this is an end-of-life product.
CVE-2019-16327
D-Link DIR-601 B1 2.00NA devices are vulnerable to authentication bypass. They do not check for authentication at the server side and rely on client-side validation, which is bypassable. NOTE: this is an end-of-life product.
CVE-2019-16780
WordPress users with lower privileges (like contributors) can inject JavaScript code in the block editor using a specific payload, which is executed within the dashboard. This can lead to XSS if an admin opens the post in the editor. Execution of this attack does require an authenticated user. This has been patched in WordPress 5.3.1, along with all the previous WordPress versions from 3.7 to 5.3 via a minor release. Automatic updates are enabled by default for minor releases and we strongly recommend that you keep them enabled.
CVE-2019-16781
In WordPress before 5.3.1, authenticated users with lower privileges (like contributors) can inject JavaScript code in the block editor, which is executed within the dashboard. It can lead to an admin opening the affected post in the editor leading to XSS.
CVE-2019-16789
In Waitress through version 1.4.0, if a proxy server is used in front of waitress, an invalid request may be sent by an attacker that bypasses the front-end and is parsed differently by waitress leading to a potential for HTTP request smuggling. Specially crafted requests containing special whitespace characters in the Transfer-Encoding header would get parsed by Waitress as being a chunked request, but a front-end server would use the Content-Length instead as the Transfer-Encoding header is considered invalid due to containing invalid characters. If a front-end server does HTTP pipelining to a backend Waitress server this could lead to HTTP request splitting which may lead to potential cache poisoning or unexpected information disclosure. This issue is fixed in Waitress 1.4.1 through more strict HTTP field validation.
CVE-2019-19389
JetBrains Ktor framework before version 1.2.6 was vulnerable to HTTP Response Splitting.
CVE-2019-19398
M5 lite 10 with versions of 8.0.0.182(C00) have an insufficient input validation vulnerability. Due to the input validation logic is incorrect, an attacker can exploit this vulnerability to modify the memory of the device by doing a series of operations. Successful exploit may lead to malicious code execution.
CVE-2019-19540
The ListingPro theme before v2.0.14.2 for WordPress has Reflected XSS via the What field on the homepage.
CVE-2019-19541
The ListingPro theme before v2.0.14.2 for WordPress has Persistent XSS via the Best Day/Night field on the new listing submit page.
CVE-2019-19542
The ListingPro theme before v2.0.14.2 for WordPress has Persistent XSS via the Good For field on the new listing submit page.
CVE-2019-19681
** DISPUTED ** Pandora FMS 7.x suffers from remote code execution vulnerability. With an authenticated user who can modify the alert system, it is possible to define and execute commands as root/Administrator. NOTE: The product vendor states that the vulnerability as it is described is not in fact an actual vulnerability. They state that to be able to create alert commands, you need to have admin rights. They also state that the extended ACL system can disable access to specific sections of the configuration, such as defining new alert commands.
CVE-2019-19977
libESMTP through 1.0.6 mishandles domain copying into a fixed-size buffer in ntlm_build_type_2 in ntlm/ntlmstruct.c, as demonstrated by a stack-based buffer over-read.
CVE-2019-19979
A flaw in the WordPress plugin, WP Maintenance before 5.0.6, allowed attackers to enable a vulnerable site's maintenance mode and inject malicious code affecting site visitors. There was CSRF with resultant XSS.
CVE-2019-19980
The WordPress plugin, Email Subscribers & Newsletters, before 4.2.3 had a privilege bypass flaw that allowed authenticated users (Subscriber or greater access) to send test emails from the administrative dashboard on behalf of an administrator. This occurs because the plugin registers a wp_ajax function to send_test_email.
CVE-2019-19981
The WordPress plugin, Email Subscribers & Newsletters, before 4.2.3 had a flaw that allowed for CSRF to be exploited on all plugin settings.
CVE-2019-19982
The WordPress plugin, Email Subscribers & Newsletters, before 4.2.3 had a flaw that allowed for unauthenticated option creation. In order to exploit this vulnerability, an attacker would need to send a /wp-admin/admin-post.php?es_skip=1&option_name= request.
CVE-2019-19983
In the WordPress plugin, Fast Velocity Minify before 2.7.7, the full web root path to the running WordPress application can be discovered. In order to exploit this vulnerability, FVM Debug Mode needs to be enabled and an admin-ajax request needs to call the fastvelocity_min_files action.
CVE-2019-19984
The WordPress plugin, Email Subscribers & Newsletters, before 4.2.3 had a flaw that allowed users with edit_post capabilities to manage plugin settings and email campaigns.
CVE-2019-19985
The WordPress plugin, Email Subscribers & Newsletters, before 4.2.3 had a flaw that allowed unauthenticated file download with user information disclosure.
CVE-2019-19995
A CSRF issue was discovered on Intelbras IWR 3000N 1.8.7 devices, leading to complete control of the router, as demonstrated by v1/system/user.
CVE-2019-19996
An issue was discovered on Intelbras IWR 3000N 1.8.7 devices. A malformed login request allows remote attackers to cause a denial of service (reboot), as demonstrated by JSON misparsing of the \""} string to v1/system/login.
CVE-2019-19998
Xiuno BBS 4.0 allows XXE via plugin/xn_wechat_public/route/token.php.
CVE-2019-19999
Halo before 1.2.0-beta.1 allows Server Side Template Injection (SSTI) because TemplateClassResolver.SAFER_RESOLVER is not used in the FreeMarker configuration.
CVE-2019-20000
The malware scan function in BullGuard Premium Protection 20.0.371.8 has a TOCTOU issue that enables a symbolic link attack, allowing privileged files to be deleted.
CVE-2019-20005
An issue was discovered in ezXML 0.8.3 through 0.8.6. The function ezxml_decode, while parsing a crafted XML file, performs incorrect memory handling, leading to a heap-based buffer over-read while running strchr() starting with a pointer after a '\0' character (where the processing of a string was finished).
CVE-2019-20006
An issue was discovered in ezXML 0.8.3 through 0.8.6. The function ezxml_char_content puts a pointer to the internal address of a larger block as xml->txt. This is later deallocated (using free), leading to a segmentation fault.
CVE-2019-20007
An issue was discovered in ezXML 0.8.2 through 0.8.6. The function ezxml_str2utf8, while parsing a crafted XML file, performs zero-length reallocation in ezxml.c, leading to returning a NULL pointer (in some compilers). After this, the function ezxml_parse_str does not check whether the s variable is not NULL in ezxml.c, leading to a NULL pointer dereference and crash (segmentation fault).
CVE-2019-20008
In Archery before 1.3, inserting an XSS payload into a project name (either by creating a new project or editing an existing one) will result in stored XSS on the vulnerability-scan scheduling page.
CVE-2019-5272
USG9500 with versions of V500R001C30;V500R001C60 have a missing integrity checking vulnerability. The software of the affected products does not check the integrity which may allow an attacker with high privilege to make malicious modifications without detection.
CVE-2019-5273
USG9500 with versions of V500R001C30;V500R001C60 have a denial of service vulnerability. Due to a flaw in the X.509 implementation in the affected products which can result in a large heap buffer overrun error, an attacker may exploit the vulnerability by a malicious certificate, resulting a denial of service on the affected products.
CVE-2019-5274
USG9500 with versions of V500R001C30;V500R001C60 have a denial of service vulnerability. Due to a flaw in the X.509 implementation in the affected products which can result in an infinite loop, an attacker may exploit the vulnerability via a malicious certificate to perform a denial of service attack on the affected products.
CVE-2019-5275
USG9500 with versions of V500R001C30;V500R001C60 have a denial of service vulnerability. Due to a flaw in the X.509 implementation in the affected products which can result in a heap buffer overflow when decoding a certificate, an attacker may exploit the vulnerability by a malicious certificate to perform a denial of service attack on the affected products.
CVE-2019-6008
An unquoted search path vulnerability in Multiple Yokogawa products for Windows (Exaopc (R1.01.00 ? R3.77.00), Exaplog (R1.10.00 ? R3.40.00), Exaquantum (R1.10.00 ? R3.02.00 and R3.15.00), Exaquantum/Batch (R1.01.00 ? R2.50.40), Exasmoc (all revisions), Exarqe (all revisions), GA10 (R1.01.01 ? R3.05.01), and InsightSuiteAE (R1.01.00 ? R1.06.00)) allow local users to gain privileges via a Trojan horse executable file and execute arbitrary code with eleveted privileges.
CVE-2019-6011
Cross-site scripting vulnerability in wpDataTables Lite Version 2.0.11 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-6012
SQL injection vulnerability in the wpDataTables Lite Version 2.0.11 and earlier allows remote authenticated attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2019-6013
DBA-1510P firmware 1.70b009 and earlier allows authenticated attackers to execute arbitrary OS commands via Command Line Interface (CLI).
CVE-2019-6014
DBA-1510P firmware 1.70b009 and earlier allows an attacker to execute arbitrary OS commands via Web User Interface.
CVE-2019-6016
Cross-site scripting vulnerability in REMISE Payment Module (2.11, 2.12 and 2.13) version 3.0.12 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-6017
REMISE Payment Module (2.11, 2.12 and 2.13) version 3.0.12 and earlier allow remote attackers to [Disclosed_Information_type] via unspecified vectors.
CVE-2019-6018
Cross-site scripting vulnerability in NetCommons 3.2.2 and earlier (NetCommons3.x) allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-6019
Untrusted search path vulnerability in STAMP Workbench installer all versions allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2019-6020
Open redirect vulnerability in PowerCMS 5.12 and earlier (PowerCMS 5.x), 4.42 and earlier (PowerCMS 4.x), and 3.293 and earlier (PowerCMS 3.x) allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a specially crafted URL.
CVE-2019-6021
Open redirect vulnerability in Library Information Management System LIMEDIO all versions allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a specially crafted URL.
CVE-2019-6022
Directory traversal vulnerability in Cybozu Office 10.0.0 to 10.8.3 allows remote authenticated attackers to alter arbitrary files via the 'Customapp' function.
CVE-2019-6023
Cybozu Office 10.0.0 to 10.8.3 allows remote authenticated attackers to bypass access restriction which may result in obtaining data without access privileges via the application 'Address'.
CVE-2019-6024
Rakuma App for Android version 7.15.0 and earlier, and for iOS version 7.16.4 and earlier allows an attacker to bypass authentication and obtain the user's authentication information via a malicious application created by the third party.
CVE-2019-6025
Open redirect vulnerability in Movable Type series Movable Type 7 r.4602 (7.1.3) and earlier (Movable Type 7), Movable Type 6.5.0 and 6.5.1 (Movable Type 6.5), Movable Type 6.3.9 and earlier (Movable Type 6.3.x, 6.2.x, 6.1.x, 6.0.x), Movable Type Advanced 7 r.4602 (7.1.3) and earlier (Movable Type 7), Movable Type Advanced 6.5.0 and 6.5.1 (Movable Type 6.5), Movable Type Advanced 6.3.9 and earlier (Movable Type 6.3.x, 6.2.x, 6.1.x, 6.0.x), Movable Type Premium 1.24 and earlier (Movable Type Premium), and Movable Type Premium (Advanced Edition) 1.24 and earlier (Movable Type Premium) allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a specially crafted URL.
CVE-2019-6026
Privilege escalation vulnerability in Multiple MOTEX products (LanScope Cat client program (MR) and LanScope Cat client program (MR)LanScope Cat detection agent (DA) prior to Ver.9.2.1.0, LanScope Cat server monitoring agent (SA, SAE) prior to Ver.9.2.2.0, LanScope An prior to Ver 2.7.7.0 (LanScope An 2 series), and LanScope An prior to Ver 3.0.8.1 (LanScope An 3 series)) allow authenticated attackers to obtain unauthorized privileges and execute arbitrary code.
CVE-2019-6027
Cross-site request forgery (CSRF) vulnerability in WP Spell Check 7.1.9 and earlier allows remote attackers to hijack the authentication of administrators via unspecified vectors.
CVE-2019-6029
Cross-site scripting vulnerability in Custom Body Class 0.6.0 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-6030
Cross-site request forgery (CSRF) vulnerability in Custom Body Class 0.6.0 and earlier allows remote attackers to hijack the authentication of administrators via unspecified vectors.
CVE-2019-6031
Cross-site scripting vulnerability in KINZA for Windows version 5.9.2 and earlier and for Mac version 5.0.0 and earlier allows remote attackers to inject arbitrary web script or HTML via RSS reader.
CVE-2019-6032
The NTV News24 prior to Ver.3.0.0 does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2019-6033
Cross-site scripting vulnerability in a-blog cms versions prior to Ver.2.10.23 (Ver.2.10.x), Ver.2.9.26 (Ver.2.9.x), and Ver.2.8.64 (Ver.2.8.x) allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-6034
a-blog cms versions prior to Ver.2.10.23 (Ver.2.10.x), Ver.2.9.26 (Ver.2.9.x), and Ver.2.8.64 (Ver.2.8.x) allows arbitrary scripts to be executed in the context of the application due to unspecified vectors.
CVE-2019-6035
Open redirect vulnerability in Athenz v1.8.24 and earlier allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a specially crafted page.
