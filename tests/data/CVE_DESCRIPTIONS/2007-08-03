CVE-2007-2403
CFNetwork on Apple Mac OS X 10.3.9 and 10.4.10 does not properly validate ftp: URIs, which allows remote attackers to trigger the transmission of arbitrary FTP commands to arbitrary FTP servers.
CVE-2007-2404
CRLF injection vulnerability in CFNetwork on Apple Mac OS X 10.3.9 and 10.4.10 before 20070731 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in an unspecified context.  NOTE: this can be leveraged for cross-site scripting (XSS) attacks.
CVE-2007-2405
Integer underflow in Preview in PDFKit on Apple Mac OS X 10.4.10 allows remote attackers to execute arbitrary code via a crafted PDF file.
CVE-2007-2406
Quartz Composer on Apple Mac OS X 10.4.10 does not initialize a certain object pointer, which might allow user-assisted remote attackers to execute arbitrary code via a crafted Quartz Composer file.
CVE-2007-2407
The Samba server on Apple Mac OS X 10.3.9 and 10.4.10, when Windows file sharing is enabled, does not enforce disk quotas after dropping privileges, which allows remote authenticated users to use disk space in excess of quota.
CVE-2007-2408
WebKit in Apple Safari 3 Beta before Update 3.0.3 does not properly recognize an unchecked "Enable Java" setting, which allows remote attackers to execute Java applets via a crafted web page.
CVE-2007-2409
Cross-domain vulnerability in WebCore on Apple Mac OS X 10.3.9 and 10.4.10 allows remote attackers to obtain sensitive information via a popup window, which is able to read the current URL of the parent window.
CVE-2007-2410
WebCore on Apple Mac OS X 10.3.9 and 10.4.10 retains properties of certain global objects when a new URL is visited in the same window, which allows remote attackers to conduct cross-site scripting (XSS) attacks.
CVE-2007-3388
Multiple format string vulnerabilities in (1) qtextedit.cpp, (2) qdatatable.cpp, (3) qsqldatabase.cpp, (4) qsqlindex.cpp, (5) qsqlrecord.cpp, (6) qglobal.cpp, and (7) qsvgdevice.cpp in QTextEdit in Trolltech Qt 3 before 3.3.8 20070727 allow remote attackers to execute arbitrary code via format string specifiers in text used to compose an error message.
CVE-2007-3742
WebKit in Apple Safari 3 Beta before Update 3.0.3, and iPhone before 1.0.1, does not properly handle the interaction between International Domain Name (IDN) support and Unicode fonts, which allows remote attackers to create a URL containing "look-alike characters" (homographs) and possibly perform phishing attacks.
CVE-2007-3743
Stack-based buffer overflow in bookmark handling in Apple Safari 3 Beta before Update 3.0.3 on Windows allows user-assisted remote attackers to cause a denial of service (application crash) or execute arbitrary code via a bookmark with a long title.
CVE-2007-3744
Heap-based buffer overflow in the UPnP IGD (Internet Gateway Device Standardized Device Control Protocol) implementation in mDNSResponder on Apple Mac OS X 10.4.10 before 20070731 allows network-adjacent remote attackers to execute arbitrary code via a crafted packet.
CVE-2007-3745
The Java interface to CoreAudio on Apple Mac OS X 10.3.9 and 10.4.10 contains an unsafe interface that is exposed by JDirect, which allows remote attackers to free arbitrary memory and thereby execute arbitrary code.
CVE-2007-3746
The Java interface to CoreAudio on Apple Mac OS X 10.3.9 and 10.4.10 does not properly check the bounds of heap read and write operations, which allows remote attackers to execute arbitrary code via a crafted applet.
CVE-2007-3747
The Java interface to CoreAudio on Apple Mac OS X 10.3.9 and 10.4.10 does not restrict object instantiation and manipulation to valid heap addresses, which allows remote attackers to execute arbitrary code via a crafted applet.
CVE-2007-3748
Buffer overflow in the UPnP IGD (Internet Gateway Device Standardized Device Control Protocol) implementation in iChat on Apple Mac OS X 10.3.9 and 10.4.10 allows network-adjacent remote attackers to execute arbitrary code via a crafted packet.
CVE-2007-4139
Cross-site scripting (XSS) vulnerability in the Temporary Uploads editing functionality (wp-admin/includes/upload.php) in WordPress 2.2.1, allows remote attackers to inject arbitrary web script or HTML via the style parameter to wp-admin/upload.php.
CVE-2007-4140
Buffer overflow in Live for Speed (LFS) S2 ALPHA PATCH 0.5x allows user-assisted remote attackers to execute arbitrary code via a .mpr file (replay file) that contains a long car name.
CVE-2007-4141
OpenRat CMS 0.8-beta1 and earlier allows remote attackers to obtain sensitive information via a request containing an XSS sequence in the action parameter to index.php, which reveals the path in an error message.
CVE-2007-4142
Cross-site scripting (XSS) vulnerability in IBM Lotus Sametime Server 7.5.1 before 20070731 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors involving a crafted Sametime meeting.
CVE-2007-4143
user.php in the Billing Control Panel in phpCoupon allows remote authenticated users to obtain Premium Member status, and possibly acquire free coupons, via a modified URL containing a certain billing parameter and REQ=auth, status=success, and custom=upgrade substrings, possibly related to PayPal transactions.
CVE-2007-4144
Cross-site scripting (XSS) vulnerability in sample-forms/simple-contact-form-with-preview/simple-contact-form-with-preview.html in MitriDAT eMail Form Processor Pro allows remote attackers to inject arbitrary web script or HTML via the base_path parameter, possibly related to (1) formprocessorpro.php in the PHP version of the product, and (2) formprocessorpro.pl in the Perl version of the product.
CVE-2007-4145
Heap-based buffer overflow in the BlueSkychat (BlueSkyCat) ActiveX control (V2.V2Ctrl.1) in v2.ocx 8.1.2.0 and earlier allows remote attackers to execute arbitrary code via a long string in the second argument to the ConnecttoServer method.
CVE-2007-4146
Cross-site scripting (XSS) vulnerability in webevent.cgi in WebEvent 2.61 through 4.03 allows remote attackers to inject arbitrary web script or HTML via the cmd parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-4147
Multiple unspecified vulnerabilities in Interspire ArticleLive NX before 1.7.1.2 have unknown impact and attack vectors, possibly related to (1) AL_SANITIZE and (2) "Calling the constructor to make sure things are checked, safe mode, etc."
CVE-2007-4148
Heap-based buffer overflow in the Visionsoft Audit on Demand Service (VSAOD) in Visionsoft Audit 12.4.0.0 allows remote attackers to cause a denial of service (persistent daemon crashes) or execute arbitrary code via a long filename in a "LOG." command.
CVE-2007-4149
The Visionsoft Audit on Demand Service (VSAOD) in Visionsoft Audit 12.4.0.0 does not require authentication for (1) the "LOG." command, which allows remote attackers to create or overwrite arbitrary files; (2) the SETTINGSFILE command, which allows remote attackers to overwrite the ini file, and reconfigure VSAOD or cause a denial of service; or (3) the UNINSTALL command, which allows remote attackers to cause a denial of service (daemon shutdown).  NOTE: vector 1 can be leveraged for code execution by writing to a Startup folder.
CVE-2007-4150
The Visionsoft Audit on Demand Service (VSAOD) in Visionsoft Audit 12.4.0.0 uses weak cryptography (XOR) when (1) transmitting passwords, which allows remote attackers to obtain sensitive information by sniffing the network; and (2) storing passwords in the configuration file, which allows local users to obtain sensitive information by reading this file.
CVE-2007-4151
The Visionsoft Audit on Demand Service (VSAOD) in Visionsoft Audit 12.4.0.0 allows remote attackers to obtain sensitive information via (1) a LOG.ON command, which reveals the logging pathname in the server response; (2) a VER command, which reveals the version number in the server response; and (3) a connection, which reveals the version number in the banner.
CVE-2007-4152
The Visionsoft Audit on Demand Service (VSAOD) in Visionsoft Audit 12.4.0.0 allows remote attackers to conduct replay attacks by capturing and resending data from the DETAILS and PROCESS sections of a session that schedules an audit.
CVE-2007-4153
Multiple cross-site scripting (XSS) vulnerabilities in WordPress 2.2.1 allow remote authenticated administrators to inject arbitrary web script or HTML via (1) the Options Database Table in the Admin Panel, accessed through options.php; or (2) the opml_url parameter to link-import.php.  NOTE: this might not cross privilege boundaries in some configurations, since the Administrator role has the unfiltered_html capability.
CVE-2007-4154
SQL injection vulnerability in options.php in WordPress 2.2.1 allows remote authenticated administrators to execute arbitrary SQL commands via the page_options parameter to (1) options-general.php, (2) options-writing.php, (3) options-reading.php, (4) options-discussion.php, (5) options-privacy.php, (6) options-permalink.php, (7) options-misc.php, and possibly other unspecified components.
CVE-2007-4155
Absolute path traversal vulnerability in a certain ActiveX control in vielib.dll in EMC VMware 6.0.0 allows remote attackers to execute arbitrary local programs via a full pathname in the first two arguments to the (1) CreateProcess or (2) CreateProcessEx method.
CVE-2007-4156
Multiple SQL injection vulnerabilities in wolioCMS allow remote attackers to execute arbitrary SQL commands via (1) the id parameter to member.php in a page action, related to a SELECT statement in common.php; and the (2) loginid parameter (uid variable), and possibly the (3) pwd parameter, to admin/index.php.
CVE-2007-4157
PHPBlogger stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing an admin password hash via a direct request for data/pref.db.  NOTE: this can be easily leveraged for administrative access because composing the authentication cookie only requires the password hash, not the cleartext version.
CVE-2007-4158
Memory leak in TIBCO Rendezvous (RV) daemon (rvd) 7.5.2, 7.5.3 and 7.5.4 allows remote attackers to cause a denial of service (memory consumption) via a packet with a length field of zero, a different vulnerability than CVE-2006-2830.
CVE-2007-4159
index.html in the HTTP administration interface in certain daemons in TIBCO Rendezvous (RV) 7.5.2 allows remote attackers to obtain sensitive information, such as a user name and IP addresses, via a direct request.
CVE-2007-4160
The default configuration of TIBCO Rendezvous (RV) 7.5.2 clients, when -no-multicast is omitted, uses a multicast group as the destination for a network message, which might make it easier for remote attackers to capture message contents by sniffing the network.
CVE-2007-4161
rvd in TIBCO Rendezvous (RV) 7.5.2, when -no-lead-wc is omitted, might allow remote attackers to cause a denial of service (network instability) via a subject name with a leading (1) '*' (asterisk) or (2) '>' (greater than) wildcard character.
CVE-2007-4162
TIBCO Rendezvous (RV) 7.5.2 does not protect confidentiality or integrity of inter-daemon communication, which allows remote attackers to capture and spoof traffic.
CVE-2007-4163
Multiple SQL injection vulnerabilities in IndexScript 2.7 and 2.8 before 20070726 allow remote attackers to execute arbitrary SQL commands via the (1) cat_id, (2) start_id, (3) row[parent_id], and (4) row[cat_id] parameters to unspecified components, related to use of these parameters within include/utils.php.  NOTE: the show_cat.php cat_id vector is already covered by CVE-2007-4069.
