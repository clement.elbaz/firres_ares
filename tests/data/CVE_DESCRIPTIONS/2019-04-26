CVE-2015-9284
The request phase of the OmniAuth Ruby gem is vulnerable to Cross-Site Request Forgery when used as part of the Ruby on Rails framework, allowing accounts to be connected without user intent, user interaction, or feedback to the user. This permits a secondary account to be able to sign into the web application as the primary account.
CVE-2018-15580
Cross-Site Scripting (XSS) vulnerability in adm/contentformupdate.php in gnuboard5 before 5.3.1.6 allows remote attackers to inject arbitrary web script or HTML.
CVE-2018-15581
Cross-Site Scripting (XSS) vulnerability in adm/faqmasterformupdate.php in gnuboard5 before 5.3.1.6 allows remote attackers to inject arbitrary web script or HTML.
CVE-2018-15582
Cross-Site Scripting (XSS) vulnerability in adm/sms_admin/num_book_write.php and adm/sms_admin/num_book_update.php in gnuboard5 before 5.3.1.6 allows remote attackers to inject arbitrary web script or HTML.
CVE-2018-15584
Cross-Site Scripting (XSS) vulnerability in adm/boardgroup_form_update.php and adm/boardgroup_list_update.php in gnuboard5 before 5.3.1.6 allows remote attackers to inject arbitrary web script or HTML.
CVE-2018-18276
XSS exists in the ProFiles 1.5 component for Joomla! via the name or path parameter when creating a new folder in the administrative panel.
CVE-2018-18509
A flaw during verification of certain S/MIME signatures causes emails to be shown in Thunderbird as having a valid digital signature, even if the shown message contents aren't covered by the signature. The flaw allows an attacker to reuse a valid S/MIME signature to craft an email message with arbitrary content. This vulnerability affects Thunderbird < 60.5.1.
CVE-2018-18510
The about:crashcontent and about:crashparent pages can be triggered by web content. These pages are used to crash the loaded page or the browser for test purposes. This issue allows for a non-persistent denial of service (DOS) attack by a malicious site which links to these pages. This vulnerability affects Firefox < 64.
CVE-2018-18511
Cross-origin images can be read from a canvas element in violation of the same-origin policy using the transferFromImageBitmap method. *Note: This only affects Firefox 65. Previous versions are unaffected.*. This vulnerability affects Firefox < 65.0.1.
CVE-2018-18512
A use-after-free vulnerability can occur while playing a sound notification in Thunderbird. The memory storing the sound data is immediately freed, although the sound is still being played asynchronously, leading to a potentially exploitable crash. This vulnerability affects Thunderbird < 60.5.
CVE-2018-18513
A crash can occur when processing a crafted S/MIME message or an XPI package containing a crafted signature. This can be used as a denial-of-service (DOS) attack because Thunderbird reopens the last seen message on restart, triggering the crash again. This vulnerability affects Thunderbird < 60.5.
CVE-2018-5124
Unsanitized output in the browser UI leaves HTML tags in place and can result in arbitrary code execution in Firefox before version 58.0.1.
CVE-2018-5179
A service worker can send the activate event on itself periodically which allows it to run perpetually, allowing it to monitor activity by users. Affects all versions prior to Firefox 60.
CVE-2019-0186
The input fields of the Apache Pluto "Chat Room" demo portlet 3.0.0 and 3.0.1 are vulnerable to Cross-Site Scripting (XSS) attacks. Mitigation: * Uninstall the ChatRoomDemo war file - or - * migrate to version 3.1.0 of the chat-room-demo war file
CVE-2019-11219
The algorithm used to generate device IDs (UIDs) for devices that utilize Shenzhen Yunni Technology iLnkP2P suffers from a predictability flaw that allows remote attackers to establish direct connections to arbitrary devices.
CVE-2019-11220
An authentication flaw in Shenzhen Yunni Technology iLnkP2P allows remote attackers to actively intercept user-to-device traffic in cleartext, including video streams and device credentials.
CVE-2019-11492
ProjectSend before r1070 writes user passwords to the server logs.
CVE-2019-11493
VeryPDF 4.1 has a Memory Overflow leading to Code Execution because pdfocx!CxImageTIF::operator in pdfocx.ocx (used by pdfeditor.exe and pdfcmd.exe) is mishandled.
CVE-2019-11533
Cross-site scripting (XSS) vulnerability in ProjectSend before r1070 allows remote attackers to inject arbitrary web script or HTML.
CVE-2019-11538
In Pulse Secure Pulse Connect Secure version 9.0RX before 9.0R3.4, 8.3RX before 8.3R7.1, 8.2RX before 8.2R12.1, and 8.1RX before 8.1R15.1, an NFS problem could allow an authenticated attacker to access the contents of arbitrary files on the affected device.
CVE-2019-11539
In Pulse Secure Pulse Connect Secure version 9.0RX before 9.0R3.4, 8.3RX before 8.3R7.1, 8.2RX before 8.2R12.1, and 8.1RX before 8.1R15.1 and Pulse Policy Secure version 9.0RX before 9.0R3.2, 5.4RX before 5.4R7.1, 5.3RX before 5.3R12.1, 5.2RX before 5.2R12.1, and 5.1RX before 5.1R15.1, the admin web interface allows an authenticated attacker to inject and execute commands.
CVE-2019-11540
In Pulse Secure Pulse Connect Secure version 9.0RX before 9.0R3.4 and 8.3RX before 8.3R7.1 and Pulse Policy Secure version 9.0RX before 9.0R3.2 and 5.4RX before 5.4R7.1, an unauthenticated, remote attacker can conduct a session hijacking attack.
CVE-2019-11541
In Pulse Secure Pulse Connect Secure version 9.0RX before 9.0R3.4, 8.3RX before 8.3R7.1, and 8.2RX before 8.2R12.1, users using SAML authentication with the Reuse Existing NC (Pulse) Session option may see authentication leaks.
CVE-2019-11542
In Pulse Secure Pulse Connect Secure version 9.0RX before 9.0R3.4, 8.3RX before 8.3R7.1, 8.2RX before 8.2R12.1, and 8.1RX before 8.1R15.1 and Pulse Policy Secure version 9.0RX before 9.0R3.2, 5.4RX before 5.4R7.1, 5.3RX before 5.3R12.1, 5.2RX before 5.2R12.1, and 5.1RX before 5.1R15.1, an authenticated attacker (via the admin web interface) can send a specially crafted message resulting in a stack buffer overflow.
CVE-2019-11543
XSS exists in the admin web console in Pulse Secure Pulse Connect Secure (PCS) 9.0RX before 9.0R3.4, 8.3RX before 8.3R7.1, and 8.1RX before 8.1R15.1 and Pulse Policy Secure 9.0RX before 9.0R3.2, 5.4RX before 5.4R7.1, and 5.2RX before 5.2R12.1.
CVE-2019-11555
The EAP-pwd implementation in hostapd (EAP server) before 2.8 and wpa_supplicant (EAP peer) before 2.8 does not validate fragmentation reassembly state properly for a case where an unexpected fragment could be received. This could result in process termination due to a NULL pointer dereference (denial of service). This affects eap_server/eap_server_pwd.c and eap_peer/eap_pwd.c.
CVE-2019-11557
The WebDorado Contact Form Builder plugin before 1.0.69 for WordPress allows CSRF via the wp-admin/admin-ajax.php action parameter, with resultant local file inclusion via directory traversal, because there can be a discrepancy between the $_POST['action'] value and the $_GET['action'] value, and the latter is unsanitized.
CVE-2019-2725
Vulnerability in the Oracle WebLogic Server component of Oracle Fusion Middleware (subcomponent: Web Services). Supported versions that are affected are 10.3.6.0.0 and 12.1.3.0.0. Easily exploitable vulnerability allows unauthenticated attacker with network access via HTTP to compromise Oracle WebLogic Server. Successful attacks of this vulnerability can result in takeover of Oracle WebLogic Server. CVSS 3.0 Base Score 9.8 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H).
CVE-2019-3705
Dell EMC iDRAC6 versions prior to 2.92, iDRAC7/iDRAC8 versions prior to 2.61.60.60, and iDRAC9 versions prior to 3.20.21.20, 3.21.24.22, 3.21.26.22 and 3.23.23.23 contain a stack-based buffer overflow vulnerability. An unauthenticated remote attacker may potentially exploit this vulnerability to crash the webserver or execute arbitrary code on the system with privileges of the webserver by sending specially crafted input data to the affected system.
CVE-2019-3706
Dell EMC iDRAC9 versions prior to 3.24.24.24, 3.21.26.22, 3.22.22.22 and 3.21.25.22 contain an authentication bypass vulnerability. A remote attacker may potentially exploit this vulnerability to bypass authentication and gain access to the system by sending specially crafted data to the iDRAC web interface.
CVE-2019-3707
Dell EMC iDRAC9 versions prior to 3.30.30.30 contain an authentication bypass vulnerability. A remote attacker may potentially exploit this vulnerability to bypass authentication and gain access to the system by sending specially crafted input data to the WS-MAN interface.
CVE-2019-3843
It was discovered that a systemd service that uses DynamicUser property can create a SUID/SGID binary that would be allowed to run as the transient service UID/GID even after the service is terminated. A local attacker may use this flaw to access resources that will be owned by a potentially different service in the future, when the UID/GID will be recycled.
CVE-2019-3844
It was discovered that a systemd service that uses DynamicUser property can get new privileges through the execution of SUID binaries, which would allow to create binaries owned by the service transient group with the setgid bit set. A local attacker may use this flaw to access resources that will be owned by a potentially different service in the future, when the GID will be recycled.
CVE-2019-6689
An issue was discovered in Dillon Kane Tidal Workload Automation Agent 3.2.0.5 (formerly known as Cisco Workload Automation or CWA). The Enterprise Scheduler for AIX allows local users to gain privileges via Command Injection in crafted Tidal Job Buffers (TJB) parameters. NOTE: this vulnerability exists because the CVE-2014-3272 solution did not address AIX operating systems.
CVE-2019-7476
A vulnerability in SonicWall Global Management System (GMS), allow a remote user to gain access to the appliance using existing SSH key. This vulnerability affects GMS versions 9.1, 9.0, 8.7, 8.6, 8.4, 8.3 and earlier.
CVE-2019-9788
Mozilla developers and community members reported memory safety bugs present in Firefox 65, Firefox ESR 60.5, and Thunderbird 60.5. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9789
Mozilla developers and community members reported memory safety bugs present in Firefox 65. Some of these bugs showed evidence of memory corruption and we presume that with enough effort that some of these could be exploited to run arbitrary code. This vulnerability affects Firefox < 66.
CVE-2019-9790
A use-after-free vulnerability can occur when a raw pointer to a DOM element on a page is obtained using JavaScript and the element is then removed while still in use. This results in a potentially exploitable crash. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9791
The type inference system allows the compilation of functions that can cause type confusions between arbitrary objects when compiled through the IonMonkey just-in-time (JIT) compiler and when the constructor function is entered through on-stack replacement (OSR). This allows for possible arbitrary reading and writing of objects during an exploitable crash. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9792
The IonMonkey just-in-time (JIT) compiler can leak an internal JS_OPTIMIZED_OUT magic value to the running script during a bailout. This magic value can then be used by JavaScript to achieve memory corruption, which results in a potentially exploitable crash. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9793
A mechanism was discovered that removes some bounds checking for string, array, or typed array accesses if Spectre mitigations have been disabled. This vulnerability could allow an attacker to create an arbitrary value in compiled JavaScript, for which the range analysis will infer a fully controlled, incorrect range in circumstances where users have explicitly disabled Spectre mitigations. *Note: Spectre mitigations are currently enabled for all users by default settings.*. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9794
A vulnerability was discovered where specific command line arguments are not properly discarded during Firefox invocation as a shell handler for URLs. This could be used to retrieve and execute files whose location is supplied through these command line arguments if Firefox is configured as the default URI handler for a given URI scheme in third party applications and these applications insufficiently sanitize URL data. *Note: This issue only affects Windows operating systems. Other operating systems are unaffected.*. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9795
A vulnerability where type-confusion in the IonMonkey just-in-time (JIT) compiler could potentially be used by malicious JavaScript to trigger a potentially exploitable crash. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9796
A use-after-free vulnerability can occur when the SMIL animation controller incorrectly registers with the refresh driver twice when only a single registration is expected. When a registration is later freed with the removal of the animation controller element, the refresh driver incorrectly leaves a dangling pointer to the driver's observer array. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9797
Cross-origin images can be read in violation of the same-origin policy by exporting an image after using createImageBitmap to read the image and then rendering the resulting bitmap image within a canvas element. This vulnerability affects Firefox < 66.
CVE-2019-9798
On Android systems, Firefox can load a library from APITRACE_LIB, which is writable by all users and applications. This could allow malicious third party applications to execute a man-in-the-middle attack if a malicious code was written to that location and loaded. *Note: This issue only affects Android. Other operating systems are unaffected.*. This vulnerability affects Firefox < 66.
CVE-2019-9799
Insufficient bounds checking of data during inter-process communication might allow a compromised content process to be able to read memory from the parent process under certain conditions. This vulnerability affects Firefox < 66.
CVE-2019-9801
Firefox will accept any registered Program ID as an external protocol handler and offer to launch this local application when given a matching URL on Windows operating systems. This should only happen if the program has specifically registered itself as a "URL Handler" in the Windows registry. *Note: This issue only affects Windows operating systems. Other operating systems are unaffected.*. This vulnerability affects Thunderbird < 60.6, Firefox ESR < 60.6, and Firefox < 66.
CVE-2019-9802
If a Sandbox content process is compromised, it can initiate an FTP download which will then use a child process to render the downloaded data. The downloaded data can then be passed to the Chrome process with an arbitrary file length supplied by an attacker, bypassing sandbox protections and allow for a potential memory read of adjacent data from the privileged Chrome process, which may include sensitive data. This vulnerability affects Firefox < 66.
CVE-2019-9803
The Upgrade-Insecure-Requests (UIR) specification states that if UIR is enabled through Content Security Policy (CSP), navigation to a same-origin URL must be upgraded to HTTPS. Firefox will incorrectly navigate to an HTTP URL rather than perform the security upgrade requested by the CSP in some circumstances, allowing for potential man-in-the-middle attacks on the linked resources. This vulnerability affects Firefox < 66.
CVE-2019-9804
In Firefox Developer Tools it is possible that pasting the result of the 'Copy as cURL' command into a command shell on macOS will cause the execution of unintended additional bash script commands if the URL was maliciously crafted. This is the result of an issue with the native version of Bash on macOS. *Note: This issue only affects macOS. Other operating systems are unaffected.*. This vulnerability affects Firefox < 66.
CVE-2019-9805
A latent vulnerability exists in the Prio library where data may be read from uninitialized memory for some functions, leading to potential memory corruption. This vulnerability affects Firefox < 66.
CVE-2019-9806
A vulnerability exists during authorization prompting for FTP transaction where successive modal prompts are displayed and cannot be immediately dismissed. This allows for a denial of service (DOS) attack. This vulnerability affects Firefox < 66.
CVE-2019-9807
When arbitrary text is sent over an FTP connection and a page reload is initiated, it is possible to create a modal alert message with this text as the content. This could potentially be used for social engineering attacks. This vulnerability affects Firefox < 66.
CVE-2019-9808
If WebRTC permission is requested from documents with data: or blob: URLs, the permission notifications do not properly display the originating domain. The notification states "Unknown origin" as the requestee, leading to user confusion about which site is asking for this permission. This vulnerability affects Firefox < 66.
CVE-2019-9809
If the source for resources on a page is through an FTP connection, it is possible to trigger a series of modal alert messages for these resources through invalid credentials or locations. These messages cannot be immediately dismissed, allowing for a denial of service (DOS) attack. This vulnerability affects Firefox < 66.
CVE-2019-9810
Incorrect alias information in IonMonkey JIT compiler for Array.prototype.slice method may lead to missing bounds check and a buffer overflow. This vulnerability affects Firefox < 66.0.1, Firefox ESR < 60.6.1, and Thunderbird < 60.6.1.
CVE-2019-9813
Incorrect handling of __proto__ mutations may lead to type confusion in IonMonkey JIT code and can be leveraged for arbitrary memory read and write. This vulnerability affects Firefox < 66.0.1, Firefox ESR < 60.6.1, and Thunderbird < 60.6.1.
