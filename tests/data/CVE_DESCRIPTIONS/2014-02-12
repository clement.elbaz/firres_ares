CVE-2013-2585
Cross-site scripting (XSS) vulnerability in Atmail Webmail Server 6.6.x before 6.6.3 and 7.0.x before 7.0.3 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to index.php/mail/viewmessage/getattachment/folder/INBOX/uniqueId/<MessageID>/filenameOriginal/.
CVE-2013-6229
Multiple cross-site scripting (XSS) vulnerabilities in Atmail Webmail Server 7.0.2 allow remote attackers to inject arbitrary web script or HTML via the (1) filter parameter to index.php/mail/mail/listfoldermessages/searching/true/selectFolder/INBOX/resultContext/searchResultsTab5 or (2) mailId[] parameter to index.php/mail/mail/movetofolder/fromFolder/INBOX/toFolder/INBOX.Trash. NOTE: the view attachment message process vector is already covered by CVE-2013-2585.
CVE-2014-0253
Microsoft .NET Framework 1.1 SP1, 2.0 SP2, 3.5, 3.5.1, 4, 4.5, and 4.5.1 does not properly determine TCP connection states, which allows remote attackers to cause a denial of service (ASP.NET daemon hang) via crafted HTTP requests that trigger persistent resource consumption for a (1) stale or (2) closed connection, as exploited in the wild in February 2014, aka "POST Request DoS Vulnerability."
CVE-2014-0254
The IPv6 implementation in Microsoft Windows 8, Windows Server 2012, and Windows RT does not properly validate packets, which allows remote attackers to cause a denial of service (system hang) via crafted ICMPv6 Router Advertisement packets, aka "TCP/IP Version 6 (IPv6) Denial of Service Vulnerability."
CVE-2014-0257
Microsoft .NET Framework 1.0 SP3, 1.1 SP1, 2.0 SP2, 3.5, 3.5.1, 4, 4.5, and 4.5.1 does not properly determine whether it is safe to execute a method, which allows remote attackers to execute arbitrary code via (1) a crafted web site or (2) a crafted .NET Framework application that exposes a COM server endpoint, aka "Type Traversal Vulnerability."
CVE-2014-0263
The Direct2D implementation in Microsoft Windows 7 SP1, Windows Server 2008 R2 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows remote attackers to execute arbitrary code via a large 2D geometric figure that is encountered with Internet Explorer, aka "Microsoft Graphics Component Memory Corruption Vulnerability."
CVE-2014-0266
The XMLHTTP ActiveX controls in XML Core Services 3.0 in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to bypass the Same Origin Policy via a web page that is visited in Internet Explorer, aka "MSXML Information Disclosure Vulnerability."
CVE-2014-0267
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0289 and CVE-2014-0290.
CVE-2014-0268
Microsoft Internet Explorer 8 through 11 does not properly restrict file installation and registry-key creation, which allows remote attackers to bypass the Mandatory Integrity Control protection mechanism via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2014-0269
Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0270
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0273, CVE-2014-0274, and CVE-2014-0288.
CVE-2014-0271
The VBScript engine in Microsoft Internet Explorer 6 through 11, and VBScript 5.6 through 5.8, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "VBScript Memory Corruption Vulnerability."
CVE-2014-0272
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0273
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0270, CVE-2014-0274, and CVE-2014-0288.
CVE-2014-0274
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0270, CVE-2014-0273, and CVE-2014-0288.
CVE-2014-0275
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0285 and CVE-2014-0286.
CVE-2014-0276
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0277
Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0278 and CVE-2014-0279.
CVE-2014-0278
Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0277 and CVE-2014-0279.
CVE-2014-0279
Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0277 and CVE-2014-0278.
CVE-2014-0280
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0281
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0287.
CVE-2014-0283
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0284
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0285
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0275 and CVE-2014-0286.
CVE-2014-0286
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0275 and CVE-2014-0285.
CVE-2014-0287
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0281.
CVE-2014-0288
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0270, CVE-2014-0273, and CVE-2014-0274.
CVE-2014-0289
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0267 and CVE-2014-0290.
CVE-2014-0290
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0267 and CVE-2014-0289.
CVE-2014-0293
Microsoft Internet Explorer 9 through 11 allows remote attackers to read content from a different (1) domain or (2) zone via a crafted web site, aka "Internet Explorer Cross-domain Information Disclosure Vulnerability."
CVE-2014-0294
Microsoft Forefront Protection 2010 for Exchange Server does not properly parse e-mail content, which might allow remote attackers to execute arbitrary code via a crafted message, aka "RCE Vulnerability."
CVE-2014-0295
VsaVb7rt.dll in Microsoft .NET Framework 2.0 SP2 and 3.5.1 does not implement the ASLR protection mechanism, which makes it easier for remote attackers to execute arbitrary code via a crafted web site, as exploited in the wild in February 2014, aka "VSAVB7RT ASLR Vulnerability."
CVE-2014-0500
Adobe Shockwave Player before 12.0.9.149 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-0501.
CVE-2014-0501
Adobe Shockwave Player before 12.0.9.149 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-0500.
