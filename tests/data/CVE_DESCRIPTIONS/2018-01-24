CVE-2017-1000474
Soyket Chowdhury Vehicle Sales Management System version 2017-07-30 is vulnerable to multiple SQL Injecting in login/vehicle.php, login/profile.php, login/Actions.php, login/manage_employee.php, and login/sell.php scripts resulting in the expose of user's login credentials, SQL Injection and Stored XSS vulnerability, which leads to remote code executing.
CVE-2017-1000475
FreeSSHd 1.3.1 version is vulnerable to an Unquoted Path Service allowing local users to launch processes with elevated privileges.
CVE-2017-1000502
Users with permission to create or configure agents in Jenkins 1.37 and earlier could configure an EC2 agent to run arbitrary shell commands on the master node whenever the agent was supposed to be launched. Configuration of these agents now requires the 'Run Scripts' permission typically only granted to administrators.
CVE-2017-1000503
A race condition during Jenkins 2.81 through 2.94 (inclusive); 2.89.1 startup could result in the wrong order of execution of commands during initialization. This could in rare cases result in failure to initialize the setup wizard on the first startup. This resulted in multiple security-related settings not being set to their usual strict default.
CVE-2017-1000504
A race condition during Jenkins 2.94 and earlier; 2.89.1 and earlier startup could result in the wrong order of execution of commands during initialization. There is a very short window of time after startup during which Jenkins may no longer show the 'Please wait while Jenkins is getting ready to work' message but Cross-Site Request Forgery (CSRF) protection may not yet be effective.
CVE-2017-12176
xorg-x11-server before 1.19.5 was missing extra length validation in ProcEstablishConnection function allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12177
xorg-x11-server before 1.19.5 was vulnerable to integer overflow in ProcDbeGetVisualInfo function allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12178
xorg-x11-server before 1.19.5 had wrong extra length check in ProcXIChangeHierarchy function allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12179
xorg-x11-server before 1.19.5 was vulnerable to integer overflow in (S)ProcXIBarrierReleasePointer functions allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12180
xorg-x11-server before 1.19.5 was missing length validation in XFree86 VidModeExtension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12181
xorg-x11-server before 1.19.5 was missing length validation in XFree86 DGA extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12182
xorg-x11-server before 1.19.5 was missing length validation in XFree86 DRI extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12183
xorg-x11-server before 1.19.5 was missing length validation in XFIXES extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12184
xorg-x11-server before 1.19.5 was missing length validation in XINERAMA extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12185
xorg-x11-server before 1.19.5 was missing length validation in MIT-SCREEN-SAVER extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12186
xorg-x11-server before 1.19.5 was missing length validation in X-Resource extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-12187
xorg-x11-server before 1.19.5 was missing length validation in RENDER extension allowing malicious X client to cause X server to crash or possibly execute arbitrary code.
CVE-2017-13696
A buffer overflow vulnerability lies in the web server component of Dup Scout Enterprise 9.9.14, Disk Savvy Enterprise 9.9.14, Sync Breeze Enterprise 9.9.16, and Disk Pulse Enterprise 9.9.16 where an attacker can craft a malicious GET request and exploit the web server component. Successful exploitation of the software will allow an attacker to gain complete access to the system with NT AUTHORITY / SYSTEM level privileges. The vulnerability lies due to improper handling and sanitization of the incoming request.
CVE-2017-15135
It was found that 389-ds-base since 1.3.6.1 up to and including 1.4.0.3 did not always handle internal hash comparison operations correctly during the authentication process. A remote, unauthenticated attacker could potentially use this flaw to bypass the authentication process under very rare and specific circumstances.
CVE-2017-15718
The YARN NodeManager in Apache Hadoop 2.7.3 and 2.7.4 can leak the password for credential store provider used by the NodeManager to YARN Applications.
CVE-2017-1769
IBM Business Process Manager 8.6 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 136783.
CVE-2017-18075
crypto/pcrypt.c in the Linux kernel before 4.14.13 mishandles freeing instances, allowing a local user able to access the AF_ALG-based AEAD interface (CONFIG_CRYPTO_USER_API_AEAD) and pcrypt (CONFIG_CRYPTO_PCRYPT) to cause a denial of service (kfree of an incorrect pointer) or possibly have unspecified other impact by executing a crafted sequence of system calls.
CVE-2018-1000005
libcurl 7.49.0 to and including 7.57.0 contains an out bounds read in code handling HTTP/2 trailers. It was reported (https://github.com/curl/curl/pull/2231) that reading an HTTP/2 trailer could mess up future trailers since the stored size was one byte less than required. The problem is that the code that creates HTTP/1-like headers from the HTTP/2 trailer data once appended a string like `:` to the target buffer, while this was recently changed to `: ` (a space was added after the colon) but the following math wasn't updated correspondingly. When accessed, the data is read out of bounds and causes either a crash or that the (too large) data gets passed to client write. This could lead to a denial-of-service situation or an information disclosure if someone has a service that echoes back or uses the trailers for something.
CVE-2018-1000006
GitHub Electron versions 1.8.2-beta.3 and earlier, 1.7.10 and earlier, 1.6.15 and earlier has a vulnerability in the protocol handler, specifically Electron apps running on Windows 10, 7 or 2008 that register custom protocol handlers can be tricked in arbitrary command execution if the user clicks on a specially crafted URL. This has been fixed in versions 1.8.2-beta.4, 1.7.11, and 1.6.16.
CVE-2018-1000007
libcurl 7.1 through 7.57.0 might accidentally leak authentication data to third parties. When asked to send custom headers in its HTTP requests, libcurl will send that set of headers first to the host in the initial URL but also, if asked to follow redirects and a 30X HTTP response code is returned, to the host mentioned in URL in the `Location:` response header value. Sending the same set of headers to subsequent hosts is in particular a problem for applications that pass on custom `Authorization:` headers, as this header often contains privacy sensitive information or data that could allow others to impersonate the libcurl-using client's request.
CVE-2018-1000018
An information disclosure in ovirt-hosted-engine-setup prior to 2.2.7 reveals the root user's password in the log file.
CVE-2018-1047
A flaw was found in Wildfly 9.x. A path traversal vulnerability through the org.wildfly.extension.undertow.deployment.ServletResourceManager.getResource method could lead to information disclosure of arbitrary local files.
CVE-2018-1048
It was found that the AJP connector in undertow, as shipped in Jboss EAP 7.1.0.GA, does not use the ALLOW_ENCODED_SLASH option and thus allow the the slash / anti-slash characters encoded in the url which may lead to path traversal and result in the information disclosure of arbitrary local files.
CVE-2018-4834
A vulnerability has been identified in Desigo Automation Controllers Products and Desigo Operator Unit PXM20-E. A remote attacker with network access to the device could potentially upload a new firmware image to the devices without prior authentication.
CVE-2018-5319
RAVPower FileHub 2.000.056 allows remote users to steal sensitive information via a crafted HTTP request.
CVE-2018-5705
Reservo Image Hosting 1.6 is vulnerable to XSS attacks. The affected function is its search engine (the t parameter to the /search URI). Since there is an user/admin login interface, it's possible for attackers to steal sessions of users and thus admin(s). By sending users an infected URL, code will be executed.
CVE-2018-5759
jsparse.c in Artifex MuJS through 1.0.2 does not properly maintain the AST depth for binary expressions, which allows remote attackers to cause a denial of service (excessive recursion) via a crafted file.
CVE-2018-5777
An issue was discovered in Ipswitch WhatsUp Gold before 2017 Plus SP1 (17.1.1). Remote clients can take advantage of a misconfiguration in the TFTP server that could allow attackers to execute arbitrary commands on the TFTP server via unspecified vectors.
CVE-2018-5778
An issue was discovered in Ipswitch WhatsUp Gold before 2017 Plus SP1 (17.1.1). Multiple SQL injection vulnerabilities are present in the legacy .ASP pages, which could allow attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2018-5969
Cross Site Request Forgery (CSRF) exists in Photography CMS 1.0 via clients/resources/ajax/ajax_new_admin.php, as demonstrated by adding an admin account.
CVE-2018-5972
SQL Injection exists in Classified Ads CMS Quickad 4.0 via the keywords, placeid, cat, or subcat parameter to the listing URI.
CVE-2018-5976
Cross Site Request Forgery (CSRF) exists in RSVP Invitation Online 1.0 via function/account.php, as demonstrated by modifying the admin password.
CVE-2018-5977
SQL Injection exists in Affiligator Affiliate Webshop Management System 2.1.0 via a search/?q=&price_type=range&price= request.
CVE-2018-5978
SQL Injection exists in Facebook Style Php Ajax Chat Zechat 1.5 via the login.php User field.
CVE-2018-5979
SQL Injection exists in Wchat Fully Responsive PHP AJAX Chat Script 1.5 via the login.php User field.
CVE-2018-5984
SQL Injection exists in the Tumder (An Arcade Games Platform) 2.1 component for Joomla! via the PATH_INFO to the category/ URI.
CVE-2018-5985
SQL Injection exists in the LiveCRM SaaS Cloud 1.0 component for Joomla! via an r=site/login&company_id= request.
CVE-2018-5986
SQL Injection exists in Easy Car Script 2014 via the s_order or s_row parameter to site_search.php.
CVE-2018-5988
SQL Injection exists in Flexible Poll 1.2 via the id parameter to mobile_preview.php or index.php.
CVE-2018-6017
Unencrypted transmission of images in Tinder iOS app and Tinder Android app allows an attacker to extract private sensitive information by sniffing network traffic.
CVE-2018-6018
Fixed sizes of HTTPS responses in Tinder iOS app and Tinder Android app allow an attacker to extract private sensitive information by sniffing network traffic.
CVE-2018-6184
ZEIT Next.js 4 before 4.2.3 has Directory Traversal under the /_next request namespace.
CVE-2018-6187
In Artifex MuPDF 1.12.0, there is a heap-based buffer overflow vulnerability in the do_pdf_save_document function in the pdf/pdf-write.c file. Remote attackers could leverage the vulnerability to cause a denial of service via a crafted pdf file.
CVE-2018-6190
Netis WF2419 V3.2.41381 devices allow XSS via the Description field on the MAC Filtering page.
CVE-2018-6191
The js_strtod function in jsdtoa.c in Artifex MuJS through 1.0.2 has an integer overflow because of incorrect exponent validation.
CVE-2018-6192
In Artifex MuPDF 1.12.0, the pdf_read_new_xref function in pdf/pdf-xref.c allows remote attackers to cause a denial of service (segmentation violation and application crash) via a crafted pdf file.
CVE-2018-6193
A Cross-Site Scripting (XSS) vulnerability was found in Routers2 2.24, affecting the 'rtr' GET parameter in a page=graph action to cgi-bin/routers2.pl.
