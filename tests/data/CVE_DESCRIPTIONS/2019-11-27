CVE-2011-2177
OpenOffice.org v3.3 allows execution of arbitrary code with the privileges of the user running the OpenOffice.org suite tools.
CVE-2011-2187
xscreensaver before 5.14 crashes during activation and leaves the screen unlocked when in Blank Only Mode and when DPMS is disabled, which allows local attackers to access resources without authentication.
CVE-2011-2207
dirmngr before 2.1.0 improperly handles certain system calls, which allows remote attackers to cause a denial of service (DOS) via a specially-crafted certificate.
CVE-2011-2480
Information Disclosure vulnerability in the 802.11 stack, as used in FreeBSD before 8.2 and NetBSD when using certain non-x86 architectures. A signedness error in the IEEE80211_IOC_CHANINFO ioctl allows a local unprivileged user to cause the kernel to copy large amounts of kernel memory back to the user, disclosing potentially sensitive information.
CVE-2011-2515
PackageKit 0.6.17 allows installation of unsigned RPM packages as though they were signed which may allow installation of non-trusted packages and execution of arbitrary code.
CVE-2011-2523
vsftpd 2.3.4 downloaded between 20110630 and 20110703 contains a backdoor which opens a shell on port 6200/tcp.
CVE-2011-2717
The DHCPv6 client (dhcp6c) as used in the dhcpv6 project through 2011-07-25 allows remote DHCP servers to execute arbitrary commands via shell metacharacters in a hostname obtained from a DHCP message.
CVE-2012-2248
An issue was discovered in dhclient 4.3.1-6 due to an embedded path variable.
CVE-2012-6655
An issue exists AccountService 0.6.37 in the user_change_password_authorized_cb() function in user.c which could let a local users obtain encrypted passwords.
CVE-2013-2625
An Access Bypass issue exists in OTRS Help Desk before 3.2.4, 3.1.14, and 3.0.19, OTRS ITSM before 3.2.3, 3.1.8, and 3.0.7, and FAQ before 2.2.3, 2.1.4, and 2.0.8. Access rights by the object linking mechanism is not verified
CVE-2014-3875
The addto parameter to fup in Frams' Fast File EXchange (F*EX, aka fex) before fex-2014053 allows remote attackers to conduct cross-site scripting (XSS) attacks
CVE-2016-1000110
The CGIHandler class in Python before 2.7.12 does not protect against the HTTP_PROXY variable name clash in a CGI script, which could allow a remote attacker to redirect HTTP requests.
CVE-2016-4980
A password generation weakness exists in xquest through 2016-06-13.
CVE-2017-12945
Insufficient validation of user-supplied input for the Solstice Pod before 2.8.4 networking configuration enables authenticated attackers to execute arbitrary commands as root.
CVE-2019-10195
A flaw was found in IPA, all 4.6.x versions before 4.6.7, all 4.7.x versions before 4.7.4 and all 4.8.x versions before 4.8.3, in the way that FreeIPA's batch processing API logged operations. This included passing user passwords in clear text on FreeIPA masters. Batch processing of commands with passwords as arguments or options is not performed by default in FreeIPA but is possible by third-party components. An attacker having access to system logs on FreeIPA masters could use this flaw to produce log file content with passwords exposed.
CVE-2019-10216
In ghostscript before version 9.50, the .buildfont1 procedure did not properly secure its privileged calls, enabling scripts to bypass `-dSAFER` restrictions. An attacker could abuse this flaw by creating a specially crafted PostScript file that could escalate privileges and access files outside of restricted areas.
CVE-2019-10220
Linux kernel CIFS implementation, version 4.9.0 is vulnerable to a relative paths injection in directory entry lists.
CVE-2019-13934
Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting') vulnerability in webclient of Siemens AG Polarion could allow an attacker to exploit a reflected XSS vulnerability. This issue affects: Siemens AG Polarion All versions < 19.2.
CVE-2019-13935
Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting') vulnerability in webclient of Siemens AG Polarion could allow an attacker to exploit a reflected XSS vulnerability. This issue affects: Siemens AG Polarion All versions < 19.2.
CVE-2019-13936
Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting') vulnerability in webclient of Siemens AG Polarion could allow an attacker to exploit a persistent XSS vulnerability. This issue affects: Siemens AG Polarion All versions < 19.2.
CVE-2019-14812
A flaw was found in all ghostscript versions 9.x before 9.50, in the .setuserparams2 procedure where it did not properly secure its privileged calls, enabling scripts to bypass `-dSAFER` restrictions. A specially crafted PostScript file could disable security protection and then have access to the file system, or execute arbitrary commands.
CVE-2019-14867
A flaw was found in IPA, all 4.6.x versions before 4.6.7, all 4.7.x versions before 4.7.4 and all 4.8.x versions before 4.8.3, in the way the internal function ber_scanf() was used in some components of the IPA server, which parsed kerberos key data. An unauthenticated attacker who could trigger parsing of the krb principal key could cause the IPA server to crash or in some conditions, cause arbitrary code to be executed on the server hosting the IPA server.
CVE-2019-14896
A heap-based buffer overflow vulnerability was found in the Linux kernel, version kernel-2.6.32, in Marvell WiFi chip driver. A remote attacker could cause a denial of service (system crash) or, possibly execute arbitrary code, when the lbs_ibss_join_existing function is called after a STA connects to an AP.
CVE-2019-15298
A problem was found in Centreon Web through 19.04.3. An authenticated command injection is present in the page include/configuration/configObject/traps-mibs/formMibs.php. This page is called from the Centreon administration interface. This is the mibs management feature that contains a file filing form. At the time of submission of a file, the mnftr parameter is sent to the page and is not filtered properly. This allows one to inject Linux commands directly.
CVE-2019-15300
A problem was found in Centreon Web through 19.04.3. An authenticated SQL injection is present in the page include/Administration/parameters/ldap/xml/ldap_host.php. The arId parameter is not properly filtered before being passed to the SQL query.
CVE-2019-15705
An Improper Input Validation vulnerability in the SSL VPN portal of FortiOS versions 6.2.1 and below, and 6.0.6 and below may allow an unauthenticated remote attacker to crash the SSL VPN service by sending a crafted POST request.
CVE-2019-18184
Crestron DMC-STRO 1.0 devices allow remote command execution as root via shell metacharacters to the ping function.
CVE-2019-18247
An attacker may use a specially crafted message to force Relion 650 series (versions 1.3.0.5 and prior) or Relion 670 series (versions 1.2.3.18, 2.0.0.11, 2.1.0.1 and prior) to reboot, which could cause a denial of service.
CVE-2019-18253
An attacker could use specially crafted paths in a specific request to read or delete files from Relion 670 Series (versions 1p1r26, 1.2.3.17, 2.0.0.10, RES670 2.0.0.4, 2.1.0.1, and prior) outside the intended directory.
CVE-2019-18660
The Linux kernel before 5.4.1 on powerpc allows Information Exposure because the Spectre-RSB mitigation is not in place for all applicable CPUs, aka CID-39e72bf96f58. This is related to arch/powerpc/kernel/entry_64.S and arch/powerpc/kernel/security.c.
CVE-2019-19242
SQLite 3.30.1 mishandles pExpr->y.pTab, as demonstrated by the TK_COLUMN case in sqlite3ExprCodeTarget in expr.c.
CVE-2019-19308
In text_to_glyphs in sushi-font-widget.c in gnome-font-viewer 3.34.0, there is a NULL pointer dereference while parsing a TTF font file that lacks a name section (due to a g_strconcat call that returns NULL).
CVE-2019-19319
In the Linux kernel 5.0.21, a setxattr operation, after a mount of a crafted ext4 image, can cause a slab-out-of-bounds write access because of an ext4_xattr_set_entry use-after-free in fs/ext4/xattr.c when a large old_size value is used in a memset call.
CVE-2019-19327
ui/ResultView.js in Wikibase Wikidata Query Service GUI before 0.3.6-SNAPSHOT 2019-11-07 allows HTML injection when reporting the number of results and number of milliseconds. NOTE: this GUI code is no longer bundled with the Wikibase Wikidata Query Service snapshots, such as 0.3.6-SNAPSHOT.
CVE-2019-19328
ui/editor/tooltip/Rdf.js in Wikibase Wikidata Query Service GUI before 0.3.6-SNAPSHOT 2019-11-07 allows HTML injection in tooltips for entities. NOTE: this GUI code is no longer bundled with the Wikibase Wikidata Query Service snapshots, such as 0.3.6-SNAPSHOT.
CVE-2019-19329
In Wikibase Wikidata Query Service GUI before 0.3.6-SNAPSHOT 2019-11-07, when mathematical expressions in results are displayed directly, arbitrary JavaScript execution can occur, aka XSS. This was addressed by introducing MathJax as a new mathematics rendering engine. NOTE: this GUI code is no longer bundled with the Wikibase Wikidata Query Service snapshots, such as 0.3.6-SNAPSHOT.
CVE-2019-19330
The HTTP/2 implementation in HAProxy before 2.0.10 mishandles headers, as demonstrated by carriage return (CR, ASCII 0xd), line feed (LF, ASCII 0xa), and the zero character (NUL, ASCII 0x0), aka Intermediary Encapsulation Attacks.
CVE-2019-19366
A cross-site scripting (XSS) vulnerability in app/xml_cdr/xml_cdr_search.php in FusionPBX 4.4.1 allows remote attackers to inject arbitrary web script or HTML via the redirect parameter.
CVE-2019-19367
A cross-site scripting (XSS) vulnerability in app/fax/fax_files.php in FusionPBX 4.4.1 allows remote attackers to inject arbitrary web script or HTML via the id parameter.
CVE-2019-6665
On BIG-IP ASM 15.0.0-15.0.1, 14.1.0-14.1.2, 14.0.0-14.0.1, and 13.1.0-13.1.3.1, BIG-IQ 6.0.0 and 5.2.0-5.4.0, iWorkflow 2.3.0, and Enterprise Manager 3.1.1, an attacker with access to the device communication between the BIG-IP ASM Central Policy Builder and the BIG-IQ/Enterprise Manager/F5 iWorkflow will be able to set up the proxy the same way and intercept the traffic.
CVE-2019-6666
On BIG-IP 15.0.0-15.0.1, 14.1.0-14.1.0.5, 14.0.0-14.0.0.4, and 13.1.0-13.1.1.4, the TMM process may produce a core file when an upstream server or cache sends the BIG-IP an invalid age header value.
CVE-2019-6667
On BIG-IP 15.0.0-15.0.1, 14.1.0-14.1.0.5, 14.0.0-14.0.0.4, 13.1.0-13.1.1.5, 12.1.0-12.1.4.1, and 11.5.1-11.6.5, under certain conditions, TMM may consume excessive resources when processing traffic for a Virtual Server with the FIX (Financial Information eXchange) profile applied.
CVE-2019-6668
The BIG-IP APM Edge Client for macOS bundled with BIG-IP APM 15.0.0-15.0.1, 14.1.0-14.1.0.5, 14.0.0-14.0.0.4, 13.1.0-13.1.1.5, 12.1.0-12.1.5, and 11.5.1-11.6.5 may allow unprivileged users to access files owned by root.
CVE-2019-6669
On BIG-IP 15.0.0-15.0.1, 14.1.0-14.1.2, 14.0.0-14.0.1, 13.1.0-13.1.3.1, 12.1.0-12.1.5, and 11.5.1-11.6.5.1, undisclosed traffic flow may cause TMM to restart under some circumstances.
CVE-2019-6670
On BIG-IP 15.0.0-15.0.1, 14.1.0-14.1.2, 14.0.0-14.0.1, 13.1.0-13.1.3.1, 12.1.0-12.1.5, and 11.5.1-11.6.5, vCMP hypervisors are incorrectly exposing the plaintext unit key for their vCMP guests on the filesystem.
CVE-2019-6671
On BIG-IP 15.0.0-15.0.1, 14.1.0-14.1.2, 14.0.0-14.0.1, and 13.1.0-13.1.3.1, under certain conditions tmm may leak memory when processing packet fragments, leading to resource starvation.
CVE-2019-6672
On BIG-IP AFM 15.0.0-15.0.1, 14.0.0-14.1.2, and 13.1.0-13.1.3.1, when bad-actor detection is configured on a wildcard virtual server on platforms with hardware-based sPVA, the performance of the BIG-IP AFM system is degraded.
CVE-2019-6673
On versions 15.0.0-15.0.1 and 14.0.0-14.1.2, when the BIG-IP is configured in HTTP/2 Full Proxy mode, specifically crafted requests may cause a disruption of service provided by the Traffic Management Microkernel (TMM).
CVE-2019-6674
On F5 SSL Orchestrator 15.0.0-15.0.1 and 14.0.0-14.1.2, TMM may crash when processing SSLO data in a service-chaining configuration.
