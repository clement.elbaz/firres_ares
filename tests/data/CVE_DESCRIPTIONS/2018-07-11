CVE-2013-0589
IBM iNotes before 8.5.3 Fix Pack 6 and 9.x before 9.0.1 allows remote attackers to bypass the remote image filtering mechanism and obtain sensitive information via a crafted e-mail message. IBM X-Force ID: 83371.
CVE-2013-0592
Cross-site scripting (XSS) vulnerability in IBM iNotes before 8.5.3 Fix Pack 6 and 9.x before 9.0.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors. IBM X-Force ID: 83815.
CVE-2013-0594
Open redirect vulnerability in IBM iNotes before 8.5.3 Fix Pack 6 and 9.x before 9.0.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors. IBM X-Force ID: 83383.
CVE-2013-2951
IBM WebSphere Portal 7.0.0.x and 8.0.0.x write passwords to a trace file when tracing is enabled for the Selfcare Portlet (Profile Management), which allows local users to obtain sensitive information by reading the file. IBM X-Force ID: 83621.
CVE-2013-2972
IBM WebSphere Cast Iron 6.3 allows remote attackers to bypass intended access restrictions via unspecified vectors. IBM X-Force ID: 83868.
CVE-2016-0708
Applications deployed to Cloud Foundry, versions v166 through v227, may be vulnerable to a remote disclosure of information, including, but not limited to environment variables and bound service details. For applications to be vulnerable, they must have been staged using automatic buildpack detection, passed through the Java Buildpack detection script, and allow the serving of static content from within the deployed artifact. The default Apache Tomcat configuration in the affected java buildpack versions for some basic web application archive (WAR) packaged applications are vulnerable to this issue.
CVE-2016-9604
It was discovered in the Linux kernel before 4.11-rc8 that root can gain direct access to an internal keyring, such as '.dns_resolver' in RHEL-7 or '.builtin_trusted_keys' upstream, by joining it as its session keyring. This allows root to bypass module signature verification by adding a new public key of its own devising to the keyring.
CVE-2017-16709
Crestron Airmedia AM-100 devices with firmware before 1.6.0 and AM-101 devices with firmware before 2.7.0 allows remote authenticated administrators to execute arbitrary code via unspecified vectors.
CVE-2017-16710
Cross-site scripting (XSS) vulnerability in Crestron Airmedia AM-100 devices with firmware before 1.6.0 and AM-101 devices with firmware before 2.7.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2017-7467
A buffer overflow flaw was found in the way minicom before version 2.7.1 handled VT100 escape sequences. A malicious terminal device could potentially use this flaw to crash minicom, or execute arbitrary code in the context of the minicom process.
CVE-2018-0024
An Improper Privilege Management vulnerability in a shell session of Juniper Networks Junos OS allows an authenticated unprivileged attacker to gain full control of the system. Affected releases are Juniper Networks Junos OS: 12.1X46 versions prior to 12.1X46-D45 on SRX Series; 12.3X48 versions prior to 12.3X48-D20 on SRX Series; 12.3 versions prior to 12.3R11 on EX Series; 14.1X53 versions prior to 14.1X53-D30 on EX2200/VC, EX3200, EX3300/VC, EX4200, EX4300, EX4550/VC, EX4600, EX6200, EX8200/VC (XRE), QFX3500, QFX3600, QFX5100;; 15.1X49 versions prior to 15.1X49-D20 on SRX Series.
CVE-2018-0025
When an SRX Series device is configured to use HTTP/HTTPS pass-through authentication services, a client sending authentication credentials in the initial HTTP/HTTPS session is at risk that these credentials may be captured during follow-on HTTP/HTTPS requests by a malicious actor through a man-in-the-middle attack or by authentic servers subverted by malicious actors. FTP, and Telnet pass-through authentication services are not affected. Affected releases are Juniper Networks SRX Series: 12.1X46 versions prior to 12.1X46-D67 on SRX Series; 12.3X48 versions prior to 12.3X48-D25 on SRX Series; 15.1X49 versions prior to 15.1X49-D35 on SRX Series.
CVE-2018-0026
After Junos OS device reboot or upgrade, the stateless firewall filter configuration may not take effect. This issue can be verified by running the command: user@re0> show interfaces <interface_name> extensive | match filters" CAM destination filters: 0, CAM source filters: 0 Note: when the issue occurs, it does not show the applied firewall filter. The correct output should show the applied firewall filter, for example: user@re0> show interfaces <interface_name> extensive | match filters" CAM destination filters: 0, CAM source filters: 0 Input Filters: FIREWAL_FILTER_NAME-<interface_name> This issue affects firewall filters for every address family. Affected releases are Juniper Networks Junos OS: 15.1R4, 15.1R5, 15.1R6 and SRs based on these MRs. 15.1X8 versions prior to 15.1X8.3.
CVE-2018-0027
Receipt of a crafted or malformed RSVP PATH message may cause the routing protocol daemon (RPD) to hang or crash. When RPD is unavailable, routing updates cannot be processed which can lead to an extended network outage. If RSVP is not enabled on an interface, then the issue cannot be triggered via that interface. This issue only affects Juniper Networks Junos OS 16.1 versions prior to 16.1R3. This issue does not affect Junos releases prior to 16.1R1.
CVE-2018-0029
While experiencing a broadcast storm, placing the fxp0 interface into promiscuous mode via the 'monitor traffic interface fxp0' can cause the system to crash and restart (vmcore). This issue only affects Junos OS 15.1 and later releases, and affects both single core and multi-core REs. Releases prior to Junos OS 15.1 are unaffected by this vulnerability. Affected releases are Juniper Networks Junos OS: 15.1 versions prior to 15.1F6-S11, 15.1R4-S9, 15.1R6-S6, 15.1R7; 15.1X49 versions prior to 15.1X49-D140; 15.1X53 versions prior to 15.1X53-D59 on EX2300/EX3400; 15.1X53 versions prior to 15.1X53-D67 on QFX10K; 15.1X53 versions prior to 15.1X53-D233 on QFX5200/QFX5110; 15.1X53 versions prior to 15.1X53-D471, 15.1X53-D490 on NFX; 16.1 versions prior to 16.1R3-S8, 16.1R5-S4, 16.1R6-S1, 16.1R7; 16.2 versions prior to 16.2R1-S6, 16.2R2-S5, 16.2R3; 17.1 versions prior to 17.1R1-S7, 17.1R2-S7, 17.1R3; 17.2 versions prior to 17.2R1-S6, 17.2R2-S4, 17.2R3; 17.2X75 versions prior to 17.2X75-D90, 17.2X75-D110; 17.3 versions prior to 17.3R1-S4, 17.3R2; 17.4 versions prior to 17.4R1-S3, 17.4R2.
CVE-2018-0030
Receipt of a specific MPLS packet may cause MPC7/8/9, PTX-FPC3 (FPC-P1, FPC-P2) line cards or PTX1K to crash and restart. By continuously sending specific MPLS packets, an attacker can repeatedly crash the line cards or PTX1K causing a sustained Denial of Service. Affected releases are Juniper Networks Junos OS with MPC7/8/9 or PTX-FPC3 (FPC-P1, FPC-P2) installed and PTX1K: 15.1F versions prior to 15.1F6-S10; 15.1 versions prior to 15.1R4-S9, 15.1R6-S6, 15.1R7; 16.1 versions prior to 16.1R3-S8, 16.1R4-S9, 16.1R5-S4, 16.1R6-S3, 16.1R7; 16.1X65 versions prior to 16.1X65-D46; 16.2 versions prior to 16.2R1-S6, 16.2R2-S5, 16.2R3; 17.1 versions prior to 17.1R1-S7, 17.1R2-S7, 17.1R3; 17.2 versions prior to 17.2R1-S4, 17.2R2-S4, 17.2R3; 17.2X75 versions prior to 17.2X75-D70, 17.2X75-D90; 17.3 versions prior to 17.3R1-S4, 17.3R2, 17.4 versions prior to 17.4R1-S2, 17.4R2. Refer to KB25385 for more information about PFE line cards.
CVE-2018-0031
Receipt of specially crafted UDP/IP packets over MPLS may be able to bypass a stateless firewall filter. The crafted UDP packets must be encapsulated and meet a very specific packet format to be classified in a way that bypasses IP firewall filter rules. The packets themselves do not cause a service interruption (e.g. RPD crash), but receipt of a high rate of UDP packets may be able to contribute to a denial of service attack. This issue only affects processing of transit UDP/IP packets over MPLS, received on an interface with MPLS enabled. TCP packet processing and non-MPLS encapsulated UDP packet processing are unaffected by this issue. Affected releases are Juniper Networks Junos OS: 12.1X46 versions prior to 12.1X46-D76; 12.3 versions prior to 12.3R12-S10; 12.3X48 versions prior to 12.3X48-D66, 12.3X48-D70; 14.1X53 versions prior to 14.1X53-D47; 15.1 versions prior to 15.1F6-S10, 15.1R4-S9, 15.1R6-S6, 15.1R7; 15.1X49 versions prior to 15.1X49-D131, 15.1X49-D140; 15.1X53 versions prior to 15.1X53-D59 on EX2300/EX3400; 15.1X53 versions prior to 15.1X53-D67 on QFX10K; 15.1X53 versions prior to 15.1X53-D233 on QFX5200/QFX5110; 15.1X53 versions prior to 15.1X53-D471, 15.1X53-D490 on NFX; 16.1 versions prior to 16.1R3-S8, 16.1R4-S9, 16.1R5-S4, 16.1R6-S3, 16.1R7; 16.2 versions prior to 16.2R1-S6, 16.2R2-S5, 16.2R3; 17.1 versions prior to 17.1R1-S7, 17.1R2-S7, 17.1R3; 17.2 versions prior to 17.2R1-S6, 17.2R2-S4, 17.2R3; 17.2X75 versions prior to 17.2X75-D100; 17.3 versions prior to 17.3R1-S4, 17.3R2-S2, 17.3R3; 17.4 versions prior to 17.4R1-S3, 17.4R2; 18.1 versions prior to 18.1R2; 18.2X75 versions prior to 18.2X75-D5.
CVE-2018-0032
The receipt of a crafted BGP UPDATE can lead to a routing process daemon (RPD) crash and restart. Repeated receipt of the same crafted BGP UPDATE can result in an extended denial of service condition for the device. This issue only affects the specific versions of Junos OS listed within this advisory. Earlier releases are unaffected by this vulnerability. This crafted BGP UPDATE does not propagate to other BGP peers. Affected releases are Juniper Networks Junos OS: 16.1X65 versions prior to 16.1X65-D47; 17.2X75 versions prior to 17.2X75-D91, 17.2X75-D110; 17.3 versions prior to 17.3R1-S4, 17.3R2; 17.4 versions prior to 17.4R1-S3, 17.4R2.
CVE-2018-0034
A Denial of Service vulnerability exists in the Juniper Networks Junos OS JDHCPD daemon which allows an attacker to core the JDHCPD daemon by sending a crafted IPv6 packet to the system. This issue is limited to systems which receives IPv6 DHCP packets on a system configured for DHCP processing using the JDHCPD daemon. This issue does not affect IPv4 DHCP packet processing. Affected releases are Juniper Networks Junos OS: 12.3 versions prior to 12.3R12-S10 on EX Series; 12.3X48 versions prior to 12.3X48-D70 on SRX Series; 14.1X53 versions prior to 14.1X53-D47 on EX2200/VC, EX3200, EX3300/VC, EX4200, EX4300, EX4550/VC, EX4600, EX6200, EX8200/VC (XRE), QFX3500, QFX3600, QFX5100; 14.1X53 versions prior to 14.1X53-D130 on QFabric; 15.1 versions prior to 15.1R4-S9, 15.1R6-S6, 15.1R7; 15.1X49 versions prior to 15.1X49-D140 on SRX Series; 15.1X53 versions prior to 15.1X53-D67 on QFX10000 Series; 15.1X53 versions prior to 15.1X53-D233 on QFX5110, QFX5200; 15.1X53 versions prior to 15.1X53-D471 on NFX 150, NFX 250; 16.1 versions prior to 16.1R3-S9, 16.1R4-S8, 16.1R5-S4, 16.1R6-S3, 16.1R7; 16.2 versions prior to 16.2R2-S5, 16.2R3; 17.1 versions prior to 17.1R1-S7, 17.1R2-S7, 17.1R3; 17.2 versions prior to 17.2R1-S6, 17.2R2-S4, 17.2R3; 17.3 versions prior to 17.3R1-S4, 17.3R2-S2, 17.3R3; 17.4 versions prior to 17.4R1-S3, 17.4R2.
CVE-2018-0035
QFX5200 and QFX10002 devices that have been shipped with Junos OS 15.1X53-D21, 15.1X53-D30, 15.1X53-D31, 15.1X53-D32, 15.1X53-D33 and 15.1X53-D60 or have been upgraded to these releases using the .bin or .iso images may contain an unintended additional Open Network Install Environment (ONIE) partition. This additional partition allows the superuser to reboot to the ONIE partition which will wipe out the content of the Junos partition and its configuration. Once rebooted, the ONIE partition will not have root password configured, thus any user can access the console or SSH, using an IP address acquired from DHCP, as root without password. Once the device has been shipped or upgraded with the ONIE partition installed, the issue will persist. Simply upgrading to higher release via the CLI will not resolve the issue. No other Juniper Networks products or platforms are affected by this issue.
CVE-2018-0037
Junos OS routing protocol daemon (RPD) process may crash and restart or may lead to remote code execution while processing specific BGP NOTIFICATION messages. By continuously sending crafted BGP NOTIFICATION messages, an attacker can repeatedly crash the RPD process causing a sustained Denial of Service. Due to design improvements, this issue does not affect Junos OS 16.1R1, and all subsequent releases. This issue only affects the receiving BGP device and is non-transitive in nature. Affected releases are Juniper Networks Junos OS: 15.1F5 versions starting from 15.1F5-S7 and all subsequent releases; 15.1F6 versions starting from 15.1F6-S3 and later releases prior to 15.1F6-S10; 15.1F7 versions 15.1 versions starting from 15.1R5 and later releases, including the Service Releases based on 15.1R5 and on 15.1R6 prior to 15.1R6-S6 and 15.1R7;
CVE-2018-0038
Juniper Networks Contrail Service Orchestration releases prior to 3.3.0 have Cassandra service enabled by default with hardcoded credentials. These credentials allow network based attackers unauthorized access to information stored in Cassandra.
CVE-2018-0039
Juniper Networks Contrail Service Orchestration releases prior to 4.0.0 have Grafana service enabled by default with hardcoded credentials. These credentials allow network based attackers unauthorized access to information stored in Grafana or exploit other weaknesses or vulnerabilities in Grafana.
CVE-2018-0040
Juniper Networks Contrail Service Orchestrator versions prior to 4.0.0 use hardcoded cryptographic certificates and keys in some cases, which may allow network based attackers to gain unauthorized access to services.
CVE-2018-0041
Juniper Networks Contrail Service Orchestration releases prior to 3.3.0 use hardcoded credentials to access Keystone service. These credentials allow network based attackers unauthorized access to information stored in keystone.
CVE-2018-0042
Juniper Networks CSO versions prior to 4.0.0 may log passwords in log files leading to an information disclosure vulnerability.
CVE-2018-0500
Curl_smtp_escape_eob in lib/smtp.c in curl 7.54.1 to and including curl 7.60.0 has a heap-based buffer overflow that might be exploitable by an attacker who can control the data that curl transmits over SMTP with certain settings (i.e., use of a nonstandard --limit-rate argument or CURLOPT_BUFFERSIZE value).
CVE-2018-0949
A security feature bypass vulnerability exists when Microsoft Internet Explorer improperly handles requests involving UNC resources, aka "Internet Explorer Security Feature Bypass Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10.
CVE-2018-10197
There is a time-based blind SQL injection vulnerability in the Access Manager component before 9.18.040 and 10.x before 10.18.040 in ELO ELOenterprise 9 and 10 and ELOprofessional 9 and 10 that makes it possible to read all database content. The vulnerability exists in the ticket HTTP GET parameter. For example, one can succeed in reading the password hash of the administrator user in the "userdata" table from the "eloam" database.
CVE-2018-10231
Cross-site scripting (XSS) vulnerability in TOPdesk before 8.05.017 (June 2018 version) and before 5.7.SR9 allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2018-10232
Cross-site request forgery (CSRF) vulnerability in TOPdesk before 8.05.017 (June 2018 version) and before 5.7.SR9 allows remote attackers to hijack the authentication of authenticated users for requests that can obtain sensitive information via unspecified vectors.
CVE-2018-10633
Universal Robots Robot Controllers Version CB 3.1, SW Version 3.4.5-100 utilizes hard-coded credentials that may allow an attacker to reset passwords for the controller.
CVE-2018-10635
In Universal Robots Robot Controllers Version CB 3.1, SW Version 3.4.5-100, ports 30001/TCP to 30003/TCP listen for arbitrary URScript code and execute the code. This enables a remote attacker who has access to the ports to remotely execute code that may allow root access to be obtained.
CVE-2018-11045
Pivotal Operations Manager, versions 2.1 prior to 2.1.6 and 2.0 prior to 2.0.15 and 1.12 prior to 1.12.22, contains a static Linux Random Number Generator (LRNG) seed file embedded in the appliance image. An attacker with knowledge of the exact version and IaaS of a running OpsManager could get the contents of the corresponding seed from the published image and therefore infer the initial state of the LRNG.
CVE-2018-11049
RSA Identity Governance and Lifecycle, RSA Via Lifecycle and Governance, and RSA IMG releases have an uncontrolled search vulnerability. The installation scripts set an environment variable in an unintended manner. A local authenticated malicious user could trick the root user to run malicious code on the targeted system.
CVE-2018-11529
VideoLAN VLC media player 2.2.x is prone to a use after free vulnerability which an attacker can leverage to execute arbitrary code via crafted MKV files. Failed exploit attempts will likely result in denial of service conditions.
CVE-2018-13878
An XSS issue was discovered in packages/rocketchat-mentions/Mentions.js in Rocket.Chat before 0.65. The real name of a username is displayed unescaped when the user is mentioned (using the @ symbol) in a channel or private chat. Consequently, it is possible to exfiltrate the secret token of every user and also admins in the channel.
CVE-2018-13879
A reflected XSS issue was discovered in the registration form in Rocket.Chat before 0.66. When one creates an account, the next step will ask for a username. This field will not save HTML control characters but an error will be displayed that shows the attempted username unescaped via packages/rocketchat-ui-login/client/username/username.js in packages/rocketchat-ui-login/client/username/username.html.
CVE-2018-13989
Grundig Smart Inter@ctive TV 3.0 devices allow CSRF attacks via a POST request to TCP port 8085 containing a predictable ID value, as demonstrated by a /sendrcpackage?keyid=-2544&keysymbol=-4081 request to shut off the device.
CVE-2018-3929
An exploitable heap corruption exists in the PowerPoint document conversion functionality of the Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312). A crafted PowerPoint (PPT) document can lead to heap corruption, resulting in remote code execution.
CVE-2018-3930
In Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312), a crafted Microsoft Word (DOC) document can lead to an out-of-bounds write, resulting in remote code execution. This vulnerability occurs in the `vbgetfp` method.
CVE-2018-3931
In Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312), a crafted Microsoft Word (DOC) document can lead to an out-of-bounds write, resulting in remote code execution. This vulnerability occurs in the `putShapeProperty` method.
CVE-2018-3932
An exploitable stack-based buffer overflow exists in the Microsoft Word document conversion functionality of the Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312). A crafted Microsoft Word (DOC) document can lead to a stack-based buffer overflow, resulting in remote code execution.
CVE-2018-3933
An exploitable out-of-bounds write exists in the Microsoft Word document conversion functionality of the Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312). A crafted Microsoft Word (DOC) document can lead to an out-of-bounds write, resulting in remote code execution. This vulnerability occurs in the `vbputanld` method.
CVE-2018-3936
In Antenna House Office Server Document Converter version V6.1 Pro MR2 for Linux64 (6,1,2018,0312), a crafted Microsoft Word (DOC) document can lead to an out-of-bounds write, resulting in remote code execution.
CVE-2018-8007
Apache CouchDB administrative users can configure the database server via HTTP(S). Due to insufficient validation of administrator-supplied configuration settings via the HTTP API, it is possible for a CouchDB administrator user to escalate their privileges to that of the operating system's user that CouchDB runs under, by bypassing the blacklist of configuration settings that are not allowed to be modified via the HTTP API. This privilege escalation effectively allows an existing CouchDB admin user to gain arbitrary remote code execution, bypassing already disclosed CVE-2017-12636. Mitigation: All users should upgrade to CouchDB releases 1.7.2 or 2.1.2.
CVE-2018-8125
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8262, CVE-2018-8274, CVE-2018-8275, CVE-2018-8279, CVE-2018-8301.
CVE-2018-8171
A Security Feature Bypass vulnerability exists in ASP.NET when the number of incorrect login attempts is not validated, aka "ASP.NET Security Feature Bypass Vulnerability." This affects ASP.NET, ASP.NET Core 1.1, ASP.NET Core 1.0, ASP.NET Core 2.0, ASP.NET MVC 5.2.
CVE-2018-8172
A remote code execution vulnerability exists in Visual Studio software when the software does not check the source markup of a file for an unbuilt project, aka "Visual Studio Remote Code Execution Vulnerability." This affects Microsoft Visual Studio, Expression Blend 4.
CVE-2018-8202
An elevation of privilege vulnerability exists in .NET Framework which could allow an attacker to elevate their privilege level, aka ".NET Framework Elevation of Privilege Vulnerability." This affects Microsoft .NET Framework 2.0, Microsoft .NET Framework 3.0, Microsoft .NET Framework 4.6.2/4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.5.2, Microsoft .NET Framework 4.6, Microsoft .NET Framework 4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.7.1/4.7.2, Microsoft .NET Framework 3.5, Microsoft .NET Framework 3.5.1, Microsoft .NET Framework 4.6/4.6.1/4.6.2, Microsoft .NET Framework 4.6/4.6.1/4.6.2/4.7/4.7.1/4.7.1/4.7.2, Microsoft .NET Framework 4.7.2.
CVE-2018-8206
A denial of service vulnerability exists when Windows improperly handles File Transfer Protocol (FTP) connections, aka "Windows FTP Server Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8222
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8232
A Tampering vulnerability exists when Microsoft Macro Assembler improperly validates code, aka "Microsoft Macro Assembler Tampering Vulnerability." This affects Microsoft Visual Studio.
CVE-2018-8238
A security feature bypass vulnerability exists when Skype for Business or Lync do not properly parse UNC path links shared via messages, aka "Skype for Business and Lync Security Feature Bypass Vulnerability." This affects Skype, Microsoft Lync.
CVE-2018-8242
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-8283, CVE-2018-8287, CVE-2018-8288, CVE-2018-8291, CVE-2018-8296, CVE-2018-8298.
CVE-2018-8260
A Remote Code Execution vulnerability exists in .NET software when the software fails to check the source markup of a file, aka ".NET Framework Remote Code Execution Vulnerability." This affects .NET Framework 4.7.2, Microsoft .NET Framework 4.7.2.
CVE-2018-8262
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8125, CVE-2018-8274, CVE-2018-8275, CVE-2018-8279, CVE-2018-8301.
CVE-2018-8274
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8125, CVE-2018-8262, CVE-2018-8275, CVE-2018-8279, CVE-2018-8301.
CVE-2018-8275
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8125, CVE-2018-8262, CVE-2018-8274, CVE-2018-8279, CVE-2018-8301.
CVE-2018-8276
A security feature bypass vulnerability exists in the Microsoft Chakra scripting engine that allows Control Flow Guard (CFG) to be bypassed, aka "Scripting Engine Security Feature Bypass Vulnerability." This affects Microsoft Edge, ChakraCore.
CVE-2018-8278
A spoofing vulnerability exists when Microsoft Edge improperly handles specific HTML content, aka "Microsoft Edge Spoofing Vulnerability." This affects Microsoft Edge.
CVE-2018-8279
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8125, CVE-2018-8262, CVE-2018-8274, CVE-2018-8275, CVE-2018-8301.
CVE-2018-8280
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8286, CVE-2018-8290, CVE-2018-8294.
CVE-2018-8281
A remote code execution vulnerability exists in Microsoft Office software when the software fails to properly handle objects in memory, aka "Microsoft Office Remote Code Execution Vulnerability." This affects Microsoft Excel Viewer, Microsoft PowerPoint Viewer, Microsoft Office, Microsoft Office Word Viewer.
CVE-2018-8282
An elevation of privilege vulnerability exists in Windows when the Windows kernel-mode driver fails to properly handle objects in memory, aka "Win32k Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8283
A remote code execution vulnerability exists in the way that the ChakraCore scripting engine handles objects in memory, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore. This CVE ID is unique from CVE-2018-8242, CVE-2018-8287, CVE-2018-8288, CVE-2018-8291, CVE-2018-8296, CVE-2018-8298.
CVE-2018-8284
A remote code execution vulnerability exists when the Microsoft .NET Framework fails to validate input properly, aka ".NET Framework Remote Code Injection Vulnerability." This affects Microsoft .NET Framework 2.0, Microsoft .NET Framework 3.0, Microsoft .NET Framework 4.6.2/4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.5.2, Microsoft .NET Framework 4.6, Microsoft .NET Framework 4.7/4.7.1/4.7.2, Microsoft .NET Framework 4.7.1/4.7.2, Microsoft .NET Framework 3.5, Microsoft .NET Framework 3.5.1, Microsoft .NET Framework 4.6/4.6.1/4.6.2, Microsoft .NET Framework 4.6/4.6.1/4.6.2/4.7/4.7.1/4.7.1/4.7.2, Microsoft .NET Framework 4.7.2.
CVE-2018-8286
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8280, CVE-2018-8290, CVE-2018-8294.
CVE-2018-8287
A remote code execution vulnerability exists in the way the scripting engine handles objects in memory in Microsoft browsers, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore, Internet Explorer 11, Microsoft Edge, Internet Explorer 10. This CVE ID is unique from CVE-2018-8242, CVE-2018-8283, CVE-2018-8288, CVE-2018-8291, CVE-2018-8296, CVE-2018-8298.
CVE-2018-8288
A remote code execution vulnerability exists in the way the scripting engine handles objects in memory in Microsoft browsers, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore, Internet Explorer 11, Microsoft Edge. This CVE ID is unique from CVE-2018-8242, CVE-2018-8283, CVE-2018-8287, CVE-2018-8291, CVE-2018-8296, CVE-2018-8298.
CVE-2018-8289
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8297, CVE-2018-8324, CVE-2018-8325.
CVE-2018-8290
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8280, CVE-2018-8286, CVE-2018-8294.
CVE-2018-8291
A remote code execution vulnerability exists in the way the scripting engine handles objects in memory in Microsoft browsers, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore, Internet Explorer 11, Microsoft Edge. This CVE ID is unique from CVE-2018-8242, CVE-2018-8283, CVE-2018-8287, CVE-2018-8288, CVE-2018-8296, CVE-2018-8298.
CVE-2018-8294
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8280, CVE-2018-8286, CVE-2018-8290.
CVE-2018-8296
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 11. This CVE ID is unique from CVE-2018-8242, CVE-2018-8283, CVE-2018-8287, CVE-2018-8288, CVE-2018-8291, CVE-2018-8298.
CVE-2018-8297
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8289, CVE-2018-8324, CVE-2018-8325.
CVE-2018-8298
A remote code execution vulnerability exists in the way that the ChakraCore scripting engine handles objects in memory, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore. This CVE ID is unique from CVE-2018-8242, CVE-2018-8283, CVE-2018-8287, CVE-2018-8288, CVE-2018-8291, CVE-2018-8296.
CVE-2018-8299
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-8323.
CVE-2018-8300
A remote code execution vulnerability exists in Microsoft SharePoint when the software fails to check the source markup of an application package, aka "Microsoft SharePoint Remote Code Execution Vulnerability." This affects Microsoft SharePoint.
CVE-2018-8301
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8125, CVE-2018-8262, CVE-2018-8274, CVE-2018-8275, CVE-2018-8279.
CVE-2018-8304
A denial of service vulnerability exists in Windows Domain Name System (DNS) DNSAPI.dll when it fails to properly handle DNS responses, aka "Windows DNSAPI Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8305
An information disclosure vulnerability exists in Windows Mail Client when a message is opened, aka "Windows Mail Client Information Disclosure Vulnerability." This affects Mail, Calendar, and People in Windows 8.1 App Store.
CVE-2018-8306
A command injection vulnerability exists in the Microsoft Wireless Display Adapter (MWDA) when the Microsoft Wireless Display Adapter does not properly manage user input, aka "Microsoft Wireless Display Adapter Command Injection Vulnerability." This affects Microsoft Wireless Display Adapter V2 Software.
CVE-2018-8307
A security feature bypass vulnerability exists when Microsoft WordPad improperly handles embedded OLE objects, aka "WordPad Security Feature Bypass Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8308
An elevation of privilege vulnerability exists when the Windows kernel fails to properly handle objects in memory, aka "Windows Kernel Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8309
A denial of service vulnerability exists when Windows improperly handles objects in memory, aka "Windows Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8310
A tampering vulnerability exists when Microsoft Outlook does not properly handle specific attachment types when rendering HTML emails, aka "Microsoft Office Tampering Vulnerability." This affects Microsoft Word, Microsoft Office.
CVE-2018-8311
A remote code execution vulnerability exists when Skype for Business and Microsoft Lync clients fail to properly sanitize specially crafted content, aka "Remote Code Execution Vulnerability in Skype For Business and Lync." This affects Skype, Microsoft Lync.
CVE-2018-8312
A remote code execution vulnerability exists when Microsoft Access fails to properly handle objects in memory, aka "Microsoft Access Remote Code Execution Vulnerability." This affects Microsoft Access, Microsoft Office.
CVE-2018-8313
An elevation of privilege vulnerability exists in the way that the Windows Kernel API enforces permissions, aka "Windows Elevation of Privilege Vulnerability." This affects Windows Server 2012 R2, Windows RT 8.1, Windows Server 2012, Windows Server 2016, Windows 8.1, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8314.
CVE-2018-8314
An elevation of privilege vulnerability exists when Windows fails a check, allowing a sandbox escape, aka "Windows Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2008 R2, Windows 10. This CVE ID is unique from CVE-2018-8313.
CVE-2018-8319
A Security Feature Bypass vulnerability exists in MSR JavaScript Cryptography Library that is caused by incorrect arithmetic computations, aka "MSR JavaScript Cryptography Library Security Feature Bypass Vulnerability." This affects Microsoft Research JavaScript Cryptography Library.
CVE-2018-8323
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-8299.
CVE-2018-8324
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8289, CVE-2018-8297, CVE-2018-8325.
CVE-2018-8325
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8289, CVE-2018-8297, CVE-2018-8324.
CVE-2018-8326
A cross-site-scripting (XSS) vulnerability exists when an open source customization for Microsoft Active Directory Federation Services (AD FS) does not properly sanitize a specially crafted web request to an affected AD FS server, aka "Open Source Customization for Active Directory Federation Services XSS Vulnerability." This affects Web Customizations.
CVE-2018-8327
A remote code execution vulnerability exists in PowerShell Editor Services, aka "PowerShell Editor Services Remote Code Execution Vulnerability." This affects PowerShell Editor, PowerShell Extension.
CVE-2018-8356
A security feature bypass vulnerability exists when Microsoft .NET Framework components do not correctly validate certificates, aka ".NET Framework Security Feature Bypass Vulnerability." This affects .NET Framework 4.7.2, Microsoft .NET Framework 3.0, Microsoft .NET Framework 4.6.2/4.7/4.7.1/4.7.2, ASP.NET Core 1.1, Microsoft .NET Framework 4.5.2, ASP.NET Core 2.0, ASP.NET Core 1.0, .NET Core 1.1, Microsoft .NET Framework 3.5, Microsoft .NET Framework 3.5.1, Microsoft .NET Framework 4.6/4.6.1/4.6.2, .NET Core 1.0, .NET Core 2.0, Microsoft .NET Framework 4.6, Microsoft .NET Framework 4.6/4.6.1/4.6.2/4.7/4.7.1/4.7.1/4.7.2, Microsoft .NET Framework 4.7.2.
