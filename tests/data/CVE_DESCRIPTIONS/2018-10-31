CVE-2016-2121
A permissions flaw was found in redis, which sets weak permissions on certain files and directories that could potentially contain sensitive information. A local, unprivileged user could possibly use this flaw to access unauthorized system information.
CVE-2016-2125
It was found that Samba before versions 4.5.3, 4.4.8, 4.3.13 always requested forwardable tickets when using Kerberos authentication. A service to which Samba authenticated using Kerberos could subsequently use the ticket to impersonate Samba to other services or domain users.
CVE-2016-5402
A code injection flaw was found in the way capacity and utilization imported control files are processed. A remote, authenticated attacker with access to the capacity and utilization feature could use this flaw to execute arbitrary code as the user CFME runs as.
CVE-2016-6328
A vulnerability was found in libexif. An integer overflow when parsing the MNOTE entry data of the input file. This can cause Denial-of-Service (DoS) and Information Disclosure (disclosing some critical heap chunk metadata, even other applications' private data).
CVE-2016-6343
JBoss BPM Suite 6 is vulnerable to a reflected XSS via dashbuilder. Remote attackers can entice authenticated users that have privileges to access dashbuilder (usually admins) to click on links to /dashbuilder/Controller containing malicious scripts. Successful exploitation would allow execution of script code within the context of the affected user.
CVE-2018-11759
The Apache Web Server (httpd) specific code that normalised the requested path before matching it to the URI-worker map in Apache Tomcat JK (mod_jk) Connector 1.2.0 to 1.2.44 did not handle some edge cases correctly. If only a sub-set of the URLs supported by Tomcat were exposed via httpd, then it was possible for a specially constructed request to expose application functionality through the reverse proxy that was not intended for clients accessing the application via the reverse proxy. It was also possible in some configurations for a specially constructed request to bypass the access controls configured in httpd. While there is some overlap between this issue and CVE-2018-1323, they are not identical.
CVE-2018-13281
Information exposure vulnerability in SYNO.Core.ACL in Synology DiskStation Manager (DSM) before 6.2-23739-2 allows remote authenticated users to determine the existence and obtain the metadata of arbitrary files via the file_path parameter.
CVE-2018-13282
Session fixation vulnerability in SYNO.PhotoStation.Auth in Synology Photo Station before 6.8.7-3481 allows remote attackers to hijack web sessions via the PHPSESSID parameter.
CVE-2018-14651
It was found that the fix for CVE-2018-10927, CVE-2018-10928, CVE-2018-10929, CVE-2018-10930, and CVE-2018-10926 was incomplete. A remote, authenticated attacker could use one of these flaws to execute arbitrary code, create arbitrary files, or cause denial of service on glusterfs server nodes via symlinks to relative paths.
CVE-2018-14652
The Gluster file system through versions 3.12 and 4.1.4 is vulnerable to a buffer overflow in the 'features/index' translator via the code handling the 'GF_XATTR_CLRLK_CMD' xattr in the 'pl_getxattr' function. A remote authenticated attacker could exploit this on a mounted volume to cause a denial of service.
CVE-2018-14653
The Gluster file system through versions 4.1.4 and 3.12 is vulnerable to a heap-based buffer overflow in the '__server_getspec' function via the 'gf_getspec_req' RPC message. A remote authenticated attacker could exploit this to cause a denial of service or other potential unspecified impact.
CVE-2018-14654
The Gluster file system through version 4.1.4 is vulnerable to abuse of the 'features/index' translator. A remote attacker with access to mount volumes could exploit this via the 'GF_XATTROP_ENTRY_IN_KEY' xattrop to create arbitrary, empty files on the target server.
CVE-2018-14659
The Gluster file system through versions 4.1.4 and 3.1.2 is vulnerable to a denial of service attack via use of the 'GF_XATTR_IOSTATS_DUMP_KEY' xattr. A remote, authenticated attacker could exploit this by mounting a Gluster volume and repeatedly calling 'setxattr(2)' to trigger a state dump and create an arbitrary number of files in the server's runtime directory.
CVE-2018-14661
It was found that usage of snprintf function in feature/locks translator of glusterfs server 3.8.4, as shipped with Red Hat Gluster Storage, was vulnerable to a format string attack. A remote, authenticated attacker could use this flaw to cause remote denial of service.
CVE-2018-15317
In BIG-IP 14.0.0-14.0.0.2, 13.0.0-13.1.1.5, 12.1.0-12.1.4.1, and 11.2.1-11.6.3.2, an attacker sending specially crafted SSL records to a SSL Virtual Server will cause corruption in the SSL data structures leading to intermittent decrypt BAD_RECORD_MAC errors. Clients will be unable to access the application load balanced by a virtual server with an SSL profile until tmm is restarted.
CVE-2018-15318
In BIG-IP 14.0.0-14.0.0.2, 13.1.0.4-13.1.1.1, or 12.1.3.4-12.1.3.6, If an MPTCP connection receives an abort signal while the initial flow is not the primary flow, the initial flow will remain after the closing procedure is complete. TMM may restart and produce a core file as a result of this condition.
CVE-2018-15319
On BIG-IP 14.0.0-14.0.0.2, 13.0.0-13.1.1.1, or 12.1.0-12.1.3.6, malicious requests made to virtual servers with an HTTP profile can cause the TMM to restart. The issue is exposed with the non-default "normalize URI" configuration options used in iRules and/or BIG-IP LTM policies.
CVE-2018-15320
On BIG-IP 14.0.0-14.0.0.2 or 13.0.0-13.1.1.1, undisclosed traffic patterns may lead to denial of service conditions for the BIG-IP system. The configuration which exposes this condition is the BIG-IP self IP address which is part of a VLAN group and has the Port Lockdown setting configured with anything other than "allow-all".
CVE-2018-15321
When BIG-IP 14.0.0-14.0.0.2, 13.0.0-13.1.0.5, 12.1.0-12.1.3.5, 11.6.0-11.6.3.2, or 11.2.1-11.5.6, BIG-IQ Centralized Management 5.0.0-5.4.0 or 4.6.0, BIG-IQ Cloud and Orchestration 1.0.0, iWorkflow 2.1.0-2.3.0, or Enterprise Manager 3.1.1 is licensed for Appliance Mode, Admin and Resource administrator roles can by-pass BIG-IP Appliance Mode restrictions to overwrite critical system files. Attackers of high privilege level are able to overwrite critical system files which bypasses security controls in place to limit TMSH commands. This is possible with an administrator or resource administrator roles when granted TMSH. Resource administrator roles must have TMSH access in order to perform this attack.
CVE-2018-15322
On BIG-IP 14.0.0-14.0.0.2, 13.0.0-13.1.0.7, 12.1.0-12.1.3.5, 11.6.0-11.6.3.2, or 11.2.1-11.5.6, BIG-IQ Centralized Management 6.0.0-6.0.1, 5.0.0-5.4.0 or 4.6.0, BIG-IQ Cloud and Orchestration 1.0.0, iWorkflow 2.0.1-2.3.0, or Enterprise Manager 3.1.1 a BIG-IP user granted with tmsh access may cause the BIG-IP system to experience denial-of-service (DoS) when the BIG-IP user uses the tmsh utility to run the edit cli preference command and proceeds to save the changes to another filename repeatedly. This action utilises storage space on the /var partition and when performed repeatedly causes the /var partition to be full.
CVE-2018-15323
On BIG-IP 14.0.0-14.0.0.2 or 13.0.0-13.1.1.1, in certain circumstances, when processing traffic through a Virtual Server with an associated MQTT profile, the TMM process may produce a core file and take the configured HA action.
CVE-2018-15324
On BIG-IP APM 14.0.0-14.0.0.2 or 13.0.0-13.1.1.1, TMM may restart when processing a specially crafted request with APM portal access.
CVE-2018-15325
In BIG-IP 14.0.0-14.0.0.2 or 13.0.0-13.1.1.1, iControl and TMSH usage by authenticated users may leak a small amount of memory when executing commands
CVE-2018-15326
In some situations on BIG-IP APM 14.0.0-14.0.0.2, 13.0.0-13.1.0.7, 12.1.0-12.1.3.5, or 11.6.0-11.6.3.2, the CRLDP Auth access policy agent may treat revoked certificates as valid when the BIG-IP APM system fails to download a new Certificate Revocation List.
CVE-2018-15327
In BIG-IP 14.0.0-14.0.0.2 or 13.0.0-13.1.1.1 or Enterprise Manager 3.1.1, when authenticated administrative users run commands in the Traffic Management User Interface (TMUI), also referred to as the BIG-IP Configuration utility, restrictions on allowed commands may not be enforced.
CVE-2018-15705
WADashboard API in Advantech WebAccess 8.3.1 and 8.3.2 allows remote authenticated attackers to write or overwrite any file on the filesystem due to a directory traversal vulnerability in the writeFile API. An attacker can use this vulnerability to remotely execute arbitrary code.
CVE-2018-15706
WADashboard API in Advantech WebAccess 8.3.1 and 8.3.2 allows remote authenticated attackers to read any file on the filesystem due to a directory traversal vulnerability in the readFile API.
CVE-2018-15707
Advantech WebAccess 8.3.1 and 8.3.2 are vulnerable to cross-site scripting in the Bwmainleft.asp page. An attacker could leverage this vulnerability to disclose credentials amongst other things.
CVE-2018-16839
Curl versions 7.33.0 through 7.61.1 are vulnerable to a buffer overrun in the SASL authentication code that may lead to denial of service.
CVE-2018-16840
A heap use-after-free flaw was found in curl versions from 7.59.0 through 7.61.1 in the code related to closing an easy handle. When closing and cleaning up an 'easy' handle in the `Curl_close()` function, the library code first frees a struct (without nulling the pointer) and might then subsequently erroneously write to a struct field within that already freed struct.
CVE-2018-16842
Curl versions 7.14.1 through 7.61.1 are vulnerable to a heap-based buffer over-read in the tool_msgs.c:voutf() function that may result in information exposure and denial of service.
CVE-2018-1851
IBM WebSphere Application Server Liberty OpenID Connect could allow a remote attacker to execute arbitrary code on the system, caused by improper deserialization. By sending a specially-crafted request to the RP service, an attacker could exploit this vulnerability to execute arbitrary code. IBM X-Force ID: 150999.
CVE-2018-18850
In Octopus Deploy 2018.8.0 through 2018.9.x before 2018.9.1, an authenticated user with permission to modify deployment processes could upload a maliciously crafted YAML configuration, potentially allowing for remote execution of arbitrary code, running in the same context as the Octopus Server (for self-hosted installations by default, SYSTEM).
CVE-2018-18853
Lightbend Spray spray-json through 1.3.4 allows remote attackers to cause a denial of service (resource consumption) because of Algorithmic Complexity during the parsing of a field composed of many decimal digits.
CVE-2018-18854
Lightbend Spray spray-json through 1.3.4 allows remote attackers to cause a denial of service (resource consumption) because of Algorithmic Complexity during the parsing of many JSON object fields (with keys that have the same hash code).
CVE-2018-18867
An SSRF issue was discovered in tecrail Responsive FileManager 9.13.4 via the upload.php url parameter. NOTE: this issue exists because of an incomplete fix for CVE-2018-15495.
CVE-2018-18868
No-CMS 1.1.3 is prone to Persistent XSS via a contact_us name parameter, as demonstrated by the VG48Z5PqVWname parameter.
CVE-2018-18869
EmpireCMS V7.5 allows remote attackers to upload and execute arbitrary code via ..%2F directory traversal in a .php filename in the upload/e/admin/ecmscom.php path parameter.
CVE-2018-18873
An issue was discovered in JasPer 2.0.14. There is a NULL pointer dereference in the function ras_putdatastd in ras/ras_enc.c.
CVE-2018-18874
nc-cms through 2017-03-10 allows remote attackers to execute arbitrary PHP code via the "Upload File or Image" feature, with a .php filename and "Content-Type: application/octet-stream" to the index.php?action=file_manager_upload URI.
