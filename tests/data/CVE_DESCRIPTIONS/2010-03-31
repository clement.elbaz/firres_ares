CVE-2010-0132
Cross-site scripting (XSS) vulnerability in ViewVC 1.1 before 1.1.5 and 1.0 before 1.0.11, when the regular expression search functionality is enabled, allows remote attackers to inject arbitrary web script or HTML via vectors related to "search_re input," a different vulnerability than CVE-2010-0736.
CVE-2010-0267
Microsoft Internet Explorer 6, 6 SP1, and 7 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 5.01 Service Pack 4 and Internet Explorer 8 are not affected by this vulnerability.'
CVE-2010-0448
Unspecified vulnerability in HP SOA Registry Foundation 6.63 and 6.64 allows remote attackers to obtain "unauthorized access to data" via unknown vectors.
CVE-2010-0449
Cross-site scripting (XSS) vulnerability in HP SOA Registry Foundation 6.63 and 6.64 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2010-0450
Unspecified vulnerability in HP SOA Registry Foundation 6.63 and 6.64 allows remote authenticated users to gain privileges via unknown vectors.
CVE-2010-0488
Microsoft Internet Explorer 5.01 SP4, 6, 6 SP1, and 7 does not properly handle unspecified "encoding strings," which allows remote attackers to bypass the Same Origin Policy and obtain sensitive information via a crafted web site, aka "Post Encoding Information Disclosure Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 8 is not affected by this vulnerability.'
CVE-2010-0489
Race condition in Microsoft Internet Explorer 5.01 SP4, 6, 6 SP1, and 7 allows remote attackers to execute arbitrary code via a crafted HTML document that triggers memory corruption, aka "Race Condition Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 8 is not affected by this vulnerability.'
CVE-2010-0490
Microsoft Internet Explorer 6, 6 SP1, 7, and 8 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "Uninitialized Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 5.01 Service Pack 4 is not affected by this vulnerability.'
CVE-2010-0491
Use-after-free vulnerability in Microsoft Internet Explorer 5.01 SP4, 6, and 6 SP1 allows remote attackers to execute arbitrary code by changing unspecified properties of an HTML object that has an onreadystatechange event handler, aka "HTML Object Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 7 and Internet Explorer 8 are not affected by this vulnerability.'
CVE-2010-0492
Use-after-free vulnerability in mstime.dll in Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code via vectors related to the TIME2 behavior, the CTimeAction object, and destruction of markup, leading to memory corruption, aka "HTML Object Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 6 and Internet Explorer 7 are not affected by this vulnerability.'
CVE-2010-0494
Cross-domain vulnerability in Microsoft Internet Explorer 6, 6 SP1, 7, and 8 allows user-assisted remote attackers to bypass the Same Origin Policy and conduct cross-site scripting (XSS) attacks via a crafted HTML document in a situation where the client user drags one browser window across another browser window, aka "HTML Element Cross-Domain Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 5.01 Service Pack 4 is not affected by this vulnerability.'
CVE-2010-0527
Integer overflow in Apple QuickTime before 7.6.6 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PICT image.
Per: http://lists.apple.com/archives/security-announce/2010//Mar/msg00002.html

'This issue does not affect Mac OS X systems'
CVE-2010-0528
Apple QuickTime before 7.6.6 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via crafted color tables in a movie file, related to malformed MediaVideo data, a sample description atom (STSD), and a crafted length value.
Per: http://lists.apple.com/archives/security-announce/2010//Mar/msg00002.html

'This issue does not affect Mac OS X systems.'
CVE-2010-0529
Heap-based buffer overflow in QuickTime.qts in Apple QuickTime before 7.6.6 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a PICT image with a BkPixPat opcode (0x12) containing crafted values that are used in a calculation for memory allocation.
Per: http://lists.apple.com/archives/security-announce/2010//Mar/msg00002.html

'This issue does not affect Mac OS X systems.'
CVE-2010-0531
Apple iTunes before 9.1 allows remote attackers to cause a denial of service (infinite loop) via a crafted MP4 podcast file.
CVE-2010-0532
Race condition in the installation package in Apple iTunes before 9.1 on Windows allows local users to gain privileges by replacing an unspecified file with a Trojan horse.
Per: http://lists.apple.com/archives/security-announce/2010//Mar/msg00003.html

'This issue does not affect Mac OS X systems.'
CVE-2010-0536
Apple QuickTime before 7.6.6 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted BMP image.
Per: http://lists.apple.com/archives/security-announce/2010//Mar/msg00002.html

' This issue does not affect Mac OS X systems.'
CVE-2010-0805
The Tabular Data Control (TDC) ActiveX control in Microsoft Internet Explorer 5.01 SP4, 6 on Windows XP SP2 and SP3, and 6 SP1 allows remote attackers to execute arbitrary code via a long URL (DataURL parameter) that triggers memory corruption in the CTDCCtl::SecurityCHeckDataURL function, aka "Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 7 and Internet Explorer 8 are not affected by this vulnerability.'
CVE-2010-0807
Microsoft Internet Explorer 7 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, leading to memory corruption, aka "HTML Rendering Memory Corruption Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-018.mspx

'Internet Explorer 5.01 Service Pack 4, Internet Explorer 6, Internet Explorer 6 Service Pack 1, and Internet Explorer 8 are not affected by this vulnerability.'
CVE-2010-1030
Unspecified vulnerability in HP-UX B.11.31, with AudFilter rules enabled, allows local users to cause a denial of service via unknown vectors.
CVE-2010-1187
The Transparent Inter-Process Communication (TIPC) functionality in Linux kernel 2.6.16-rc1 through 2.6.33, and possibly other versions, allows local users to cause a denial of service (kernel OOPS) by sending datagrams through AF_TIPC before entering network mode, which triggers a NULL pointer dereference.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2010-1188
Use-after-free vulnerability in net/ipv4/tcp_input.c in the Linux kernel 2.6 before 2.6.20, when IPV6_RECVPKTINFO is set on a listening socket, allows remote attackers to cause a denial of service (kernel panic) via a SYN packet while the socket is in a listening (TCP_LISTEN) state, which is not properly handled and causes the skb structure to be freed.
CVE-2010-1189
MediaWiki before 1.15.2 does not prevent wiki editors from linking to images from other web sites in wiki pages, which allows editors to obtain IP addresses and other information of wiki users by adding a link to an image on an attacker-controlled web site, aka "CSS validation issue."
CVE-2010-1190
thumb.php in MediaWiki before 1.15.2, when used with access-restriction mechanisms such as img_auth.php, does not check user permissions before providing scaled images, which allows remote attackers to bypass intended access restrictions and read private images via unspecified manipulations.
CVE-2010-1191
Sahana disaster management system 0.6.2.2, and possibly other versions, allows remote attackers to bypass intended access restrictions and disable administrator authentication via a direct request to stream.php in an acl_enable_acl action to the admin module.
CVE-2010-1192
libESMTP, probably 1.0.4 and earlier, does not properly handle a '\0' character in a domain name in the subject's Common Name (CN) field of an X.509 certificate, which allows man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority, a related issue to CVE-2009-2408.
CVE-2010-1194
The match_component function in smtp-tls.c in libESMTP 1.0.3.r1, and possibly other versions including 1.0.4, treats two strings as equal if one is a substring of the other, which allows remote attackers to spoof trusted certificates via a crafted subjectAltName.
CVE-2010-1195
Cross-site scripting (XSS) vulnerability in the htmlscrubber component in ikiwiki 2.x before 2.53.5 and 3.x before 3.20100312 allows remote attackers to inject arbitrary web script or HTML via a crafted data:image/svg+xml URI.
