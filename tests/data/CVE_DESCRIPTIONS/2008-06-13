CVE-2008-2364
The ap_proxy_http_process_response function in mod_proxy_http.c in the mod_proxy module in the Apache HTTP Server 2.0.63 and 2.2.8 does not limit the number of forwarded interim responses, which allows remote HTTP servers to cause a denial of service (memory consumption) via a large number of interim responses.
CVE-2008-2654
Off-by-one error in the read_client function in webhttpd.c in Motion 3.2.10 and earlier might allow remote attackers to execute arbitrary code via a long request to a Motion HTTP Control interface, which triggers a stack-based buffer overflow with some combinations of processor architecture and compiler.
CVE-2008-2686
webinc/bxe/scripts/loadsave.php in Flux CMS 1.5.0 and earlier allows remote attackers to execute arbitrary code by overwriting a PHP file in webinc/bxe/scripts/ via a filename in the XML parameter and PHP sequences in the request body, then making a direct request for this filename.
CVE-2008-2687
Directory traversal vulnerability in inc/config.php in ProManager 0.73 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the language parameter.
CVE-2008-2688
SQL injection vulnerability in pilot.asp in ASPilot Pilot Cart 7.3 allows remote attackers to execute arbitrary SQL commands via the article parameter in a kb action.
CVE-2008-2689
PHP remote file inclusion vulnerability in pub/clients.php in BrowserCRM 5.002.00 allows remote attackers to execute arbitrary PHP code via a URL in the bcrm_pub_root parameter.
CVE-2008-2690
Multiple PHP remote file inclusion vulnerabilities in BrowserCRM 5.002.00, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the bcrm_pub_root parameter to (1) kb.php, (2) login.php, (3) index.php, (4) contact_view.php, and (5) contact.php in pub/, different vectors than CVE-2008-2689.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-2691
SQL injection vulnerability in read.asp in JiRo's FAQ Manager eXperience 1.0 allows remote attackers to execute arbitrary SQL commands via the fID parameter.
CVE-2008-2692
SQL injection vulnerability in the yvComment (com_yvcomment) component 1.16.0 and earlier for Joomla! allows remote attackers to execute arbitrary SQL commands via the ArticleID parameter in a comment action to index.php.
CVE-2008-2693
Stack-based buffer overflow in the BITIFF.BITiffCtrl.1 ActiveX control in BITiff.ocx 10.9.3.0 in Black Ice Barcode SDK 5.01 allows remote attackers to execute arbitrary code via a long first argument to the SetByteOrder method.
CVE-2008-2694
Cross-site scripting (XSS) vulnerability in search.php in phpInv 0.8.0 allows remote attackers to inject arbitrary web script or HTML via the keyword parameter.
CVE-2008-2695
Directory traversal vulnerability in entry.php in phpInv 0.8.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the action parameter.
CVE-2008-2696
Exiv2 0.16 allows user-assisted remote attackers to cause a denial of service (divide-by-zero and application crash) via a zero value in Nikon lens information in the metadata of an image, related to "pretty printing" and the RationalValue::toLong function.
CVE-2008-2697
SQL injection vulnerability in the Rapid Recipe (com_rapidrecipe) component 1.6.6 and 1.6.7 for Joomla! allows remote attackers to execute arbitrary SQL commands via the recipe_id parameter in a viewrecipe action to index.php.
CVE-2008-2698
Multiple cross-site scripting (XSS) vulnerabilities in photo_add-c.php (aka the "add comment" section) in WEBalbum 2.0 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) comment, (2) id, or (3) category parameter.
CVE-2008-2699
Multiple directory traversal vulnerabilities in Galatolo WebManager (GWM) 1.0 allow remote attackers to include and execute arbitrary local files via directory traversal sequences in (1) the plugin parameter to admin/plugins.php or (2) the com parameter to index.php.
CVE-2008-2700
SQL injection vulnerability in view.php in Galatolo WebManager 1.0 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-2701
SQL injection vulnerability in the GameQ (com_gameq) component 4.0 and earlier for Joomla! allows remote attackers to execute arbitrary SQL commands via the category_id parameter in a page action to index.php.
CVE-2008-2702
Directory traversal vulnerability in the FTP client in ALTools ESTsoft ALFTP 4.1 beta 2 and 5.0 allows remote FTP servers to create or overwrite arbitrary files via a .. (dot dot) in a response to a LIST command, a related issue to CVE-2002-1345.  NOTE: this can be leveraged for code execution by writing to a Startup folder.
CVE-2008-2703
Multiple stack-based buffer overflows in Novell GroupWise Messenger (GWIM) Client before 2.0.3 HP1 for Windows allow remote attackers to execute arbitrary code via "spoofed server responses" that contain a long string after the NM_A_SZ_TRANSACTION_ID field name.
CVE-2008-2704
Novell GroupWise Messenger (GWIM) before 2.0.3 Hot Patch 1 allows remote attackers to cause a denial of service (crash) via a long user ID, possibly involving a popup alert.  NOTE: it is not clear whether this issue crosses privilege boundaries.
