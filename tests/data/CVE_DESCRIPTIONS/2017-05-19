CVE-2015-5241
After logging into the portal, the logout jsp page redirects the browser back to the login page after. It is feasible for malicious users to redirect the browser to an unintended web page in Apache jUDDI 3.1.2, 3.1.3, 3.1.4, and 3.1.5 when utilizing the portlets based user interface also known as 'Pluto', 'jUDDI Portal', 'UDDI Portal' or 'uddi-console'. User session data, credentials, and auth tokens are cleared before the redirect.
CVE-2017-4978
EMC RSA Adaptive Authentication (On-Premise) versions prior to 7.3 P2 (exclusive) contains a fix for a cross-site scripting vulnerability that could potentially be exploited by malicious users to compromise the affected system.
CVE-2017-4979
EMC Isilon OneFS 8.0.1.0, OneFS 8.0.0.0 - 8.0.0.2, OneFS 7.2.1.0 - 7.2.1.3, and OneFS 7.2.0.x is affected by an NFS export vulnerability. Under certain conditions, after upgrading a cluster from OneFS 7.1.1.x or earlier, users may have unexpected levels of access to some NFS exports.
CVE-2017-5173
An Improper Neutralization of Special Elements (in an OS command) issue was discovered in Geutebruck IP Camera G-Cam/EFD-2250 Version 1.11.0.12. An improper neutralization of special elements vulnerability has been identified. If special elements are not properly neutralized, an attacker can call multiple parameters that can allow access to the root level operating system which could allow remote code execution.
CVE-2017-5174
An Authentication Bypass issue was discovered in Geutebruck IP Camera G-Cam/EFD-2250 Version 1.11.0.12. An authentication bypass vulnerability has been identified. The existing file system architecture could allow attackers to bypass the access control that may allow remote code execution.
CVE-2017-5176
A DLL Hijack issue was discovered in Rockwell Automation Connected Components Workbench (CCW). The following versions are affected: Connected Components Workbench - Developer Edition, v9.01.00 and earlier: 9328-CCWDEVENE, 9328-CCWDEVZHE, 9328-CCWDEVFRE, 9328-CCWDEVITE, 9328-CCWDEVDEE, 9328-CCWDEVESE, and 9328-CCWDEVPTE; and Connected Components Workbench - Free Standard Edition (All Supported Languages), v9.01.00 and earlier. Certain DLLs included with versions of CCW software can be potentially hijacked to allow an attacker to gain rights to a victim's affected personal computer. Such access rights can be at the same or potentially higher level of privileges as the compromised user account, including and up to computer administrator privileges.
CVE-2017-5177
A Stack Buffer Overflow issue was discovered in VIPA Controls WinPLC7 5.0.45.5921 and prior. A stack-based buffer overflow vulnerability has been identified, where an attacker with a specially crafted packet could overflow the fixed length buffer. This could allow remote code execution.
CVE-2017-6016
An Improper Access Control issue was discovered in LCDS - Leao Consultoria e Desenvolvimento de Sistemas LTDA ME LAquis SCADA. The following versions are affected: Versions 4.1 and prior versions released before January 20, 2017. An Improper Access Control vulnerability has been identified, which may allow an authenticated user to modify application files to escalate privileges.
CVE-2017-6025
A Stack Buffer Overflow issue was discovered in 3S-Smart Software Solutions GmbH CODESYS Web Server. The following versions of CODESYS Web Server, part of the CODESYS WebVisu web browser visualization software, are affected: CODESYS Web Server Versions 2.3 and prior. A malicious user could overflow the stack buffer by providing overly long strings to functions that handle the XML. Because the function does not verify string size before copying to memory, the attacker may then be able to crash the application or run arbitrary code.
CVE-2017-6027
An Arbitrary File Upload issue was discovered in 3S-Smart Software Solutions GmbH CODESYS Web Server. The following versions of CODESYS Web Server, part of the CODESYS WebVisu web browser visualization software, are affected: CODESYS Web Server Versions 2.3 and prior. A specially crafted web server request may allow the upload of arbitrary files (with a dangerous type) to the CODESYS Web Server without authorization which may allow remote code execution.
CVE-2017-6048
A Command Injection issue was discovered in Satel Iberia SenNet Data Logger and Electricity Meters: SenNet Optimal DataLogger V5.37c-1.43c and prior, SenNet Solar Datalogger V5.03-1.56a and prior, and SenNet Multitask Meter V5.21a-1.18b and prior. Successful exploitation of this vulnerability could result in the attacker breaking out of the jailed shell and gaining full access to the system.
CVE-2017-7475
Cairo version 1.15.4 is vulnerable to a NULL pointer dereference related to the FT_Load_Glyph and FT_Render_Glyph resulting in an application crash.
CVE-2017-7504
HTTPServerILServlet.java in JMS over HTTP Invocation Layer of the JbossMQ implementation, which is enabled by default in Red Hat Jboss Application Server <= Jboss 4.X does not restrict the classes for which it performs deserialization, which allows remote attackers to execute arbitrary code via crafted serialized data.
CVE-2017-7907
An Improper XML Parser Configuration issue was discovered in Schneider Electric Wonderware Historian Client 2014 R2 SP1 and prior. An improperly restricted XML parser (with improper restriction of XML external entity reference, or XXE) may allow an attacker to enter malicious input through the application which could cause a denial of service or disclose file contents from a server or connected network.
CVE-2017-7935
A Resource Exhaustion issue was discovered in Phoenix Contact GmbH mGuard firmware versions 8.3.0 to 8.4.2. An attacker may compromise the device's availability by performing multiple initial VPN requests.
CVE-2017-7937
An Improper Authentication issue was discovered in Phoenix Contact GmbH mGuard firmware versions 8.3.0 to 8.4.2. An attacker may be able to gain unauthorized access to the user firewall when RADIUS servers are unreachable.
CVE-2017-7968
An Incorrect Default Permissions issue was discovered in Schneider Electric Wonderware InduSoft Web Studio v8.0 Patch 3 and prior versions. Upon installation, Wonderware InduSoft Web Studio creates a new directory and two files, which are placed in the system's path and can be manipulated by non-administrators. This could allow an authenticated user to escalate his or her privileges.
CVE-2017-9074
The IPv6 fragmentation implementation in the Linux kernel through 4.11.1 does not consider that the nexthdr field may be associated with an invalid option, which allows local users to cause a denial of service (out-of-bounds read and BUG) or possibly have unspecified other impact via crafted socket and send system calls.
CVE-2017-9075
The sctp_v6_create_accept_sk function in net/sctp/ipv6.c in the Linux kernel through 4.11.1 mishandles inheritance, which allows local users to cause a denial of service or possibly have unspecified other impact via crafted system calls, a related issue to CVE-2017-8890.
CVE-2017-9076
The dccp_v6_request_recv_sock function in net/dccp/ipv6.c in the Linux kernel through 4.11.1 mishandles inheritance, which allows local users to cause a denial of service or possibly have unspecified other impact via crafted system calls, a related issue to CVE-2017-8890.
CVE-2017-9077
The tcp_v6_syn_recv_sock function in net/ipv6/tcp_ipv6.c in the Linux kernel through 4.11.1 mishandles inheritance, which allows local users to cause a denial of service or possibly have unspecified other impact via crafted system calls, a related issue to CVE-2017-8890.
CVE-2017-9078
The server in Dropbear before 2017.75 might allow post-authentication root remote code execution because of a double free in cleanup of TCP listeners when the -a option is enabled.
CVE-2017-9079
Dropbear before 2017.75 might allow local users to read certain files as root, if the file has the authorized_keys file format with a command= option. This occurs because ~/.ssh/authorized_keys is read with root privileges and symlinks are followed.
CVE-2017-9080
PlaySMS 1.4 allows remote code execution because PHP code in the name of an uploaded .php file is executed. sendfromfile.php has a combination of Unrestricted File Upload and Code Injection.
CVE-2017-9083
poppler 0.54.0, as used in Evince and other products, has a NULL pointer dereference in the JPXStream::readUByte function in JPXStream.cc. For example, the perf_test utility will crash (segmentation fault) when parsing an invalid PDF file.
CVE-2017-9090
reg.php in Allen Disk 1.6 doesn't check if isset($_SESSION['captcha']['code'])==1, which makes it possible to bypass the CAPTCHA via an empty $_POST['captcha'].
CVE-2017-9091
/admin/loginc.php in Allen Disk 1.6 doesn't check if isset($_SESSION['captcha']['code']) == 1, which leads to CAPTCHA bypass by emptying $_POST['captcha'].
CVE-2017-9093
The my_skip_input_data_fn function in imagew-jpeg.c in libimageworsener.a in ImageWorsener 1.3.1 allows remote attackers to cause a denial of service (infinite loop) via a crafted image.
CVE-2017-9094
The lzw_add_to_dict function in imagew-gif.c in libimageworsener.a in ImageWorsener 1.3.1 allows remote attackers to cause a denial of service (infinite loop) via a crafted image.
CVE-2017-9098
ImageMagick before 7.0.5-2 and GraphicsMagick before 1.3.24 use uninitialized memory in the RLE decoder, allowing an attacker to leak sensitive information from process memory space, as demonstrated by remote attacks against ImageMagick code in a long-running server process that converts image data on behalf of multiple users. This is caused by a missing initialization step in the ReadRLEImage function in coders/rle.c.
