CVE-2013-6078
The default configuration of EMC RSA BSAFE Toolkits and RSA Data Protection Manager (DPM) 20130918 uses the Dual Elliptic Curve Deterministic Random Bit Generation (Dual_EC_DRBG) algorithm, which makes it easier for context-dependent attackers to defeat cryptographic protection mechanisms by leveraging unspecified "security concerns," aka the ESA-2013-068 issue.  NOTE: this issue has been SPLIT from CVE-2007-6755 because the vendor announcement did not state a specific technical rationale for a change in the algorithm; thus, CVE cannot reach a conclusion that a CVE-2007-6755 concern was the reason, or one of the reasons, for this change.
As with CVE-2007-6755 this vulnerability has been scored with the assumption the relationship between P and Q is known to the attacker. Please see CVE-2007-6755 [link: http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2007-6755] more information.
CVE-2014-0478
APT before 1.0.4 does not properly validate source packages, which allows man-in-the-middle attackers to download and install Trojan horse packages by removing the Release signature.
CVE-2014-3249
Puppet Enterprise 2.8.x before 2.8.7 allows remote attackers to obtain sensitive information via vectors involving hiding and unhiding nodes.
CVE-2014-3476
OpenStack Identity (Keystone) before 2013.2.4, 2014.1 before 2014.1.2, and Juno before Juno-2 does not properly handle chained delegation, which allows remote authenticated users to gain privileges by leveraging a (1) trust or (2) OAuth token with impersonation enabled to create a new token with additional roles.
CVE-2014-4038
ppc64-diag 2.6.1 allows local users to overwrite arbitrary files via a symlink attack related to (1) rtas_errd/diag_support.c and /tmp/get_dt_files, (2) scripts/ppc64_diag_mkrsrc and /tmp/diagSEsnap/snapH.tar.gz, or (3) lpd/test/lpd_ela_test.sh and /var/tmp/ras.
CVE-2014-4039
ppc64-diag 2.6.1 uses 0775 permissions for /tmp/diagSEsnap and does not properly restrict permissions for /tmp/diagSEsnap/snapH.tar.gz, which allows local users to obtain sensitive information by reading files in this archive, as demonstrated by /var/log/messages and /etc/yaboot.conf.
CVE-2014-4040
snap in powerpc-utils 1.2.20 produces an archive with fstab and yaboot.conf files potentially containing cleartext passwords, and lacks a warning about reviewing this archive to detect included passwords, which might allow remote attackers to obtain sensitive information by leveraging access to a technical-support data stream.
CVE-2014-4044
OpenAFS 1.6.8 does not properly clear the fields in the host structure, which allows remote attackers to cause a denial of service (uninitialized memory access and crash) via unspecified vectors related to TMAY requests.
CVE-2014-4045
The Publish/Subscribe Framework in the PJSIP channel driver in Asterisk Open Source 12.x before 12.3.1, when sub_min_expiry is set to zero, allows remote attackers to cause a denial of service (assertion failure and crash) via an unsubscribe request when not subscribed to the device.
CVE-2014-4046
Asterisk Open Source 11.x before 11.10.1 and 12.x before 12.3.1 and Certified Asterisk 11.6 before 11.6-cert3 allows remote authenticated Manager users to execute arbitrary shell commands via a MixMonitor action.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-4047
Asterisk Open Source 1.8.x before 1.8.28.1, 11.x before 11.10.1, and 12.x before 12.3.1 and Certified Asterisk 1.8.15 before 1.8.15-cert6 and 11.6 before 11.6-cert3 allows remote attackers to cause a denial of service (connection consumption) via a large number of (1) inactive or (2) incomplete HTTP connections.
CVE-2014-4048
The PJSIP Channel Driver in Asterisk Open Source before 12.3.1 allows remote attackers to cause a denial of service (deadlock) by terminating a subscription request before it is complete, which triggers a SIP transaction timeout.
CVE-2014-4187
Cross-site scripting (XSS) vulnerability in signup.php in ClipBucket allows remote attackers to inject arbitrary web script or HTML via the Username field.
CVE-2014-4188
Cross-site request forgery (CSRF) vulnerability in Hitachi Tuning Manager before 7.6.1-06 and 8.x before 8.0.0-04 and JP1/Performance Management - Manager Web Option 07-00 through 07-54 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2014-4189
Cross-site scripting (XSS) vulnerability in Hitachi Tuning Manager before 7.6.1-06 and 8.x before 8.0.0-04 and JP1/Performance Management - Manager Web Option 07-00 through 07-54 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-4190
Multiple heap-based buffer overflows in Huawei Campus Series Switches S3700HI, S5700, S6700, S3300HI, S5300, S6300, S9300, S7700, and LSW S9700 with software V200R001 before V200R001SPH013; S5700, S6700, S5300, and S6300 with software V200R002 before V200R002SPH005; S7700, S9300, S9300E, S5300, S5700, S6300, S6700, S2350, S2750, and LSW S9700 with software V200R003 before V200R003SPH005; and S7700, S9300, S9300E, and LSW S9700 with software V200R005 before V200R005C00SPC300 allow remote attackers to cause a denial of service (device restart) via a crafted length field in a packet.
CVE-2014-4191
The TLS implementation in EMC RSA BSAFE-C Toolkits (aka Share for C and C++) sends a long series of random bytes during use of the Dual_EC_DRBG algorithm, which makes it easier for remote attackers to obtain plaintext from TLS sessions by recovering the algorithm's inner state, a different issue than CVE-2007-6755.
As with CVE-2007-6755 this vulnerability has been scored with the assumption the relationship between P and Q is known to the attacker. Please see CVE-2007-6755 [link: http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2007-6755] more information.
CVE-2014-4192
The Dual_EC_DRBG implementation in EMC RSA BSAFE-C Toolkits (aka Share for C and C++) processes certain requests for output bytes by considering only the requested byte count and not the use of cached bytes, which makes it easier for remote attackers to obtain plaintext from TLS sessions by recovering the algorithm's inner state, a different issue than CVE-2007-6755.
As with CVE-2007-6755 this vulnerability has been scored with the assumption the relationship between P and Q is known to the attacker. Please see CVE-2007-6755 [link: http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2007-6755] more information.
CVE-2014-4193
The TLS implementation in EMC RSA BSAFE-Java Toolkits (aka Share for Java) supports the Extended Random extension during use of the Dual_EC_DRBG algorithm, which makes it easier for remote attackers to obtain plaintext from TLS sessions by requesting long nonces from a server, a different issue than CVE-2007-6755.
As with CVE-2007-6755 this vulnerability has been scored with the assumption the relationship between P and Q is known to the attacker. Please see CVE-2007-6755 [link: http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2007-6755] more information.
