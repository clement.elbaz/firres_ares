CVE-2013-1149
Cisco Adaptive Security Appliances (ASA) devices with software 7.x before 7.2(5.10), 8.0 before 8.0(5.28), 8.1 and 8.2 before 8.2(5.35), 8.3 before 8.3(2.34), 8.4 before 8.4(4.11), 8.6 before 8.6(1.10), and 8.7 before 8.7(1.3), and Cisco Firewall Services Module (FWSM) software 3.1 and 3.2 before 3.2(24.1) and 4.0 and 4.1 before 4.1(11.1), allow remote attackers to cause a denial of service (device reload) via a crafted IKEv1 message, aka Bug IDs CSCub85692 and CSCud20267.
CVE-2013-1150
The authentication-proxy implementation on Cisco Adaptive Security Appliances (ASA) devices with software 7.x before 7.2(5.10), 8.0 before 8.0(5.31), 8.1 and 8.2 before 8.2(5.38), 8.3 before 8.3(2.37), 8.4 before 8.4(5.3), 8.5 and 8.6 before 8.6(1.10), 8.7 before 8.7(1.4), 9.0 before 9.0(1.1), and 9.1 before 9.1(1.2) allows remote attackers to cause a denial of service (device reload) via a crafted URL, aka Bug ID CSCud16590.
CVE-2013-1151
Cisco Adaptive Security Appliances (ASA) devices with software 7.x before 7.2(5.10), 8.0 before 8.0(5.31), 8.1 and 8.2 before 8.2(5.38), 8.3 before 8.3(2.37), 8.4 before 8.4(5), 8.5 before 8.5(1.17), 8.6 before 8.6(1.10), and 8.7 before 8.7(1.3) allow remote attackers to cause a denial of service (device reload) via a crafted certificate, aka Bug ID CSCuc72408.
CVE-2013-1152
Cisco Adaptive Security Appliances (ASA) devices with software 9.0 before 9.0(1.2) allow remote attackers to cause a denial of service (device reload) via a crafted field in a DNS message, aka Bug ID CSCuc80080.
CVE-2013-1155
The auth-proxy functionality in Cisco Firewall Services Module (FWSM) software 3.1 and 3.2 before 3.2(20.1), 4.0 before 4.0(15.2), and 4.1 before 4.1(5.1) allows remote attackers to cause a denial of service (device reload) via a crafted URL, aka Bug ID CSCtg02624.
CVE-2013-1164
Cisco IOS XE 3.4 before 3.4.4S, 3.5, and 3.6 on 1000 series Aggregation Services Routers (ASR) does not properly implement the Cisco Multicast Leaf Recycle Elimination (MLRE) feature, which allows remote attackers to cause a denial of service (card reload) via fragmented IPv6 multicast packets, aka Bug ID CSCtz97563.
CVE-2013-1165
Cisco IOS XE 2.x and 3.x before 3.4.5S, and 3.5 through 3.7 before 3.7.1S, on 1000 series Aggregation Services Routers (ASR) allows remote attackers to cause a denial of service (card reload) by sending many crafted L2TP packets, aka Bug ID CSCtz23293.
CVE-2013-1166
Cisco IOS XE 3.2 through 3.4 before 3.4.5S, and 3.5 through 3.7 before 3.7.1S, on 1000 series Aggregation Services Routers (ASR), when VRF-aware NAT and SIP ALG are enabled, allows remote attackers to cause a denial of service (card reload) by sending many SIP packets, aka Bug ID CSCuc65609.
CVE-2013-1167
Cisco IOS XE 3.2 through 3.4 before 3.4.2S, and 3.5, on 1000 series Aggregation Services Routers (ASR), when bridge domain interface (BDI) is enabled, allows remote attackers to cause a denial of service (card reload) via packets that are not properly handled during the processing of encapsulation, aka Bug ID CSCtt11558.
CVE-2013-1168
The web server in Cisco Unified MeetingPlace Application Server 7.x before 7.1MR1 Patch 2, 8.0 before 8.0MR1 Patch 1, and 8.5 before 8.5MR3 Patch 1 does not invalidate a session upon a logout action, which makes it easier for remote attackers to hijack sessions by leveraging knowledge of a session cookie, aka Bug ID CSCuc64885.
CVE-2013-1169
Cisco Unified MeetingPlace Web Conferencing Server 7.x before 7.1MR1 Patch 2, 8.0 before 8.0MR1 Patch 2, and 8.5 before 8.5MR3 Patch 1, when the Remember Me option is used, does not properly verify cookies, which allows remote attackers to impersonate users via a crafted login request, aka Bug ID CSCuc64846.
CVE-2013-1170
The Cisco Prime Network Control System (NCS) appliance with software before 1.1.1.24 has a default password for the database user account, which makes it easier for remote attackers to change the configuration or cause a denial of service (service disruption) via unspecified vectors, aka Bug ID CSCtz30468.
CVE-2013-1172
The Cisco Security Service in Cisco AnyConnect Secure Mobility Client (aka AnyConnect VPN Client) does not properly verify files, which allows local users to gain privileges via unspecified vectors, aka Bug ID CSCud14153.
CVE-2013-1173
Heap-based buffer overflow in ciscod.exe in the Cisco Security Service in Cisco AnyConnect Secure Mobility Client (aka AnyConnect VPN Client) allows local users to gain privileges via unspecified vectors, aka Bug ID CSCud14143.
CVE-2013-1189
Cisco Universal Broadband (aka uBR) 10000 series routers, when an IPv4/IPv6 dual-stack modem is used, allow remote attackers to cause a denial of service (routing-engine reload) via unspecified changes to IP address assignments, aka Bug ID CSCue15313.
CVE-2013-2779
Cisco IOS XE 3.4 before 3.4.5S, and 3.5 through 3.7 before 3.7.1S, on 1000 series Aggregation Services Routers (ASR) does not properly implement the Cisco Multicast Leaf Recycle Elimination (MLRE) feature, which allows remote attackers to cause a denial of service (card reload) via fragmented IPv6 MVPN (aka MVPNv6) packets, aka Bug ID CSCub34945, a different vulnerability than CVE-2013-1164.
