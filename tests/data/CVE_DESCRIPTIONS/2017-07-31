CVE-2016-9714
IBM InfoSphere Master Data Management Server 10.1, 11.0, 11.3, 11.4, 11.5, and 11.6 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 119727.
CVE-2016-9715
IBM InfoSphere Master Data Management Server 11.0, 11.3, 11.4, 11.5, and 11.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 119728.
CVE-2016-9716
IBM InfoSphere Master Data Management Server 11.0, 11.3, 11.4, 11.5, and 11.6 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 119729.
CVE-2016-9717
HTTP Parameter Override is identified in the IBM Infosphere Master Data Management (MDM) 10.1. 11.0. 11.3, 11.4, 11.5, and 11.6 product. It enables attackers by exposing the presence of duplicated parameters which may produce an anomalous behavior in the application that can be potentially exploited.
CVE-2016-9718
IBM InfoSphere Master Data Management Server 10.1. 11.0. 11.3, 11.4, 11.5, and 11.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 119732.
CVE-2016-9719
IBM InfoSphere Master Data Management Server 10.1. 11.0. 11.3, 11.4, 11.5, and 11.6 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 119733.
CVE-2017-11114
The put_chars function in html_r.c in Twibright Links 2.14 allows remote attackers to cause a denial of service (buffer over-read) via a crafted HTML file.
CVE-2017-11115
The ExifJpegHUFFTable::deriveTable function in ExifHuffmanTable.cpp in OpenExif 2.1.4 allows remote attackers to cause a denial of service (heap-based buffer overflow and application crash) via a crafted jpg file.
CVE-2017-11116
The ExifImageFile::readDQT function in ExifImageFileRead.cpp in OpenExif 2.1.4 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted jpg file.
CVE-2017-11117
The ExifImageFile::readDHT function in ExifImageFileRead.cpp in OpenExif 2.1.4 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted jpg file.
CVE-2017-11118
The ExifImageFile::readImage function in ExifImageFileRead.cpp in OpenExif 2.1.4 allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a crafted jpg file.
CVE-2017-11119
The chk_mem_access function in cpu/nes6502/nes6502.c in libnosefart.a in Nosefart 2.9-mls allows remote attackers to cause a denial of service (invalid memory read and application crash) via a crafted nsf file.
CVE-2017-11330
The DivFixppCore::avi_header_fix function in DivFix++Core.cpp in DivFix++ v0.34 allows remote attackers to cause a denial of service (invalid memory write and application crash) via a crafted avi file.
CVE-2017-11331
The wav_open function in oggenc/audio.c in Xiph.Org vorbis-tools 1.4.0 allows remote attackers to cause a denial of service (memory allocation error) via a crafted wav file.
CVE-2017-11332
The startread function in wav.c in Sound eXchange (SoX) 14.4.2 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted wav file.
CVE-2017-11333
The vorbis_analysis_wrote function in lib/block.c in Xiph.Org libvorbis 1.3.5 allows remote attackers to cause a denial of service (OOM) via a crafted wav file.
CVE-2017-11358
The read_samples function in hcom.c in Sound eXchange (SoX) 14.4.2 allows remote attackers to cause a denial of service (invalid memory read and application crash) via a crafted hcom file.
CVE-2017-11359
The wavwritehdr function in wav.c in Sound eXchange (SoX) 14.4.2 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted snd file, during conversion to a wav file.
CVE-2017-11546
The insert_note_steps function in readmidi.c in TiMidity++ 2.14.0 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted mid file. NOTE: a crash might be relevant when using the --background option.
CVE-2017-11547
The resample_gauss function in resample.c in TiMidity++ 2.14.0 allows remote attackers to cause a denial of service (heap-based buffer over-read) via a crafted mid file. NOTE: a crash might be relevant when using the --background option. NOTE: the TiMidity++ README.alsaseq documentation suggests a setuid-root installation.
CVE-2017-11548
The _tokenize_matrix function in audio_out.c in Xiph.Org libao 1.2.0 allows remote attackers to cause a denial of service (memory corruption) via a crafted MP3 file.
CVE-2017-11549
The play_midi function in playmidi.c in TiMidity++ 2.14.0 allows remote attackers to cause a denial of service (large loop and CPU consumption) via a crafted mid file. NOTE: CPU consumption might be relevant when using the --background option.
CVE-2017-11550
The id3_ucs4_length function in ucs4.c in libid3tag 0.15.1b allows remote attackers to cause a denial of service (NULL Pointer Dereference and application crash) via a crafted mp3 file.
CVE-2017-11551
The id3_field_parse function in field.c in libid3tag 0.15.1b allows remote attackers to cause a denial of service (OOM) via a crafted MP3 file.
CVE-2017-11648
Techroutes TR 1803-3G Wireless Cellular Router/Modem 2.4.25 devices do not possess any protection against a CSRF vulnerability, as demonstrated by a goform/BasicSettings request to disable port filtering.
CVE-2017-11668
An out-of-bounds read flaw related to the assess_packet function in eapmd5pass.c:134 was found in the way eapmd5pass 1.4 handled processing of network packets. A remote attacker could potentially use this flaw to crash the eapmd5pass process under certain circumstances by generating specially crafted network traffic.
CVE-2017-11669
An out-of-bounds read flaw related to the assess_packet function in eapmd5pass.c:211 was found in the way eapmd5pass 1.4 handled processing of network packets. A remote attacker could potentially use this flaw to crash the eapmd5pass process under certain circumstances by generating specially crafted network traffic.
CVE-2017-11670
A length validation (leading to out-of-bounds read and write) flaw was found in the way eapmd5pass 1.4 handled network traffic in the extract_eapusername function. A remote attacker could potentially use this flaw to crash the eapmd5pass process by generating specially crafted network traffic.
CVE-2017-11726
services/system_io/actionprocessor/System.rails in ConnectWise Manage 2017.5 is vulnerable to Cross-Site Request Forgery (CSRF), as demonstrated by changing an e-mail address setting.
CVE-2017-11727
services/system_io/actionprocessor/Contact.rails in ConnectWise Manage 2017.5 allows arbitrary client-side JavaScript code execution (involving a ContactCommon field) on victims who click on a crafted link, aka XSS.
CVE-2017-11735
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue in the originally named product.  Notes: none.
CVE-2017-11743
MEDHOST Connex contains a hard-coded Mirth Connect admin credential that is used for customer Mirth Connect management access. An attacker with knowledge of the hard-coded credential and the ability to communicate directly with the Mirth Connect management console may be able to intercept sensitive patient information. The admin account password is hard-coded as $K8t1ng throughout the application, and is the same across all installations. Customers do not have the option to change the Mirth Connect admin account password. The Mirth Connect admin account is created during the Connex install. The plaintext account password is hard-coded multiple times in the Connex install and update scripts.
CVE-2017-11757
Heap-based buffer overflow in Actian Pervasive PSQL v12.10 and Zen v13 allows remote attackers to execute arbitrary code via crafted traffic to TCP port 1583. The overflow occurs after Server-Client encryption-key exchange. The issue results from an integer underflow that leads to a zero-byte allocation. The _srvLnaConnectMP1 function is affected.
CVE-2017-11760
uploadImage.php in ProjeQtOr before 6.3.2 allows remote authenticated users to execute arbitrary PHP code by uploading a .php file composed of concatenated image data and script data, as demonstrated by uploading as an image within the description text area.
CVE-2017-1227
IBM Tivoli Endpoint Manager could allow a unauthorized user to consume all resources and crash the system. IBM X-Force ID: 123906.
CVE-2017-1303
IBM WebSphere Portal and Web Content Manager 7.0, 8.0, 8.5, and 9.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 125457.
CVE-2017-1332
IBM iNotes 8.5 and 9.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126234.
CVE-2017-1370
IBM Jazz Reporting Service (JRS) 5.0 and 6.0 could disclose sensitive information, including user credentials, through an error message from the Report Builder administrator configuration page. IBM X-Force ID: 126863.
CVE-2017-1386
IBM API Connect 5.0.0.0 could allow a user to bypass policy restrictions and create non-compliant passwords which could be intercepted and decrypted using man in the middle techniques. IBM X-Force ID: 127160.
CVE-2017-1460
IBM i OSPF 6.1, 7.1, 7.2, and 7.3 is vulnerable when a rogue router spoofs its origin. Routing tables are affected by a missing LSA, which may lead to loss of connectivity. IBM X-Force ID: 128379.
CVE-2017-1496
IBM Sterling B2B Integrator Standard Edition 5.2.x is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128694.
CVE-2017-9475
Comcast XFINITY WiFi Home Hotspot devices allow remote attackers to spoof the identities of Comcast customers via a forged MAC address.
CVE-2017-9476
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST); Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST); and Arris TG1682G (eMTA&DOCSIS version 10.0.132.SIP.PC20.CT, software version TG1682_2.2p7s2_PROD_sey) devices makes it easy for remote attackers to determine the hidden SSID and passphrase for a Home Security Wi-Fi network.
CVE-2017-9477
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST) and DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to discover the CM MAC address by connecting to the device's xfinitywifi hotspot.
CVE-2017-9478
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST) and DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices sets the CM MAC address to a value with a two-byte offset from the MTA/VoIP MAC address, which indirectly allows remote attackers to discover hidden Home Security Wi-Fi networks by leveraging the embedding of the MTA/VoIP MAC address into the DNS hostname.
CVE-2017-9479
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to execute arbitrary commands as root by leveraging local network access and connecting to the syseventd server, as demonstrated by copying configuration data into a readable filesystem.
CVE-2017-9480
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows local users (e.g., users who have command access as a consequence of CVE-2017-9479 exploitation) to read arbitrary files via UPnP access to /var/IGD/.
CVE-2017-9481
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to obtain unintended access to the Network Processor (NP) 169.254/16 IP network by adding a routing-table entry that specifies the LAN IP address as the router for that network.
CVE-2017-9482
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to obtain root access to the Network Processor (NP) Linux system by enabling a TELNET daemon (through CVE-2017-9479 exploitation) and then establishing a TELNET session.
CVE-2017-9483
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows Network Processor (NP) Linux users to obtain root access to the Application Processor (AP) Linux system via shell metacharacters in commands.
CVE-2017-9484
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST) and DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to discover a CM MAC address by sniffing Wi-Fi traffic and performing simple arithmetic calculations.
CVE-2017-9485
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to write arbitrary data to a known /var/tmp/sess_* pathname by leveraging the device's operation in UI dev mode.
CVE-2017-9486
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) devices allows remote attackers to compute password-of-the-day values via unspecified vectors.
CVE-2017-9487
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) and DPC3941T (firmware version DPC3941_2.5s3_PROD_sey) devices allows remote attackers to discover a WAN IPv6 IP address by leveraging knowledge of the CM MAC address.
CVE-2017-9488
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST) and DPC3941T (firmware version DPC3941_2.5s3_PROD_sey) devices allows remote attackers to access the web UI by establishing a session to the wan0 WAN IPv6 address and then entering unspecified hardcoded credentials. This wan0 interface cannot be accessed from the public Internet.
CVE-2017-9489
The Comcast firmware on Cisco DPC3939B (firmware version dpc3939b-v303r204217-150321a-CMCST) devices allows configuration changes via CSRF.
CVE-2017-9490
The Comcast firmware on Arris TG1682G (eMTA&DOCSIS version 10.0.132.SIP.PC20.CT, software version TG1682_2.2p7s2_PROD_sey) devices allows configuration changes via CSRF.
CVE-2017-9491
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST); Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST); Cisco DPC3939B (firmware version dpc3939b-v303r204217-150321a-CMCST); Cisco DPC3941T (firmware version DPC3941_2.5s3_PROD_sey); and Arris TG1682G (eMTA&DOCSIS version 10.0.132.SIP.PC20.CT, software version TG1682_2.2p7s2_PROD_sey) devices does not set the secure flag for cookies in an https session to an administration application, which makes it easier for remote attackers to capture these cookies by intercepting their transmission within an http session.
CVE-2017-9492
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST); Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST); Cisco DPC3939B (firmware version dpc3939b-v303r204217-150321a-CMCST); Cisco DPC3941T (firmware version DPC3941_2.5s3_PROD_sey); and Arris TG1682G (eMTA&DOCSIS version 10.0.132.SIP.PC20.CT, software version TG1682_2.2p7s2_PROD_sey) devices does not include the HTTPOnly flag in a Set-Cookie header for administration applications, which makes it easier for remote attackers to obtain potentially sensitive information via script access to cookies.
CVE-2017-9493
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) devices allows remote attackers to conduct successful forced-pairing attacks (between an RF4CE remote and a set-top box) by repeatedly transmitting the same pairing code.
CVE-2017-9494
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) devices allows remote attackers to enable a Remote Web Inspector that is accessible from the public Internet.
CVE-2017-9495
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) devices allows physically proximate attackers to read arbitrary files by pressing "EXIT, Down, Down, 2" on an RF4CE remote to reach the diagnostic display, and then launching a Remote Web Inspector script.
CVE-2017-9496
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) devices allows physically proximate attackers to access an SNMP server by connecting a cable to the Ethernet port, and then establishing communication with the device's link-local IPv6 address.
CVE-2017-9497
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) devices allows physically proximate attackers to execute arbitrary commands as root by pulling up the diagnostics menu on the set-top box, and then posting to a Web Inspector route.
CVE-2017-9498
The Comcast firmware on Motorola MX011ANM (firmware version MX011AN_2.9p6s1_PROD_sey) and Xfinity XR11-20 Voice Remote devices allows local users to upload arbitrary firmware images to an XR11 by leveraging root access. In other words, there is no protection mechanism involving digital signatures for the firmware.
CVE-2017-9521
The Comcast firmware on Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421733-160420a-CMCST); Cisco DPC3939 (firmware version dpc3939-P20-18-v303r20421746-170221a-CMCST); Cisco DPC3939B (firmware version dpc3939b-v303r204217-150321a-CMCST); Cisco DPC3941T (firmware version DPC3941_2.5s3_PROD_sey); and Arris TG1682G (eMTA&DOCSIS version 10.0.132.SIP.PC20.CT, software version TG1682_2.2p7s2_PROD_sey) devices allows remote attackers to execute arbitrary code via a specific (but unstated) exposed service. NOTE: the scope of this CVE does NOT include the concept of "Unnecessary Services" in general; the scope is only a single service that is unnecessarily exposed, leading to remote code execution. The details of that service might be disclosed at a later date.
CVE-2017-9522
The Time Warner firmware on Technicolor TC8717T devices sets the default Wi-Fi passphrase to a combination of the SSID and BSSID, which makes it easier for remote attackers to obtain network access by reading a beacon frame.
