CVE-2016-6804
The Apache OpenOffice installer (versions prior to 4.1.3, including some branded as OpenOffice.org) for Windows contains a defective operation that allows execution of arbitrary code with elevated privileges. This requires that the location in which the installer is run has been previously poisoned by a file that impersonates a dynamic-link library that the installer depends upon.
CVE-2017-11400
An issue has been discovered on the Belden Hirschmann Tofino Xenon Security Appliance before 03.2.00. An incomplete firmware signature allows a local attacker to upgrade the equipment (kernel, file system) with unsigned, attacker-controlled, data. This occurs because the appliance_config file is signed but the .tar.sec file is unsigned.
CVE-2017-11401
An issue has been discovered on the Belden Hirschmann Tofino Xenon Security Appliance before 03.2.00. Improper handling of the mbap.length field of ModBus packets in the ModBus DPI filter allows an attacker to send malformed/crafted packets to a protected asset, bypassing function code filtering.
CVE-2017-11402
An issue has been discovered on the Belden Hirschmann Tofino Xenon Security Appliance before 03.2.00. Design flaws in OPC classic and in custom netfilter modules allow an attacker to remotely activate rules on the firewall and to connect to any TCP port of a protected asset, thus bypassing the firewall. The attack methodology is a crafted OPC dynamic port shift.
CVE-2017-12110
An exploitable integer overflow vulnerability exists in the xls_appendSST function of libxls 1.4.A specially crafted XLS file can cause memory corruption resulting in remote code execution.
CVE-2017-12111
An exploitable out-of-bounds vulnerability exists in the xls_addCell function of libxls 1.4. A specially crafted XLS file with a formula record can cause memory corruption resulting in remote code execution. An attacker can send a malicious XLS file to trigger this vulnerability.
CVE-2017-12607
A vulnerability in OpenOffice's PPT file parser before 4.1.4, and specifically in PPTStyleSheet, allows attackers to craft malicious documents that cause denial of service (memory corruption and application crash) potentially resulting in arbitrary code execution.
CVE-2017-12608
A vulnerability in Apache OpenOffice Writer DOC file parser before 4.1.4, and specifically in ImportOldFormatStyles, allows attackers to craft malicious documents that cause denial of service (memory corruption and application crash) potentially resulting in arbitrary code execution.
CVE-2017-15110
In Moodle 3.x, students can find out email addresses of other students in the same course. Using search on the Participants page, students could search email addresses of all participants regardless of email visibility. This allows enumerating and guessing emails of other students.
CVE-2017-15527
Prior to ITMS 8.1 RU4, the Symantec Management Console can be susceptible to a directory traversal exploit, which is a type of attack that can occur when there is insufficient security validation / sanitization of user-supplied input file names, such that characters representing "traverse to parent directory" are passed through to the file APIs.
CVE-2017-16544
In the add_match function in libbb/lineedit.c in BusyBox through 1.27.2, the tab autocomplete feature of the shell, used to get a list of filenames in a directory, does not sanitize filenames and results in executing any escape sequence in the terminal. This could potentially result in code execution, arbitrary file writes, or other attacks.
CVE-2017-16894
In Laravel framework through 5.5.21, remote attackers can obtain sensitive information (such as externally usable passwords) via a direct request for the /.env URI. NOTE: this CVE is only about Laravel framework's writeNewEnvironmentFileWith function in src/Illuminate/Foundation/Console/KeyGenerateCommand.php, which uses file_put_contents without restricting the .env permissions. The .env filename is not used exclusively by Laravel framework.
CVE-2017-16896
A SQL injection in classes/handler/public.php in the forgotpass component of Tiny Tiny RSS 17.4 exists via the login parameter.
CVE-2017-16898
The printMP3Headers function in util/listmp3.c in libming v0.4.8 or earlier is vulnerable to a global buffer overflow, which may allow attackers to cause a denial of service via a crafted file, a different vulnerability than CVE-2016-9264.
CVE-2017-16899
An array index error in the fig2dev program in Xfig 3.2.6a allows remote attackers to cause a denial-of-service attack or information disclosure with a maliciously crafted Fig format file, related to a negative font value in dev/gentikz.c, and the read_textobject functions in read.c and read1_3.c.
CVE-2017-16902
On the Vonage VDV-23 115 3.2.11-0.9.40 home router, sending a long string of characters in the loginPassword and/or loginUsername field to goform/login causes the router to reboot.
CVE-2017-16903
LvyeCMS through 3.1 allows remote attackers to upload and execute arbitrary PHP code via directory traversal sequences in the dir parameter, in conjunction with PHP code in the content parameter, within a template Style add request to index.php.
CVE-2017-16904
The Public tologin feature in admin.php in LvyeCMS through 3.1 allows XSS via a crafted username that is mishandled during later log viewing by an administrator.
CVE-2017-16906
In Horde Groupware 5.2.19-5.2.22, there is XSS via the URL field in a "Calendar -> New Event" action.
CVE-2017-16907
In Horde Groupware 5.2.19 and 5.2.21, there is XSS via the Color field in a Create Task List action.
CVE-2017-16908
In Horde Groupware 5.2.19, there is XSS via the Name field during creation of a new Resource. This can be leveraged for remote code execution after compromising an administrator account, because the CVE-2015-7984 CSRF protection mechanism can then be bypassed.
CVE-2017-2896
An exploitable out-of-bounds write vulnerability exists in the xls_mergedCells function of libxls 1.4. . A specially crafted XLS file can cause a memory corruption resulting in remote code execution. An attacker can send malicious XLS file to trigger this vulnerability.
CVE-2017-2897
An exploitable out-of-bounds write vulnerability exists in the read_MSAT function of libxls 1.4. A specially crafted XLS file can cause a memory corruption resulting in remote code execution. An attacker can send malicious XLS file to trigger this vulnerability.
CVE-2017-2919
An exploitable stack based buffer overflow vulnerability exists in the xls_getfcell function of libxls 1.3.4. A specially crafted XLS file can cause a memory corruption resulting in remote code execution. An attacker can send malicious XLS file to trigger this vulnerability
CVE-2017-3157
By exploiting the way Apache OpenOffice before 4.1.4 renders embedded objects, an attacker could craft a document that allows reading in a file from the user's filesystem. Information could be retrieved by the attacker by, e.g., using hidden sections to store the information, tricking the user into saving the document and convincing the user to send the document back to the attacker. The vulnerability is mitigated by the need for the attacker to know the precise file path in the target system, and the need to trick the user into saving the document and sending it back.
CVE-2017-9806
A vulnerability in the OpenOffice Writer DOC file parser before 4.1.4, and specifically in the WW8Fonts Constructor, allows attackers to craft malicious documents that cause denial of service (memory corruption and application crash) potentially resulting in arbitrary code execution.
