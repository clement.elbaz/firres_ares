CVE-2016-10994
The Truemag theme 2016 Q2 for WordPress has XSS via the s parameter.
CVE-2016-10995
The Tevolution plugin before 2.3.0 for WordPress has arbitrary file upload via single_upload.php or single-upload.php.
CVE-2018-1847
IBM Financial Transaction Manager (FTM) for Multi-Platform (MP) v2.0.0.0 through 2.0.0.5, v2.1.0.0 through 2.1.0.4, v2.1.1.0 through 2.1.1.4, and v3.0.0.0 through 3.0.0.8 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system. IBM X-Force ID: 150946.
CVE-2019-11210
The server component of TIBCO Software Inc.'s TIBCO Enterprise Runtime for R - Server Edition, and TIBCO Spotfire Analytics Platform for AWS Marketplace contains a vulnerability that theoretically allows an unauthenticated user to bypass access controls and remotely execute code using the operating system account hosting the affected component. This issue affects: TIBCO Enterprise Runtime for R - Server Edition versions 1.2.0 and below, and TIBCO Spotfire Analytics Platform for AWS Marketplace versions 10.4.0 and 10.5.0.
CVE-2019-11211
The server component of TIBCO Software Inc.'s TIBCO Enterprise Runtime for R - Server Edition, and TIBCO Spotfire Analytics Platform for AWS Marketplace contains a vulnerability that theoretically allows an authenticated user to trigger remote code execution in certain circumstances. When the affected component runs with the containerized TERR service on Linux the host can theoretically be tricked into running malicious code. This issue affects: TIBCO Enterprise Runtime for R - Server Edition version 1.2.0 and below, and TIBCO Spotfire Analytics Platform for AWS Marketplace 10.4.0; 10.5.0.
CVE-2019-11661
Allow changes to some table by non-SysAdmin in Micro Focus Service Manager product versions 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, 9.41, 9.50, 9.51, 9.52, 9.60, 9.61, 9.62. This vulnerability could be exploited to allow unauthorized access and modification of data.
CVE-2019-11662
Class and method names in error message in Micro Focus Service Manager product versions 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, 9.41, 9.50, 9.51, 9.52, 9.60, 9.61, 9.62. This vulnerability could be exploited in some special cases to allow information exposure through an error message.
CVE-2019-11663
Clear text credentials are used to access managers app in Tomcat in Micro Focus Service Manager product versions 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, 9.41, 9.50, 9.51, 9.52, 9.60, 9.61, 9.62. The vulnerability could be exploited to allow sensitive data exposure.
CVE-2019-11664
Clear text password in browser in Micro Focus Service Manager product versions 9.30, 9.31, 9.32, 9.33, 9.34, 9.35, 9.40, 9.41, 9.50, 9.51, 9.52, 9.60, 9.61, 9.62. The vulnerability could be exploited to allow sensitive data exposure.
CVE-2019-11778
If an MQTT v5 client connects to Eclipse Mosquitto versions 1.6.0 to 1.6.4 inclusive, sets a last will and testament, sets a will delay interval, sets a session expiry interval, and the will delay interval is set longer than the session expiry interval, then a use after free error occurs, which has the potential to cause a crash in some situations.
CVE-2019-12620
A vulnerability in the statistics collection service of Cisco HyperFlex Software could allow an unauthenticated, remote attacker to inject arbitrary values on an affected device. The vulnerability is due to insufficient authentication for the statistics collection service. An attacker could exploit this vulnerability by sending properly formatted data values to the statistics collection service of an affected device. A successful exploit could allow the attacker to cause the web interface statistics view to present invalid data to users.
CVE-2019-13550
In WebAccess, versions 8.4.1 and prior, an improper authorization vulnerability may allow an attacker to disclose sensitive information, cause improper control of generation of code, which may allow remote code execution or cause a system crash.
CVE-2019-13552
In WebAccess versions 8.4.1 and prior, multiple command injection vulnerabilities are caused by a lack of proper validation of user-supplied data and may allow arbitrary file deletion and remote code execution.
CVE-2019-13556
In WebAccess versions 8.4.1 and prior, multiple stack-based buffer overflow vulnerabilities are caused by a lack of proper validation of the length of user-supplied data. Exploitation of these vulnerabilities may allow remote code execution.
CVE-2019-13558
In WebAccess versions 8.4.1 and prior, an exploit executed over the network may cause improper control of generation of code, which may allow remote code execution, data exfiltration, or cause a system crash.
CVE-2019-14252
An issue was discovered in the secure portal in Publisure 2.1.2. Once successfully authenticated as an administrator, one is able to inject arbitrary PHP code by using the adminCons.php form. The code is then stored in the E:\PUBLISURE\webservice\webpages\AdminDir\Templates\ folder even if removed from the adminCons.php view (i.e., the rogue PHP file can be hidden).
CVE-2019-14253
An issue was discovered in servletcontroller in the secure portal in Publisure 2.1.2. One can bypass authentication and perform a query on PHP forms within the /AdminDir folder that should be restricted.
CVE-2019-14254
An issue was discovered in the secure portal in Publisure 2.1.2. Because SQL queries are not well sanitized, there are multiple SQL injections in userAccFunctions.php functions. Using this, an attacker can access passwords and/or grant access to the user account "user" in order to become "Administrator" (for example).
CVE-2019-14458
VIVOTEK IP Camera devices with firmware before 0x20x allow a denial of service via a crafted HTTP header.
CVE-2019-15301
A SQL injection vulnerability in the method Terrasoft.Core.DB.Column.Const() in Terrasoft Bpm'online CRM-System SDK 7.13 allows attackers to execute arbitrary SQL commands via the value parameter.
CVE-2019-15843
A malicious file upload vulnerability was discovered in Xiaomi Millet mobile phones 1-6.3.9.3. A particular condition involving a man-in-the-middle attack may lead to partial data leakage or malicious file writing.
CVE-2019-16215
The Markdown parser in Zulip server before 2.0.5 used a regular expression vulnerable to exponential backtracking. A user who is logged into the server could send a crafted message causing the server to spend an effectively arbitrary amount of CPU time and stall the processing of future messages.
CVE-2019-16216
Zulip server before 2.0.5 incompletely validated the MIME types of uploaded files. A user who is logged into the server could upload files of certain types to mount a stored cross-site scripting attack on other logged-in users. On a Zulip server using the default local uploads backend, the attack is only effective against browsers lacking support for Content-Security-Policy such as Internet Explorer 11. On a Zulip server using the S3 uploads backend, the attack is confined to the origin of the configured S3 uploads hostname and cannot reach the Zulip server itself.
CVE-2019-16399
Western Digital WD My Book World through II 1.02.12 suffers from Broken Authentication, which allows an attacker to access the /admin/ directory without credentials. An attacker can easily enable SSH from /admin/system_advanced.php?lang=en and login with the default root password welc0me.
CVE-2019-16403
In Webkul Bagisto before 0.1.5, the functionalities for customers to change their own values (such as address, review, orders, etc.) can also be manipulated by other customers.
CVE-2019-1975
A vulnerability in the web-based interface of Cisco HyperFlex Software could allow an unauthenticated, remote attacker to execute a cross-frame scripting (XFS) attack on an affected device. This vulnerability is due to insufficient HTML iframe protection. An attacker could exploit this vulnerability by directing a user to an attacker-controlled web page that contains a malicious HTML iframe. A successful exploit could allow the attacker to conduct clickjacking or other clientside browser attacks.
CVE-2019-3738
RSA BSAFE Crypto-J versions prior to 6.2.5 are vulnerable to a Missing Required Cryptographic Step vulnerability. A malicious remote attacker could potentially exploit this vulnerability to coerce two parties into computing the same predictable shared key.
CVE-2019-3739
RSA BSAFE Crypto-J versions prior to 6.2.5 are vulnerable to Information Exposure Through Timing Discrepancy vulnerabilities during ECDSA key generation. A malicious remote attacker could potentially exploit those vulnerabilities to recover ECDSA keys.
CVE-2019-3740
RSA BSAFE Crypto-J versions prior to 6.2.5 are vulnerable to an Information Exposure Through Timing Discrepancy vulnerabilities during DSA key generation. A malicious remote attacker could potentially exploit those vulnerabilities to recover DSA keys.
CVE-2019-3756
RSA Archer, versions prior to 6.6 P3 (6.6.0.3), contain an information disclosure vulnerability. Information relating to the backend database gets disclosed to low-privileged RSA Archer users' UI under certain error conditions.
CVE-2019-3758
RSA Archer, versions prior to 6.6 P2 (6.6.0.2), contain an improper authentication vulnerability. The vulnerability allows sysadmins to create user accounts with insufficient credentials. Unauthenticated attackers could gain unauthorized access to the system using those accounts.
CVE-2019-5042
An exploitable Use-After-Free vulnerability exists in the way FunctionType 0 PDF elements are processed in Aspose.PDF 19.2 for C++. A specially crafted PDF can cause a dangling heap pointer, resulting in a use-after-free. An attacker can send a malicious PDF to trigger this vulnerability.
CVE-2019-5066
An exploitable use-after-free vulnerability exists in the way LZW-compressed streams are processed in Aspose.PDF 19.2 for C++. A specially crafted PDF can cause a dangling heap pointer, resulting in a use-after-free condition. To trigger this vulnerability, a specifically crafted PDF document needs to be processed by the target application.
CVE-2019-5067
An uninitialized memory access vulnerability exists in the way Aspose.PDF 19.2 for C++ handles invalid parent object pointers. A specially crafted PDF can cause a read and write from uninitialized memory, resulting in memory corruption and possibly arbitrary code execution. To trigger this vulnerability, a specifically crafted PDF document needs to be processed by the target application.
CVE-2019-5531
VMware vSphere ESXi (6.7 prior to ESXi670-201810101-SG, 6.5 prior to ESXi650-201811102-SG, and 6.0 prior to ESXi600-201807103-SG) and VMware vCenter Server (6.7 prior to 6.7 U1b, 6.5 prior to 6.5 U2b, and 6.0 prior to 6.0 U3j) contain an information disclosure vulnerability in clients arising from insufficient session expiration. An attacker with physical access or an ability to mimic a websocket connection to a user?s browser may be able to obtain control of a VM Console after the user has logged out or their session has timed out.
CVE-2019-5532
VMware vCenter Server (6.7.x prior to 6.7 U3, 6.5 prior to 6.5 U3 and 6.0 prior to 6.0 U3j) contains an information disclosure vulnerability due to the logging of credentials in plain-text for virtual machines deployed through OVF. A malicious user with access to the log files containing vCenter OVF-properties of a virtual machine deployed from an OVF may be able to view the credentials used to deploy the OVF (typically the root account of the virtual machine).
CVE-2019-5534
VMware vCenter Server (6.7.x prior to 6.7 U3, 6.5 prior to 6.5 U3 and 6.0 prior to 6.0 U3j) contains an information disclosure vulnerability where Virtual Machines deployed from an OVF could expose login information via the virtual machine's vAppConfig properties. A malicious actor with access to query the vAppConfig properties of a virtual machine deployed from an OVF may be able to view the credentials used to deploy the OVF (typically the root account of the virtual machine).
CVE-2019-9677
The specific fields of CGI interface of some Dahua products are not strictly verified, an attacker can cause a buffer overflow by constructing malicious packets. Affected products include: IPC-HDW1X2X,IPC-HFW1X2X,IPC-HDW2X2X,IPC-HFW2X2X,IPC-HDW4X2X,IPC-HFW4X2X,IPC-HDBW4X2X,IPC-HDW5X2X,IPC-HFW5X2X for versions which Build time is before August 18, 2019.
CVE-2019-9678
Some Dahua products have the problem of denial of service during the login process. An attacker can cause a device crashed by constructing a malicious packet. Affected products include: IPC-HDW1X2X,IPC-HFW1X2X,IPC-HDW2X2X,IPC-HFW2X2X,IPC-HDW4X2X,IPC-HFW4X2X,IPC-HDBW4X2X,IPC-HDW5X2X,IPC-HFW5X2X for versions which Build time is before August 18, 2019.
CVE-2019-9679
Some of Dahua's Debug functions do not have permission separation. Low-privileged users can use the Debug function after logging in. Affected products include: IPC-HDW1X2X,IPC-HFW1X2X,IPC-HDW2X2X,IPC-HFW2X2X,IPC-HDW4X2X,IPC-HFW4X2X,IPC-HDBW4X2X,IPC-HDW5X2X,IPC-HFW5X2X for versions which Build time is before August 18,2019.
CVE-2019-9680
Some Dahua products have information leakage issues. Attackers can obtain the IP address and device model information of the device by constructing malicious data packets. Affected products include: IPC-HDW1X2X,IPC-HFW1X2X,IPC-HDW2X2X,IPC-HFW2X2X,IPC-HDW4X2X,IPC-HFW4X2X,IPC-HDBW4X2X,IPC-HDW5X2X,IPC-HFW5X2X for versions which Build time is before August 18, 2019.
