CVE-2009-4898
Cross-site request forgery (CSRF) vulnerability in TWiki before 4.3.2 allows remote attackers to hijack the authentication of arbitrary users for requests that update pages, as demonstrated by a URL for a save script in the ACTION attribute of a FORM element, in conjunction with a call to the submit method in the onload attribute of a BODY element.  NOTE: this issue exists because of an insufficient fix for CVE-2009-1339.
CVE-2009-4996
** DISPUTED **  Xfce4-session 4.5.91 in Xfce does not lock the screen when the suspend or hibernate button is pressed, which might make it easier for physically proximate attackers to access an unattended laptop via a resume action, a related issue to CVE-2010-2532.  NOTE: there is no general agreement that this is a vulnerability, because separate control over locking can be an equally secure, or more secure, behavior in some threat environments.
CVE-2009-4997
gnome-power-manager 2.27.92 does not properly implement the lock_on_suspend and lock_on_hibernate settings for locking the screen when the suspend or hibernate button is pressed, which might make it easier for physically proximate attackers to access an unattended laptop via a resume action, a related issue to CVE-2010-2532.  NOTE: this issue exists because of a regression that followed a gnome-power-manager fix a few years earlier.
CVE-2010-2248
fs/cifs/cifssmb.c in the CIFS implementation in the Linux kernel before 2.6.34-rc4 allows remote attackers to cause a denial of service (panic) via an SMB response packet with an invalid CountHigh value, as demonstrated by a response from an OS/2 server, related to the CIFSSMBWrite and CIFSSMBWrite2 functions.
CVE-2010-2521
Multiple buffer overflows in fs/nfsd/nfs4xdr.c in the XDR implementation in the NFS server in the Linux kernel before 2.6.34-rc6 allow remote attackers to cause a denial of service (panic) or possibly execute arbitrary code via a crafted NFSv4 compound WRITE request, related to the read_buf and nfsd4_decode_compound functions.
CVE-2010-2739
Buffer overflow in the CreateDIBPalette function in win32k.sys in Microsoft Windows XP SP3, Server 2003 R2 Enterprise SP2, Vista Business SP1, Windows 7, and Server 2008 SP2 allows local users to cause a denial of service (crash) and possibly execute arbitrary code by performing a clipboard operation (GetClipboardData API function) with a crafted bitmap with a palette that contains a large number of colors.
CVE-2010-2802
Cross-site scripting (XSS) vulnerability in MantisBT before 1.2.2 allows remote authenticated users to inject arbitrary web script or HTML via an HTML document with a .gif filename extension, related to inline attachments.
CVE-2010-2874
Unspecified vulnerability in Adobe Shockwave Player before 11.5.8.612 allows remote attackers to execute arbitrary code via unknown vectors that trigger memory corruption.  NOTE: due to conflicting information and use of the same CVE identifier by the vendor, ZDI, and TippingPoint, it is not clear whether this issue is related to use of an uninitialized pointer, an incorrect pointer offset calculation, or both.
CVE-2010-3213
Cross-site request forgery (CSRF) vulnerability in Microsoft Outlook Web Access (owa/ev.owa) 2007 through SP2 allows remote attackers to hijack the authentication of e-mail users for requests that perform Outlook requests, as demonstrated by setting the auto-forward rule.
CVE-2010-3244
BbtsConnection_Edit.exe in Blackboard Transact Suite (formerly Blackboard Commerce Suite) before 3.6.0.2 relies on field names when determining whether it is appropriate to decrypt a connection.xml field value, which allows local users to discover the database password via a modified connection.xml file that contains an encrypted password in the <Server> field.
CVE-2010-3245
The automated-backup functionality in Blackboard Transact Suite (formerly Blackboard Commerce Suite) stores the (1) database username and (2) database password in cleartext in (a) script and (b) batch (.bat) files, which allows local users to obtain sensitive information by reading a file.
CVE-2010-3246
Google Chrome before 6.0.472.53 does not properly handle the _blank value for the target attribute of unspecified elements, which allows remote attackers to bypass the pop-up blocker via unknown vectors.
CVE-2010-3247
Google Chrome before 6.0.472.53 does not properly restrict the characters in URLs, which allows remote attackers to spoof the appearance of the URL bar via homographic sequences.
CVE-2010-3248
Google Chrome before 6.0.472.53 does not properly restrict copying to the clipboard, which has unspecified impact and attack vectors.
CVE-2010-3249
Google Chrome before 6.0.472.53 does not properly implement SVG filters, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors, related to a "stale pointer" issue.
CVE-2010-3250
Unspecified vulnerability in Google Chrome before 6.0.472.53 allows remote attackers to enumerate the set of installed extensions via unknown vectors.
CVE-2010-3251
The WebSockets implementation in Google Chrome before 6.0.472.53 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via unspecified vectors.
CVE-2010-3252
Use-after-free vulnerability in the Notifications presenter in Google Chrome before 6.0.472.53 allows attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2010-3253
The implementation of notification permissions in Google Chrome before 6.0.472.53 allows attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-3254
The WebSockets implementation in Google Chrome before 6.0.472.53 does not properly handle integer values, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2010-3255
Google Chrome before 6.0.472.53 and webkitgtk before 1.2.6 do not properly handle counter nodes, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-3256
Google Chrome before 6.0.472.53 does not properly limit the number of stored autocomplete entries, which has unspecified impact and attack vectors.
CVE-2010-3257
Use-after-free vulnerability in WebKit, as used in Apple Safari before 4.1.3 and 5.0.x before 5.0.3, Google Chrome before 6.0.472.53, and webkitgtk before 1.2.6, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving element focus.
CVE-2010-3258
The sandbox implementation in Google Chrome before 6.0.472.53 does not properly deserialize parameters, which has unspecified impact and remote attack vectors.
CVE-2010-3259
WebKit, as used in Apple Safari before 4.1.3 and 5.0.x before 5.0.3, Google Chrome before 6.0.472.53, and webkitgtk before 1.2.6, does not properly restrict read access to images derived from CANVAS elements, which allows remote attackers to bypass the Same Origin Policy and obtain potentially sensitive image data via a crafted web site.
