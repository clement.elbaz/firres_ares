CVE-2009-3555
The TLS protocol, and the SSL protocol 3.0 and possibly earlier, as used in Microsoft Internet Information Services (IIS) 7.0, mod_ssl in the Apache HTTP Server 2.2.14 and earlier, OpenSSL before 0.9.8l, GnuTLS 2.8.5 and earlier, Mozilla Network Security Services (NSS) 3.12.4 and earlier, multiple Cisco products, and other products, does not properly associate renegotiation handshakes with an existing connection, which allows man-in-the-middle attackers to insert data into HTTPS sessions, and possibly other types of sessions protected by TLS or SSL, by sending an unauthenticated request that is processed retroactively by a server in a post-renegotiation context, related to a "plaintext injection" attack, aka the "Project Mogul" issue.
CVE-2009-3726
The nfs4_proc_lock function in fs/nfs/nfs4proc.c in the NFSv4 client in the Linux kernel before 2.6.31-rc4 allows remote NFS servers to cause a denial of service (NULL pointer dereference and panic) by sending a certain response containing incorrect file attributes, which trigger attempted use of an open file that lacks NFSv4 state.
CVE-2009-3728
Directory traversal vulnerability in the ICC_Profile.getInstance method in Java Runtime Environment (JRE) in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, allows remote attackers to determine the existence of local International Color Consortium (ICC) profile files via a .. (dot dot) in a pathname, aka Bug Id 6631533.
CVE-2009-3729
Unspecified vulnerability in the TrueType font parsing functionality in Sun Java SE 5.0 before Update 22 and 6 before Update 17 allows remote attackers to cause a denial of service (application crash) via a certain test suite, aka Bug Id 6815780.
CVE-2009-3879
Multiple unspecified vulnerabilities in the (1) X11 and (2) Win32GraphicsDevice subsystems in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, have unknown impact and attack vectors, related to failure to clone arrays that are returned by the getConfigurations function, aka Bug Id 6822057.
CVE-2009-3880
The Abstract Window Toolkit (AWT) in Java Runtime Environment (JRE) in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, does not properly restrict the objects that may be sent to loggers, which allows attackers to obtain sensitive information via vectors related to the implementation of Component, KeyboardFocusManager, and DefaultKeyboardFocusManager, aka Bug Id 6664512.
CVE-2009-3881
Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, does not prevent the existence of children of a resurrected ClassLoader, which allows remote attackers to gain privileges via unspecified vectors, related to an "information leak vulnerability," aka Bug Id 6636650.
CVE-2009-3882
Multiple unspecified vulnerabilities in the Swing implementation in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, have unknown impact and remote attack vectors, related to "information leaks in mutable variables," aka Bug Id 6657026.
CVE-2009-3883
Multiple unspecified vulnerabilities in the Windows Pluggable Look and Feel (PL&F) feature in the Swing implementation in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, have unknown impact and remote attack vectors, related to "information leaks in mutable variables," aka Bug Id 6657138.
CVE-2009-3884
The TimeZone.getTimeZone method in Sun Java SE 5.0 before Update 22 and 6 before Update 17, and OpenJDK, allows remote attackers to determine the existence of local files via vectors related to handling of zoneinfo (aka tz) files, aka Bug Id 6824265.
CVE-2009-3885
Sun Java SE 5.0 before Update 22 and 6 before Update 17 on Windows allows remote attackers to cause a denial of service via a BMP file containing a link to a UNC share pathname for an International Color Consortium (ICC) profile file, probably a related issue to CVE-2007-2789, aka Bug Id 6632445.
CVE-2009-3886
The Java Web Start implementation in Sun Java SE 6 before Update 17 does not properly handle the interaction between a signed JAR file and a JNLP (1) application or (2) applet, which has unspecified impact and attack vectors, related to a "regression," aka Bug Id 6870531.
CVE-2009-3911
Cross-site scripting (XSS) vulnerability in settings.php in TFTgallery 0.13 allows remote attackers to inject arbitrary web script or HTML via the sample parameter.
CVE-2009-3912
Directory traversal vulnerability in index.php in TFTgallery 0.13 allows remote attackers to read arbitrary files via a ..%2F (encoded dot dot slash) in the album parameter.
CVE-2009-3913
SQL injection vulnerability in summary.php in Xerox Fiery Webtools allows remote attackers to execute arbitrary SQL commands via the select parameter.
CVE-2009-3914
Cross-site scripting (XSS) vulnerability in the Temporary Invitation module 5.x before 5.x-2.3 for Drupal allows remote attackers to inject arbitrary web script or HTML via the Name field in an invitation.
CVE-2009-3915
Cross-site scripting (XSS) vulnerability in the "Separate title and URL" formatter in the Link module 5.x before 5.x-2.6 and 6.x before 6.x-2.7, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via the link title field.
CVE-2009-3916
Cross-site scripting (XSS) vulnerability in the Node Hierarchy module 5.x before 5.x-1.3 and 6.x before 6.x-1.3, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via a child node title.
CVE-2009-3917
Cross-site scripting (XSS) vulnerability in the S5 Presentation Player module 6.x-1.x before 6.x-1.1 for Drupal allows remote attackers to inject arbitrary web script or HTML via an unspecified field that is copied to the HTML HEAD element.
CVE-2009-3918
Cross-site scripting (XSS) vulnerability in the Zoomify module 5.x before 5.x-2.2 and 6.x before 6.x-1.4, a module for Drupal, allows remote attackers to inject arbitrary web script or HTML via the node title.
CVE-2009-3919
Cross-site scripting (XSS) vulnerability in the NGP COO/CWP Integration (crmngp) module 6.x before 6.x-1.12 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified "user-supplied information."
CVE-2009-3920
An administration page in the NGP COO/CWP Integration (crmngp) module 6.x before 6.x-1.12 for Drupal does not perform the expected access control, which allows remote attackers to read log information via unspecified vectors.
CVE-2009-3921
The Smartqueue_og module 5.x before 5.x-1.3 and 6.x before 6.x-1.0-rc3, a module for Drupal, does not verify group-node privileges in certain circumstances involving subqueue creation, which allows remote authenticated users to discover arbitrary organic group names by reading confirmation messages.
CVE-2009-3922
Multiple cross-site request forgery (CSRF) vulnerabilities in the User Protect module 5.x before 5.x-1.4 and 6.x before 6.x-1.3, a module for Drupal, allow remote attackers to hijack the authentication of administrators for requests that (1) delete the editing protection of a user or (2) delete a certain type of administrative-bypass rule.
