CVE-2014-9758
Cross-site scripting (XSS) vulnerability in Magento E-Commerce Platform 1.9.0.1.
CVE-2015-0162
IBM Security SiteProtector System 3.0, 3.1, and 3.1.1 allows local users to gain privileges.
CVE-2015-1329
Use-after-free vulnerability in oxide::qt::URLRequestDelegatedJob in oxide-qt in Ubuntu 15.04 and 14.04 LTS might allow remote attackers to execute arbitrary code.
CVE-2015-1865
fts.c in coreutils 8.4 allows local users to delete arbitrary files.
CVE-2015-1866
Cross-site scripting (XSS) vulnerability in Ember.js 1.10.x before 1.10.1 and 1.11.x before 1.11.2.
CVE-2015-2826
WordPress Simple Ads Manager plugin 2.5.94 and 2.5.96 allows remote attackers to obtain sensitive information.
CVE-2015-2927
node 0.3.2 and URONode before 1.0.5r3 allows remote attackers to cause a denial of service (bandwidth consumption).
CVE-2015-3890
Use-after-free vulnerability in Open Litespeed before 1.3.10.
CVE-2015-4072
Multiple cross-site scripting (XSS) vulnerabilities in the Helpdesk Pro plugin before 1.4.0 for Joomla! allow remote attackers to inject arbitrary web script or HTML via vectors related to name and message.
CVE-2015-4073
Multiple SQL injection vulnerabilities in the Helpdesk Pro plugin before 1.4.0 for Joomla! allow remote attackers to execute arbitrary SQL commands via the (1) ticket_code or (2) email parameter or (3) remote authenticated users to execute arbitrary SQL commands via the filter_order parameter.
CVE-2015-4074
Directory traversal vulnerability in the Helpdesk Pro plugin before 1.4.0 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the filename parameter in a ticket.download_attachment task.
CVE-2015-4075
The Helpdesk Pro plugin before 1.4.0 for Joomla! allows remote attackers to write to arbitrary .ini files via a crafted language.save task.
CVE-2015-4707
Cross-site scripting (XSS) vulnerability in IPython before 3.2 allows remote attackers to inject arbitrary web script or HTML via vectors involving JSON error messages and the /api/notebooks path.
CVE-2015-5179
FreeIPA might display user data improperly via vectors involving non-printable characters.
CVE-2015-5248
Reflected file download vulnerability in Red Hat Feedhenry Enterprise Mobile Application Platform.
CVE-2015-5395
Cross-site request forgery (CSRF) vulnerability in SOGo before 3.1.0.
CVE-2015-5607
Cross-site request forgery in the REST API in IPython 2 and 3.
CVE-2015-5608
Open redirect vulnerability in Joomla! CMS 3.0.0 through 3.4.1.
CVE-2015-6673
Use-after-free vulnerability in Decoder.cpp in libpgf before 6.15.32.
CVE-2015-7347
Cross-site scripting (XSS) vulnerability in ZCMS JavaServer Pages Content Management System 1.1.
CVE-2015-8224
Huawei P8 before GRA-CL00C92B210, before GRA-L09C432B200, before GRA-TL00C01B210, and before GRA-UL00C00B210 allows remote attackers to obtain user equipment (aka UE) measurements of signal strengths.
CVE-2015-9231
iTerm2 3.x before 3.1.1 allows remote attackers to discover passwords by reading DNS queries. A new (default) feature was added to iTerm2 version 3.0.0 (and unreleased 2.9.x versions such as 2.9.20150717) that resulted in a potential information disclosure. In an attempt to see whether the text under the cursor (or selected text) was a URL, the text would be sent as an unencrypted DNS query. This has the potential to result in passwords and other sensitive information being sent in cleartext without the user being aware.
CVE-2015-9232
The Good for Enterprise application 3.0.0.415 for Android does not use signature protection for its Authentication Delegation API intent. Also, the Good Dynamic application activation process does not attempt to detect malicious activation attempts involving modified names beginning with a com.good.gdgma substring. Consequently, an attacker could obtain access to intranet data. This issue is only relevant in cases where the user has already downloaded a malicious Android application.
CVE-2016-6795
In the Convention plugin in Apache Struts 2.3.x before 2.3.31, and 2.5.x before 2.5.5, it is possible to prepare a special URL which will be used for path traversal and execution of arbitrary code on server side.
CVE-2016-8738
In Apache Struts 2.5 through 2.5.5, if an application allows entering a URL in a form field and the built-in URLValidator is used, it is possible to prepare a special URL which will be used to overload server process when performing validation of the URL.
CVE-2017-12168
The access_pmu_evcntr function in arch/arm64/kvm/sys_regs.c in the Linux kernel before 4.8.11 allows privileged KVM guest OS users to cause a denial of service (assertion failure and host OS crash) by accessing the Performance Monitors Cycle Count Register (PMCCNTR).
CVE-2017-12611
In Apache Struts 2.0.0 through 2.3.33 and 2.5 through 2.5.10.1, using an unintentional expression in a Freemarker tag instead of string literals can lead to a RCE attack.
CVE-2017-14339
The DNS packet parser in YADIFA before 2.2.6 does not check for the presence of infinite pointer loops, and thus it is possible to force it to enter an infinite loop. This can cause high CPU usage and makes the server unresponsive.
CVE-2017-14595
In Joomla! before 3.8.0, a logic bug in a SQL query could lead to the disclosure of article intro texts when these articles are in the archived state.
CVE-2017-14596
In Joomla! before 3.8.0, inadequate escaping in the LDAP authentication plugin can result in a disclosure of a username and password.
CVE-2017-14604
GNOME Nautilus before 3.23.90 allows attackers to spoof a file type by using the .desktop file extension, as demonstrated by an attack in which a .desktop file's Name field ends in .pdf but this file's Exec field launches a malicious "sh -c" command. In other words, Nautilus provides no UI indication that a file actually has the potentially unsafe .desktop extension; instead, the UI only shows the .pdf extension. One (slightly) mitigating factor is that an attack requires the .desktop file to have execute permission. The solution is to ask the user to confirm that the file is supposed to be treated as a .desktop file, and then remember the user's answer in the metadata::trusted field.
CVE-2017-14607
In ImageMagick 7.0.7-4 Q16, an out of bounds read flaw related to ReadTIFFImage has been reported in coders/tiff.c. An attacker could possibly exploit this flaw to disclose potentially sensitive memory or cause an application crash.
CVE-2017-14608
In LibRaw through 0.18.4, an out of bounds read flaw related to kodak_65000_load_raw has been reported in dcraw/dcraw.c and internal/dcraw_common.cpp. An attacker could possibly exploit this flaw to disclose potentially sensitive memory or cause an application crash.
CVE-2017-14609
The server daemons in Kannel 1.5.0 and earlier create a PID file after dropping privileges to a non-root account, which might allow local users to kill arbitrary processes by leveraging access to this non-root account for PID file modification before a root script executes a "kill `cat /pathname`" command, as demonstrated by bearerbox.
CVE-2017-14610
bareos-dir, bareos-fd, and bareos-sd in bareos-core in Bareos 16.2.6 and earlier create a PID file after dropping privileges to a non-root account, which might allow local users to kill arbitrary processes by leveraging access to this non-root account for PID file modification before a root script executes a "kill `cat /pathname`" command.
CVE-2017-14615
An FBX-5313 issue was discovered in WatchGuard Fireware before 12.0. When a failed login attempt is made to the login endpoint of the XML-RPC interface, if JavaScript code, properly encoded to be consumed by XML parsers, is embedded as value of the user element, the code will be rendered in the context of any logged in user in the Web UI visiting "Traffic Monitor" sections "Events" and "All." As a side effect, no further events will be visible in the Traffic Monitor until the device is restarted.
CVE-2017-14616
An FBX-5312 issue was discovered in WatchGuard Fireware before 12.0. If a login attempt is made in the XML-RPC interface with an XML message containing an empty member element, the wgagent crashes, logging out any user with a session opened in the UI. By continuously executing the failed login attempts, UI management of the device becomes impossible.
CVE-2017-14617
In Poppler 0.59.0, a floating point exception occurs in the ImageStream class in Stream.cc, which may lead to a potential attack when handling malicious PDF files.
CVE-2017-14618
Cross-site scripting (XSS) vulnerability in inc/PMF/Faq.php in phpMyFAQ through 2.9.8 allows remote attackers to inject arbitrary web script or HTML via the Questions field in an "Add New FAQ" action.
CVE-2017-14619
Cross-site scripting (XSS) vulnerability in phpMyFAQ through 2.9.8 allows remote attackers to inject arbitrary web script or HTML via the "Title of your FAQ" field in the Configuration Module.
CVE-2017-14621
Portus 2.2.0 has XSS via the Team field, related to typeahead.
CVE-2017-14623
In the ldap.v2 (aka go-ldap) package through 2.5.0 for Go, an attacker may be able to login with an empty password. This issue affects an application using this package if these conditions are met: (1) it relies only on the return error of the Bind function call to determine whether a user is authorized (i.e., a nil return value is interpreted as successful authorization) and (2) it is used with an LDAP server allowing unauthenticated bind.
CVE-2017-7924
An Improper Input Validation issue was discovered in Rockwell Automation MicroLogix 1100 controllers 1763-L16BWA, 1763-L16AWA, 1763-L16BBB, and 1763-L16DWD. A remote, unauthenticated attacker could send a single, specially crafted Programmable Controller Communication Commands (PCCC) packet to the controller that could potentially cause the controller to enter a DoS condition.
CVE-2017-8770
There is LFD (local file disclosure) on BE126 WIFI repeater 1.0 devices that allows attackers to read the entire filesystem on the device via a crafted getpage parameter.
CVE-2017-8771
On BE126 WIFI repeater 1.0 devices, an attacker can log into telnet (which is open by default) with default credentials as root (username:"root" password:"root"). The attacker can make a user that is connected to the repeater click on a malicious link that will log into the telnet and will infect the device with malicious code.
CVE-2017-8772
On BE126 WIFI repeater 1.0 devices, an attacker can log into telnet (which is open by default) with default credentials as root (username:"root" password:"root") and can: 1. Read the entire file system; 2. Write to the file system; or 3. Execute any code that attacker desires (malicious or not).
CVE-2017-9607
The BL1 FWU SMC handling code in ARM Trusted Firmware before 1.4 might allow attackers to write arbitrary data to secure memory, bypass the bl1_plat_mem_check protection mechanism, cause a denial of service, or possibly have unspecified other impact via a crafted AArch32 image, which triggers an integer overflow.
CVE-2017-9645
An Inadequate Encryption Strength issue was discovered in Mirion Technologies DMC 3000 Transmitter Module, iPam Transmitter f/DMC 2000, RDS-31 iTX and variants (including RSD31-AM Package), DRM-1/2 and variants (including Solar PWR Package), DRM and RDS Based Boundary Monitors, External Transmitters, Telepole II, and MESH Repeater (Telemetry Enabled Devices). Decryption of data is possible at the hardware level.
CVE-2017-9649
A Use of Hard-Coded Cryptographic Key issue was discovered in Mirion Technologies DMC 3000 Transmitter Module, iPam Transmitter f/DMC 2000, RDS-31 iTX and variants (including RSD31-AM Package), DRM-1/2 and variants (including Solar PWR Package), DRM and RDS Based Boundary Monitors, External Transmitters, Telepole II, and MESH Repeater (Telemetry Enabled Devices). An unchangeable, factory-set key is included in the 900 MHz transmitter firmware.
CVE-2017-9793
The REST Plugin in Apache Struts 2.1.x, 2.3.7 through 2.3.33 and 2.5 through 2.5.12 is using an outdated XStream library which is vulnerable and allow perform a DoS attack using malicious request with specially crafted XML payload.
CVE-2017-9804
In Apache Struts 2.3.7 through 2.3.33 and 2.5 through 2.5.12, if an application allows entering a URL in a form field and built-in URLValidator is used, it is possible to prepare a special URL which will be used to overload server process when performing validation of the URL.  NOTE: this vulnerability exists because of an incomplete fix for S2-047 / CVE-2017-7672.
