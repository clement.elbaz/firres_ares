CVE-2013-1436
The XMonad.Hooks.DynamicLog module in xmonad-contrib before 0.11.2 allows remote attackers to execute arbitrary commands via a web page title, which activates the commands when the user clicks on the xmobar window title, as demonstrated using an action tag.
CVE-2013-2645
Multiple cross-site request forgery (CSRF) vulnerabilities on the TP-LINK WR1043N router with firmware TL-WR1043ND_V1_120405 allow remote attackers to hijack the authentication of administrators for requests that (1) enable FTP access (aka "FTP directory traversal") to /tmp via the shareEntire parameter to userRpm/NasFtpCfgRpm.htm, (2) change the FTP administrative password via the nas_admin_pwd parameter to userRpm/NasUserAdvRpm.htm, (3) enable FTP on the WAN interface via the internetA parameter to userRpm/NasFtpCfgRpm.htm, (4) launch the FTP service via the startFtp parameter to userRpm/NasFtpCfgRpm.htm, or (5) enable or disable bandwidth limits via the QoSCtrl parameter to userRpm/QoSCfgRpm.htm.
CVE-2013-6496
Red Hat Conga 0.12.2 allows remote attackers to obtain sensitive information via a crafted request to the (1) homebase, (2) cluster, (3) storage, (4) portal_skins/custom, or (5) logs Luci extension.
CVE-2013-7329
The CGI::Application module before 4.50_50 and 4.50_51 for Perl, when run modes are not specified, allows remote attackers to obtain sensitive information (web queries and environment details) via vectors related to the dump_html function.
CVE-2014-0074
Apache Shiro 1.x before 1.2.3, when using an LDAP server with unauthenticated bind enabled, allows remote attackers to bypass authentication via an empty (1) username or (2) password.
CVE-2014-0140
Red Hat CloudForms 3.1 Management Engine (CFME) before 5.3 allows remote authenticated users to access sensitive controllers and actions via a direct HTTP or HTTPS request.
CVE-2014-0168
Cross-site request forgery (CSRF) vulnerability in Jolokia before 1.2.1 allows remote attackers to hijack the authentication of users for requests that execute MBeans methods via a crafted web page.
CVE-2014-0397
Multiple unspecified vulnerabilities in libXtsol in Oracle Solaris 10 and 11.1 have unspecified impact and attack vectors related to "Buffer errors."
CVE-2014-0994
Heap-based buffer overflow in the ReadDIB function in the Vcl.Graphics.TPicture.Bitmap implementation in the Visual Component Library (VCL) in Embarcadero Delphi XE6 20.0.15596.9843 and C++ Builder XE6 20.0.15596.9843 allows context-dependent attackers to execute arbitrary code via the BITMAPINFOHEADER.biClrUsed field in a BMP file.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-0993.
CVE-2014-1224
Incomplete blacklist vulnerability in the user registration feature in rexx Recruitment R6.1 and R7 without "fixes from 2014-01-15" allows remote attackers to conduct cross-site scripting (XSS) attacks via the oninput event handler in the fname parameter to the default URI in /reg.
CVE-2014-1868
Restlet Framework 2.1.x before 2.1.7 and 2.x.x before 2.2 RC1, when using XMLRepresentation or XML serializers, allows attackers to cause a denial of service via an XML Entity Expansion (XEE) attack.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-1875
The Capture::Tiny module before 0.24 for Perl allows local users to write to arbitrary files via a symlink attack on a temporary file.
CVE-2014-2044
Incomplete blacklist vulnerability in ajax/upload.php in ownCloud before 5.0, when running on Windows, allows remote authenticated users to bypass intended access restrictions, upload files with arbitrary names, and execute arbitrary code via an Alternate Data Stream (ADS) syntax in the filename parameter, as demonstrated using .htaccess::$DATA to upload a PHP program.
CVE-2014-2644
Cross-site scripting (XSS) vulnerability in HP Systems Insight Manager (SIM) before 7.4 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2014-3521
The component in (1) /luci/homebase and (2) /luci/cluster menu in Red Hat Conga 0.12.2 allows remote authenticated users to bypass intended access restrictions via a crafted URL.
CVE-2014-3608
The VMWare driver in OpenStack Compute (Nova) before 2014.1.3 allows remote authenticated users to bypass the quota limit and cause a denial of service (resource consumption) by putting the VM into the rescue state, suspending it, which puts into an ERROR state, and then deleting the image.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-2573.
CVE-2014-3633
The qemuDomainGetBlockIoTune function in qemu/qemu_driver.c in libvirt before 1.2.9, when a disk has been hot-plugged or removed from the live image, allows remote attackers to cause a denial of service (crash) or read sensitive heap information via a crafted blkiotune query, which triggers an out-of-bounds read.
CVE-2014-3642
vmdb/app/controllers/application_controller/performance.rb in Red Hat CloudForms 3.1 Management Engine (CFME) before 5.3 allows remote authenticated users to gain privileges via unspecified vectors, related to an "insecure send method."
CVE-2014-3657
The virDomainListPopulate function in conf/domain_conf.c in libvirt before 1.2.9 does not clean up the lock on the list of domains, which allows remote attackers to cause a denial of service (deadlock) via a NULL value in the second parameter in the virConnectListAllDomains API command.
CVE-2014-4043
The posix_spawn_file_actions_addopen function in glibc before 2.20 does not copy its path argument in accordance with the POSIX specification, which allows context-dependent attackers to trigger use-after-free vulnerabilities.
CVE-2014-4510
Cross-site scripting (XSS) vulnerability in job.cc in apt-cacher-ng 0.7.26 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-5389
SQL injection vulnerability in content-audit-schedule.php in the Content Audit plugin before 1.6.1 for WordPress allows remote attackers to execute arbitrary SQL commands via the "Audited content types" option in the content-audit page to wp-admin/options-general.php.
CVE-2014-6054
The rfbProcessClientNormalMessage function in libvncserver/rfbserver.c in LibVNCServer 0.9.9 and earlier allows remote attackers to cause a denial of service (divide-by-zero error and server crash) via a zero value in the scaling factor in a (1) PalmVNCSetScaleFactor or (2) SetScale message.
CVE-2014-6389
backup.php in PHPCompta/NOALYSS before 6.7.2 allows remote attackers to execute arbitrary commands via shell metacharacters in the d parameter.
CVE-2014-6409
Cross-site request forgery (CSRF) vulnerability in M/Monit 3.3.2 and earlier allows remote attackers to hijack the authentication of administrators for requests that change user passwords via the fullname and password parameters to /admin/users/update.
CVE-2014-6607
M/Monit 3.3.2 and earlier does not verify the original password before changing passwords, which allows remote attackers to change the password of other users and gain privileges via the fullname and password parameters, a different vulnerability than CVE-2014-6409.
CVE-2014-7869
Cross-site scripting (XSS) vulnerability in the configuration UI in the Context Form Alteration module 7.x-1.x before 7.x-1.2 for Drupal allows remote authenticated users with the "administer contexts" permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-7870
Cross-site scripting (XSS) vulnerability in the Custom Search module 6.x-1.x before 6.x-1.12 and 7.x-1.x before 7.x-1.14 for Drupal allows remote authenticated users with the "administer custom search" permission to inject arbitrary web script or HTML via the "Label text" field to admin/config/search/custom_search/results.
