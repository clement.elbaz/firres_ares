CVE-2009-4883
SQL injection vulnerability in index.php in PHPRecipeBook 2.24 and 2.39 allows remote attackers to execute arbitrary SQL commands via the (1) base_id or (2) course_id parameter in a search action.
CVE-2009-4884
Multiple SQL injection vulnerabilities in phpCommunity 2 2.1.8, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via (1) the forum_id parameter in a forum action to index.php, (2) the topic_id parameter in a forum action to index.php, (3) the wert parameter in an id search action to index.php, (4) the wert parameter in a nick search action to index.php, or (5) the wert parameter in a forum search action to index.php, related to class_forum.php and class_search.php.
CVE-2009-4885
Cross-site scripting (XSS) vulnerability in templates/1/login.php in phpCommunity 2 2.1.8 allows remote attackers to inject arbitrary web script or HTML via the msg parameter.
CVE-2009-4886
Multiple directory traversal vulnerabilities in phpCommunity 2 2.1.8 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) file parameter to module/admin/files/show_file.php and the (2) path parameter to module/admin/files/show_source.php.
CVE-2009-4887
PHP remote file inclusion vulnerability in index.php in CMS S.Builder 3.7 and earlier, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in a binn_include_path cookie.  NOTE: this can also be leveraged to include and execute arbitrary local files.
CVE-2009-4888
Cross-site scripting (XSS) vulnerability in poster.php in PHortail 1.2.1 allows remote attackers to inject arbitrary web script or HTML via the (1) pseudo, (2) email, (3) ti, and (4) txt parameters.
CVE-2009-4889
SQL injection vulnerability in books.php in the Book Panel (book_panel) module for PHP-Fusion allows remote attackers to execute arbitrary SQL commands via the bookid parameter.
CVE-2009-4890
Multiple cross-site scripting (XSS) vulnerabilities in the login application in vBook 4.2.17 allow remote attackers to inject arbitrary web script or HTML via the (1) title and (2) message parameters.
CVE-2009-4891
SQL injection vulnerability in index.php in CS-Cart 2.0.0 Beta 3 allows remote attackers to execute arbitrary SQL commands via the product_id parameter in a products.view action.
CVE-2009-4892
SQL injection vulnerability in Content Management System WEBjump! allows remote attackers to execute arbitrary SQL commands via the id parameter to (1) portfolio_genre.php and (2) news_id.php.
CVE-2010-0544
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via vectors related to a malformed URL.
CVE-2010-1384
Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not provide a warning about a (1) http or (2) https URL that contains a username and password, which makes it easier for remote attackers to conduct phishing attacks via a crafted URL.
CVE-2010-1385
Use-after-free vulnerability in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PDF document.
CVE-2010-1388
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6, and before 4.1 on Mac OS X 10.4, does not properly handle clipboard (1) drag and (2) paste operations for URLs, which allows user-assisted remote attackers to read arbitrary files via a crafted HTML document.
CVE-2010-1389
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows user-assisted remote attackers to inject arbitrary web script or HTML via vectors involving a (1) paste or (2) drag-and-drop operation for a selection.
CVE-2010-1390
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via vectors related to improper UTF-7 canonicalization, and lack of termination of a quoted string in an HTML document.
CVE-2010-1391
Multiple directory traversal vulnerabilities in the (a) Local Storage and (b) Web SQL database implementations in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allow remote attackers to create arbitrary database files via vectors involving a (1) %2f and .. (dot dot) or (2) %5c and .. (dot dot) in a URL.
CVE-2010-1392
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to HTML buttons and the first-letter CSS style.
CVE-2010-1393
The Cascading Style Sheets (CSS) implementation in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to discover sensitive URLs via an HREF attribute associated with a redirecting URL.
CVE-2010-1394
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via vectors involving HTML document fragments.
CVE-2010-1395
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via vectors involving DOM constructor objects, related to a "scope management issue."
CVE-2010-1396
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to the contentEditable attribute and removing container elements.
CVE-2010-1397
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to a layout change during selection rendering and the DOCUMENT_POSITION_DISCONNECTED attribute in a container of an unspecified type.
CVE-2010-1398
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly perform ordered list insertions, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted HTML document, related to the insertion of an unspecified element into an editable container and the access of an uninitialized element.
CVE-2010-1399
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, accesses uninitialized memory during a selection change on a form input element, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted HTML document.
CVE-2010-1400
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving caption elements.
CVE-2010-1401
Use-after-free vulnerability in the Cascading Style Sheets (CSS) implementation in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving the :first-letter pseudo-element.
CVE-2010-1402
Double free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to an event listener in an SVG document, related to duplicate event listeners, a timer, and an AnimateTransform object.
CVE-2010-1403
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, accesses uninitialized memory during the handling of a use element in an SVG document, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted document containing XML that triggers a parsing error, related to ProcessInstruction.
CVE-2010-1404
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via an SVG document that contains recursive Use elements, which are not properly handled during page deconstruction.
CVE-2010-1405
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via an HTML element that has custom vertical positioning.
CVE-2010-1406
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, sends an https URL in the Referer header of an http request in certain circumstances involving https to http redirection, which allows remote HTTP servers to obtain potentially sensitive information via standard HTTP logging, a related issue to CVE-2010-0660.
CVE-2010-1408
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to bypass intended restrictions on outbound connections to "non-default TCP ports" via a crafted port number, related to an "integer truncation issue." NOTE: this may overlap CVE-2010-1099.
CVE-2010-1409
Incomplete blacklist vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to trigger disclosure of data over IRC via vectors involving an IRC service port.
Per: http://cwe.mitre.org/data/definitions/184.html

'Incomplete Blacklist'
CVE-2010-1410
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via an SVG document with nested use elements.
CVE-2010-1412
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to hover events.
CVE-2010-1413
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, sends NTLM credentials in cleartext in unspecified circumstances, which allows man-in-the-middle attackers to obtain sensitive information via unspecified vectors.
CVE-2010-1414
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to the removeChild DOM method.
CVE-2010-1415
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly handle libxml contexts, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted HTML document, related to an "API abuse issue."
CVE-2010-1416
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly restrict the reading of a canvas that contains an SVG image pattern from a different web site, which allows remote attackers to read images from other sites via a crafted canvas, related to a "cross-site image capture issue."
CVE-2010-1417
The Cascading Style Sheets (CSS) implementation in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via HTML content that contains multiple :after pseudo-selectors.
CVE-2010-1418
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via a FRAME element with a SRC attribute composed of a javascript: sequence preceded by spaces.
CVE-2010-1419
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows user-assisted remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving a certain window close action that occurs during a drag-and-drop operation.
CVE-2010-1421
The execCommand JavaScript function in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly restrict remote execution of clipboard commands, which allows remote attackers to modify the clipboard via a crafted HTML document.
CVE-2010-1422
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly handle changes to keyboard focus that occur during processing of key press events, which allows remote attackers to force arbitrary key presses via a crafted HTML document.
CVE-2010-1749
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to the Cascading Style Sheets (CSS) run-in property and multiple invocations of a destructor for a child element that has been referenced multiple times.
CVE-2010-1750
Use-after-free vulnerability in Apple Safari before 5.0 on Windows allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to improper window management.
CVE-2010-1758
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving DOM Range objects.
CVE-2010-1759
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to the Node.normalize method.
CVE-2010-1761
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving HTML document subtrees.
CVE-2010-1762
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to inject arbitrary web script or HTML via vectors involving HTML in a TEXTAREA element.
CVE-2010-1764
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, follows multiple redirections during form submission, which allows remote web servers to obtain sensitive information by recording the form data.
CVE-2010-1770
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, Apple Safari before 4.1 on Mac OS X 10.4, and Google Chrome before 5.0.375.70 does not properly handle a transformation of a text node that has the IBM1147 character set, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted HTML document containing a BR element, related to a "type checking issue."
CVE-2010-1771
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving fonts.
CVE-2010-1774
WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, accesses out-of-bounds memory during processing of HTML tables, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted HTML document.
CVE-2010-2264
The Cascading Style Sheets (CSS) implementation in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, and before 4.1 on Mac OS X 10.4, does not properly handle the :visited pseudo-class, which allows remote attackers to obtain sensitive information about visited web pages via a crafted HTML document.
