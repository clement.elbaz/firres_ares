CVE-2009-5047
Jetty 6.x through 6.1.22 suffers from an escape sequence injection vulnerability from an attack vector by means of: 1) "Cookie Dump Servlet" and 2) Http Content-Length header. 1) A POST request to the form at "/test/cookie/" with the "Age" parameter set to a string throws a "java.lang.NumberFormatException" which reflects binary characters including ESC. These characters could be used to execute arbitrary commands or buffer dumps in the terminal. 2) The attack vector in 1) can be exploited by requesting a page using an HTTP request "Content-Length" header set to a consonant string (string including only letters).
CVE-2011-0703
In gksu-polkit before 0.0.3, the source file for xauth may contain arbitrary commands that may allow an attacker to overtake an administrator X11 session.
CVE-2011-2726
An access bypass issue was found in Drupal 7.x before version 7.5. If a Drupal site has the ability to attach File upload fields to any entity type in the system or has the ability to point individual File upload fields to the private file directory in comments, and the parent node is denied access, non-privileged users can still download the file attached to the comment if they know or guess its direct URL.
CVE-2011-2910
The AX.25 daemon (ax25d) in ax25-tools before 0.0.8-13 does not check the return value of a setuid call. The setuid call is responsible for dropping privileges but if the call fails the daemon would continue to run with root privileges which can allow possible privilege escalation.
CVE-2011-2916
qtnx 0.9 stores non-custom SSH keys in a world-readable configuration file. If a user has a world-readable or world-executable home directory, another local system user could obtain the private key used to connect to remote NX sessions.
CVE-2013-4584
Perdition before 2.2 may have weak security when handling outbound connections, caused by an error in the STARTTLS IMAP and POP server. ssl_outgoing_ciphers not being applied to STARTTLS connections
CVE-2013-7087
ClamAV before 0.97.7 has WWPack corrupt heap memory
CVE-2013-7088
ClamAV before 0.97.7 has buffer overflow in the libclamav component
CVE-2013-7089
ClamAV before 0.97.7: dbg_printhex possible information leak
CVE-2014-0021
Chrony before 1.29.1 has traffic amplification in cmdmon protocol
CVE-2014-0023
OpenShift: Install script has temporary file creation vulnerability which can result in arbitrary code execution
CVE-2016-5285
A Null pointer dereference vulnerability exists in Mozilla Network Security Services due to a missing NULL check in PK11_SignWithSymKey / ssl3_ComputeRecordMACConstantTime, which could let a remote malicious user cause a Denial of Service.
CVE-2018-18368
Symantec Endpoint Protection Manager (SEPM), prior to 14.2 RU1, may be susceptible to a privilege escalation vulnerability, which is a type of issue whereby an attacker may attempt to compromise the software application to gain elevated access to resources that are normally protected from an application or user.
CVE-2019-12756
Symantec Endpoint Protection (SEP), prior to 14.2 RU2 may be susceptible to a password protection bypass vulnerability whereby the secondary layer of password protection could by bypassed for individuals with local administrator rights.
CVE-2019-12757
Symantec Endpoint Protection (SEP), prior to 14.2 RU2 & 12.1 RU6 MP10 and Symantec Endpoint Protection Small Business Edition (SEP SBE) prior to 12.1 RU6 MP10d (12.1.7510.7002), may be susceptible to a privilege escalation vulnerability, which is a type of issue whereby an attacker may attempt to compromise the software application to gain elevated access to resources that are normally protected from an application or user.
CVE-2019-12758
Symantec Endpoint Protection, prior to 14.2 RU2, may be susceptible to an unsigned code execution vulnerability, which may allow an individual to execute code without a resident proper digital signature.
CVE-2019-12759
Symantec Endpoint Protection Manager (SEPM) and Symantec Mail Security for MS Exchange (SMSMSE), prior to versions 14.2 RU2 and 7.5.x respectively, may be susceptible to a privilege escalation vulnerability, which is a type of issue whereby an attacker may attempt to compromise the software application to gain elevated access to resources that are normally protected from an application or user.
CVE-2019-13581
An issue was discovered in Marvell 88W8688 Wi-Fi firmware before version p52, as used on Tesla Model S/X vehicles manufactured before March 2018, via the Parrot Faurecia Automotive FC6050W module. A heap-based buffer overflow allows remote attackers to cause a denial of service or execute arbitrary code via malformed Wi-Fi packets.
CVE-2019-13582
An issue was discovered in Marvell 88W8688 Wi-Fi firmware before version p52, as used on Tesla Model S/X vehicles manufactured before March 2018, via the Parrot Faurecia Automotive FC6050W module. A stack overflow could lead to denial of service or arbitrary code execution.
CVE-2019-14343
TemaTres 3.0 has stored XSS via the value parameter to the vocab/admin.php?vocabulario_id=list URI.
CVE-2019-14345
TemaTres 3.0 allows remote unprivileged users to create an administrator account
CVE-2019-14869
A flaw was found in all versions of ghostscript 9.x before 9.50, where the `.charkeys` procedure, where it did not properly secure its privileged calls, enabling scripts to bypass `-dSAFER` restrictions. An attacker could abuse this flaw by creating a specially crafted PostScript file that could escalate privileges within the Ghostscript and access files outside of restricted areas or execute commands.
CVE-2019-16761
A specially crafted Bitcoin script can cause a discrepancy between the specified SLP consensus rules and the validation result of the slp-validate@1.0.0 npm package. An attacker could create a specially crafted Bitcoin script in order to cause a hard-fork from the SLP consensus. All versions >1.0.0 have been patched.
CVE-2019-16762
A specially crafted Bitcoin script can cause a discrepancy between the specified SLP consensus rules and the validation result of the slpjs npm package. An attacker could create a specially crafted Bitcoin script in order to cause a hard-fork from the SLP consensus. Affected users can upgrade to any version >= 0.21.4.
CVE-2019-18372
Symantec Endpoint Protection, prior to 14.2 RU2, may be susceptible to a privilege escalation vulnerability, which is a type of issue whereby an attacker may attempt to compromise the software application to gain elevated access to resources that are normally protected from an application or user.
CVE-2019-18928
Cyrus IMAP 2.5.x before 2.5.14 and 3.x before 3.0.12 allows privilege escalation because an HTTP request may be interpreted in the authentication context of an unrelated previous request that arrived over the same connection.
CVE-2019-18981
Pimcore before 6.2.2 lacks an Access Denied outcome for a certain scenario of an incorrect recipient ID of a notification.
CVE-2019-18982
bundles/AdminBundle/Controller/Admin/EmailController.php in Pimcore before 6.3.0 allows script execution in the Email Log preview window because of the lack of a Content-Security-Policy header.
CVE-2019-18985
Pimcore before 6.2.2 lacks brute force protection for the 2FA token.
CVE-2019-18986
Pimcore before 6.2.2 allow attackers to brute-force (guess) valid usernames by using the 'forgot password' functionality as it returns distinct messages for invalid password and non-existing users.
CVE-2019-18987
An issue was discovered in the AbuseFilter extension through 1.34 for MediaWiki. Once a specific abuse filter has (accidentally or otherwise) been made public, its previous versions can be exposed, thus potentially disclosing private or sensitive information within the filter's definition.
CVE-2019-6659
On version 14.0.0-14.1.0.1, BIG-IP virtual servers with TLSv1.3 enabled may experience a denial of service due to undisclosed incoming messages.
CVE-2019-6660
On BIG-IP 14.1.0-14.1.2, 14.0.0-14.0.1, and 13.1.0-13.1.1, undisclosed HTTP requests may consume excessive amounts of systems resources which may lead to a denial of service.
CVE-2019-6661
When the BIG-IP APM 14.1.0-14.1.2, 14.0.0-14.0.1, 13.1.0-13.1.3.1, 12.1.0-12.1.4.1, or 11.5.1-11.6.5 system processes certain requests, the APD/APMD daemon may consume excessive resources.
CVE-2019-6662
On BIG-IP 13.1.0-13.1.1.4, sensitive information is logged into the local log files and/or remote logging targets when restjavad processes an invalid request. Users with access to the log files would be able to view that data.
CVE-2019-6663
The BIG-IP 15.0.0-15.0.1, 14.0.0-14.1.2.2, 13.1.0-13.1.3.1, 12.1.0-12.1.5, and 11.5.1-11.6.5.1, BIG-IQ 7.0.0, 6.0.0-6.1.0, and 5.2.0-5.4.0, iWorkflow 2.3.0, and Enterprise Manager 3.1.1 configuration utility is vulnerable to Anti DNS Pinning (DNS Rebinding) attack.
CVE-2019-6664
On BIG-IP 15.0.0 and 14.1.0-14.1.0.6, under certain conditions, network protections on the management port do not follow current best practices.
