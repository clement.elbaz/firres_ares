CVE-2009-3305
Polipo 1.0.4, and possibly other versions, allows remote attackers to cause a denial of service (crash) via a request with a Cache-Control header that lacks a value for the max-age field, which triggers a segmentation fault in the httpParseHeaders function in http_parse.c, and possibly other unspecified vectors.
CVE-2009-4137
The loadContentFromCookie function in core/Cookie.php in Piwik before 0.5 does not validate strings obtained from cookies before calling the unserialize function, which allows remote attackers to execute arbitrary code or upload arbitrary files via vectors related to the __destruct function in the Piwik_Config class; php://filter URIs; the __destruct functions in Zend Framework, as demonstrated by the Zend_Log destructor; the shutdown functions in Zend Framework, as demonstrated by the Zend_Log_Writer_Mail class; the render function in the Piwik_View class; Smarty templates; and the _eval function in Smarty.
CVE-2009-4410
The fuse_ioctl_copy_user function in the ioctl handler in fs/fuse/file.c in the Linux kernel 2.6.29-rc1 through 2.6.30.y uses the wrong variable in an argument to the kunmap function, which allows local users to cause a denial of service (panic) via unknown vectors.
CVE-2009-4411
The (1) setfacl and (2) getfacl commands in XFS acl 2.2.47, when running in recursive (-R) mode, follow symbolic links even when the --physical (aka -P) or -L option is specified, which might allow local users to modify the ACL for arbitrary files or directories via a symlink attack.
CVE-2009-4412
Unrestricted file upload vulnerability in Serendipity before 1.5 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension followed by a safe extension, then accessing it via a direct request to the file in an unspecified directory.  NOTE: some of these details are obtained from third party information.
CVE-2009-4413
The httpClientDiscardBody function in client.c in Polipo 0.9.8, 0.9.12, 1.0.4, and possibly other versions, allows remote attackers to cause a denial of service (crash) via a request with a large Content-Length value, which triggers an integer overflow, a signed-to-unsigned conversion error with a negative value, and a segmentation fault.
CVE-2009-4414
SQL injection vulnerability in phpgwapi /inc/class.auth_sql.inc.php in phpGroupWare 0.9.16.12, and possibly other versions before 0.9.16.014, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the passwd parameter to login.php.
CVE-2009-4415
Multiple directory traversal vulnerabilities in phpGroupWare 0.9.16.12, and possibly other versions before 0.9.16.014, allow remote attackers to (1) read arbitrary files via the csvfile parameter to addressbook/csv_import.php, or (2) include and execute arbitrary local files via the conv_type parameter in addressbook/inc/class.uiXport.inc.php.
CVE-2009-4416
Cross-site scripting (XSS) vulnerability in login.php in phpGroupWare 0.9.16.12, and possibly other versions before 0.9.16.014, allows remote attackers to inject arbitrary web script or HTML via an arbitrary parameter whose name begins with the "phpgw_" sequence.
CVE-2009-4417
The shutdown function in the Zend_Log_Writer_Mail class in Zend Framework (ZF) allows context-dependent attackers to send arbitrary e-mail messages to any recipient address via vectors related to "events not yet mailed."
CVE-2009-4418
The unserialize function in PHP 5.3.0 and earlier allows context-dependent attackers to cause a denial of service (resource consumption) via a deeply nested serialized variable, as demonstrated by a string beginning with a:1: followed by many {a:1: sequences.
CVE-2009-4419
Intel Q35, GM45, PM45 Express, Q45, and Q43 Express chipsets in the SINIT Authenticated Code Module (ACM), which allows local users to bypass the Trusted Execution Technology protection mechanism and gain privileges by modifying the MCHBAR register to point to an attacker-controlled region, which prevents the SENTER instruction from properly applying VT-d protection while an MLE is being loaded.
CVE-2009-4420
Buffer overflow in the bd daemon in F5 Networks BIG-IP Application Security Manager (ASM) 9.4.4 through 9.4.7 and 10.0.0 through 10.0.1, and Protocol Security Manager (PSM) 9.4.5 through 9.4.7 and 10.0.0 through 10.0.1, allows remote attackers to cause a denial of service (crash) via unknown vectors.  NOTE: some of these details are obtained from third party information.
CVE-2009-4421
Directory traversal vulnerability in languages_cgi.php in Simple PHP Blog 0.5.1 and earlier allows remote authenticated users to include and execute arbitrary local files via a .. (dot dot) in the blog_language1 parameter.
CVE-2009-4422
Multiple cross-site scripting (XSS) vulnerabilities in the GetURLArguments function in jpgraph.php in Aditus Consulting JpGraph 3.0.6 allow remote attackers to inject arbitrary web script or HTML via a key to csim_in_html_ex1.php, and other unspecified vectors.
CVE-2009-4423
SQL injection vulnerability in index.php in weenCompany 4.0.0 allows remote attackers to execute arbitrary SQL commands via the moduleid parameter.  NOTE: some of these details are obtained from third party information.
