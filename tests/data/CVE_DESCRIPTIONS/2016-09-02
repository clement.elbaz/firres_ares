CVE-2016-0772
The smtplib library in CPython (aka Python) before 2.7.12, 3.x before 3.4.5, and 3.5.x before 3.5.2 does not return an error when StartTLS fails, which might allow man-in-the-middle attackers to bypass the TLS protections by leveraging a network position between the client and the registry to block the StartTLS command, aka a "StartTLS stripping attack."
CVE-2016-1470
Cross-site request forgery (CSRF) vulnerability in the web-based management interface on Cisco Small Business 220 devices with firmware before 1.0.1.1 allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCuz76230.
CVE-2016-1471
Cross-site scripting (XSS) vulnerability in the web-based management interface on Cisco Small Business 220 devices with firmware before 1.0.1.1 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuz76232.
CVE-2016-1472
The web-based management interface on Cisco Small Business 220 devices with firmware before 1.0.1.1 allows remote attackers to cause a denial of service (interface outage) via a crafted HTTP request, aka Bug ID CSCuz76238.
CVE-2016-1473
Cisco Small Business 220 devices with firmware before 1.0.1.1 have a hardcoded SNMP community, which allows remote attackers to read or modify SNMP objects by leveraging knowledge of this community, aka Bug ID CSCuz76216.
CVE-2016-4848
Cross-site scripting (XSS) vulnerability in ClipBucket before 2.8.1 RC2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-4851
Cross-site scripting (XSS) vulnerability in Let's PHP! simple chat before 2016-08-15 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-4853
AKABEi SOFT2 games allow remote attackers to execute arbitrary OS commands via crafted saved data, as demonstrated by Happy Wardrobe.
CVE-2016-4952
QEMU (aka Quick Emulator), when built with VMWARE PVSCSI paravirtual SCSI bus emulation support, allows local guest OS administrators to cause a denial of service (out-of-bounds array access) via vectors related to the (1) PVSCSI_CMD_SETUP_RINGS or (2) PVSCSI_CMD_SETUP_MSG_RING SCSI command.
CVE-2016-5105
The megasas_dcmd_cfg_read function in hw/scsi/megasas.c in QEMU, when built with MegaRAID SAS 8708EM2 Host Bus Adapter emulation support, uses an uninitialized variable, which allows local guest administrators to read host memory via vectors involving a MegaRAID Firmware Interface (MFI) command.
CVE-2016-5106
The megasas_dcmd_set_properties function in hw/scsi/megasas.c in QEMU, when built with MegaRAID SAS 8708EM2 Host Bus Adapter emulation support, allows local guest administrators to cause a denial of service (out-of-bounds write access) via vectors involving a MegaRAID Firmware Interface (MFI) command.
CVE-2016-5107
The megasas_lookup_frame function in QEMU, when built with MegaRAID SAS 8708EM2 Host Bus Adapter emulation support, allows local guest OS administrators to cause a denial of service (out-of-bounds read and crash) via unspecified vectors.
CVE-2016-5636
Integer overflow in the get_data function in zipimport.c in CPython (aka Python) before 2.7.12, 3.x before 3.4.5, and 3.5.x before 3.5.2 allows remote attackers to have unspecified impact via a negative data size value, which triggers a heap-based buffer overflow.
CVE-2016-5699
CRLF injection vulnerability in the HTTPConnection.putheader function in urllib2 and urllib in CPython (aka Python) before 2.7.10 and 3.x before 3.4.4 allows remote attackers to inject arbitrary HTTP headers via CRLF sequences in a URL.
CVE-2016-5879
MQCLI on IBM MQ Appliance M2000 and M2001 devices allows local users to execute arbitrary shell commands via a crafted (1) Disaster Recovery or (2) High Availability command.
CVE-2016-6376
The Adaptive Wireless Intrusion Prevention System (wIPS) feature on Cisco Wireless LAN Controller (WLC) devices before 8.0.140.0, 8.1.x and 8.2.x before 8.2.121.0, and 8.3.x before 8.3.102.0 allows remote attackers to cause a denial of service (device restart) via a malformed wIPS packet, aka Bug ID CSCuz40263.
CVE-2016-6483
The media-file upload feature in vBulletin before 3.8.7 Patch Level 6, 3.8.8 before Patch Level 2, 3.8.9 before Patch Level 1, 4.x before 4.2.2 Patch Level 6, 4.2.3 before Patch Level 2, 5.x before 5.2.0 Patch Level 3, 5.2.1 before Patch Level 1, and 5.2.2 before Patch Level 1 allows remote attackers to conduct SSRF attacks via a crafted URL that results in a Redirection HTTP status code.
CVE-2016-6893
Cross-site request forgery (CSRF) vulnerability in the user options page in GNU Mailman 2.1.x before 2.1.23 allows remote attackers to hijack the authentication of arbitrary users for requests that modify an option, as demonstrated by gaining access to the credentials of a victim's account.
CVE-2016-7123
Cross-site request forgery (CSRF) vulnerability in the admin web interface in GNU Mailman before 2.1.15 allows remote attackers to hijack the authentication of administrators.
