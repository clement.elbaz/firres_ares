CVE-2008-0169
Plugin/passwordauth.pm (aka the passwordauth plugin) in ikiwiki 1.34 through 2.47 allows remote attackers to bypass authentication, and login to any account for which an OpenID identity is configured and a password is not configured, by specifying an empty password during the login sequence.
CVE-2008-1035
Use-after-free vulnerability in Apple iCal 3.0.1 on Mac OS X allows remote CalDAV servers, and user-assisted remote attackers, to trigger memory corruption or possibly execute arbitrary code via an "ATTACH;VALUE=URI:S=osumi" line in a .ics file, which triggers a "resource liberation" bug.  NOTE: CVE-2008-2007 was originally used for this issue, but this is the appropriate identifier.
CVE-2008-2516
pam_sm_authenticate in pam_pgsql.c in libpam-pgsql 0.6.3 does not properly consider operator precedence when evaluating the success of a pam_get_pass function call, which allows local users to gain privileges via a SIGINT signal when this function is executing, as demonstrated by a CTRL-C sequence at a sudo password prompt in an "auth sufficient pam_pgsql.so" configuration.
CVE-2008-2517
The sarab.sh script in SaraB before 0.2.4 places the dar program's encryption key on the command line, which allows local users to obtain sensitive information by listing the process.
CVE-2008-2518
Cross-site scripting (XSS) vulnerability in the advanced search mechanism (webapps/search/advanced.jsp) in Sun Java System Web Server 6.1 before SP9 and 7.0 before Update 3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, probably related to the next parameter.
CVE-2008-2519
Directory traversal vulnerability in Core FTP client 2.1 Build 1565 allows remote FTP servers to create or overwrite arbitrary files via .. (dot dot) sequences in responses to LIST commands, a related issue to CVE-2002-1345.  NOTE: this can be leveraged for code execution by writing to a Startup folder.
CVE-2008-2520
Multiple PHP remote file inclusion vulnerabilities in BigACE 2.4, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the (1) GLOBALS[_BIGACE][DIR][addon] parameter to (a) addon/smarty/plugins/function.captcha.php and (b) system/classes/sql/AdoDBConnection.php; and the (2) GLOBALS[_BIGACE][DIR][admin] parameter to (c) item_information.php and (d) jstree.php in system/application/util/, and (e) system/admin/plugins/menu/menuTree/plugin.php, different vectors than CVE-2006-4423.
CVE-2008-2521
SQL injection vulnerability in members.php in YABSoft Mega File Hosting Script (aka MFH or MFHS) 1.2 allows remote authenticated users to execute arbitrary SQL commands via the fid parameter.
CVE-2008-2522
SQL injection vulnerability in members.php in Battle.net Clan Script for PHP 1.5.3 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the showmember parameter in a members action.
CVE-2008-2523
SQL injection vulnerability in the Autopatcher server plugin in RakNet before 3.23 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2008-2524
BlogPHP 2.0 allows remote attackers to bypass authentication, and post (1) messages or (2) comments as an arbitrary user, via a modified blogphp_username field in a cookie.
CVE-2008-2525
Cross-site scripting (XSS) vulnerability in the Event Database (aka rlmp_eventdb) extension before 1.1.2 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-2526
Cross-site scripting (XSS) vulnerability in the WT Gallery (aka wt_gallery) extension 2.6.2 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-2527
Cross-site scripting (XSS) vulnerability in view.php in ActualScripts ActualAnalyzer Server 8.37 and earlier, ActualAnalyzer Gold 7.74 and earlier, ActualAnalyzer Pro 6.95 and earlier, and ActualAnalyzer Lite 2.78 and earlier allows remote attackers to inject arbitrary web script or HTML via the language parameter.
CVE-2008-2528
Unspecified vulnerability in Citrix Access Gateway Standard Edition 4.5.7 and earlier and Advanced Edition 4.5 HF2 and earlier allows attackers to bypass authentication and gain "access to network resources" via unspecified vectors.
CVE-2008-2529
SQL injection vulnerability in read.php in Advanced Links Management (ALM) 1.5.2 allows remote attackers to execute arbitrary SQL commands via the catId parameter.
CVE-2008-2530
Multiple SQL injection vulnerabilities in Concepts & Solutions QuickUpCMS allow remote attackers to execute arbitrary SQL commands via the (1) nr parameter to (a) frontend/news.php, the (2) id parameter to (b) events3.php and (c) videos2.php in frontend/, the (3) y parameter to (d) frontend/events2.php, and the (4) ser parameter to (e) frontend/fotos2.php.
CVE-2008-2531
Cross-site scripting (XSS) vulnerability in the search script in Build A Niche Store (BANS) 3.0 allows remote attackers to inject arbitrary web script or HTML via the q parameter.
CVE-2008-2532
SQL injection vulnerability in forum/topic_detail.php in AJ Square aj-hyip (aka AJ HYIP Acme) allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-2533
Multiple cross-site scripting (XSS) vulnerabilities in Phoenix View CMS Pre Alpha2 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) ltarget parameter to (a) admin/admin_frame.php and the (2) conf parameter to (b) gbuch.admin.php, (c) links.admin.php, (d) menue.admin.php, (e) news.admin.php, and (f) todo.admin.php in admin/module/.
CVE-2008-2534
Directory traversal vulnerability in admin/admin_frame.php in Phoenix View CMS Pre Alpha2 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the ltarget parameter.
CVE-2008-2535
Multiple SQL injection vulnerabilities in Phoenix View CMS Pre Alpha2 and earlier allow remote attackers to execute arbitrary SQL commands via the del parameter to (1) gbuch.admin.php, (2) links.admin.php, (3) menue.admin.php, (4) news.admin.php, and (5) todo.admin.php in admin/module/.
CVE-2008-2536
SQL injection vulnerability in out.php in YABSoft Advanced Image Hosting (AIH) Script 2.1 and earlier allows remote attackers to execute arbitrary SQL commands via the t parameter.
CVE-2008-2537
SQL injection vulnerability in cat.php in HispaH Model Search allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2008-2538
Unspecified vulnerability in crontab on Sun Solaris 8 through 10, and OpenSolaris before snv_93, allows local users to insert cron jobs into the crontab files of arbitrary users via unspecified vectors.
CVE-2008-2539
The Sun Cluster Global File System in Sun Cluster 3.1 on Sun Solaris 8 through 10, when an underlying ufs filesystem is used, might allow local users to read data from arbitrary deleted files, or corrupt files in global filesystems, via unspecified vectors.
CVE-2008-2540
Apple Safari on Mac OS X, and before 3.1.2 on Windows, does not prompt the user before downloading an object that has an unrecognized content type, which allows remote attackers to place malware into the (1) Desktop directory on Windows or (2) Downloads directory on Mac OS X, and subsequently allows remote attackers to execute arbitrary code on Windows by leveraging an untrusted search path vulnerability in (a) Internet Explorer 7 on Windows XP or (b) the SearchPath function in Windows XP, Vista, and Server 2003 and 2008, aka a "Carpet Bomb" and a "Blended Threat Elevation of Privilege Vulnerability," a different issue than CVE-2008-1032. NOTE: Apple considers this a vulnerability only because the Microsoft products can load application libraries from the desktop and, as of 20080619, has not covered the issue in an advisory for Mac OS X.
