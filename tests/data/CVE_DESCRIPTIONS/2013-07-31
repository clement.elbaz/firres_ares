CVE-2013-0943
EMC NetWorker 7.6.x and 8.x before 8.1 allows local users to obtain sensitive configuration information by leveraging operating-system privileges to perform decryption with nsradmin.
CVE-2013-1377
Adobe Digital Editions 2.x before 2.0.1 allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2013-1968
Subversion before 1.6.23 and 1.7.x before 1.7.10 allows remote authenticated users to cause a denial of service (FSFS repository corruption) via a newline character in a file name.
CVE-2013-2056
The Inter-Satellite Sync (ISS) operation in Red Hat Network (RHN) Satellite 5.3, 5.4, and 5.5 does not properly check client "authenticity," which allows remote attackers to obtain channel content by skipping the initial authentication call.
CVE-2013-2088
contrib/hook-scripts/svn-keyword-check.pl in Subversion before 1.6.23 allows remote authenticated users with commit permissions to execute arbitrary commands via shell metacharacters in a filename.
CVE-2013-2112
The svnserve server in Subversion before 1.6.23 and 1.7.x before 1.7.10 allows remote attackers to cause a denial of service (exit) by aborting a connection.
CVE-2013-2113
The create method in app/controllers/users_controller.rb in Foreman before 1.2.0-RC2 allows remote authenticated users with permissions to create or edit other users to gain privileges by (1) changing the admin flag or (2) assigning an arbitrary role.
CVE-2013-2121
Eval injection vulnerability in the create method in the Bookmarks controller in Foreman before 1.2.0-RC2 allows remote authenticated users with permissions to create bookmarks to execute arbitrary code via a controller name attribute.
CVE-2013-2174
Heap-based buffer overflow in the curl_easy_unescape function in lib/escape.c in cURL and libcurl 7.7 through 7.30.0 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted string ending in a "%" (percent) character.
CVE-2013-2189
Apache OpenOffice.org (OOo) before 4.0 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via invalid PLCF data in a DOC document file.
CVE-2013-2209
Cross-site scripting (XSS) vulnerability in the auto-complete widget in htdocs/media/rb/js/reviews.js in Review Board 1.6.x before 1.6.17 and 1.7.x before 1.7.10 allows remote attackers to inject arbitrary web script or HTML via a full name.
CVE-2013-2219
The Red Hat Directory Server before 8.2.11-13 and 389 Directory Server do not properly restrict access to entity attributes, which allows remote authenticated users to obtain sensitive information via a search query for the attribute.
CVE-2013-2220
Buffer overflow in the radius_get_vendor_attr function in the Radius extension before 1.2.7 for PHP allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large Vendor Specific Attributes (VSA) length value.
CVE-2013-2367
Multiple unspecified vulnerabilities in HP SiteScope 11.20 and 11.21, when SOAP is used, allow remote attackers to execute arbitrary code via unknown vectors, aka ZDI-CAN-1678.
CVE-2013-2630
Cross-site scripting (XSS) vulnerability in CA Service Desk Manager 12.5 through 12.7 allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2013-2785
Multiple buffer overflows in CimWebServer.exe in the WebView component in GE Intelligent Platforms Proficy HMI/SCADA - CIMPLICITY before 8.0 SIM 27, 8.1 before SIM 25, and 8.2 before SIM 19, and Proficy Process Systems with CIMPLICITY, allow remote attackers to execute arbitrary code via crafted data in packets to TCP port 10212, aka ZDI-CAN-1621 and ZDI-CAN-1624.
CVE-2013-2881
Google Chrome before 28.0.1500.95 does not properly handle frames, which allows remote attackers to bypass the Same Origin Policy via a crafted web site.
CVE-2013-2882
Google V8, as used in Google Chrome before 28.0.1500.95, allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors that leverage "type confusion."
CVE-2013-2883
Use-after-free vulnerability in Google Chrome before 28.0.1500.95 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to deleting the registration of a MutationObserver object.
CVE-2013-2884
Use-after-free vulnerability in the DOM implementation in Google Chrome before 28.0.1500.95 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to improper tracking of which document owns an Attr object.
CVE-2013-2885
Use-after-free vulnerability in Google Chrome before 28.0.1500.95 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to not properly considering focus during the processing of JavaScript events in the presence of a multiple-fields input type.
CVE-2013-2886
Multiple unspecified vulnerabilities in Google Chrome before 28.0.1500.95 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2013-3425
The Meeting Center component in Cisco WebEx 11 generates different error messages for invalid file-access attempts depending on whether a file exists, which allows remote authenticated users to enumerate files via a series of SPI calls, aka Bug ID CSCuc35965.
CVE-2013-3697
Integer overflow in the NWFS.SYS kernel driver 4.91.5.8 in Novell Client 4.91 SP5 on Windows XP and Windows Server 2003 and the NCPL.SYS kernel driver in Novell Client 2 SP2 on Windows Vista and Windows Server 2008 and Novell Client 2 SP3 on Windows Server 2008 R2, Windows 7, Windows 8, and Windows Server 2012 might allow local users to gain privileges via a crafted 0x1439EB IOCTL call.
CVE-2013-3956
The NICM.SYS kernel driver 3.1.11.0 in Novell Client 4.91 SP5 on Windows XP and Windows Server 2003; Novell Client 2 SP2 on Windows Vista and Windows Server 2008; and Novell Client 2 SP3 on Windows Server 2008 R2, Windows 7, Windows 8, and Windows Server 2012 allows local users to gain privileges via a crafted 0x143B6B IOCTL call.
CVE-2013-4131
The mod_dav_svn Apache HTTPD server module in Subversion 1.7.0 through 1.7.10 and 1.8.x before 1.8.1 allows remote authenticated users to cause a denial of service (assertion failure or out-of-bounds read) via a certain (1) COPY, (2) DELETE, or (3) MOVE request against a revision root.
CVE-2013-4156
Apache OpenOffice.org (OOo) before 4.0 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via a crafted element in an OOXML document file.
CVE-2013-4674
Cross-site scripting (XSS) vulnerability in the Web Email Protection component in Symantec Encryption Management Server (formerly Symantec PGP Universal Server) before 3.3.0 MP2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted encrypted e-mail attachment.
CVE-2013-4697
Multiple unspecified vulnerabilities in Hitachi JP1/IT Desktop Management - Manager 09-50 through 09-50-03, 09-51 through 09-51-05, 10-00 through 10-00-02, and 10-01 through 10-01-02; Hitachi Job Management Partner 1/IT Desktop Management - Manager 09-50 through 09-50-03 and 10-01; and Hitachi IT Operations Director 02-50 through 02-50-07, 03-00 through 03-00-12, and 04-00 through 04-00-01 allow remote authenticated users to gain privileges via unknown vectors.
CVE-2013-4995
Cross-site scripting (XSS) vulnerability in phpMyAdmin 3.5.x before 3.5.8.2 and 4.0.x before 4.0.4.2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted SQL query that is not properly handled during the display of row information.
CVE-2013-4996
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 3.5.x before 3.5.8.2 and 4.0.x before 4.0.4.2 allow remote attackers to inject arbitrary web script or HTML via vectors involving (1) a crafted database name, (2) a crafted user name, (3) a crafted logo URL in the navigation panel, (4) a crafted entry in a certain proxy list, or (5) crafted content in a version.json file.
CVE-2013-4997
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 3.5.x before 3.5.8.2 allow remote attackers to inject arbitrary web script or HTML via vectors involving a JavaScript event in (1) an anchor identifier to setup/index.php or (2) a chartTitle (aka chart title) value.
CVE-2013-4998
phpMyAdmin 3.5.x before 3.5.8.2 and 4.0.x before 4.0.4.2 allows remote attackers to obtain sensitive information via an invalid request, which reveals the installation path in an error message, related to pmd_common.php and other files.
CVE-2013-4999
phpMyAdmin 4.0.x before 4.0.4.2 allows remote attackers to obtain sensitive information via an invalid request, which reveals the installation path in an error message, related to Error.class.php and Error_Handler.class.php.
CVE-2013-5000
phpMyAdmin 3.5.x before 3.5.8.2 allows remote attackers to obtain sensitive information via an invalid request, which reveals the installation path in an error message, related to config.default.php and other files.
CVE-2013-5001
Cross-site scripting (XSS) vulnerability in libraries/plugins/transformations/abstract/TextLinkTransformationsPlugin.class.php in phpMyAdmin 4.0.x before 4.0.4.2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted object name associated with a TextLinkTransformationPlugin link.
CVE-2013-5002
Cross-site scripting (XSS) vulnerability in libraries/schema/Export_Relation_Schema.class.php in phpMyAdmin 3.5.x before 3.5.8.2 and 4.0.x before 4.0.4.2 allows remote authenticated users to inject arbitrary web script or HTML via a crafted pageNumber value to schema_export.php.
CVE-2013-5003
Multiple SQL injection vulnerabilities in phpMyAdmin 3.5.x before 3.5.8.2 and 4.0.x before 4.0.4.2 allow remote authenticated users to execute arbitrary SQL commands via (1) the scale parameter to pmd_pdf.php or (2) the pdf_page_number parameter to schema_export.php.
CVE-2013-5006
main_internet.php on the Western Digital My Net N600 and N750 with firmware 1.03.12 and 1.04.16, and the N900 and N900C with firmware 1.05.12, 1.06.18, and 1.06.28, allows remote attackers to discover the cleartext administrative password by reading the "var pass=" line within the HTML source code.
CVE-2013-5019
Stack-based buffer overflow in Ultra Mini HTTPD 1.21 allows remote attackers to execute arbitrary code via a long resource name in an HTTP request.
CVE-2013-5020
Multiple cross-site scripting (XSS) vulnerabilities in bb_admin.php in MiniBB before 3.0.1 allow remote attackers to inject arbitrary web script or HTML via the (1) forum_name, (2) forum_group, (3) forum_icon, or (4) forum_desc parameter.  NOTE: the whatus vector is already covered by CVE-2008-2066.
