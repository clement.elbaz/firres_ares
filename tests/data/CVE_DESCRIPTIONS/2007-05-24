CVE-2007-0448
The fopen function in PHP 5.2.0 does not properly handle invalid URI handlers, which allows context-dependent attackers to bypass safe_mode restrictions and read arbitrary files via a file path specified with an invalid URI, as demonstrated via the srpath URI.
CVE-2007-0740
Alias Manager in Apple Mac OS X 10.3.9 and 10.4.9 does not display files with the same name in mounted disk images that have the same name, which might allow user-assisted attackers to trick a user into executing malicious files.
CVE-2007-0750
Integer overflow in CoreGraphics in Apple Mac OS X 10.4 up to 10.4.9 allows remote user-assisted attackers to cause a denial of service (application termination) or execute arbitrary code via a crafted PDF file.
CVE-2007-0751
A cleanup script in crontabs in Apple Mac OS X 10.3.9 and 10.4.9 might delete filesystems that have been mounted in /tmp, which might allow local users to cause a denial of service, related to the find command.
CVE-2007-0752
The PPP daemon (pppd) in Apple Mac OS X 10.4.8 checks ownership of the stdin file descriptor to determine if the invoker has sufficient privileges, which allows local users to load arbitrary plugins and gain root privileges by bypassing this check.
CVE-2007-0753
Format string vulnerability in the VPN daemon (vpnd) in Apple Mac OS X 10.3.9 and 10.4.9 allows local users to execute arbitrary code via the -i parameter.
CVE-2007-2386
Buffer overflow in mDNSResponder in Apple Mac OS X 10.4 up to 10.4.9 allows remote attackers to cause a denial of service (application termination) or execute arbitrary code via a crafted UPnP Internet Gateway Device (IGD) packet.
CVE-2007-2390
Buffer overflow in iChat in Apple Mac OS X 10.3.9 and 10.4.9 allows remote attackers to cause a denial of service (application termination) and possibly execute arbitrary code via a crafted UPnP Internet Gateway Device (IGD) packet.
CVE-2007-2687
Stack-based buffer overflow in the MicroWorld Agent service (MWAGENT.EXE) in MicroWorld Technologies eScan before 9.0.718.1 allows remote attackers to execute arbitrary code via a long command.
The vendor has addressed this issue with the following product update:
http://www.mwti.net/support/microworld_support.asp 

CVE-2007-2829
The 802.11 network stack in net80211/ieee80211_input.c in MadWifi before 0.9.3.1 allows remote attackers to cause a denial of service (system hang) via a crafted length field in nested 802.3 Ethernet frames in Fast Frame packets, which results in a NULL pointer dereference.
CVE-2007-2830
The ath_beacon_config function in if_ath.c in MadWifi before 0.9.3.1 allows remote attackers to cause a denial of service (system crash) via crafted beacon interval information when scanning for access points, which triggers a divide-by-zero error.
CVE-2007-2831
Array index error in the (1) ieee80211_ioctl_getwmmparams and (2) ieee80211_ioctl_setwmmparams functions in net80211/ieee80211_wireless.c in MadWifi before 0.9.3.1 allows local users to cause a denial of service (system crash), possibly obtain kernel memory contents, and possibly execute arbitrary code via a large negative array index value.
CVE-2007-2832
Cross-site scripting (XSS) vulnerability in the web application firewall in Cisco CallManager before 3.3(5)sr3, 4.1 before 4.1(3)sr5, 4.2 before 4.2(3)sr2, and 4.3 before 4.3(1)sr1 allows remote attackers to inject arbitrary web script or HTML via the pattern parameter to CCMAdmin/serverlist.asp (aka the search-form) and possibly other unspecified vectors.
CVE-2007-2843
Cross-domain vulnerability in Apple Safari 2.0.4 allows remote attackers to access restricted information from other domains via Javascript, as demonstrated by a js script that accesses the location information of cross-domain web pages, probably involving setTimeout and timed events.
CVE-2007-2844
PHP 4.x and 5.x before 5.2.1, when running on multi-threaded systems, does not ensure thread safety for libc crypt function calls using protection schemes such as a mutex, which creates race conditions that allow remote attackers to overwrite internal program memory and gain system access.
CVE-2007-2845
Heap-based buffer overflow in the CAB unpacker in avast! Anti-Virus Managed Client before 4.7.700 allows user-assisted remote attackers to execute arbitrary code via a crafted CAB archive, resulting from an "integer cast around".
CVE-2007-2846
Heap-based buffer overflow in the SIS unpacker in avast! Anti-Virus Managed Client before 4.7.700 allows user-assisted remote attackers to execute arbitrary code via a crafted SIS archive, resulting from an "integer cast around."
CVE-2007-2847
Multiple cross-site scripting (XSS) vulnerabilities in hlstats.php in HLstats 1.35, and possibly earlier, allow remote attackers to inject arbitrary web script or HTML via the (1) authusername or (2) authpassword parameter, different vectors than CVE-2007-0840 and CVE-2007-2812.
CVE-2007-2848
Stack-based buffer overflow in the SetPath function in the shComboBox ActiveX control (shcmb80.ocx) in Sky Software Shell MegaPack ActiveX 8.0 allows remote attackers to execute arbitrary code via a long argument.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2849
KnowledgeTree Document Management (aka KnowledgeTree Open Source) before STABLE 3.3.7 does not require a password for an unregistered user, when the user exists in Active Directory, which allows remote attackers to log onto KTDMS without the intended authorization check.
CVE-2007-2850
The Session Reliability Service (XTE) in Citrix MetaFrame Presentation Server 3.0, Presentation Server 4.0, and Access Essentials 1.0 and 1.5, allows remote attackers to bypass network security policies and connect to arbitrary TCP ports via a modified address:port string.
The vendor has addressed this issue with the following product updates:

MetaFrame Presentation Server 3.0 for Windows 2000 Server:
EN - http://support.citrix.com/article/CTX112818
FR - http://support.citrix.com/article/CTX112821
DE - http://support.citrix.com/article/CTX112819
JA - http://support.citrix.com/article/CTX112820
ES - http://support.citrix.com/article/CTX112822

MetaFrame Presentation Server 3.0 for Windows Server 2003:
EN - http://support.citrix.com/article/CTX112813
FR - http://support.citrix.com/article/CTX112816
DE - http://support.citrix.com/article/CTX112814
JA - http://support.citrix.com/article/CTX112815
ES - http://support.citrix.com/article/CTX112817

Citrix Presentation Server 4.0 for Windows 2000 Server:
EN - http://support.citrix.com/article/CTX112844
FR - http://support.citrix.com/article/CTX112847
DE - http://support.citrix.com/article/CTX112845
JA - http://support.citrix.com/article/CTX112848
ES - http://support.citrix.com/article/CTX112846

Citrix Presentation Server 4.0 for Windows Server 2003:
EN - http://support.citrix.com/article/CTX112839
FR - http://support.citrix.com/article/CTX112842
DE - http://support.citrix.com/article/CTX112840
JA - http://support.citrix.com/article/CTX112843
ES - http://support.citrix.com/article/CTX112841

Citrix Presentation Server 4.0 for Windows Server 2003 x64 Editions:
EN - http://support.citrix.com/article/CTX112886
FR - http://support.citrix.com/article/CTX112887
DE - http://support.citrix.com/article/CTX112888
JA - http://support.citrix.com/article/CTX112890
ES - http://support.citrix.com/article/CTX112889

Citrix Access Essentials 1.0:
EN - http://support.citrix.com/article/CTX112839
FR - http://support.citrix.com/article/CTX112842
DE - http://support.citrix.com/article/CTX112840
ES - http://support.citrix.com/article/CTX112841

Citrix Access Essentials 1.5:
EN - http://support.citrix.com/article/CTX112839
FR - http://support.citrix.com/article/CTX112842
DE - http://support.citrix.com/article/CTX112840
ES - http://support.citrix.com/article/CTX112841

CVE-2007-2851
A certain ActiveX control in LeadTools Raster Variant Object Library (LTRVR14e.dll) 14.5.0.44 allows remote attackers to overwrite arbitrary files via the WriteDataToFile method.
CVE-2007-2852
Multiple stack-based buffer overflows in ESET NOD32 Antivirus before 2.70.37.0 allow remote attackers to execute arbitrary code during (1) delete/disinfect or (2) rename operations via a crafted directory name.
CVE-2007-2853
The VCDAPILibApi ActiveX control in vc9api.DLL 9.0.0.57 in Virtual CD 9.0.0.2 allows remote attackers to execute arbitrary commands via a command line in the first argument to the VCDLaunchAndWait function.
CVE-2007-2854
Multiple SQL injection vulnerabilities in account_change.php in BtiTracker 1.4.1 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) style or (2) langue parameter.
CVE-2007-2855
Buffer overflow in a certain ActiveX control in DartZipLite.dll 1.8.5.3 in Dart ZipLite Compression for ActiveX allows user-assisted remote attackers to execute arbitrary code via a long first argument to the QuickZip function, a related issue to CVE-2007-2856.
CVE-2007-2856
Buffer overflow in the Dart Communications PowerTCP ZIP Compression ActiveX control in DartZip.dll 1.8.5.3, when Internet Explorer 6 is used, allows user-assisted remote attackers to execute arbitrary code via a long first argument to the QuickZip function, a related issue to CVE-2007-2855.
CVE-2007-2857
PHP remote file inclusion vulnerability in sample/xls2mysql in ABC Excel Parser Pro 4.0 allows remote attackers to execute arbitrary PHP code via a URL in the parser_path parameter.
CVE-2007-2858
SQL injection vulnerability in the IP-Search functionality in the IP-Tracking Mod for phpBB 2.0.x allows remote authenticated administrators to execute arbitrary SQL commands via the Search Query field.
CVE-2007-2859
Multiple PHP remote file inclusion vulnerabilities in SimpGB 1.46.0 allow remote attackers to execute arbitrary PHP code via a URL in the path_simpgb parameter to (1) guestbook.php, (2) search.php, (3) mailer.php, (4) avatars.php, (5) ccode.php, (6) comments.php, (7) emoticons.php, (8) gbdownload.php, and possibly other PHP scripts.
CVE-2007-2860
user.php in BoastMachine 3.0 platinum allows remote authenticated users to gain privileges via a modified id parameter, as demonstrated by an edit_post action.
CVE-2007-2861
Multiple PHP remote file inclusion vulnerabilities in Simple Accessible XHTML Online News (SAXON) 4.6 allow remote attackers to execute arbitrary PHP code via a URL in the template parameter to (1) news.php, (2) preview.php, or (3) archive-display.php.
CVE-2007-2862
Multiple SQL injection vulnerabilities in CubeCart 3.0.16 might allow remote attackers to execute arbitrary SQL commands via an unspecified parameter to cart.inc.php and certain other files in an include directory, related to missing sanitization of the $option variable and possibly cookie modification.
