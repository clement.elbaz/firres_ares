CVE-2013-1057
Untrusted search path vulnerability in maas-import-pxe-files in MAAS before 13.10 allows local users to execute arbitrary code via a Trojan horse import_pxe_files configuration file in the current working directory.
CVE-2013-1418
The setup_server_realm function in main.c in the Key Distribution Center (KDC) in MIT Kerberos 5 (aka krb5) before 1.10.7, when multiple realms are configured, allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via a crafted request.
CWE-476: NULL Pointer Dereference per http://cwe.mitre.org/data/definitions/476.html
CVE-2013-1741
Integer overflow in Mozilla Network Security Services (NSS) 3.15 before 3.15.3 allows remote attackers to cause a denial of service or possibly have unspecified other impact via a large size value.
CVE-2013-2031
MediaWiki before 1.19.6 and 1.20.x before 1.20.5 allows remote attackers to conduct cross-site scripting (XSS) attacks, as demonstrated by a CDATA section containing valid UTF-7 encoded sequences in a SVG file, which is then incorrectly interpreted as UTF-8 by Chrome and Firefox.
CVE-2013-2032
MediaWiki before 1.19.6 and 1.20.x before 1.20.5 does not allow extensions to prevent password changes without using both Special:PasswordReset and Special:ChangePassword, which allows remote attackers to bypass the intended restrictions of an extension that only implements one of these blocks.
CVE-2013-2061
The openvpn_decrypt function in crypto.c in OpenVPN 2.3.0 and earlier, when running in UDP mode, allows remote attackers to obtain sensitive information via a timing attack involving an HMAC comparison function that does not run in constant time and a padding oracle attack on the CBC mode cipher.
CVE-2013-2114
Unrestricted file upload vulnerability in the chunk upload API in MediaWiki 1.19 through 1.19.6 and 1.20.x before 1.20.6 allows remote attackers to execute arbitrary code by uploading a file with an executable extension.
CWE-434: Unrestricted Upload of File with Dangerous Type per http://cwe.mitre.org/data/definitions/434.html
CVE-2013-3030
The servlet gateway in IBM Cognos Business Intelligence 8.4.1 before IF3, 10.1.0 before IF4, 10.1.1 before IF4, 10.2.0 before IF4, 10.2.1 before IF2, and 10.2.1.1 before IF1 allows remote attackers to cause a denial of service (temporary gateway outage) via crafted HTTP requests.
CVE-2013-3406
The "Files Available for Download" implementation in the Cisco Intelligent Automation for Cloud component in Cisco Services Portal 9.4(1) allows remote authenticated users to read arbitrary files via a crafted request, aka Bug ID CSCug65687.
CVE-2013-3407
The web interface in Cisco Server Provisioner 6.4.0 Patch 5-1301292331 and earlier does not require authentication for unspecified pages, which allows remote attackers to obtain sensitive information via a direct request, aka Bug ID CSCug65664.
CVE-2013-3694
BlackBerry Link before 1.2.1.31 on Windows and before 1.1.1 build 39 on Mac OS X does not require authentication for remote file-access folders, which allows remote attackers to read or create arbitrary files via IPv6 WebDAV requests, as demonstrated by a CSRF attack involving DNS rebinding.
CVE-2013-3876
DirectAccess in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly verify server X.509 certificates, which allows man-in-the-middle attackers to spoof servers and read encrypted domain credentials via a crafted certificate.
CVE-2013-4006
IBM WebSphere Application Server (WAS) Liberty Profile 8.5 before 8.5.5.1 uses weak permissions for unspecified files, which allows local users to obtain sensitive information via standard filesystem operations.
CVE-2013-4034
IBM Cognos Business Intelligence 8.4.1 before IF3, 10.1.0 before IF4, 10.1.1 before IF4, 10.2.0 before IF4, 10.2.1 before IF2, and 10.2.1.1 before IF1 allows remote authenticated users to read arbitrary files via an XML external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2013-4204
Multiple cross-site scripting (XSS) vulnerabilities in the JUnit files in the GWTTestCase in Google Web Toolkit (GWT) before 2.5.1 RC1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-4425
The DICOM listener in OsiriX before 5.8 and before 2.5-MD, when starting up, encrypts the TLS private key file using "SuperSecretPassword" as the hardcoded password, which allows local users to obtain the private key.
According to several reference links Osirix MD before 2.8 are vulnerable

http://www.securityfocus.com/bid/63566/discuss

http://archives.neohapsis.com/archives/bugtraq/2013-11/0029.html
CVE-2013-4480
Red Hat Satellite 5.6 and earlier does not disable the web interface that is used to create the first user for a satellite, which allows remote attackers to create administrator accounts.
CVE-2013-4510
Directory traversal vulnerability in the client in Tryton 3.0.0, as distributed before 20131104 and earlier, allows remote servers to write arbitrary files via path separators in the extension of a report.
CVE-2013-4551
Xen 4.2.x and 4.3.x, when nested virtualization is disabled, does not properly check the emulation paths for (1) VMLAUNCH and (2) VMRESUME, which allows local HVM guest users to cause a denial of service (host crash) via unspecified vectors related to "guest VMX instruction execution."
CVE-2013-4555
Cross-site request forgery (CSRF) vulnerability in ecrire/action/logout.php in SPIP before 2.1.24 allows remote attackers to hijack the authentication of arbitrary users for requests that logout the user via unspecified vectors.
CVE-2013-4556
Cross-site scripting (XSS) vulnerability in the author page (prive/formulaires/editer_auteur.php) in SPIP before 2.1.24 and 3.0.x before 3.0.12 allows remote attackers to inject arbitrary web script or HTML via the url_site parameter.
CVE-2013-4557
The Security Screen (_core_/securite/ecran_securite.php) before 1.1.8 for SPIP, as used in SPIP 3.0.x before 3.0.12, allows remote attackers to execute arbitrary PHP via the connect parameter.
CVE-2013-4842
Cross-site scripting (XSS) vulnerability in HP Integrated Lights-Out 4 (iLO4) with firmware before 1.32 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-4843
Unspecified vulnerability in HP Integrated Lights-Out 4 (iLO4) with firmware before 1.32 allows remote authenticated users to obtain sensitive information via unknown vectors.
CVE-2013-5193
The App Store component in Apple iOS before 7.0.4 does not properly enforce an intended transaction-time password requirement, which allows local users to complete a (1) App purchase or (2) In-App purchase by leveraging previous entry of Apple ID credentials.
CVE-2013-5414
The migration functionality in IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.31, 8.0 before 8.0.0.8, and 8.5 before 8.5.5.1 does not properly support the distinction between the admin role and the adminsecmanager role, which allows remote authenticated users to gain privileges in opportunistic circumstances by accessing resources in between a migration and a role evaluation.
CVE-2013-5417
Cross-site scripting (XSS) vulnerability in IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.31, 8.0 before 8.0.0.8, and 8.5 before 8.5.5.1 allows remote attackers to inject arbitrary web script or HTML via HTTP response data.
CVE-2013-5418
Cross-site scripting (XSS) vulnerability in the Administrative console in IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.31, 8.0 before 8.0.0.8, and 8.5 before 8.5.5.1 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2013-5425
Cross-site scripting (XSS) vulnerability in the Administration Console in IBM WebSphere Virtual Enterprise 6.1 before 6.1.1.6 and 7.0 before 7.0.0.4 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2013-5454
IBM WebSphere Portal 6.0 through 6.0.1.7, 6.1.0 through 6.1.0.6 CF27, 6.1.5 through 6.1.5.3 CF27, 7.0 through 7.0.0.2 CF25, and 8.0 through 8.0.0.1 CF08 allows remote attackers to read arbitrary files via a modified URL.
CVE-2013-5556
The license-installation module on the Cisco Nexus 1000V switch 4.2(1)SV1(5.2b) and earlier for VMware vSphere, Cisco Nexus 1000V switch 5.2(1)SM1(5.1) for Microsoft Hyper-V, and Cisco Virtual Security Gateway 4.2(1)VSG1(1) for Nexus 1000V switches allows local users to gain privileges and execute arbitrary commands via crafted "install all iso" arguments, aka Bug ID CSCui21340.
CVE-2013-5605
Mozilla Network Security Services (NSS) 3.14 before 3.14.5 and 3.15 before 3.15.3 allows remote attackers to cause a denial of service or possibly have unspecified other impact via invalid handshake packets.
CVE-2013-5606
The CERT_VerifyCert function in lib/certhigh/certvfy.c in Mozilla Network Security Services (NSS) 3.15 before 3.15.3 provides an unexpected return value for an incompatible key-usage certificate when the CERTVerifyLog argument is valid, which might allow remote attackers to bypass intended access restrictions via a crafted certificate.
CVE-2013-5972
VMware Workstation 9.x before 9.0.3 and VMware Player 5.x before 5.0.3 on Linux do not properly handle shared libraries, which allows host OS users to gain host OS privileges via unspecified vectors.
CVE-2013-6632
Integer overflow in Google Chrome before 31.0.1650.57 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, as demonstrated during a Mobile Pwn2Own competition at PacSec 2013.
CVE-2013-6686
The SSL VPN implementation in Cisco IOS 15.3(1)T2 and earlier allows remote authenticated users to cause a denial of service (interface queue wedge) via crafted DTLS packets in an SSL session, aka Bug IDs CSCuh97409 and CSCud90568.
CVE-2013-6688
Directory traversal vulnerability in the license-upload interface in the Enterprise License Manager (ELM) component in Cisco Unified Communications Manager 9.1(1) and earlier allows remote authenticated users to create arbitrary files via a crafted path, aka Bug ID CSCui58222.
CVE-2013-6689
Cisco Unified Communications Manager (Unified CM) 9.1(1) and earlier allows local users to bypass file permissions, and read, modify, or create arbitrary files, via an "overload" of the command-line utility, aka Bug ID CSCui58229.
CVE-2013-6798
BlackBerry Link before 1.2.1.31 on Windows and before 1.1.1 build 39 on Mac OS X does not properly determine the user account for execution of Peer Manager in certain situations involving successive logins with different accounts, which allows context-dependent attackers to bypass intended restrictions on remote file-access folders via IPv6 WebDAV requests, a different vulnerability than CVE-2013-3694.
CVE-2013-6799
Apple Mac OS X 10.9 allows local users to cause a denial of service (memory corruption or panic) by creating a hard link to a directory. NOTE: this vulnerability exists because of an incomplete fix for CVE-2010-0105.
CVE-2013-6800
An unspecified third-party database module for the Key Distribution Center (KDC) in MIT Kerberos 5 (aka krb5) 1.10.x allows remote authenticated users to cause a denial of service (NULL pointer dereference and daemon crash) via a crafted request, a different vulnerability than CVE-2013-1418.
CWE-476: NULL Pointer Dereference per http://cwe.mitre.org/data/definitions/476.html
CVE-2013-6801
Microsoft Word 2003 SP2 and SP3 on Windows XP SP3 allows remote attackers to cause a denial of service (CPU consumption) via a malformed .doc file containing an embedded image, as demonstrated by word2003forkbomb.doc, related to a "fork bomb" issue.
CVE-2013-6802
Google Chrome before 31.0.1650.57 allows remote attackers to bypass intended sandbox restrictions by leveraging access to a renderer process, as demonstrated during a Mobile Pwn2Own competition at PacSec 2013, a different vulnerability than CVE-2013-6632.
