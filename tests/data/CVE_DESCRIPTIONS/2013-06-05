CVE-2013-0508
Multiple buffer overflows in IBM Tivoli Netcool System Service Monitors (SSM) and Application Service Monitors (ASM) 4.0.0 before FP14 and 4.0.1 before FP1 allow context-dependent attackers to execute arbitrary code or cause a denial of service via a long line in (1) hrfstable.idx, (2) hrdevice.idx, (3) hrstorage.idx, or (4) lotusmapfile in the SSM Config directory, or (5) .manifest.hive in the main agent directory.
CVE-2013-0509
Buffer overflow in the Transaction MIB agent in IBM Tivoli Netcool System Service Monitors (SSM) and Application Service Monitors (ASM) 4.0.0 before FP14 allows remote attackers to execute arbitrary code via a SQL transaction with a long table name that is not properly handled by a packet decoder.
CVE-2013-0975
Buffer overflow in QuickDraw Manager in Apple Mac OS X before 10.8.4 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PICT image.
CVE-2013-0982
The Private Browsing feature in CFNetwork in Apple Mac OS X before 10.8.4 does not prevent storage of permanent cookies upon exit from Safari, which might allow physically proximate attackers to bypass cookie-based authentication by leveraging an unattended workstation.
CVE-2013-0983
Stack consumption vulnerability in CoreAnimation in Apple Mac OS X before 10.8.4 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted text glyph in a URL encountered by Safari.
CVE-2013-0984
Directory Service in Apple Mac OS X through 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (daemon crash) via a crafted message.
CVE-2013-0985
Disk Management in Apple Mac OS X before 10.8.4 does not properly authenticate attempts to disable FileVault, which allows local users to cause a denial of service (loss of encryption functionality) via an unspecified command line.
CVE-2013-0990
SMB in Apple Mac OS X before 10.8.4, when file sharing is enabled, allows remote authenticated users to create or modify files outside of a shared directory via unspecified vectors.
CVE-2013-1009
WebKit, as used in Apple Safari before 6.0.5, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site, a different vulnerability than CVE-2013-1023.
CVE-2013-1012
Cross-site scripting (XSS) vulnerability in WebKit in Apple Safari before 6.0.5 allows remote attackers to inject arbitrary web script or HTML via vectors involving IFRAME elements.
CVE-2013-1013
XSS Auditor in WebKit in Apple Safari before 6.0.5 does not properly rewrite URLs, which allows remote attackers to trigger unintended form submissions via unspecified vectors.
CVE-2013-1023
WebKit, as used in Apple Safari before 6.0.5, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site, a different vulnerability than CVE-2013-1009.
CVE-2013-1024
CoreMedia Playback in Apple Mac OS X before 10.8.4 does not properly initialize memory during the processing of text tracks, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file.
CVE-2013-2854
Google Chrome before 27.0.1453.110 on Windows provides an incorrect handle to a renderer process in unspecified circumstances, which allows remote attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2013-2855
The Developer Tools API in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2013-2856
Use-after-free vulnerability in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the handling of input.
CVE-2013-2857
Use-after-free vulnerability in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the handling of images.
CVE-2013-2858
Use-after-free vulnerability in the HTML5 Audio implementation in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2013-2859
Google Chrome before 27.0.1453.110 allows remote attackers to bypass the Same Origin Policy and trigger namespace pollution via unspecified vectors.
CVE-2013-2860
Use-after-free vulnerability in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors involving access to a database API by a worker process.
CVE-2013-2861
Use-after-free vulnerability in the SVG implementation in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2013-2862
Skia, as used in Google Chrome before 27.0.1453.110, does not properly handle GPU acceleration, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2013-2863
Google Chrome before 27.0.1453.110 does not properly handle SSL sockets, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2013-2864
The PDF functionality in Google Chrome before 27.0.1453.110 allows remote attackers to cause a denial of service (invalid free operation) or possibly have unspecified other impact via unknown vectors.
CVE-2013-2865
Multiple unspecified vulnerabilities in Google Chrome before 27.0.1453.110 allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2013-3475
Stack-based buffer overflow in db2aud in the Audit Facility in IBM DB2 and DB2 Connect 9.1, 9.5, 9.7, 9.8, and 10.1, as used in Smart Analytics System 7600 and other products, allows local users to gain privileges via unspecified vectors.
Per: http://www-01.ibm.com/support/docview.wss?uid=swg21639355

'The following IBM DB2 and DB2 Connect V9.1, V9.5, V9.7 and V10.1 editions running on AIX, Linux, HP and Solaris (this vulnerability is not applicable to DB2 on Windows.).'
CVE-2013-3948
Apple iOS 6.1.3 does not follow redirects during determination of the hostname to display in an iOS Enterprise Deployment installation dialog, which makes it easier for remote attackers to trigger installation of arbitrary applications via a download-manifest itms-services:// URL that leverages an open redirect vulnerability within a trusted domain.
CVE-2013-3949
The posix_spawn system call in the XNU kernel in Apple Mac OS X 10.8.x does not prevent use of the _POSIX_SPAWN_DISABLE_ASLR and _POSIX_SPAWN_ALLOW_DATA_EXEC flags for setuid and setgid programs, which allows local users to bypass intended access restrictions via a wrapper program that calls the posix_spawnattr_setflags function.
CVE-2013-3950
Stack-based buffer overflow in the openSharedCacheFile function in dyld.cpp in dyld in Apple iOS 5.1.x and 6.x through 6.1.3 makes it easier for attackers to conduct untethering attacks via a long string in the DYLD_SHARED_CACHE_DIR environment variable.
CVE-2013-3951
sys/openbsd/stack_protector.c in libc in Apple iOS 6.1.3 and Mac OS X 10.8.x does not properly parse the Apple strings employed in the user-space stack-cookie implementation, which allows local users to bypass cookie randomization by executing a program with a call-path beginning with the stack-guard= substring, as demonstrated by an iOS untethering attack or an attack against a setuid Mac OS X program.
CVE-2013-3952
The fill_pipeinfo function in bsd/kern/sys_pipe.c in the XNU kernel in Apple Mac OS X 10.8.x allows local users to defeat the KASLR protection mechanism via the PROC_PIDFDPIPEINFO option to the proc_info system call for a kernel pipe handle.
CVE-2013-3953
The mach_port_space_info function in osfmk/ipc/mach_debug.c in the XNU kernel in Apple Mac OS X 10.8.x does not initialize a certain structure member, which allows local users to obtain sensitive information from kernel heap memory via a crafted call.
CVE-2013-3954
The posix_spawn system call in the XNU kernel in Apple Mac OS X 10.8.x does not properly validate the data for file actions and port actions, which allows local users to (1) cause a denial of service (panic) via a size value that is inconsistent with a header count field, or (2) obtain sensitive information from kernel heap memory via a certain size value in conjunction with a crafted buffer.
CVE-2013-3955
The get_xattrinfo function in the XNU kernel in Apple iOS 5.x and 6.x through 6.1.3 on iPad devices does not properly validate the header of an AppleDouble file, which might allow local users to cause a denial of service (memory corruption) or have unspecified other impact via an invalid file on an msdosfs filesystem.
