CVE-2008-5559
SQL injection vulnerability in sendcard.cfm in PostEcards allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2008-5560
PostEcards stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file via a direct request for postcards.mdb.
CVE-2008-5561
SQL injection vulnerability in Netref 4.0 allows remote attackers to execute arbitrary SQL commands via the id parameter to (1) fiche_product.php and (2) presentation.php.
CVE-2008-5562
ASPPortal stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file via a direct request for xportal.mdb.
CVE-2008-5563
Aruba Mobility Controller 2.4.8.x-FIPS, 2.5.x, 3.1.x, 3.2.x, 3.3.1.x, and 3.3.2.x allows remote attackers to cause a denial of service (device crash) via a malformed Extensible Authentication Protocol (EAP) frame.
http://secunia.com/advisories/33057

Note: When using wireless, this only affects devices running in WPA/WPA2 Enterprise modes.

The vulnerability is reported in ArubaOS 2.4.8.x-FIPS, 2.5.x, 3.1.x, 3.2.x, 3.3.1.x, and 3.3.2.x.
CVE-2008-5564
Unspecified vulnerability in the media server in Orb Networks Orb before 2.01.0025 allows remote attackers to cause a denial of service (daemon crash) via a malformed HTTP request.
CVE-2008-5565
Cross-site request forgery (CSRF) vulnerability in admin/settings.php in DL PayCart 1.34 and earlier allows remote attackers to change the admin password via a logout action in conjunction with the NewAdmin, NewPass1, and NewPass2 parameters.
CVE-2008-5566
Cross-site scripting (XSS) vulnerability in index.php in Triangle Solutions PHP Multiple Newsletters 2.7 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2008-5567
Cross-site request forgery (CSRF) vulnerability in admin/ad_settings.php in Bonza Cart 1.10 and earlier allows remote attackers to change the admin password via a logout action in conjunction with the NewAdmin, NewPass1, and NewPass2 parameters.
CVE-2008-5568
Cross-site request forgery (CSRF) vulnerability in admin/settings.php in IPN Pro 3 1.44 and earlier allows remote attackers to change the admin password via a logout action in conjunction with the admin_id, newpass_1, and newpass_2 parameters.
CVE-2008-5569
Multiple cross-site scripting (XSS) vulnerabilities in PHPepperShop 1.4 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) index.php or (2) shop/kontakt.php, or (3) shop_kunden_mgmt.php or (4) SHOP_KONFIGURATION.php in shop/Admin/.
CVE-2008-5570
Directory traversal vulnerability in index.php in PHP Multiple Newsletters 2.7, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the lang parameter.
CVE-2008-5571
SQL injection vulnerability in admin/login.asp in Professional Download Assistant 0.1 allows remote attackers to execute arbitrary SQL commands via the (1) uname parameter (aka user field) or the (2) psw parameter (aka passwd field).  NOTE: some of these details are obtained from third party information.
CVE-2008-5572
Professional Download Assistant 0.1 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the database file via a direct request for database/downloads.mdb.
CVE-2008-5573
SQL injection vulnerability in the login feature in Poll Pro 2.0 allows remote attackers to execute arbitrary SQL commands via the (1) Password and (2) username parameters.
CVE-2008-5574
SQL injection vulnerability in member.php in Webmaster Marketplace allows remote attackers to execute arbitrary SQL commands via the u parameter.
CVE-2008-5575
Session fixation vulnerability in Pro Clan Manager 0.4.2 and earlier allows remote attackers to hijack web sessions by setting the PHPSESSID parameter.
CVE-2008-5576
admin/forums.php in sCssBoard 1.0, 1.1, 1.11, and 1.12 allows remote attackers to bypass authentication and gain administrative access via a large value of the current_user[users_level] parameter.
CVE-2008-5577
PHP remote file inclusion vulnerability in index.php in sCssBoard 1.0, 1.1, 1.11, and 1.12 allows remote attackers to execute arbitrary PHP code via a URL in the inc_function parameter.
CVE-2008-5578
Multiple SQL injection vulnerabilities in index.php in sCssBoard 1.0, 1.1, 1.11, and 1.12 allow remote attackers to execute arbitrary SQL commands via (1) the f parameter in a showforum action, (2) the u parameter in a profile action, (3) the viewcat parameter, or (4) a combination of scb_uid and scb_ident cookie values.
CVE-2008-5579
Absolute path traversal vulnerability in mini-pub.php/front-end/cat.php in mini-pub 0.3 allows remote attackers to read arbitrary files via a full pathname in the sFileName parameter.
CVE-2008-5580
mini-pub.php/front-end/cat.php in mini-pub 0.3 allows remote attackers to execute arbitrary commands via shell metacharacters in the sFileName argument.
CVE-2008-5581
PHP remote file inclusion vulnerability in mini-pub.php/front-end/img.php in mini-pub 0.3 allows remote attackers to execute arbitrary PHP code via a URL in the sFileName parameter.
CVE-2008-5582
SQL injection vulnerability in utilities/login.asp in Nukedit 4.9.x, and possibly earlier, allows remote attackers to execute arbitrary SQL commands via the email parameter.
CVE-2008-5583
Cross-site request forgery (CSRF) vulnerability in index.php in ProjectPier 0.8 and earlier allows remote attackers to perform actions as an administrator via the query string, as demonstrated by a delete project action.
CVE-2008-5584
Multiple cross-site scripting (XSS) vulnerabilities in ProjectPier 0.8 and earlier allow remote attackers to inject arbitrary web script or HTML via (1) a message, (2) a milestone, or (3) a display name in a profile, or the (4) a or (5) c parameter to index.php.
