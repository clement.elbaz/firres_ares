CVE-2008-3358
Cross-site scripting (XSS) vulnerability in Web Dynpro (WD) in the SAP NetWeaver portal, when Internet Explorer 7.0.5730 is used, allows remote attackers to inject arbitrary web script or HTML via a crafted URI, which causes the XSS payload to be reflected in a text/plain document.
CVE-2008-5983
Untrusted search path vulnerability in the PySys_SetArgv API function in Python 2.6 and earlier, and possibly later versions, prepends an empty string to sys.path when the argv[0] argument does not contain a path separator, which might allow local users to execute arbitrary code via a Trojan horse Python file in the current working directory.
CVE-2008-5984
Untrusted search path vulnerability in the Python plugin in Dia 0.96.1, and possibly other versions, allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2008-5985
Untrusted search path vulnerability in the Python interface in Epiphany 2.22.3, and possibly other versions, allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2008-5986
Untrusted search path vulnerability in the (1) "VST plugin with Python scripting" and (2) "VST plugin for writing score generators in Python" in Csound 5.08.2, and possibly other versions, allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2008-5987
Untrusted search path vulnerability in the Python interface in Eye of GNOME (eog) 2.22.3, and possibly other versions, allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2008-5988
SQL injection vulnerability in scripts/recruit_details.php in Jadu CMS for Government allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5989
Directory traversal vulnerability in defs.php in PHPcounter 1.3.2 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the l parameter.
CVE-2008-5990
Directory traversal vulnerability in connect/init.inc in emergecolab 1.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the sitecode parameter to connect/index.php.
CVE-2008-5991
Directory traversal vulnerability in docs.php in MailWatch for MailScanner 1.0.4 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the doc parameter.
CVE-2008-5992
Multiple SQL injection vulnerabilities in Jetik Emlak Sistem A (ESA) 2.0 allow remote attackers to execute arbitrary SQL commands via the KayitNo parameter to (1) diger.php and (2) sayfalar.php.
CVE-2008-5993
Directory traversal vulnerability in image.php in Barcode Generator 1D (barcodegen) 2.0.0 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the code parameter.
CVE-2008-5994
Cross-site scripting (XSS) vulnerability in index.php in Check Point Connectra NGX R62 HFA_01 allows remote attackers to inject arbitrary web script or HTML via the dir parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5995
Cross-site scripting (XSS) vulnerability in the freeCap CAPTCHA (sr_freecap) extension before 1.0.4 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
Solution: An updated version 1.0.4 is available from the TYPO3 extension manager and at typo3.org/extensions/repository/view/sr_freecap/1.0.4/. Users of the extension are advised to update the extension as soon as possible.
CVE-2008-5996
Cross-site scripting (XSS) vulnerability in the Simplenews module 5.x before 5.x-1.5 and 6.x before 6.x-1.0-beta4, a module for Drupal, allows remote authenticated users, with "administer taxonomy" permissions, to inject arbitrary web script or HTML via a Newsletter category field.
CVE-2008-5997
Absolute path traversal vulnerability in admin/fileKontrola/browser.asp in Omnicom Content Platform (OCP) 2.0 allows remote attackers to list arbitrary directories via a full pathname in the root parameter.
CVE-2008-5998
Multiple SQL injection vulnerabilities in the ajax_checklist_save function in the Ajax Checklist module 5.x before 5.x-1.1 for Drupal allow remote authenticated users, with "update ajax checklists" permissions, to execute arbitrary SQL commands via a save operation, related to the (1) nid, (2) qid, and (3) state parameters.
CVE-2008-5999
Cross-site scripting (XSS) vulnerability in the Ajax Checklist module 5.x before 5.x-1.1 for Drupal allows remote authenticated users, with create and edit permissions for posts, to inject arbitrary web script or HTML via unspecified vectors involving the ajax_checklist filter.
CVE-2008-6000
The GDTdiIcpt.sys driver in G DATA AntiVirus 2008, InternetSecurity 2008, and TotalCare 2008 populates kernel registers with IOCTL 0x8317001c input values, which allows local users to cause a denial of service (system crash) or gain privileges via a crafted IOCTL request, as demonstrated by execution of the KeSetEvent function with modified register contents.
Per http://trapkit.de/advisories/TKADV2008-008.txt  

Upgrade to G DATA AntiVirus/InternetSecurity/TotalCare 2009.
  
  http://www.gdata.de/
CVE-2008-6001
index.php in ADN Forum 1.0b and earlier allows remote attackers to bypass authentication and gain sysop access via a fpusuario cookie composed of an initial sysop: string, an arbitrary password field, and a final :sysop:0 string.
CVE-2008-6002
Absolute path traversal vulnerability in sendfile.php in web-cp 0.5.7, when register_globals is enabled, allows remote attackers to read arbitrary files via a full pathname in the filelocation parameter.
CVE-2008-6003
SQL injection vulnerability in sellers_othersitem.php in AJ Auction Pro Platinum 2 allows remote attackers to execute arbitrary SQL commands via the seller_id parameter.
CVE-2008-6004
Cross-site scripting (XSS) vulnerability in search.php in AJ Auction Pro Platinum 2 allows remote attackers to inject arbitrary web script or HTML via the product parameter.
CVE-2008-6005
Multiple buffer overflows in the CheckUniqueName function in W3C Amaya Web Browser 10.0.1, and possibly other versions including 11.0.1, might allow remote attackers to execute arbitrary code via "duplicated" attribute value inputs.
CVE-2009-0042
Multiple unspecified vulnerabilities in the Arclib library (arclib.dll) before 7.3.0.15 in the CA Anti-Virus engine for CA Anti-Virus for the Enterprise 7.1, r8, and r8.1; Anti-Virus 2007 v8 and 2008; Internet Security Suite 2007 v3 and 2008; and other CA products allow remote attackers to bypass virus detection via a malformed archive file.
CVE-2009-0312
Cross-site scripting (XSS) vulnerability in the antispam feature (security/antispam.py) in MoinMoin 1.7 and 1.8.1 allows remote attackers to inject arbitrary web script or HTML via crafted, disallowed content.
CVE-2009-0313
winetricks before 20081223 allows local users to overwrite arbitrary files via a symlink attack on the x_showmenu.txt temporary file.
CVE-2009-0314
Untrusted search path vulnerability in the Python module in gedit allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2009-0315
Untrusted search path vulnerability in the Python module in xchat allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2009-0316
Untrusted search path vulnerability in src/if_python.c in the Python interface in Vim before 7.2.045 allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983), as demonstrated by an erroneous search path for plugin/bike.vim in bicyclerepair.
CVE-2009-0317
Untrusted search path vulnerability in the Python language bindings for Nautilus (nautilus-python) allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2009-0318
Untrusted search path vulnerability in the GObject Python interpreter wrapper in Gnumeric allows local users to execute arbitrary code via a Trojan horse Python file in the current working directory, related to a vulnerability in the PySys_SetArgv function (CVE-2008-5983).
CVE-2009-0319
Unspecified vulnerability in the autofs module in the kernel in Sun Solaris 8 through 10, and OpenSolaris before snv_108, allows local users to cause a denial of service (autofs mount outage) or possibly gain privileges via vectors related to "xdr processing problems."
CVE-2009-0320
Microsoft Windows XP, Server 2003 and 2008, and Vista exposes I/O activity measurements of all processes, which allows local users to obtain sensitive information, as demonstrated by reading the I/O Other Bytes column in Task Manager (aka taskmgr.exe) to estimate the number of characters that a different user entered at a runas.exe password prompt, related to a "benchmarking attack."
CVE-2009-0321
Apple Safari 3.2.1 (aka AppVer 3.525.27.1) on Windows allows remote attackers to cause a denial of service (infinite loop or access violation) via a link to an http URI in which the authority (aka hostname) portion is either a (1) . (dot) or (2) .. (dot dot) sequence.
CVE-2009-0322
drivers/firmware/dell_rbu.c in the Linux kernel before 2.6.27.13, and 2.6.28.x before 2.6.28.2, allows local users to cause a denial of service (system crash) via a read system call that specifies zero bytes from the (1) image_type or (2) packet_size file in /sys/devices/platform/dell_rbu/.
CVE-2009-0323
Multiple stack-based buffer overflows in W3C Amaya Web Browser 10.0 and 11.0 allow remote attackers to execute arbitrary code via (1) a long type parameter in an input tag, which is not properly handled by the EndOfXmlAttributeValue function; (2) an "HTML GI" in a start tag, which is not properly handled by the ProcessStartGI function; and unspecified vectors in (3) html2thot.c and (4) xml2thot.c, related to the msgBuffer variable.  NOTE: these are different vectors than CVE-2008-6005.
