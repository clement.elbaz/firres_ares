CVE-2016-4552
Cross-site scripting (XSS) vulnerability in Roundcube Webmail before 1.2.0 allows remote attackers to inject arbitrary web script or HTML via the href attribute in an area tag in an e-mail message.
CVE-2016-5303
Cross-site scripting (XSS) vulnerability in the Horde Text Filter API in Horde Groupware and Horde Groupware Webmail Edition before 5.2.16 allows remote attackers to inject arbitrary web script or HTML via crafted data:text/html content in a form (1) action or (2) xlink attribute.
CVE-2016-7181
Microsoft Edge allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Microsoft Edge Memory Corruption Vulnerability."
CVE-2016-7206
Cross-site scripting (XSS) vulnerability in Microsoft Edge allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka "Microsoft Edge Information Disclosure Vulnerability," a different vulnerability than CVE-2016-7280.
CVE-2016-7219
The Crypto driver in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allows local users to obtain sensitive information via a crafted application, aka "Windows Crypto Driver Information Disclosure Vulnerability."
CVE-2016-7257
The GDI component in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Office for Mac 2011, and Office 2016 for Mac allows remote attackers to obtain sensitive information from process memory via a crafted web site, aka "GDI Information Disclosure Vulnerability."
CVE-2016-7258
The kernel in Microsoft Windows 10 Gold, 1511, and 1607 and Windows Server 2016 mishandles page-fault system calls, which allows local users to obtain sensitive information from arbitrary processes via a crafted application, aka "Windows Kernel Memory Address Information Disclosure Vulnerability."
CVE-2016-7259
The Graphics Component in the kernel-mode drivers in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allows local users to gain privileges via a crafted application, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2016-7260
The kernel-mode drivers in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allow local users to gain privileges via a crafted application, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2016-7262
Microsoft Excel 2007 SP3, Excel 2010 SP2, Excel 2013 SP1, Excel 2013 RT SP1, Excel 2016, Office Compatibility Pack SP3, and Excel Viewer allow user-assisted remote attackers to execute arbitrary commands via a crafted cell that is mishandled upon a click, aka "Microsoft Office Security Feature Bypass Vulnerability."
CVE-2016-7263
Microsoft Excel for Mac 2011 and Excel 2016 for Mac allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2016-7264
Microsoft Excel 2007 SP3, Office Compatibility Pack SP3, Excel Viewer, Excel for Mac 2011, and Excel 2016 for Mac allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability."
CVE-2016-7265
Microsoft Excel 2007 SP3, Excel 2010 SP2, Excel 2013 SP1, Excel 2013 RT SP1, Excel 2016, Office Compatibility Pack SP3, Excel Viewer, Excel Services on SharePoint Server 2007 SP3, and Excel Services on SharePoint Server 2010 SP2 allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability."
CVE-2016-7266
Microsoft Excel 2007 SP3, Excel 2010 SP2, Excel 2013 SP1, Excel 2013 RT SP1, Excel 2016, Office Compatibility Pack SP3, Excel Viewer, and Excel 2016 for Mac mishandle a registry check, which allows user-assisted remote attackers to execute arbitrary commands via crafted embedded content in a document, aka "Microsoft Office Security Feature Bypass Vulnerability."
CVE-2016-7267
Microsoft Excel 2010 SP2, 2013 SP1, 2013 RT SP1, and 2016 misparses file formats, which makes it easier for remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Security Feature Bypass Vulnerability."
CVE-2016-7268
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, Office Compatibility Pack SP3, Word Viewer, Word for Mac 2011, Word Automation Services on SharePoint Server 2010 SP2, and Office Web Apps 2010 SP2 allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability."
CVE-2016-7270
The Data Provider for SQL Server in Microsoft .NET Framework 4.6.2 mishandles a developer-supplied key, which allows remote attackers to bypass the Always Encrypted protection mechanism and obtain sensitive cleartext information by leveraging key guessability, aka ".NET Information Disclosure Vulnerability."
CVE-2016-7271
The Secure Kernel Mode implementation in Microsoft Windows 10 Gold, 1511, and 1607 and Windows Server 2016 allows local users to bypass the virtual trust level (VTL) protection mechanism via a crafted application, aka "Secure Kernel Mode Elevation of Privilege Vulnerability."
CVE-2016-7272
The Graphics component in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allows remote attackers to execute arbitrary code via a crafted web site, aka "Windows Graphics Remote Code Execution Vulnerability."
CVE-2016-7273
The Graphics component in Microsoft Windows 10 Gold, 1511, and 1607 and Windows Server 2016 allows remote attackers to execute arbitrary code via a crafted web site, aka "Windows Graphics Remote Code Execution Vulnerability."
CVE-2016-7274
Uniscribe in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allows remote attackers to execute arbitrary code via a crafted web site, aka "Windows Uniscribe Remote Code Execution Vulnerability."
CVE-2016-7275
Microsoft Office 2010 SP2, 2013 SP1, 2013 RT SP1, and 2016 mishandles library loading, which allows local users to gain privileges via a crafted application, aka "Microsoft Office OLE DLL Side Loading Vulnerability."
CVE-2016-7276
Microsoft Office 2007 SP3, Office 2010 SP2, Office 2013 SP1, Office for Mac 2011, and Office 2016 for Mac allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability."
CVE-2016-7277
Microsoft Office 2016 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2016-7278
Microsoft Internet Explorer 9 through 11 allows remote attackers to obtain sensitive information from process memory via a crafted web site, aka "Windows Hyperlink Object Library Information Disclosure Vulnerability."
CVE-2016-7279
Microsoft Internet Explorer 9 through 11 and Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Microsoft Browser Memory Corruption Vulnerability."
CVE-2016-7280
Cross-site scripting (XSS) vulnerability in Microsoft Edge allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka "Microsoft Edge Information Disclosure Vulnerability," a different vulnerability than CVE-2016-7206.
CVE-2016-7281
The Web Workers implementation in Microsoft Internet Explorer 10 and 11 and Microsoft Edge allows remote attackers to bypass the Same Origin Policy via unspecified vectors, aka "Microsoft Browser Security Feature Bypass Vulnerability."
CVE-2016-7282
Cross-site scripting (XSS) vulnerability in Microsoft Internet Explorer 9 through 11 and Microsoft Edge allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka "Microsoft Browser Information Disclosure Vulnerability."
CVE-2016-7283
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2016-7284
Microsoft Internet Explorer 10 and 11 allows remote attackers to obtain sensitive information from process memory via a crafted web site, aka "Internet Explorer Information Disclosure Vulnerability."
CVE-2016-7286
The scripting engines in Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Scripting Engine Memory Corruption Vulnerability," a different vulnerability than CVE-2016-7288, CVE-2016-7296, and CVE-2016-7297.
CVE-2016-7287
The scripting engines in Microsoft Internet Explorer 11 and Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Scripting Engine Memory Corruption Vulnerability."
CVE-2016-7288
The scripting engines in Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Scripting Engine Memory Corruption Vulnerability," a different vulnerability than CVE-2016-7286, CVE-2016-7296, and CVE-2016-7297.
CVE-2016-7289
Microsoft Publisher 2010 SP2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2016-7290
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, Office Compatibility Pack SP3, Word for Mac 2011, Word Automation Services on SharePoint Server 2010 SP2, and Office Web Apps 2010 SP2 allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability," a different vulnerability than CVE-2016-7291.
CVE-2016-7291
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, Office Compatibility Pack SP3, Word for Mac 2011, Word Automation Services on SharePoint Server 2010 SP2, and Office Web Apps 2010 SP2 allow remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) via a crafted document, aka "Microsoft Office Information Disclosure Vulnerability," a different vulnerability than CVE-2016-7290.
CVE-2016-7292
The Installer in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 mishandles library loading, which allows local users to gain privileges via a crafted application, aka "Windows Installer Elevation of Privilege Vulnerability."
CVE-2016-7295
The Common Log File System (CLFS) driver in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT 8.1, Windows 10 Gold, 1511, and 1607, and Windows Server 2016 allows local users to obtain sensitive information from process memory via a crafted application, aka "Windows Common Log File System Driver Information Disclosure Vulnerability."
CVE-2016-7296
The scripting engines in Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Scripting Engine Memory Corruption Vulnerability," a different vulnerability than CVE-2016-7286, CVE-2016-7288, and CVE-2016-7297.
CVE-2016-7297
The scripting engines in Microsoft Edge allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Scripting Engine Memory Corruption Vulnerability," a different vulnerability than CVE-2016-7286, CVE-2016-7288, and CVE-2016-7296.
CVE-2016-7298
Microsoft Office 2007 SP3, Office 2010 SP2, Word Viewer, Office for Mac 2011, and Office 2016 for Mac allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2016-7300
Untrusted search path vulnerability in Microsoft Auto Updater for Mac allows local users to gain privileges via a Trojan horse executable file, aka "Microsoft (MAU) Office Elevation of Privilege Vulnerability."
CVE-2016-9757
In the Create Tags page of the Rapid7 Nexpose version 6.4.12 user interface, any authenticated user who has the capability to create tags can inject cross-site scripting (XSS) elements in the tag name field. Once this tag is viewed in the Tag Detail page of the Rapid7 Nexpose 6.4.12 UI by another authenticated user, the script is run in that user's browser context.
