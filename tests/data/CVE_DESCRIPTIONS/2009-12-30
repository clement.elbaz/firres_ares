CVE-2008-7249
Buffer overflow in Squid Analysis Report Generator (Sarg) 2.2.3.1, and probably later, allows user-assisted remote attackers to execute arbitrary code via a long HTTP request method in a crafted access.log file, a different vulnerability than CVE-2008-1167.
CVE-2008-7250
Cross-site scripting (XSS) vulnerability in Squid Analysis Report Generator (Sarg) 2.2.4 allows remote attackers to inject arbitrary web script or HTML via a JavaScript onload event in the User-Agent header, which is not properly handled when displaying the Squid proxy log. NOTE: this issue exists because of an incomplete fix for CVE-2008-1168.
CVE-2009-4456
SQL injection vulnerability in news_detail.php in Green Desktiny 2.3.1, and possibly earlier versions, allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-4457
Multiple unspecified vulnerabilities in the Vsftpd Webmin module before 1.3b for the Vsftpd server have unknown impact and attack vectors related to "Some security issues."
CVE-2009-4458
Multiple cross-site scripting (XSS) vulnerabilities in FreePBX 2.5.2 and 2.6.0rc2, and possibly other versions, allow remote attackers to inject arbitrary web script or HTML via the (1) tech parameter to admin/admin/config.php during a trunks display action, the (2) description parameter during an Add Zap Channel action, and (3) unspecified vectors during an Add Recordings action.
CVE-2009-4459
Redmine 0.8.7 and earlier uses the title tag before defining the character encoding in a meta tag, which allows remote attackers to conduct cross-site scripting (XSS) attacks and inject arbitrary script via UTF-7 encoded values in the title parameter to a new issue page, which may be interpreted as script by Internet Explorer 7 and 8.
CVE-2009-4460
Multiple cross-site scripting (XSS) vulnerabilities in Auto-Surf Traffic Exchange Script 1.1 allow remote attackers to inject arbitrary web script or HTML via the rid parameter to (1) index.php, (2) faq.php, and (3) register.php.
CVE-2009-4461
Multiple cross-site scripting (XSS) vulnerabilities in FlatPress 0.909 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) contact.php, (2) login.php, and (3) search.php.
CVE-2009-4462
Stack-based buffer overflow in the NetBiterConfig utility (NetBiterConfig.exe) 1.3.0 for Intellicom NetBiter WebSCADA allows remote attackers to execute arbitrary code via a long hn (hostname) parameter in a crafted HICP-protocol UDP packet.
CVE-2009-4463
Intellicom NetBiter WebSCADA devices use default passwords for the HICP network configuration service, which makes it easier for remote attackers to modify network settings and cause a denial of service. NOTE: this is only a vulnerability when the administrator does not follow recommendations in the product's installation documentation. NOTE: this issue was originally reported to be hard-coded passwords, not default passwords.
CVE-2009-4464
Cross-site scripting (XSS) vulnerability in searchadvance.asp in Active Business Directory 2 allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2009-4465
DeluxeBB 1.3 stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain user and configuration information, log data, and gain administrative access via a direct request to scripts in (1) templates/ including (2) templates/deluxe/admincp/, (3) templates/corporate/admincp/, and (4) templates/blue/admincp/; (5) images/; (6) logs/ including (7) logs/cp.php; (8) wysiwyg/; (9) docs/; (10) classes/; (11) lang/; and (12) settings/.
CVE-2009-4466
DeluxeBB 1.3 allows remote attackers to obtain sensitive information via a crafted page parameter to misc.php, which reveals the installation path in an error message.  NOTE: this issue might be resultant from improperly controlled computation in tools.php that leads to a denial of service (CPU or memory consumption).
CVE-2009-4467
misc.php in DeluxeBB 1.3 allows remote attackers to register accounts without a valid email address via a valemail action with the valmem set to a pre-assigned user ID, which is visible from a memberlist action.
CVE-2009-4468
Cross-site scripting (XSS) vulnerability in misc.php in DeluxeBB 1.3 allows remote attackers to inject arbitrary web script or HTML via the page parameter.
CVE-2009-4469
Multiple cross-site scripting (XSS) vulnerabilities in pagenumber.inc.php in phpPowerCards 2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) PATH_INFO, the (2) archiv parameter, and the (3) subcat parameter.
CVE-2009-4470
SQL injection vulnerability in boardrule.php in DVBBS 2.0 allows remote attackers to execute arbitrary SQL commands via the groupboardid parameter.
CVE-2009-4471
Multiple PHP remote file inclusion vulnerabilities in FreeSchool 1.1.0 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the CLASSPATH parameter to (1) bib_form.php, (2) bib_pldetails.php, (3) bib_plform.php, (4) bib_plsearchc.php, (5) bib_plsearchs.php, (6) bib_save.php, (7) bib_searchc.php, (8) bib_searchs.php, (9) edi_form.php, (10) edi_save.php, (11) gen_form.php, (12) gen_save.php, (13) lin_form.php, (14) lin_save.php, (15) luo_form.php, (16) luo_save.php, (17) sog_form.php, or (18) sog_save.php in biblioteca/; (19) cal_insert.php, (20) cal_save.php, or (21) cal_saveactivity.php in calendario/; (22) circolari/cir_save.php; or (23) modulistica/mdl_save.php.
CVE-2009-4472
Multiple PHP remote file inclusion vulnerabilities in PHPope 1.0.0 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the (1) GLOBALS[config][dir][plugins] parameter to plugins/address/admin/index.php, (2) GLOBALS[config][dir][functions] parameter to plugins/im/compose.php, and (3) GLOBALS[config][dir][classes] parameter to plugins/cssedit/admin/index.php.
CVE-2009-4473
Multiple cross-site scripting (XSS) vulnerabilities in WorkArea/ContentDesigner/ekformsiframe.aspx in Ektron CMS400.NET 7.6.1.53 and 7.6.6.47, and possibly 7.52 through 7.66sp2, allow remote attackers to inject arbitrary web script or HTML via the (1) css, (2) eca, (3) id, and (4) skin parameters.  NOTE: some of these details are obtained from third party information.
CVE-2009-4474
SQL injection vulnerability in the Mike de Boer zoom (com_zoom) component 2.0 for Mambo allows remote attackers to execute arbitrary SQL commands via the catid parameter to index.php.
CVE-2009-4475
SQL injection vulnerability in the Joomlub (com_joomlub) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the aid parameter in an auction edit action to index.php.
CVE-2009-4476
Stack-based buffer overflow in HAURI ViRobot Desktop 5.5 before 2009-09-28.00 allows remote attackers to execute arbitrary code via unspecified vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.15 through 8.11.  NOTE: some of these details are obtained from third party information.
CVE-2009-4477
SQL injection vulnerability in page.html in Xstate Real Estate 1.0 allows remote attackers to execute arbitrary SQL commands via the pid parameter.
CVE-2009-4478
Multiple cross-site scripting (XSS) vulnerabilities in Xstate Real Estate 1.0 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) home.html or (2) lands.html.
CVE-2009-4479
LDAP3A.exe in MailSite 8.0.4 allows remote attackers to cause a denial of service (heap memory corruption and daemon crash) via unspecified vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.13 through 8.11.  NOTE: as of 20091229, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-4480
Buffer overflow in the web service in AzeoTech DAQFactory 5.77 might allow remote attackers to execute arbitrary code via unspecified vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.16 through 8.11.  NOTE: as of 20091229, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-4481
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2009-3111.  Reason: This candidate is a duplicate of CVE-2009-3111.  Notes: All CVE users should reference CVE-2009-3111 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2009-4482
Buffer overflow in MediaServer.exe in TVersity 1.6 allows remote attackers to execute arbitrary code via unspecified vectors, as demonstrated by the vd_tversity module in VulnDisco Pack Professional 8.11.  NOTE: as of 20091229, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-4483
Unspecified vulnerability in LDAP3A.exe in MailSite 8.0.4 allows remote attackers to cause a denial of service (daemon crash) via unknown vectors, as demonstrated by a certain module in VulnDisco Pack Professional 7.13 through 8.11.  NOTE: as of 20091229, this disclosure has no actionable information. However, because the VulnDisco Pack author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2009-4484
Multiple stack-based buffer overflows in the CertDecoder::GetName function in src/asn.cpp in TaoCrypt in yaSSL before 1.9.9, as used in mysqld in MySQL 5.0.x before 5.0.90, MySQL 5.1.x before 5.1.43, MySQL 5.5.x through 5.5.0-m2, and other products, allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption and daemon crash) by establishing an SSL connection and sending an X.509 client certificate with a crafted name field, as demonstrated by mysql_overflow1.py and the vd_mysql5 module in VulnDisco Pack Professional 8.11. NOTE: this was originally reported for MySQL 5.0.51a.
