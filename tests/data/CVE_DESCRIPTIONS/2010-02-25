CVE-2010-0011
The eval_js function in uzbl-core.c in Uzbl before 2010.01.05 exposes the run method of the Uzbl object, which allows remote attackers to execute arbitrary commands via JavaScript code.
CVE-2010-0118
Bournal before 1.4.1 allows local users to overwrite arbitrary files via a symlink attack on unspecified temporary files associated with a --hack_the_gibson update check.
CVE-2010-0119
Bournal before 1.4.1 on FreeBSD 8.0, when the -K option is used, places a ccrypt key on the command line, which allows local users to obtain sensitive information by listing the process and its arguments, related to "echoing."
CVE-2010-0412
stap-server in SystemTap 1.1 does not properly restrict the value of the -B (aka BUILD) option, which allows attackers to have an unspecified impact via vectors associated with executing the make program, a different vulnerability than CVE-2009-4273.
CVE-2010-0424
The edit_cmd function in crontab.c in (1) cronie before 1.4.4 and (2) Vixie cron (vixie-cron) allows local users to change the modification times of arbitrary files, and consequently cause a denial of service, via a symlink attack on a temporary file in the /tmp directory.
CVE-2010-0427
sudo 1.6.x before 1.6.9p21, when the runas_default option is used, does not properly set group memberships, which allows local users to gain privileges via a sudo command.
CVE-2010-0620
Directory traversal vulnerability in the SSL Service in EMC HomeBase Server 6.2.x before 6.2.3 and 6.3.x before 6.3.2 allows remote attackers to overwrite arbitrary files with any content, and consequently execute arbitrary code, via a .. (dot dot) in an unspecified parameter.
Per: http://seclists.org/bugtraq/2010/Feb/222

Affected products:

EMC HomeBase Server version 6.2.x

EMC HomeBase Server version 6.3.x

CVE-2010-0683
Unspecified vulnerability in TIBRepoServer5.jar in TIBCO Administrator 5.4.0 through 5.6.0, when JMS transport is used, allows remote authenticated users to execute arbitrary code on all domain nodes via vectors related to leveraging administrative credentials.
CVE-2010-0704
Cross-site scripting (XSS) vulnerability in the Portlet Palette in IBM WebSphere Portal 6.0.1.5 wp6015_008_01 allows remote attackers to inject arbitrary web script or HTML via the search field.
CVE-2010-0705
Aavmker4.sys in avast! 4.8 through 4.8.1368.0 and 5.0 before 5.0.418.0 running on Windows 2000 and XP does not properly validate input to IOCTL 0xb2d60030, which allows local users to cause a denial of service (system crash) or execute arbitrary code to gain privileges via IOCTL requests using crafted kernel addresses that trigger memory corruption.
CVE-2010-0706
Cross-site scripting (XSS) vulnerability in the login/prompt component in Subex Nikira Fraud Management System allows remote attackers to inject arbitrary web script or HTML via the message parameter.
CVE-2010-0707
Cross-site request forgery (CSRF) vulnerability in add_user.php in Employee Timeclock Software 0.99 allows remote attackers to hijack the authentication of an administrator for requests that create new administrative users.  NOTE: some of these details are obtained from third party information.
CVE-2010-0708
Multiple unspecified vulnerabilities in (1) ns-slapd and (2) slapd.exe in Sun Directory Server Enterprise Edition 7.0, Sun Java System Directory Server 5.2, and Sun Java System Directory Server Enterprise Edition 6.0 through 6.3.1 allow remote attackers to cause a denial of service (daemon crash) via a crafted LDAP search request.
CVE-2010-0709
Multiple cross-site request forgery (CSRF) vulnerabilities in Limny 2.0 allow remote attackers to (1) hijack the authentication of users or administrators for requests that change the email address or password via the user action to index.php, and (2) hijack the authentication of the administrator for requests that create a new user via the admin/modules/user/new action to limny/index.php.
CVE-2010-0710
SQL injection vulnerability in default.asp in ASPCode CMS 1.5.8, 2.0.0 Build 103, and possibly other versions, allows remote attackers to execute arbitrary SQL commands via the newsid parameter when the sec parameter is 26.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-0711
Cross-site request forgery (CSRF) vulnerability in default.asp in ASPCode CMS 1.5.8, 2.0.0 Build 103, and possibly other versions, allows remote attackers to hijack the authentication of an administrator for requests that (1) delete users via the delete action in the ma2 parameter or (2) create administrators via the update action in the ma2 parameter.
