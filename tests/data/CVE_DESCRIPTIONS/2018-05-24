CVE-2013-3018
The AXIS webapp in deploy-tomcat/axis in IBM Tivoli Application Dependency Discovery Manager (TADDM) 7.1.2 and 7.2.0 through 7.2.1.4 allows remote attackers to obtain sensitive configuration information via a direct request, as demonstrated by happyaxis.jsp. IBM X-Force ID: 84354.
CVE-2013-3023
IBM Tivoli Application Dependency Discovery Manager (TADDM) 7.1.2 and 7.2.0 through 7.2.1.4 might allow remote attackers to obtain sensitive information about Tomcat credentials by sniffing the network for a session in which HTTP is used. IBM X-Force ID: 84361.
CVE-2013-3024
IBM WebSphere Application Server (WAS) 8.5 through 8.5.0.2 on UNIX allows local users to gain privileges by leveraging improper process initialization. IBM X-Force ID: 84362.
CVE-2017-14187
A local privilege escalation and local code execution vulnerability in Fortinet FortiOS 5.6.0 to 5.6.2, 5.4.0 to 5.4.8, and 5.2 and below versions allows attacker to execute unauthorized binary program contained on an USB drive plugged into a FortiGate via linking the aforementioned binary program to a command that is allowed to be run by the fnsysctl CLI command.
CVE-2017-17158
Some Huawei smart phones with the versions before Berlin-L21HNC185B381; the versions before Prague-AL00AC00B223; the versions before Prague-AL00BC00B223; the versions before Prague-AL00CC00B223; the versions before Prague-L31C432B208; the versions before Prague-TL00AC01B223; the versions before Prague-TL00AC01B223 have an information exposure vulnerability. When the user's smart phone connects to the malicious device for charging, an unauthenticated attacker may activate some specific function by sending some specially crafted messages. Due to insufficient input validation of the messages, successful exploit may cause information exposure.
CVE-2017-17315
Huawei DP300 V500R002C00; RP200 V600R006C00; TE30 V100R001C10; V500R002C00; V600R006C00; TE40 V500R002C00; V600R006C00; TE50 V500R002C00; V600R006C00; TE60 V100R001C10; V500R002C00; V600R006C00 have a numeric errors vulnerability. An unauthenticated, remote attacker may send specially crafted SCCP messages to the affected products. Due to the improper validation of the messages, it will cause numeric errors when handling the messages. Successful exploit will cause some services abnormal.
CVE-2017-9421
Authentication Bypass vulnerability in Accellion kiteworks before 2017.01.00 allows remote attackers to execute certain API calls on behalf of a web user using a gathered token via a POST request to /oauth/token.
CVE-2017-9664
In ABB SREA-01 revisions A, B, C: application versions up to 3.31.5, and SREA-50 revision A: application versions up to 3.32.8, an attacker may access internal files of ABB SREA-01 and SREA-50 legacy remote monitoring tools without any authorization over the network using a HTTP request which refers to files using ../../ relative paths. Once the internal password file is retrieved, the password hash can be identified using a brute force attack. There is also an exploit allowing running of commands after authorization.
CVE-2018-1000036
In MuPDF 1.12.0 and earlier, multiple memory leaks in the PDF parser allow an attacker to cause a denial of service (memory leak) via a crafted file.
CVE-2018-1000037
In MuPDF 1.12.0 and earlier, multiple reachable assertions in the PDF parser allow an attacker to cause a denial of service (assert crash) via a crafted file.
CVE-2018-1000038
In MuPDF 1.12.0 and earlier, a stack buffer overflow in function pdf_lookup_cmap_full in pdf/pdf-cmap.c could allow an attacker to execute arbitrary code via a crafted file.
CVE-2018-1000039
In MuPDF 1.12.0 and earlier, multiple heap use after free bugs in the PDF parser could allow an attacker to execute arbitrary code, read memory, or cause a denial of service via a crafted file.
CVE-2018-1000040
In MuPDF 1.12.0 and earlier, multiple use of uninitialized value bugs in the PDF parser could allow an attacker to cause a denial of service (crash) or influence program flow via a crafted file.
CVE-2018-1000155
OpenFlow version 1.0 onwards contains a Denial of Service and Improper authorization vulnerability in OpenFlow handshake: The DPID (DataPath IDentifier) in the features_reply message are inherently trusted by the controller. that can result in Denial of Service, Unauthorized Access, Network Instability. This attack appear to be exploitable via Network connectivity: the attacker must first establish a transport connection with the OpenFlow controller and then initiate the OpenFlow handshake.
CVE-2018-1000199
The Linux Kernel version 3.18 contains a dangerous feature vulnerability in modify_user_hw_breakpoint() that can result in crash and possibly memory corruption. This attack appear to be exploitable via local code execution and the ability to use ptrace. This vulnerability appears to have been fixed in git commit f67b15037a7a50c57f72e69a6d59941ad90a0f0f.
CVE-2018-1000300
curl version curl 7.54.1 to and including curl 7.59.0 contains a CWE-122: Heap-based Buffer Overflow vulnerability in denial of service and more that can result in curl might overflow a heap based memory buffer when closing down an FTP connection with very long server command replies.. This vulnerability appears to have been fixed in curl < 7.54.1 and curl >= 7.60.0.
CVE-2018-1000301
curl version curl 7.20.0 to and including curl 7.59.0 contains a CWE-126: Buffer Over-read vulnerability in denial of service that can result in curl can be tricked into reading data beyond the end of a heap based buffer used to store downloaded RTSP content.. This vulnerability appears to have been fixed in curl < 7.20.0 and curl >= 7.60.0.
CVE-2018-10593
A vulnerability in DB Manager version 3.0.1.0 and previous and PerformA version 3.0.0.0 and previous allows an authorized user with access to a privileged account on a BD Kiestra system (Kiestra TLA, Kiestra WCA, and InoqulA+ specimen processor) to issue SQL commands, which may result in data corruption.
CVE-2018-10595
A vulnerability in ReadA version 1.1.0.2 and previous allows an authorized user with access to a privileged account on a BD Kiestra system (Kiestra TLA, Kiestra WCA, and InoqulA+ specimen processor) to issue SQL commands, which may result in loss or corruption of data.
CVE-2018-11332
Stored cross-site scripting (XSS) vulnerability in the "Site Name" field found in the "site" tab under configurations in ClipperCMS 1.3.3 allows remote attackers to inject arbitrary web script or HTML via a crafted site name to the manager/processors/save_settings.processor.php file.
CVE-2018-11399
SimpliSafe Original has Unencrypted Sensor Transmissions, which allows physically proximate attackers to obtain potentially sensitive information about the specific times when alarm-system events occur.
CVE-2018-11400
In SimpliSafe Original, the Base Station fails to detect tamper attempts: it does not send a notification if a physically proximate attacker removes the battery and external power.
CVE-2018-11401
In SimpliSafe Original, RF Interference (e.g., an extremely strong 433.92 MHz signal) by a physically proximate attacker does not cause a notification.
CVE-2018-11402
SimpliSafe Original has Unencrypted Keypad Transmissions, which allows physically proximate attackers to discover the PIN.
CVE-2018-11403
DomainMod v4.09.03 has XSS via the assets/edit/account-owner.php oid parameter.
CVE-2018-11404
DomainMod v4.09.03 has XSS via the assets/edit/ssl-provider-account.php sslpaid parameter.
CVE-2018-11405
Kliqqi 2.0.2 has CSRF in admin/admin_users.php.
CVE-2018-11410
An issue was discovered in Liblouis 3.5.0. A invalid free in the compileRule function in compileTranslationTable.c allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact.
CVE-2018-11411
The transferFrom function of a smart contract implementation for DimonCoin (FUD), an Ethereum ERC20 token, allows attackers to steal assets (e.g., transfer all victims' balances into their account) because certain computations involving _value are incorrect.
CVE-2018-11412
In the Linux kernel 4.13 through 4.16.11, ext4_read_inline_data() in fs/ext4/inline.c performs a memcpy with an untrusted length value in certain circumstances involving a crafted filesystem that stores the system.data extended attribute value in a dedicated inode.
CVE-2018-11413
An issue was discovered in BearAdmin 0.5. Remote attackers can download arbitrary files via /admin/databack/download.html?name= directory traversal sequences, as demonstrated by name=../application/database.php to read the MySQL credentials in the configuration.
CVE-2018-11414
An issue was discovered in BearAdmin 0.5. There is admin/admin_log/index.html?user_id= SQL injection because admin\controller\AdminLog.php constructs a MySQL query improperly.
CVE-2018-11415
SAP Internet Transaction Server (ITS) 6200.X.X has Reflected Cross Site Scripting (XSS) via certain wgate URIs. NOTE: the vendor has reportedly indicated that there will not be any further releases of this product.
CVE-2018-11416
jpegoptim.c in jpegoptim 1.4.5 (fixed in 1.4.6) has an invalid use of realloc() and free(), which allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact.
CVE-2018-11418
An issue was discovered in JerryScript 1.0. There is a heap-based buffer over-read in the lit_read_code_unit_from_utf8 function via a RegExp("[\\u0020") payload, related to re_parse_char_class in parser/regexp/re-parser.c.
CVE-2018-11419
An issue was discovered in JerryScript 1.0. There is a heap-based buffer over-read in the lit_read_code_unit_from_hex function via a RegExp("[\\u0") payload, related to re_parse_char_class in parser/regexp/re-parser.c.
CVE-2018-5485
NetApp OnCommand Unified Manager for Windows versions 7.2 through 7.3 are susceptible to a vulnerability which could lead to a privilege escalation attack.
CVE-2018-5487
NetApp OnCommand Unified Manager for Linux versions 7.2 through 7.3 ship with the Java Management Extension Remote Method Invocation (JMX RMI) service bound to the network, and are susceptible to unauthenticated remote code execution.
CVE-2018-5674
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. Crafted data in the PDF file can trigger an overflow of a heap-based buffer. An attacker can leverage this vulnerability to execute code under the context of the current process, a different vulnerability than CVE-2018-5676 and CVE-2018-5678.
CVE-2018-5675
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. Crafted data in the PDF file can trigger an out-of-bounds write on a buffer. An attacker can leverage this vulnerability to execute code under the context of the current process.
CVE-2018-5676
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. Crafted data in the PDF file can trigger an overflow of a heap-based buffer. An attacker can leverage this vulnerability to execute code under the context of the current process, a different vulnerability than CVE-2018-5674 and CVE-2018-5678.
CVE-2018-5677
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. The issue results from the lack of proper validation of user-supplied data, which can result in a read past the end of an allocated object. An attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process, a different vulnerability than CVE-2018-5679 and CVE-2018-5680.
CVE-2018-5678
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. Crafted data in the PDF file can trigger an overflow of a heap-based buffer. An attacker can leverage this vulnerability to execute code under the context of the current process, a different vulnerability than CVE-2018-5674 and CVE-2018-5676.
CVE-2018-5679
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. The issue results from the lack of proper validation of user-supplied data, which can result in a read past the end of an allocated object. An attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process, a different vulnerability than CVE-2018-5677 and CVE-2018-5680.
CVE-2018-5680
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Foxit Reader before 9.1 and PhantomPDF before 9.1. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of specially crafted pdf files with embedded u3d images. The issue results from the lack of proper validation of user-supplied data, which can result in a read past the end of an allocated object. An attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process, a different vulnerability than CVE-2018-5677 and CVE-2018-5679.
CVE-2018-7406
An issue was discovered in Foxit Reader before 9.1 and PhantomPDF before 9.1. This vulnerability allows remote attackers to execute arbitrary code. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the u3d images inside of a pdf. The issue results from the lack of proper validation of user-supplied data, which can result in an array indexing issue. An attacker can leverage this to execute code in the context of the current process.
CVE-2018-7407
An issue was discovered in Foxit Reader before 9.1 and PhantomPDF before 9.1. This vulnerability allows remote attackers to execute arbitrary code. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists when rendering U3D images inside of pdf files. The issue results from the lack of proper validation of user-supplied data, which can result in a type confusion condition. An attacker can leverage this to execute code in the context of the current process.
CVE-2018-7518
In TotalAlert Web Application in BeaconMedaes Scroll Medical Air Systems prior to v4107600010.23, an attacker with network access to the integrated web server could retrieve default or user defined credentials stored and transmitted in an insecure manner.
CVE-2018-7526
In TotalAlert Web Application in BeaconMedaes Scroll Medical Air Systems prior to v4107600010.23, by accessing a specific uniform resource locator (URL) on the webserver, a malicious user may be able to access information in the application without authenticating.
CVE-2018-7902
Huawei 1288H V5 and 288H V5 with software of V100R005C00 have a JSON injection vulnerability. An authenticated, remote attacker can launch a JSON injection to modify the password of administrator. Due to insufficient verification of the input, this could be exploited to obtain the management privilege of the system.
CVE-2018-7903
Huawei 1288H V5 and 288H V5 with software of V100R005C00 have a JSON injection vulnerability. An authenticated, remote attacker can launch a JSON injection to modify the password of administrator. Due to insufficient verification of the input, this could be exploited to obtain the management privilege of the system.
CVE-2018-7904
Huawei 1288H V5 and 288H V5 with software of V100R005C00 have a JSON injection vulnerability. An authenticated, remote attacker can launch a JSON injection to modify the password of administrator. Due to insufficient verification of the input, this could be exploited to obtain the management privilege of the system.
CVE-2018-7942
The iBMC (Intelligent Baseboard Management Controller) of some Huawei servers have an authentication bypass vulnerability. An unauthenticated, remote attacker may send some specially crafted messages to the affected products. Due to improper authentication design, successful exploit may cause some information leak.
CVE-2018-8013
In Apache Batik 1.x before 1.10, when deserializing subclass of `AbstractDocument`, the class takes a string from the inputStream as the class name which then use it to call the no-arg constructor of the class. Fix was to check the class type before calling newInstance in deserialization.
CVE-2018-9920
Server side request forgery exists in the runtime application in K2 smartforms 4.6.11 via a modified hostname in an https://*/Identity/STS/Forms/Scripts URL.
