CVE-2009-2733
Multiple cross-site scripting (XSS) vulnerabilities in Achievo before 1.4.0 allow remote attackers to inject arbitrary web script or HTML via (1) the scheduler title in the scheduler module, and the (2) atksearch[contractnumber], (3) atksearch_AE_customer[customer], (4) atksearchmode[contracttype], and possibly (5) atksearch[contractname] parameters to the Organization Contracts administration page, reachable through dispatch.php.
CVE-2009-2734
SQL injection vulnerability in the get_employee function in classweekreport.inc in Achievo before 1.4.0 allows remote attackers to execute arbitrary SQL commands via the userid parameter (aka user_id variable) to dispatch.php.
CVE-2009-2874
The TimesTenD process in Cisco Unified Presence 1.x, 6.x before 6.0(6), and 7.x before 7.0(4) allows remote attackers to cause a denial of service (process crash) via a large number of TCP connections to ports 16200 and 22794, aka Bug ID CSCsy17662.
CVE-2009-3281
The vmx86 kernel extension in VMware Fusion before 2.0.6 build 196839 does not use correct file permissions, which allows host OS users to gain privileges on the host OS via unspecified vectors.
CVE-2009-3282
Integer overflow in the vmx86 kernel extension in VMware Fusion before 2.0.6 build 196839 allows host OS users to cause a denial of service to the host OS via unspecified vectors.
Per: http://lists.vmware.com/pipermail/security-announce/2009/000066.html 

Solution

   Please review the patch/release notes for your product and version
   and verify the md5sum and/or the sha1sum of your downloaded file.

   VMware Fusion 2.0.6 (for Intel-based Macs): Download including
   VMware Fusion and a 12 month complimentary subscription to McAfee
   VirusScan Plus 2009

   md5sum: d35490aa8caa92e21339c95c77314b2f
   sha1sum: 9c41985d754ac718032a47af8a3f98ea28fddb26

   VMware Fusion 2.0.6 (for Intel-based Macs): Download including only
   VMware Fusion software

   md5sum: 2e8d39defdffed224c4bab4218cc6659
   sha1sum: 453d54a2f37b257a0aad17c95843305250c7b6ef

CVE-2009-3696
Cross-site scripting (XSS) vulnerability in phpMyAdmin 2.11.x before 2.11.9.6 and 3.x before 3.2.2.1 allows remote attackers to inject arbitrary web script or HTML via a crafted name for a MySQL table.
CVE-2009-3697
SQL injection vulnerability in the PDF schema generator functionality in phpMyAdmin 2.11.x before 2.11.9.6 and 3.x before 3.2.2.1 allows remote attackers to execute arbitrary SQL commands via unspecified interface parameters.
CVE-2009-3704
ZoIPer 2.22, and possibly other versions before 2.24 Library 5324, allows remote attackers to cause a denial of service (crash) via a SIP INVITE request with an empty Call-Info header.
CVE-2009-3705
PHP remote file inclusion vulnerability in debugger.php in Achievo before 1.4.0 allows remote attackers to execute arbitrary PHP code via a URL in the config_atkroot parameter.
CVE-2009-3706
Unspecified vulnerability in the ZFS filesystem in Sun Solaris 10, and OpenSolaris snv_100 through snv_117, allows local users to bypass intended limitations of the file_chown_self privilege via certain uses of the chown system call.
Per:  http://sunsolve.sun.com/search/document.do?assetkey=1-66-265908-1

1. Impact

A security vulnerability in the ZFS file system in OpenSolaris and Solaris 10 systems with patches 137137-09 (SPARC) or 137138-09 (x86) installed may allow a local unprivileged user with the 'file_chown_self' privilege to take ownership of files belonging to another user.
Per:  http://sunsolve.sun.com/search/document.do?assetkey=1-66-265908-1

"Notes:

1. Solaris 8 and 9 are not impacted by this issue.

2. OpenSolaris distributions may include additional bug fixes above and beyond the build from which it was derived. The base build can be derived as follows:

$ uname -v
snv_86

3. This issue only affects systems with ZFS file systems where local users have been granted the {PRIV_FILE_CHOWN_SELF} (see chown(2)) privilege which allows them to modify ownership of files where the ownership matches the user's current effective user ID. If the default operating system configuration option '{_POSIX_CHOWN_RESTRICTED}' has been disabled then the 'file_chown_self' privilege is asserted in the inheritable set of all processes unless overridden by policy.conf(4) or user_attr(4)."
CVE-2009-3707
VMware Authentication Daemon 1.0 in vmware-authd.exe in the VMware Authorization Service in VMware Workstation 7.0 before 7.0.1 build 227600 and 6.5.x before 6.5.4 build 246459, VMware Player 3.0 before 3.0.1 build 227600 and 2.5.x before 2.5.4 build 246459, VMware ACE 2.6 before 2.6.1 build 227600 and 2.5.x before 2.5.4 build 246459, and VMware Server 2.x allows remote attackers to cause a denial of service (process crash) via a \x25\xFF sequence in the USER and PASS commands, related to a "format string DoS" issue. NOTE: some of these details are obtained from third party information.
CVE-2009-3708
Stack-based buffer overflow in the Meta Content Optimizer in Konae Technologies Alleycode HTML Editor 2.21 allows user-assisted remote attackers to execute arbitrary code via a long value in a (1) description or (2) keyword META tag.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-3709
Stack-based buffer overflow in the Meta Content Optimizer in Konae Technologies Alleycode HTML Editor 2.21 allows user-assisted remote attackers to execute arbitrary code via a long value in a TITLE tag.
CVE-2009-3710
RioRey RIOS 4.6.6 and 4.7.0 uses an undocumented, hard-coded username (dbadmin) and password (sq!us3r) for an SSH tunnel, which allows remote attackers to gain privileges via port 8022.
CVE-2009-3711
Stack-based buffer overflow in the h_handlepeer function in http.cpp in httpdx 1.4, and possibly 1.4.3, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long HTTP GET request.
CVE-2009-3712
Multiple SQL injection vulnerabilities in Ebay Clone 2009 allow remote attackers to execute arbitrary SQL commands via the (1) user_id parameter to feedback.php; and the item_id parameter to (2) view_full_size.php, (3) classifide_ad.php, and (4) crosspromoteitems.php.
CVE-2009-3713
SQL injection vulnerability in fichero.php in MorcegoCMS 1.7.6 and earlier allows remote attackers to execute arbitrary SQL commands via the query string.
CVE-2009-3714
Cross-site scripting (XSS) vulnerability in admin_login.php in MCshoutbox 1.1 allows remote attackers to inject arbitrary web script or HTML via the loginerror parameter.
CVE-2009-3715
Multiple SQL injection vulnerabilities in scr_login.php in MCshoutbox 1.1, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters.
CVE-2009-3716
Unrestricted file upload vulnerability in admin.php in MCshoutbox 1.1 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in smilies/.
CVE-2009-3717
Heap-based buffer overflow in LucVil PatPlayer 3.9 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a long URI in a playlist (.m3u) file.
CVE-2009-3718
SQL injection vulnerability in admin/authenticate.asp in Battle Blog 1.25 and 1.30 build 2 allows remote attackers to execute arbitrary SQL commands via the UserName parameter.
CVE-2009-3719
Cross-site scripting (XSS) vulnerability in comment.asp in Battle Blog 1.25 and 1.30 build 2 allows remote attackers to inject arbitrary web script or HTML via a comment.
