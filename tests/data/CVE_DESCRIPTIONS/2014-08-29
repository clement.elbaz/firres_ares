CVE-2010-5110
DCTStream.cc in Poppler before 0.13.3 allows remote attackers to cause a denial of service (crash) via a crafted PDF file.
CVE-2012-1503
Cross-site scripting (XSS) vulnerability in Six Apart (formerly Six Apart KK) Movable Type (MT) Pro 5.13 allows remote attackers to inject arbitrary web script or HTML via the comment section.
CVE-2013-5467
Monitoring Agent for UNIX Logs 6.2.0 through FP03, 6.2.1 through FP04, 6.2.2 through FP09, and 6.2.3 through FP04 and Monitoring Server (ms) and Shared Libraries (ax) 6.2.0 through FP03, 6.2.1 through FP04, 6.2.2 through FP08, 6.2.3 through FP01, and 6.3.0 through FP01 in IBM Tivoli Monitoring (ITM) on UNIX allow local users to gain privileges via unspecified vectors.
CVE-2014-0600
FileUploadServlet in the Administration service in Novell GroupWise 2014 before SP1 allows remote attackers to read or write to arbitrary files via the poLibMaintenanceFileSave parameter, aka ZDI-CAN-2287.
CVE-2014-0888
IBM Worklight Foundation 5.x and 6.x before 6.2.0.0, as used in Worklight and Mobile Foundation, allows remote authenticated users to bypass the application-authenticity feature via unspecified vectors.
CVE-2014-0897
The Configuration Patterns component in IBM Flex System Manager (FSM) 1.2.0.x, 1.2.1.x, 1.3.0.x, and 1.3.1.x uses a weak algorithm in an encryption step during Chassis Management Module (CMM) account creation, which makes it easier for remote authenticated users to defeat cryptographic protection mechanisms via unspecified vectors.
CVE-2014-2390
Cross-site request forgery (CSRF) vulnerability in the User Management module in McAfee Network Security Manager (NSM) before 6.1.15.39 7.1.5.x before 7.1.5.15, 7.1.15.x before 7.1.15.7, 7.5.x before 7.5.5.9, and 8.x before 8.1.7.3 allows remote attackers to hijack the authentication of users for requests that modify user accounts via unspecified vectors.
Per: https://kc.mcafee.com/corporate/index?page=content&id=SB10081

"Affected Versions:

    8.1.7.2 and earlier
    7.5.5.8 and earlier
    7.1.15.6 and earlier
    7.1.5.14 and earlier
    6.1.15.38 and earlier"
CVE-2014-2593
The management console in Aruba Networks ClearPass Policy Manager 6.3.0.60730 allows local users to execute arbitrary commands via shell metacharacters in certain arguments of a valid command, as demonstrated by the (1) system status-rasession and (2) network ping commands.
Per: http://osvdb.org/show/osvdb/109662

"Aruba Networks ClearPass Policy Manager contains a flaw in the Management console. The issue is triggered when parsing commands. With a specially crafted command, an authenticated remote attacker can execute arbitrary commands with root privileges."
CVE-2014-3024
Cross-site request forgery (CSRF) vulnerability in IBM Maximo Asset Management 7.1 through 7.1.1.12 and 7.5 through 7.5.0.6 and Maximo Asset Management 7.5.0 through 7.5.0.3 and 7.5.1 through 7.5.1.2 for SmartCloud Control Desk allows remote authenticated users to hijack the authentication of arbitrary users.
CVE-2014-3084
IBM Maximo Asset Management 6.1 through 6.5, 7.1 through 7.1.1.13, and 7.5 through 7.5.0.6; Maximo Asset Management 7.5.0 through 7.5.0.3 and 7.5.1 through 7.5.1.2 for SmartCloud Control Desk; and Maximo Asset Management 6.2.8, 7.1, and 7.2 for Tivoli IT Asset Management for IT and certain other products allow remote authenticated users to bypass intended write-access restrictions on calendar entries via unspecified vectors.
CVE-2014-3093
IBM PowerVC 1.2.0 before FP3 and 1.2.1 before FP2 uses cleartext passwords in (1) api-paste.ini, (2) debug logs, (3) the installation process, (4) environment checks, (5) powervc-ldap-config, (6) powervc-restore, and (7) powervc-diag, which allows local users to obtain sensitive information by entering a ps command or reading a file.
CVE-2014-3346
The web framework in Cisco Transport Gateway for Smart Call Home (aka TG-SCH or Transport Gateway Installation Software) does not validate an unspecified parameter, which allows remote authenticated users to cause a denial of service (service crash) via a crafted string, aka Bug ID CSCuq31819.
CVE-2014-3349
Cisco Intelligent Automation for Cloud (aka Cisco Cloud Portal) does not validate file types during the handling of file submission, which allows remote authenticated users to upload arbitrary files via a crafted request, aka Bug ID CSCuh87410.
CVE-2014-3350
Cisco Intelligent Automation for Cloud (aka Cisco Cloud Portal) does not properly implement URL redirection, which allows remote authenticated users to obtain sensitive information via a crafted URL, aka Bug ID CSCuh84870.
CVE-2014-3351
Cisco Intelligent Automation for Cloud (aka Cisco Cloud Portal) does not properly consider whether a session is a problematic NULL session, which allows remote attackers to obtain sensitive information via crafted packets, aka Bug IDs CSCuh87398 and CSCuh87380.
CVE-2014-4806
The installation process in IBM Security AppScan Enterprise 8.x before 8.6.0.2 iFix 003, 8.7.x before 8.7.0.1 iFix 003, 8.8.x before 8.8.0.1 iFix 002, and 9.0.x before 9.0.0.1 iFix 001 on Linux places a cleartext password in a temporary file, which allows local users to obtain sensitive information by reading this file.
CVE-2014-4930
Multiple cross-site scripting (XSS) vulnerabilities in event/index2.do in ManageEngine EventLog Analyzer before 9.0 build 9002 allow remote attackers to inject arbitrary web script or HTML via the (1) width, (2) height, (3) url, (4) helpP, (5) tab, (6) module, (7) completeData, (8) RBBNAME, (9) TC, (10) rtype, (11) eventCriteria, (12) q, (13) flushCache, or (14) product parameter.
CVE-2014-5073
vmtadmin.cgi in VMTurbo Operations Manager before 4.6 build 28657 allows remote attackers to execute arbitrary commands via shell metacharacters in the fileDate parameter in a DOWN call.
<a href="http://cwe.mitre.org/data/definitions/77.html" target="_blank">CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')</a>
CVE-2014-5119
Off-by-one error in the __gconv_translit_find function in gconv_trans.c in GNU C Library (aka glibc) allows context-dependent attackers to cause a denial of service (crash) or execute arbitrary code via vectors related to the CHARSET environment variable and gconv transliteration modules.
CVE-2014-5127
Open redirect vulnerability in Innovative Interfaces Encore Discovery Solution 4.3 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in an unspecified parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html" target="_blank">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2014-5128
Innovative Interfaces Encore Discovery Solution 4.3 places a session token in the URI, which might allow remote attackers to obtain sensitive information via unspecified vectors.
CVE-2014-5147
Xen 4.4.x, when running a 64-bit kernel on an ARM system, does not properly handle traps from the guest domain that use a different address width, which allows local guest users to cause a denial of service (host crash) via a crafted 32-bit process.
CVE-2014-5247
The _UpgradeBeforeConfigurationChange function in lib/client/gnt_cluster.py in Ganeti 2.10.0 before 2.10.7 and 2.11.0 before 2.11.5 uses world-readable permissions for the configuration backup file, which allows local users to obtain SSL keys, remote API credentials, and other sensitive information by reading the file, related to the upgrade command.
CVE-2014-5337
The WordPress Mobile Pack plugin before 2.0.2 for WordPress does not properly restrict access to password protected posts, which allows remote attackers to obtain sensitive information via an exportarticles action to export/content.php.
