CVE-2018-20331
Local attackers can trigger a Kernel Pool Buffer Overflow in Antiy AVL ATool v1.0.0.22. An attacker must first obtain the ability to execute low-privileged code on the target system in order to exploit this vulnerability. The specific flaw exists within the processing of IOCTL 0x80002004 by the ssdt.sys kernel driver. The bug is caused by failure to properly validate the length of the user-supplied data. An attacker can leverage this vulnerability to execute arbitrary code in the context of the kernel, which could lead to privilege escalation. A failed exploit could lead to denial of service.
CVE-2018-20368
The Master Slider plugin 3.2.7 and 3.5.1 for WordPress has XSS via the wp-admin/admin-ajax.php Name input field of the MSPanel.Settings value on Callback.
CVE-2018-20369
Barracuda Message Archiver 2018 has XSS in the error_msg exception-handling value for the ldap_user parameter to the cgi-mod/ldap_load_entry.cgi module. The injection point of the issue is the Add_Update module.
CVE-2018-20370
SZ NetChat before 7.9 has XSS in the MyName input field of the Options module. Attackers are able to inject commands to compromise the enabled HTTP server web frontend.
CVE-2018-20371
PhotoRange Photo Vault 1.2 appends the password to the URI for authorization, which makes it easier for remote attackers to bypass intended GET restrictions via a brute-force approach, as demonstrated by "GET /login.html__passwd1" and "GET /login.html__passwd2" and so on.
CVE-2018-20372
TP-Link TD-W8961ND devices allow XSS via the hostname of a DHCP client.
CVE-2018-20373
Tenda ADSL modem routers 1.0.1 allow XSS via the hostname of a DHCP client.
CVE-2018-20374
An issue was discovered in Tiny C Compiler (aka TinyCC or TCC) 0.9.27. Compiling a crafted source file leads to an 8 byte out of bounds write in the use_section1 function in tccasm.c.
CVE-2018-20375
An issue was discovered in Tiny C Compiler (aka TinyCC or TCC) 0.9.27. Compiling a crafted source file leads to an 8 byte out of bounds write in the sym_pop function in tccgen.c.
CVE-2018-20376
An issue was discovered in Tiny C Compiler (aka TinyCC or TCC) 0.9.27. Compiling a crafted source file leads to an 8 byte out of bounds write in the asm_parse_directive function in tccasm.c.
CVE-2018-20377
Orange Livebox 00.96.320S devices allow remote attackers to discover Wi-Fi credentials via /get_getnetworkconf.cgi on port 8080, leading to full control if the admin password equals the Wi-Fi password or has the default admin value. This is related to Firmware 01.11.2017-11:43:44, Boot v0.70.03, Modem 5.4.1.10.1.1A, Hardware 02, and Arcadyan ARV7519RW22-A-L T VR9 1.2.
CVE-2018-20379
Technicolor DPC3928SL D3928SL-PSIP-13-A010-c3420r55105-160428a devices allow XSS via a Cross Protocol Injection attack with setSSID of 1.3.6.1.4.1.4413.2.2.2.1.18.1.2.1.1.3.10001.
CVE-2018-20380
Ambit DDW2600 5.100.1009, DDW2602 5.105.1003, T60C926 4.64.1012, and U10C019 5.66.1026 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20381
Technicolor DPC2320 dpc2300r2-v202r1244101-150420a-v6 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20382
Jiuzhou BCM93383WRG 139.4410mp1.3921132mp1.899.004404.004 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20383
ARRIS DG950A 7.10.145 and DG950S 7.10.145.EURO devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20384
iNovo Broadband IB-8120-W21 139.4410mp1.004200.002 and IB-8120-W21E1 139.4410mp1.3921132mp1.899.004404.004 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20385
CastleNet CBV38Z4EC 125.553mp1.39219mp1.899.007, CBV38Z4ECNIT 125.553mp1.39219mp1.899.005ITT, CBW383G4J 37.556mp5.008, and CBW38G4J 37.553mp1.008 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20386
ARRIS SBG6580-2 D30GW-SEAEAGLE-1.5.2.5-GA-00-NOSH devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20387
Bnmux BCW700J 5.20.7, BCW710J 5.30.6a, and BCW710J2 5.30.16 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20388
Comtrend CM-6200un 123.447.007 and CM-6300n 123.553mp1.005 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20389
D-Link DCM-604 DCM604_C1_ViaCabo_1.04_20130606 and DCM-704 EU_DCM-704_1.10 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20390
Kaonmedia CG2001-AN22A 1.2.1, CG2001-UDBNA 3.0.8, and CG2001-UN2NA 3.0.8 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20391
TEKNOTEL CBW700N 81.447.392110.729.024 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20392
S-A WebSTAR DPC2100 v2.0.2r1256-060303 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20393
Technicolor CGA0111 CGA0111E-ES-13-E23E-c8000r5712-170217-0829-TRU, CWA0101 CWA0101E-A23E-c7000r5712-170315-SKC, DPC3928SL D3928SL-PSIP-13-A010-c3420r55105-170214a, TC7110.AR STD3.38.03, TC7110.B STC8.62.02, TC7110.D STDB.79.02, TC7200.d1I TC7200.d1IE-N23E-c7000r5712-170406-HAT, and TC7200.TH2v2 SC05.00.22 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20394
Thomson DWG849 STC0.01.16, DWG850-4 ST9C.05.25, DWG855 ST80.20.26, and TWG870 STB2.01.36 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20395
NETWAVE MNG6200 C4835805jrc12FU121413.cpr devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20396
NET&SYS MNG2120J 5.76.1006c and MNG6300 5.83.6305jrc2 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20397
mplus CBC383Z CBC383Z_mplus_MDr026 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20398
Skyworth CM5100 V1.1.0, CM5100-440 V1.2.1, CM5100-511 4.1.0.14, CM5100-GHD00 V1.2.2, and CM5100.g2 4.1.0.17 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20399
Motorola SBG901 SBG901-2.10.1.1-GA-00-581-NOSH, SBG941 SBG941-2.11.0.0-GA-07-624-NOSH, and SVG1202 SVG1202-2.1.0.0-GA-14-LTSH devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20400
Ubee DVW2108 6.28.1017 and DVW2110 6.28.2012 devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20401
Zoom 5352 v5.5.8.6Y devices allow remote attackers to discover credentials via iso.3.6.1.4.1.4491.2.4.1.1.6.1.1.0 and iso.3.6.1.4.1.4491.2.4.1.1.6.1.2.0 SNMP requests.
CVE-2018-20402
Safe Software FME Server through 2018.1 creates and enables three additional accounts in addition to the initial administrator account. The passwords to the three accounts are the same as the usernames, which are guest, user, and author. Logging in with these accounts will grant any user the default privilege roles that were also created for each of the accounts.
CVE-2018-20405
BigTree 4.3 allows full path disclosure via authenticated admin/news/ input that triggers a syntax error.
CVE-2018-20406
Modules/_pickle.c in Python before 3.7.1 has an integer overflow via a large LONG_BINPUT value that is mishandled during a "resize to twice the size" attempt. This issue might cause memory exhaustion, but is only relevant if the pickle format is used for serializing tens or hundreds of gigabytes of data.
CVE-2018-20407
An issue was discovered in Bento4 1.5.1-627. There is a memory leak in AP4_DescriptorFactory::CreateDescriptorFromStream in Core/Ap4DescriptorFactory.cpp, as demonstrated by mp42hls.
CVE-2018-20408
An issue was discovered in Bento4 1.5.1-627. There is a memory leak in AP4_StdcFileByteStream::Create in System/StdC/Ap4StdCFileByteStream.cpp, as demonstrated by mp42hls.
CVE-2018-20409
An issue was discovered in Bento4 1.5.1-627. There is a heap-based buffer over-read in AP4_AvccAtom::Create in Core/Ap4AvccAtom.cpp, as demonstrated by mp42hls.
