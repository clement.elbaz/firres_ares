CVE-2007-0770
Buffer overflow in GraphicsMagick and ImageMagick allows user-assisted remote attackers to cause a denial of service and possibly execute arbitrary code via a PALM image that is not properly handled by the ReadPALMImage function in coders/palm.c. NOTE: this issue is due to an incomplete patch for CVE-2006-5456.
CVE-2007-0871
Unrestricted file upload vulnerability in eXtremePow eXtreme File Hosting allows remote attackers to upload arbitrary PHP code via a filename with a double extension such as (1) .rar.php or (2) .zip.php.
CVE-2007-0872
Directory traversal vulnerability in the Plain Old Webserver (POW) add-on before 0.0.9 for Mozilla Firefox allows remote attackers to read arbitrary files via a .. (dot dot) in the URI.
CVE-2007-0873
nabopoll 1.1.2 allows remote attackers to bypass authentication and access certain administrative functionality via a direct request for (1) config_edit.php, (2) template_edit.php, or (3) survey_edit.php in admin/.
CVE-2007-0874
Allons_voter 1.0 allows remote attackers to bypass authentication and access certain administrative functionality via a direct request for (1) admin_ajouter.php or (2) admin_supprimer.php.  NOTE: this could be leveraged to conduct cross-site scripting (XSS) attacks.
CVE-2007-0875
** DISPUTED **  SQL injection vulnerability in install.php in mcRefer allows remote attackers to execute arbitrary SQL commands via unspecified vectors. NOTE: this issue has been disputed by a third party, stating that the file does not use a SQL database.
CVE-2007-0876
Cross-site scripting (XSS) vulnerability in Quick Digital Image Gallery (Qdig) 1.2.9.3 and devel-20060624 allows remote attackers to inject arbitrary web script or HTML via the Qwd parameter to the top-level URI.
CVE-2007-0877
Unspecified vulnerability in March Networks DVR 3000 and 4000 Digital Video Recorders allows attackers to cause an unspecified denial of service.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0878
Unspecified vulnerability in Microsoft Internet Explorer on Windows Mobile 5.0 allows remote attackers to cause a denial of service (loss of browser and other device functionality) via a malformed WML page, related to an "overflow state." NOTE: it is possible that this issue is related to CVE-2007-0685.
CVE-2007-0879
Buffer overflow in SmidgeonSoft PEBrowse Professional 8.2.1.0 allows user-assisted remote attackers to execute arbitrary code via certain executable files in PE format.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0880
Capital Request Forms stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain database credentials via a direct request for inc/common_db.inc.
CVE-2007-0881
PHP remote file inclusion vulnerability in the Seitenschutz plugin for OPENi-CMS 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the (1) config[oi_dir] and possibly (2) config[openi_dir] parameters to open-admin/plugins/site_protection/index.php.  NOTE: vector 2 might be the same as CVE-2006-4750.
Successful exploitation requires that "register_globals" is enabled.
CVE-2007-0882
Argument injection vulnerability in the telnet daemon (in.telnetd) in Solaris 10 and 11 (SunOS 5.10 and 5.11) misinterprets certain client "-f" sequences as valid requests for the login program to skip authentication, which allows remote attackers to log into certain accounts, as demonstrated by the bin account.
CVE-2007-0883
Directory traversal vulnerability in portalgroups/portalgroups/getfile.cgi in IP3 NetAccess before firmware 4.1.9.6 allows remote attackers to read arbitrary files via a .. (dot dot) in the filename parameter.
CVE-2007-0884
Buffer overflow in Roaring Penguin MIMEDefang 2.59 and 2.60 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via unspecified vectors.
Upgrade to 2.61
CVE-2007-0885
Cross-site scripting (XSS) vulnerability in jira/secure/BrowseProject.jspa in Rainbow with the Zen (Rainbow.Zen) extension allows remote attackers to inject arbitrary web script or HTML via the id parameter.
CVE-2007-0886
Heap-based buffer underflow in axigen 1.2.6 through 2.0.0b1 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via certain base64-encoded data on the pop3 port (110/tcp), which triggers an integer overflow.
CVE-2007-0887
axigen 1.2.6 through 2.0.0b1 does not properly parse login credentials, which allows remote attackers to cause a denial of service (NULL dereference and application crash) via a base64-encoded "*\x00" sequence on the imap port (143/tcp).
CVE-2007-0888
Directory traversal vulnerability in the TFTP server in Kiwi CatTools before 3.2.0 beta allows remote attackers to read arbitrary files, and upload files to arbitrary locations, via ..// (dot dot) sequences in the pathname argument to an FTP (1) GET or (2) PUT command.
This vulnerability is addressed in the following product update:
Kiwi Enterprises, Kiwi CatTools, 3.2.0 Beta
CVE-2007-0889
Kiwi CatTools before 3.2.0 beta uses weak encryption ("reversible encoding") for passwords, account names, and IP addresses in kiwidb-cattools.kdb, which might allow local users to gain sensitive information by decrypting the file.  NOTE: this issue could be leveraged with a directory traversal vulnerability for a remote attack vector.
CVE-2007-0890
Cross-site scripting (XSS) vulnerability in scripts/passwdmysql in cPanel WebHost Manager (WHM) 11.0.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the password parameter.
CVE-2007-0891
Cross-site scripting (XSS) vulnerability in the GetCurrentCompletePath function in phpmyvisites.php in phpMyVisites before 2.2 allows remote attackers to inject arbitrary web script or HTML via the query string.
CVE-2007-0892
CRLF injection vulnerability in phpMyVisites before 2.2 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in the url parameter, when the pagename parameter begins with "FILE:".
CVE-2007-0893
Directory traversal vulnerability in phpMyVisites before 2.2 allows remote attackers to include arbitrary files via leading ".." sequences on the pmv_ck_view COOKIE parameter, which bypasses the protection scheme.
CVE-2007-0894
MediaWiki before 1.9.2 allows remote attackers to obtain sensitive information via a direct request to (1) Simple.deps.php, (2) MonoBook.deps.php, (3) MySkin.deps.php, or (4) Chick.deps.php in wiki/skins, which shows the installation path in the resulting error message.
