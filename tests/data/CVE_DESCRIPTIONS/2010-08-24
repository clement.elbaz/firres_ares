CVE-2010-0428
libspice, as used in QEMU-KVM in the Hypervisor (aka rhev-hypervisor) in Red Hat Enterprise Virtualization (RHEV) 2.2 and qspice 0.3.0, does not properly validate guest QXL driver pointers, which allows guest OS users to cause a denial of service (invalid pointer dereference and guest OS crash) or possibly gain privileges via unspecified vectors.
CVE-2010-0429
libspice, as used in QEMU-KVM in the Hypervisor (aka rhev-hypervisor) in Red Hat Enterprise Virtualization (RHEV) 2.2 and qspice 0.3.0, does not properly restrict the addresses upon which memory-management actions are performed, which allows guest OS users to cause a denial of service (guest OS crash) or possibly gain privileges via unspecified vectors.
CVE-2010-0431
QEMU-KVM, as used in the Hypervisor (aka rhev-hypervisor) in Red Hat Enterprise Virtualization (RHEV) 2.2 and KVM 83, does not properly validate guest QXL driver pointers, which allows guest OS users to cause a denial of service (invalid pointer dereference and guest OS crash) or possibly gain privileges via unspecified vectors.
CVE-2010-0435
The Hypervisor (aka rhev-hypervisor) in Red Hat Enterprise Virtualization (RHEV) 2.2, and KVM 83, when the Intel VT-x extension is enabled, allows guest OS users to cause a denial of service (NULL pointer dereference and host OS crash) via vectors related to instruction emulation.
Per: http://cwe.mitre.org/data/definitions/476.html

'NULL Pointer Dereference'
CVE-2010-1526
Multiple integer overflows in libgdiplus 2.6.7, as used in Mono, allow attackers to execute arbitrary code via (1) a crafted TIFF file, related to the gdip_load_tiff_image function in tiffcodec.c; (2) a crafted JPEG file, related to the gdip_load_jpeg_image_internal function in jpegcodec.c; or (3) a crafted BMP file, related to the gdip_read_bmp_image function in bmpcodec.c, leading to heap-based buffer overflows.
CVE-2010-2784
The subpage MMIO initialization functionality in the subpage_register function in exec.c in QEMU-KVM, as used in the Hypervisor (aka rhev-hypervisor) in Red Hat Enterprise Virtualization (RHEV) 2.2 and KVM 83, does not properly select the index for access to the callback array, which allows guest OS users to cause a denial of service (guest OS crash) or possibly gain privileges via unspecified vectors.
CVE-2010-2811
Virtual Desktop Server Manager (VDSM) in Red Hat Enterprise Virtualization (RHEV) 2.2 does not properly accept TCP connections for SSL sessions, which allows remote attackers to cause a denial of service (daemon outage) via crafted SSL traffic.
CVE-2010-2947
Heap-based buffer overflow in the HX_split function in string.c in libHX before 3.6 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a string that is inconsistent with the expected number of fields.
CVE-2010-3055
The configuration setup script (aka scripts/setup.php) in phpMyAdmin 2.11.x before 2.11.10.1 does not properly restrict key names in its output file, which allows remote attackers to execute arbitrary PHP code via a crafted POST request.
CVE-2010-3056
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin 2.11.x before 2.11.10.1 and 3.x before 3.3.5.1 allow remote attackers to inject arbitrary web script or HTML via vectors related to (1) db_search.php, (2) db_sql.php, (3) db_structure.php, (4) js/messages.php, (5) libraries/common.lib.php, (6) libraries/database_interface.lib.php, (7) libraries/dbi/mysql.dbi.lib.php, (8) libraries/dbi/mysqli.dbi.lib.php, (9) libraries/db_info.inc.php, (10) libraries/sanitizing.lib.php, (11) libraries/sqlparser.lib.php, (12) server_databases.php, (13) server_privileges.php, (14) setup/config.php, (15) sql.php, (16) tbl_replace.php, and (17) tbl_sql.php.
CVE-2010-3111
Google Chrome before 6.0.472.53 does not properly mitigate an unspecified flaw in the Windows kernel, which has unknown impact and attack vectors, a different vulnerability than CVE-2010-2897.
CVE-2010-3112
Google Chrome before 5.0.375.127 does not properly implement file dialogs, which allows attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-3113
Google Chrome before 5.0.375.127, and webkitgtk before 1.2.5, does not properly handle SVG documents, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors related to state changes when using DeleteButtonController.
CVE-2010-3114
The text-editing implementation in Google Chrome before 5.0.375.127, and webkitgtk before 1.2.6, does not check a node type before performing a cast, which has unspecified impact and attack vectors related to (1) DeleteSelectionCommand.cpp, (2) InsertLineBreakCommand.cpp, or (3) InsertParagraphSeparatorCommand.cpp in WebCore/editing/.
CVE-2010-3115
Google Chrome before 5.0.375.127, and webkitgtk before 1.2.6, does not properly implement the history feature, which might allow remote attackers to spoof the address bar via unspecified vectors.
CVE-2010-3116
Multiple use-after-free vulnerabilities in WebKit, as used in Apple Safari before 4.1.3 and 5.0.x before 5.0.3, Google Chrome before 5.0.375.127, and webkitgtk before 1.2.6, allow remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors related to improper handling of MIME types by plug-ins.
CVE-2010-3117
Google Chrome before 5.0.375.127 does not properly implement the notifications feature, which allows remote attackers to cause a denial of service (application crash) and possibly have unspecified other impact via unknown vectors.
CVE-2010-3118
The autosuggest feature in the Omnibox implementation in Google Chrome before 5.0.375.127 does not anticipate entry of passwords, which might allow remote attackers to obtain sensitive information by reading the network traffic generated by this feature.
CVE-2010-3119
Google Chrome before 5.0.375.127 and webkitgtk before 1.2.6 do not properly support the Ruby language, which allows attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-3120
Google Chrome before 5.0.375.127 does not properly implement the Geolocation feature, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
