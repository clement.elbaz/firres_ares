CVE-2007-1355
Multiple cross-site scripting (XSS) vulnerabilities in the appdev/sample/web/hello.jsp example application in Tomcat 4.0.0 through 4.0.6, 4.1.0 through 4.1.36, 5.0.0 through 5.0.30, 5.5.0 through 5.5.23, and 6.0.0 through 6.0.10 allow remote attackers to inject arbitrary web script or HTML via the test parameter and unspecified vectors.
CVE-2007-2684
Jetbox CMS 2.1 allows remote attackers to obtain sensitive information via (1) a direct request to (a) main_page.php, (b) open_tree.php, and (c) outputs.php; (2) a malformed view parameter to index.php, as demonstrated with an SQL injection manipulation; or (3) the id[] parameter to admin/cms/opentree.php, which reveals the installation path in the resulting error message.
CVE-2007-2685
Multiple SQL injection vulnerabilities in index.php in Jetbox CMS 2.1 allow remote attackers to execute arbitrary SQL commands via the (1) view or (2) login parameter.
CVE-2007-2767
Unspecified vulnerability in BES before 3.5.0 in OPeNDAP 4 (Hydrax) before 1.2.1 allows remote attackers to list filesystem contents and obtain sensitive information via unknown vectors.
CVE-2007-2768
OpenSSH, when using OPIE (One-Time Passwords in Everything) for PAM, allows remote attackers to determine the existence of certain user accounts, which displays a different response if the user account exists and is configured to use one-time passwords (OTP), a similar issue to CVE-2007-2243.
CVE-2007-2769
BES before 3.5.0 in OPeNDAP 4 (Hydrax) before 1.2.1 does not properly handle compressed files, which allows remote attackers to upload arbitrary files or execute arbitrary commands via a crafted compressed file.
CVE-2007-2770
Stack-based buffer overflow in Eudora 7.1 allows user-assisted, remote SMTP servers to execute arbitrary code via a long SMTP reply.  NOTE: the user must click through a warning about a possible buffer overflow exploit to trigger this issue.
CVE-2007-2771
Stack-based buffer overflow in the LEAD Technologies LeadTools JPEG 2000 LEADJ2K.LEADJ2K.140 ActiveX control (LTJ2K14.ocx) 14.5.0.35 allows remote attackers to execute arbitrary code via a long BitmapDataPath property.
CVE-2007-2772
(1) caloggerd.exe (camt70.dll) and (2) mediasvr.exe (catirpc.dll and rwxdr.dll) in CA BrightStor Backup 11.5.2.0 SP2 allow remote attackers to cause a denial of service (NULL dereference and application crash) via a crafted RPC packet.
CVE-2007-2773
SQL injection vulnerability in plugins/mp3playlist/mp3playlist.php in Zomplog 3.8 and earlier allows remote attackers to execute arbitrary SQL commands via the speler parameter.
CVE-2007-2774
Multiple PHP remote file inclusion vulnerabilities in SunLight CMS 5.3 allow remote attackers to execute arbitrary PHP code via a URL in the root parameter to (1) _connect.php or (2) modules/startup.php.
CVE-2007-2775
AlstraSoft Live Support 1.21 sends a redirect to the web browser but does not exit when administrative credentials are missing, which allows remote attackers to obtain administrative access via a direct request to admin/managesettings.php.
CVE-2007-2776
AlstraSoft Template Seller Pro 3.25 and earlier sends a redirect to the web browser but does not exit when administrative credentials are missing, which allows remote attackers to inject a credential variable setting and obtain administrative access via a direct request to admin/changeinfo.php.
CVE-2007-2777
Unrestricted file upload vulnerability in admin/addsptemplate.php in AlstraSoft Template Seller Pro 3.25 and earlier allows remote attackers to execute arbitrary PHP code via an arbitrary .php filename in the zip parameter, which is created under sptemplates/.
CVE-2007-2778
Multiple directory traversal vulnerabilities in MolyX BOARD 2.5.0 allow remote attackers to read arbitrary files via a .. (dot dot) in the lang parameter to index.php and other unspecified PHP scripts.
CVE-2007-2779
PHP remote file inclusion vulnerability in template_csv.php in Libstats 1.0.3 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the rInfo[content] parameter.
CVE-2007-2780
PsychoStats 3.0.6b and earlier allows remote attackers to obtain sensitive information via a request for server.php with a missing or invalid newtheme parameter, which reveals a path in an error message.
CVE-2007-2781
Cross-site scripting (XSS) vulnerability in include/sessionRegister.php in WikyBlog before 1.4.13 allows remote attackers to inject arbitrary web script or HTML, probably via vectors related to a certain data2 array element.
CVE-2007-2782
Packeteer PacketShaper uses fixed increments in TCP initial sequence number (ISN) values, which allows remote attackers to predict the ISN value, and perform session hijacking or disruption.
CVE-2007-2783
Unspecified vulnerability in Rational Soft Hidden Administrator 1.7 and earlier allows remote attackers to bypass authentication and execute arbitrary code via unspecified vectors.  NOTE: this issue has no actionable information, and perhaps should not be included in CVE.
CVE-2007-2784
Unspecified vulnerability in globus-job-manager in Globus Toolkit 4.1.1 and earlier (globus_nexus-6.6 and earlier) allows remote attackers to cause a denial of service (resource exhaustion and system crash) via certain requests to temporary TCP ports for a GRAM2 job or its MPICH-G2 applications.
CVE-2007-2785
manage-admins.php in eSyndiCat Pro 1.x allows remote attackers to create additional administrative accounts, and have other unspecified impact, via modified username, new_pass, new_pass2, status, super, and certain other parameters in an add action.
CVE-2007-2786
Ratbox IRC Daemon (aka ircd-ratbox) 2.2.5 and earlier allows remote attackers to cause a denial of service (resource exhaustion) by making many requests from a single client.
CVE-2007-2787
Stack-based buffer overflow in the BrowseDir function in the (1) lttmb14E.ocx or (2) LTRTM14e.DLL ActiveX control in LeadTools Raster Thumbnail Object Library 14.5.0.44 allows remote attackers to execute arbitrary code via a long argument.
