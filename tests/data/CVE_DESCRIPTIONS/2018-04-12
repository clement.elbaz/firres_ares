CVE-2014-6120
IBM Rational AppScan Source 8.0 through 8.0.0.2 and 8.5 through 8.5.0.1 and Security AppScan Source 8.6 through 8.6.0.2, 8.7 through 8.7.0.1, 8.8, 9.0 through 9.0.0.1, and 9.0.1 allow remote attackers to execute arbitrary commands on the installation server via unspecified vectors. IBM X-Force ID: 96721.
CVE-2014-6169
Cross-site scripting (XSS) vulnerability in IBM Forms Experience Builder 8.5.0 and 8.5.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors. IBM X-Force ID: 97777.
CVE-2014-6309
The HTTP and WebSocket engine components in the server in Kaazing Gateway 4.0.2, 4.0.3, and 4.0.4 and Gateway - JMS Edition 4.0.2, 4.0.3, and 4.0.4 allow remote attackers to obtain sensitive information via vectors related to HTTP request handling.
CVE-2014-6412
WordPress before 4.4 makes it easier for remote attackers to predict password-recovery tokens via a brute-force approach.
CVE-2014-6633
The safe_eval function in trytond in Tryton before 2.4.15, 2.6.x before 2.6.14, 2.8.x before 2.8.11, 3.0.x before 3.0.7, and 3.2.x before 3.2.3 allows remote authenticated users to execute arbitrary commands via shell metacharacters in (1) the collection.domain in the webdav module or (2) the formula field in the price_list module.
CVE-2014-8421
Unify (former Siemens) OpenStage SIP and OpenScape Desk Phone IP V3 devices before R3.32.0 allow remote attackers to gain super-user privileges by leveraging SSH access and incorrect ownership of (1) ConfigureCoreFile.sh, (2) Traceroute.sh, (3) apps.sh, (4) conversion_java2native.sh, (5) coreCompression.sh, (6) deletePasswd.sh, (7) findHealthSvcFDs.sh, (8) fw_printenv.sh, (9) fw_setenv.sh, (10) hw_wd_kicker.sh, (11) new_rootfs.sh, (12) opera_killSnmpd.sh, (13) opera_startSnmpd.sh, (14) rebootOperaSoftware.sh, (15) removeLogFiles.sh, (16) runOperaServices.sh, (17) setPasswd.sh, (18) startAccTestSvcs.sh, (19) usbNotification.sh, or (20) appWeb in /Opera_Deploy.
CVE-2014-8422
The web-based management (WBM) interface in Unify (former Siemens) OpenStage SIP and OpenScape Desk Phone IP V3 devices before R3.32.0 generates session cookies with insufficient entropy, which makes it easier for remote attackers to hijack sessions via a brute-force attack.
CVE-2014-8888
The remote administration interface in D-Link DIR-815 devices with firmware before 2.03.B02 allows remote attackers to execute arbitrary commands via vectors related to an "HTTP command injection issue."
CVE-2014-9563
CRLF injection vulnerability in the web-based management (WBM) interface in Unify (former Siemens) OpenStage SIP and OpenScape Desk Phone IP V3 devices before R3.32.0 allows remote authenticated users to modify the root password and consequently access the debug port using the serial interface via the ssh-password parameter to page.cmd.
CVE-2015-0150
The remote administration UI in D-Link DIR-815 devices with firmware before 2.07.B01 allows remote attackers to bypass intended access restrictions via unspecified vectors.
CVE-2015-0151
Cross-site request forgery (CSRF) vulnerability in D-Link DIR-815 devices with firmware before 2.07.B01 allows remote attackers to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2015-0152
D-Link DIR-815 devices with firmware before 2.07.B01 allow remote attackers to obtain sensitive information by leveraging cleartext storage of the administrative password.
CVE-2015-0153
D-Link DIR-815 devices with firmware before 2.07.B01 allow remote attackers to obtain sensitive information by leveraging cleartext storage of the wireless key.
CVE-2015-1777
rhnreg_ks in Red Hat Network Client Tools (aka rhn-client-tools) on Red Hat Gluster Storage 2.1 and Enterprise Linux (RHEL) 5, 6, and 7 does not properly validate hostnames in X.509 certificates from SSL servers, which allows remote attackers to prevent system registration via a man-in-the-middle attack.
CVE-2015-4557
Cross-site scripting (XSS) vulnerability in the new_Twitter_sign_button function in nextend-Twitter-connect.php in the Nextend Twitter Connect plugin before 1.5.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the redirect_to parameter.  NOTE: this may overlap CVE-2015-4413.
CVE-2017-1790
IBM DOORS Next Generation (DNG/RRC) 5.0, 5.0.1, 5.0.2, and 6.0 through 6.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137035.
CVE-2017-6910
The HTTP and WebSocket engine components in the server in Kaazing Gateway before 4.5.3 hotfix-1, Gateway - JMS Edition before 4.0.5 hotfix-15, 4.0.6 before hotfix-4, 4.0.7, 4.0.9 before hotfix-19, 4.4.x before 4.4.2 hotfix-1, 4.5.x before 4.5.3 hotfix-1, and Gateway Community and Enterprise Editions before 5.6.0 allow remote attackers to bypass intended access restrictions and obtain sensitive information via vectors related to HTTP request handling.
CVE-2018-0870
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 11. This CVE ID is unique from CVE-2018-0991, CVE-2018-0997, CVE-2018-1018, CVE-2018-1020.
CVE-2018-0887
An information disclosure vulnerability exists when the Windows kernel fails to properly initialize a memory address, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0890
A security feature bypass vulnerability exists when Active Directory incorrectly applies Network Isolation settings, aka "Active Directory Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-0892
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-0998.
CVE-2018-0920
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Microsoft Excel. This CVE ID is unique from CVE-2018-1011, CVE-2018-1027, CVE-2018-1029.
CVE-2018-0950
An information disclosure vulnerability exists when Office renders Rich Text Format (RTF) email messages containing OLE objects when a message is opened or previewed, aka "Microsoft Office Information Disclosure Vulnerability." This affects Microsoft Word, Microsoft Office. This CVE ID is unique from CVE-2018-1007.
CVE-2018-0956
A denial of service vulnerability exists in the HTTP 2.0 protocol stack (HTTP.sys) when HTTP.sys improperly parses specially crafted HTTP 2.0 requests, aka "HTTP.sys Denial of Service Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-0957
An information disclosure vulnerability exists when Windows Hyper-V on a host operating system fails to properly validate input from an authenticated user on a guest operating system, aka "Hyper-V Information Disclosure Vulnerability." This affects Windows Server 2012 R2, Windows RT 8.1, Windows Server 2016, Windows 8.1, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0964.
CVE-2018-0960
An information disclosure vulnerability exists when the Windows kernel improperly handles objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0963
An elevation of privilege vulnerability exists in the way that the Windows Kernel handles objects in memory, aka "Windows Kernel Elevation of Privilege Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-0964
An information disclosure vulnerability exists when Windows Hyper-V on a host operating system fails to properly validate input from an authenticated user on a guest operating system, aka "Hyper-V Information Disclosure Vulnerability." This affects Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0957.
CVE-2018-0966
A security feature bypass exists when Device Guard incorrectly validates an untrusted file, aka "Device Guard Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-0967
A denial of service vulnerability exists in the way that Windows SNMP Service handles malformed SNMP traps, aka "Windows SNMP Service Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-0968
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows Server 2012 R2, Windows RT 8.1, Windows Server 2016, Windows 8.1, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0969
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0970
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0971
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0972
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0973, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0973
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0974, CVE-2018-0975.
CVE-2018-0974
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0975.
CVE-2018-0975
An information disclosure vulnerability exists in the Windows kernel that could allow an attacker to retrieve information that could lead to a Kernel Address Space Layout Randomization (ASLR) bypass, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-0887, CVE-2018-0960, CVE-2018-0968, CVE-2018-0969, CVE-2018-0970, CVE-2018-0971, CVE-2018-0972, CVE-2018-0973, CVE-2018-0974.
CVE-2018-0976
A denial of service vulnerability exists in Remote Desktop Protocol (RDP) when an attacker connects to the target system using RDP and sends specially crafted requests, aka "Windows Remote Desktop Protocol (RDP) Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-0979
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0980, CVE-2018-0990, CVE-2018-0993, CVE-2018-0994, CVE-2018-0995, CVE-2018-1019.
CVE-2018-0980
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0990, CVE-2018-0993, CVE-2018-0994, CVE-2018-0995, CVE-2018-1019.
CVE-2018-0981
An information disclosure vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Information Disclosure Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0987, CVE-2018-0989, CVE-2018-1000.
CVE-2018-0987
An information disclosure vulnerability exists when the scripting engine does not properly handle objects in memory in Internet Explorer, aka "Scripting Engine Information Disclosure Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0981, CVE-2018-0989, CVE-2018-1000.
CVE-2018-0988
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0996, CVE-2018-1001.
CVE-2018-0989
An information disclosure vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Information Disclosure Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0981, CVE-2018-0987, CVE-2018-1000.
CVE-2018-0990
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0980, CVE-2018-0993, CVE-2018-0994, CVE-2018-0995, CVE-2018-1019.
CVE-2018-0991
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0870, CVE-2018-0997, CVE-2018-1018, CVE-2018-1020.
CVE-2018-0993
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0980, CVE-2018-0990, CVE-2018-0994, CVE-2018-0995, CVE-2018-1019.
CVE-2018-0994
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0980, CVE-2018-0990, CVE-2018-0993, CVE-2018-0995, CVE-2018-1019.
CVE-2018-0995
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0980, CVE-2018-0990, CVE-2018-0993, CVE-2018-0994, CVE-2018-1019.
CVE-2018-0996
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0988, CVE-2018-1001.
CVE-2018-0997
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 11. This CVE ID is unique from CVE-2018-0870, CVE-2018-0991, CVE-2018-1018, CVE-2018-1020.
CVE-2018-0998
An information disclosure vulnerability exists when Microsoft Edge PDF Reader improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-0892.
CVE-2018-1000
An information disclosure vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Information Disclosure Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0981, CVE-2018-0987, CVE-2018-0989.
CVE-2018-1001
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0988, CVE-2018-0996.
CVE-2018-1003
A buffer overflow vulnerability exists in the Microsoft JET Database Engine that could allow remote code execution on an affected system, aka "Microsoft JET Database Engine Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10.
CVE-2018-1004
A remote code execution vulnerability exists in the way that the VBScript engine handles objects in memory, aka "Windows VBScript Engine Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Internet Explorer 9, Windows RT 8.1, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10.
CVE-2018-1005
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-1014, CVE-2018-1032, CVE-2018-1034.
CVE-2018-10059
Cacti before 1.1.37 has XSS because the get_current_page function in lib/functions.php relies on $_SERVER['PHP_SELF'] instead of $_SERVER['SCRIPT_NAME'] to determine a page name.
CVE-2018-10060
Cacti before 1.1.37 has XSS because it does not properly reject unintended characters, related to use of the sanitize_uri function in lib/functions.php.
CVE-2018-10061
Cacti before 1.1.37 has XSS because it makes certain htmlspecialchars calls without the ENT_QUOTES flag (these calls occur when the html_escape function in lib/html.php is not used).
CVE-2018-10063
The Convert Forms extension before 2.0.4 for Joomla! is vulnerable to Remote Command Execution using CSV Injection that is mishandled when exporting a Leads file.
CVE-2018-10068
The jDownloads extension before 3.2.59 for Joomla! has XSS.
CVE-2018-1007
An information disclosure vulnerability exists when Microsoft Office improperly discloses the contents of its memory, aka "Microsoft Office Information Disclosure Vulnerability." This affects Microsoft Office. This CVE ID is unique from CVE-2018-0950.
CVE-2018-10071
windrvr1260.sys in Jungo DriverWizard WinDriver 12.6.0 allows attackers to cause a denial of service (BSOD) via a 0x953826DB DeviceIoControl call.
CVE-2018-10072
windrvr1260.sys in Jungo DriverWizard WinDriver 12.6.0 allows attackers to cause a denial of service (BSOD) via a 0x953827bf DeviceIoControl call.
CVE-2018-10073
joyplus-cms 1.6.0 has XSS in manager/admin_vod.php via the keyword parameter.
CVE-2018-10074
The hi3660_stub_clk_probe function in drivers/clk/hisilicon/clk-hi3660-stub.c in the Linux kernel before 4.16 allows local users to cause a denial of service (NULL pointer dereference) by triggering a failure of resource retrieval.
CVE-2018-1008
An elevation of privilege vulnerability exists in Windows Adobe Type Manager Font Driver (ATMFD.dll) when it fails to properly handle objects in memory, aka "OpenType Font Driver Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-1009
An elevation of privilege vulnerability exists when Windows improperly handles objects in memory and incorrectly maps kernel memory, aka "Microsoft DirectX Graphics Kernel Subsystem Elevation of Privilege Vulnerability." This affects Windows Server 2012 R2, Windows RT 8.1, Windows Server 2012, Windows Server 2016, Windows 8.1, Windows 10, Windows 10 Servers.
CVE-2018-1010
A remote code execution vulnerability exists when the Windows font library improperly handles specially crafted embedded fonts, aka "Microsoft Graphics Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1012, CVE-2018-1013, CVE-2018-1015, CVE-2018-1016.
CVE-2018-1011
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Microsoft Excel. This CVE ID is unique from CVE-2018-0920, CVE-2018-1027, CVE-2018-1029.
CVE-2018-1012
A remote code execution vulnerability exists when the Windows font library improperly handles specially crafted embedded fonts, aka "Microsoft Graphics Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1010, CVE-2018-1013, CVE-2018-1015, CVE-2018-1016.
CVE-2018-1013
A remote code execution vulnerability exists when the Windows font library improperly handles specially crafted embedded fonts, aka "Microsoft Graphics Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1010, CVE-2018-1012, CVE-2018-1015, CVE-2018-1016.
CVE-2018-1014
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-1005, CVE-2018-1032, CVE-2018-1034.
CVE-2018-1015
A remote code execution vulnerability exists when the Windows font library improperly handles specially crafted embedded fonts, aka "Microsoft Graphics Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1010, CVE-2018-1012, CVE-2018-1013, CVE-2018-1016.
CVE-2018-1016
A remote code execution vulnerability exists when the Windows font library improperly handles specially crafted embedded fonts, aka "Microsoft Graphics Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1010, CVE-2018-1012, CVE-2018-1013, CVE-2018-1015.
CVE-2018-1018
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 11. This CVE ID is unique from CVE-2018-0870, CVE-2018-0991, CVE-2018-0997, CVE-2018-1020.
CVE-2018-1019
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-0979, CVE-2018-0980, CVE-2018-0990, CVE-2018-0993, CVE-2018-0994, CVE-2018-0995.
CVE-2018-1020
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-0870, CVE-2018-0991, CVE-2018-0997, CVE-2018-1018.
CVE-2018-1023
A remote code execution vulnerability exists in the way that Microsoft browsers access objects in memory, aka "Microsoft Browser Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore.
CVE-2018-1026
A remote code execution vulnerability exists in Microsoft Office software when the software fails to properly handle objects in memory, aka "Microsoft Office Remote Code Execution Vulnerability." This affects Microsoft Office. This CVE ID is unique from CVE-2018-1030.
CVE-2018-1027
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Microsoft Excel, Microsoft Office. This CVE ID is unique from CVE-2018-0920, CVE-2018-1011, CVE-2018-1029.
CVE-2018-1028
A remote code execution vulnerability exists when the Office graphics component improperly handles specially crafted embedded fonts, aka "Microsoft Office Graphics Remote Code Execution Vulnerability." This affects Word, Microsoft Office, Microsoft SharePoint, Excel, Microsoft SharePoint Server.
CVE-2018-1029
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Microsoft Excel Viewer, Microsoft Office, Microsoft Excel. This CVE ID is unique from CVE-2018-0920, CVE-2018-1011, CVE-2018-1027.
CVE-2018-1030
A remote code execution vulnerability exists in Microsoft Office software when the software fails to properly handle objects in memory, aka "Microsoft Office Remote Code Execution Vulnerability." This affects Microsoft Office. This CVE ID is unique from CVE-2018-1026.
CVE-2018-1032
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint Server, Microsoft SharePoint. This CVE ID is unique from CVE-2018-1005, CVE-2018-1014, CVE-2018-1034.
CVE-2018-1034
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-1005, CVE-2018-1014, CVE-2018-1032.
CVE-2018-1037
An information disclosure vulnerability exists when Visual Studio improperly discloses limited contents of uninitialized memory while compiling program database (PDB) files, aka "Microsoft Visual Studio Information Disclosure Vulnerability." This affects Microsoft Visual Studio.
CVE-2018-1079
pcs before version 0.9.164 and 0.10 is vulnerable to a privilege escalation via authorized user malicious REST call. The REST interface of the pcsd service did not properly sanitize the file name from the /remote/put_file query. If the /etc/booth directory exists, an authenticated attacker with write permissions could create or overwrite arbitrary files with arbitrary data outside of the /etc/booth directory, in the context of the pcsd process.
CVE-2018-1084
corosync before version 2.4.4 is vulnerable to an integer overflow in exec/totemcrypto.c.
CVE-2018-1086
pcs before versions 0.9.164 and 0.10 is vulnerable to a debug parameter removal bypass. REST interface of the pcsd service did not properly remove the pcs debug argument from the /run_pcs query, possibly disclosing sensitive information. A remote attacker with a valid token could use this flaw to elevate their privilege.
CVE-2018-3861
A specially crafted TIFF image processed via the application can lead to an out-of-bounds write, overwriting arbitrary data. An attacker can deliver a TIFF image to trigger this vulnerability and gain code execution.
CVE-2018-3862
A specially crafted TIFF image processed via the application can lead to an out-of-bounds write, overwriting
CVE-2018-3868
A specially crafted TIFF image processed via the application can lead to an out-of-bounds write, overwriting arbitrary data. An attacker can deliver a TIFF image to trigger this vulnerability and gain code execution.
CVE-2018-3889
A specially crafted PCX image processed via the application can lead to an out-of-bounds write, overwriting arbitrary data. An attacker can deliver a PCX image to trigger this vulnerability and gain code execution.
CVE-2018-5254
Arista EOS before 4.20.2F allows remote BGP peers to cause a denial of service (Rib agent restart) via a malformed path attribute in an UPDATE message.
CVE-2018-6870
Reflected XSS exists in PHP Scripts Mall Website Seller Script 2.0.3 via the Listings Search feature.
CVE-2018-6879
PHP Scripts Mall Website Seller Script 2.0.3 uses the client side to enforce validation of an e-mail address, which allows remote attackers to modify a registered e-mail address by removing the validation code.
CVE-2018-6900
PHP Scripts Mall Website Broker Script 3.0.6 has XSS via the Last Name field on the My Profile page.
CVE-2018-6902
PHP Scripts Mall Image Sharing Script 1.3.3 has XSS via the Full Name field in an Edit Profile action.
CVE-2018-6903
PHP Scripts Mall Hot Scripts Clone Script Classified v3.1 uses the client side to enforce validation of an e-mail address, which allows remote attackers to modify a registered e-mail address by removing the validation code.
CVE-2018-6904
PHP Scripts Mall Car Rental Script 2.0.8 has XSS via the User Name field in an Edit Profile action.
CVE-2018-6934
CSRF exists in student/personal-info in PHP Scripts Mall Online Tutoring Script 2.0.3.
CVE-2018-6935
PHP Scripts Mall Student Profile Management System Script v2.0.6 has XSS via the Name field to list_student.php.
CVE-2018-8116
A denial of service vulnerability exists in the way that Windows handles objects in memory, aka "Microsoft Graphics Component Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8117
A security feature bypass vulnerability exists in the Microsoft Wireless Keyboard 850 which could allow an attacker to reuse an AES encryption key to send keystrokes to other keyboard devices or to read keystrokes sent by other keyboards for the affected devices, aka "Microsoft Wireless Keyboard 850 Security Feature Bypass Vulnerability." This affects Microsoft Wireless Keyboard 850.
CVE-2018-9118
exports/download.php in the 99 Robots WP Background Takeover Advertisements plugin before 4.1.5 for WordPress has Directory Traversal via a .. in the filename parameter.
CVE-2018-9155
Cross-site scripting (XSS) vulnerability in Open-AudIT Professional 2.1.1 allows remote attackers to inject arbitrary web script or HTML via a crafted name of a component, as demonstrated by the Admin->Logs section (with a logs?logs.type= URI) and the Manage->Attributes section (via the "Name (display)" field to the attributes/create URI).
CVE-2018-9842
CyberArk Password Vault before 9.7 allows remote attackers to obtain sensitive information from process memory by replaying a logon message.
CVE-2018-9843
The REST API in CyberArk Password Vault Web Access before 9.9.5 and 10.x before 10.1 allows remote attackers to execute arbitrary code via a serialized .NET object in an Authorization HTTP header.
CVE-2018-9860
An issue was discovered in Botan 1.11.32 through 2.x before 2.6.0. An off-by-one error when processing malformed TLS-CBC ciphertext could cause the receiving side to include in the HMAC computation exactly 64K bytes of data following the record buffer, aka an over-read. The MAC comparison will subsequently fail and the connection will be closed. This could be used for denial of service. No information leak occurs.
