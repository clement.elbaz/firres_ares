CVE-2017-12070
Unsigned versions of the DLLs distributed by the OPC Foundation may be replaced with malicious code.
CVE-2017-17172
Huawei smart phones LYO-L21 with software LYO-L21C479B107, LYO-L21C479B107 have a privilege escalation vulnerability. An authenticated, local attacker can crafts malformed packets after tricking a user to install a malicious application and exploit this vulnerability when in the exception handling process. Successful exploitation may cause the attacker to obtain a higher privilege of the smart phones.
CVE-2017-17173
Due to insufficient parameters verification GPU driver of Mate 9 Pro Huawei smart phones with the versions before LON-AL00B 8.0.0.356(C00) has an arbitrary memory free vulnerability. An attacker can tricks a user into installing a malicious application on the smart phone, and send given parameter to driver to release special kernel memory resource. Successful exploit may result in phone crash or arbitrary code execution.
CVE-2017-17309
Huawei HG255s-10 V100R001C163B025SP02 has a path traversal vulnerability due to insufficient validation of the received HTTP requests, a remote attacker may access the local files on the device without authentication.
CVE-2018-0871
An information disclosure vulnerability exists when Edge improperly marks files, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8234.
CVE-2018-0978
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-8249.
CVE-2018-0982
An elevation of privilege vulnerability exists in the way that the Windows Kernel API enforces permissions, aka "Windows Elevation of Privilege Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-1036
An elevation of privilege vulnerability exists when NTFS improperly checks access, aka "NTFS Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-1040
A denial of service vulnerability exists in the way that the Windows Code Integrity Module performs hashing, aka "Windows Code Integrity Module Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-10821
Cross-site scripting (XSS) vulnerability in backend/pages/modify.php in BlackCatCMS 1.3 allows remote authenticated users with the Admin role to inject arbitrary web script or HTML via the search panel.
CVE-2018-11574
Improper input validation together with an integer overflow in the EAP-TLS protocol implementation in PPPD may cause a crash, information disclosure, or authentication bypass. This implementation is distributed as a patch for PPPD 0.91, and includes the affected eap.c and eap-tls.c files. Configurations that use the `refuse-app` option are unaffected.
CVE-2018-11689
Smart Viewer in Samsung Web Viewer for Samsung DVR is vulnerable to cross-site scripting, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability via a crafted URL to execute script in a victim's Web browser within the security context of the hosting Web site, once the URL is clicked. An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2018-11690
The Balbooa Gridbox extension version 2.4.0 and previous versions for Joomla! is vulnerable to cross-site scripting, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability via a crafted URL to execute script in a victim's Web browser within the security context of the hosting Web site, once the URL is clicked. An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2018-12114
Maccms 10 allows CSRF via admin.php/admin/admin/info.html to add user accounts.
CVE-2018-12418
Archive.java in Junrar before 1.0.1, as used in Apache Tika and other products, is affected by a denial of service vulnerability due to an infinite loop when handling corrupt RAR files.
CVE-2018-12420
IceHrm before 23.0.1.OS has a risky usage of a hashed password in a request.
CVE-2018-12421
LTB (aka LDAP Tool Box) Self Service Password before 1.3 allows a change to a user password (without knowing the old password) via a crafted POST request, because the ldap_bind return value is mishandled and the PHP data type is not constrained to be a string.
CVE-2018-12423
In Synapse before 0.31.2, unauthorised users can hijack rooms when there is no m.room.power_levels event in force.
CVE-2018-12431
SeaCMS V6.61 has XSS via the site name parameter on an adm1n/admin_config.php page (aka a system management page).
CVE-2018-12432
JavaMelody through 1.60.0 has XSS via the counter parameter in a clear_counter action to the /monitoring URI.
CVE-2018-4833
A vulnerability has been identified in RFID 181-EIP (All versions), RUGGEDCOM Win (V4.4, V4.5, V5.0, and V5.1), SCALANCE X-200 switch family (incl. SIPLUS NET variants) (All versions < V5.2.3), SCALANCE X-200IRT switch family (incl. SIPLUS NET variants) (All versions < V5.4.1), SCALANCE X-200RNA switch family (All versions < V3.2.6), SCALANCE X-300 switch family (incl. SIPLUS NET variants) (All versions < V4.1.3), SCALANCE X408 (All versions < V4.1.3), SCALANCE X414 (All versions), SIMATIC RF182C (All versions). Unprivileged remote attackers located in the same local network segment (OSI Layer 2) could gain remote code execution on the affected products by sending a specially crafted DHCP response to a client's DHCP request.
CVE-2018-4842
A vulnerability has been identified in SCALANCE X-200IRT switch family (incl. SIPLUS NET variants) (All versions < V5.4.1), SCALANCE X-300 switch family (incl. X408 and SIPLUS NET variants) (All versions < V4.1.3). A remote, authenticated attacker with access to the configuration web server could be able to store script code on the web site, if the HRP redundancy option is set. This code could be executed in the web browser of victims visiting this web site (XSS), affecting its confidentiality, integrity and availability. User interaction is required for successful exploitation, as the user needs to visit the manipulated web site. At the stage of publishing this security advisory no public exploitation is known. The vendor has confirmed the vulnerability and provides mitigations to resolve it.
CVE-2018-4848
A vulnerability has been identified in SCALANCE X-200 switch family (incl. SIPLUS NET variants) (All versions < V5.2.3), SCALANCE X-200IRT switch family (incl. SIPLUS NET variants) (All versions < V5.4.1), SCALANCE X-300 switch family (incl. X408 and SIPLUS NET variants) (All versions < V4.1.3). The integrated configuration web server of the affected Scalance X Switches could allow Cross-Site Scripting (XSS) attacks if unsuspecting users are tricked into accessing a malicious link. User interaction is required for a successful exploitation. The user must be logged into the web interface in order for the exploitation to succeed. At the stage of publishing this security advisory no public exploitation is known. The vendor has confirmed the vulnerability and provides mitigations to resolve it.
CVE-2018-6516
On Windows only, with a specifically crafted configuration file an attacker could get Puppet PE client tools (aka pe-client-tools) 16.4.x prior to 16.4.6, 17.3.x prior to 17.3.6, and 18.1.x prior to 18.1.2 to load arbitrary code with privilege escalation.
CVE-2018-8110
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8111, CVE-2018-8236.
CVE-2018-8111
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8110, CVE-2018-8236.
CVE-2018-8113
A security feature bypass vulnerability exists in Internet Explorer that allows for bypassing Mark of the Web Tagging (MOTW), aka "Internet Explorer Security Feature Bypass Vulnerability." This affects Internet Explorer 11.
CVE-2018-8121
An information disclosure vulnerability exists when the Windows kernel improperly initializes objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 10 Servers, Windows 10. This CVE ID is unique from CVE-2018-8207.
CVE-2018-8140
An Elevation of Privilege vulnerability exists when Cortana retrieves data from user input services without consideration for status, aka "Cortana Elevation of Privilege Vulnerability." This affects Windows 10 Servers, Windows 10.
CVE-2018-8169
An elevation of privilege vulnerability exists when the (Human Interface Device) HID Parser Library driver improperly handles objects in memory, aka "HIDParser Elevation of Privilege Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8175
An denial of service vulnerability exists when Windows NT WEBDAV Minirdr attempts to query a WEBDAV directory, aka "WEBDAV Denial of Service Vulnerability." This affects Windows 10 Servers, Windows 10.
CVE-2018-8201
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8211, CVE-2018-8212, CVE-2018-8215, CVE-2018-8216, CVE-2018-8217, CVE-2018-8221.
CVE-2018-8205
A denial of service vulnerability exists when Windows improperly handles objects in memory, aka "Windows Denial of Service Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8207
An information disclosure vulnerability exists when the Windows kernel improperly handles objects in memory, aka "Windows Kernel Information Disclosure Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8121.
CVE-2018-8208
An elevation of privilege vulnerability exists in Windows when Desktop Bridge does not properly manage the virtual registry, aka "Windows Desktop Bridge Elevation of Privilege Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8214.
CVE-2018-8209
An information disclosure vulnerability exists when Windows allows a normal user to access the Wireless LAN profile of an administrative user, aka "Windows Wireless Network Profile Information Disclosure Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8210
A remote code execution vulnerability exists when Windows improperly handles objects in memory, aka "Windows Remote Code Execution Vulnerability." This affects Windows Server 2012 R2, Windows RT 8.1, Windows Server 2012, Windows Server 2016, Windows 8.1, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8213.
CVE-2018-8211
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows 10 Servers, Windows 10. This CVE ID is unique from CVE-2018-8201, CVE-2018-8212, CVE-2018-8215, CVE-2018-8216, CVE-2018-8217, CVE-2018-8221.
CVE-2018-8212
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8201, CVE-2018-8211, CVE-2018-8215, CVE-2018-8216, CVE-2018-8217, CVE-2018-8221.
CVE-2018-8213
A remote code execution vulnerability exists when Windows improperly handles objects in memory, aka "Windows Remote Code Execution Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8210.
CVE-2018-8214
An elevation of privilege vulnerability exists in Windows when Desktop Bridge does not properly manage the virtual registry, aka "Windows Desktop Bridge Elevation of Privilege Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8208.
CVE-2018-8215
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8201, CVE-2018-8211, CVE-2018-8212, CVE-2018-8216, CVE-2018-8217, CVE-2018-8221.
CVE-2018-8216
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10. This CVE ID is unique from CVE-2018-8201, CVE-2018-8211, CVE-2018-8212, CVE-2018-8215, CVE-2018-8217, CVE-2018-8221.
CVE-2018-8217
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10. This CVE ID is unique from CVE-2018-8201, CVE-2018-8211, CVE-2018-8212, CVE-2018-8215, CVE-2018-8216, CVE-2018-8221.
CVE-2018-8218
A denial of service vulnerability exists when Microsoft Hyper-V Network Switch on a host server fails to properly validate input from a privileged user on a guest operating system, aka "Windows Hyper-V Denial of Service Vulnerability." This affects Windows 10, Windows 10 Servers.
CVE-2018-8219
An elevation of privilege vulnerability exists when Windows Hyper-V instruction emulation fails to properly enforce privilege levels, aka "Hypervisor Code Integrity Elevation of Privilege Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8221
A security feature bypass vulnerability exists in Device Guard that could allow an attacker to inject malicious code into a Windows PowerShell session, aka "Device Guard Code Integrity Policy Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-8201, CVE-2018-8211, CVE-2018-8212, CVE-2018-8215, CVE-2018-8216, CVE-2018-8217.
CVE-2018-8224
An elevation of privilege vulnerability exists when the Windows kernel fails to properly handle objects in memory, aka "Windows Kernel Elevation of Privilege Vulnerability." This affects Windows Server 2008, Windows 7, Windows Server 2008 R2.
CVE-2018-8225
A remote code execution vulnerability exists in Windows Domain Name System (DNS) DNSAPI.dll when it fails to properly handle DNS responses, aka "Windows DNSAPI Remote Code Execution Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2008, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8226
A denial of service vulnerability exists in the HTTP 2.0 protocol stack (HTTP.sys) when HTTP.sys improperly parses specially crafted HTTP 2.0 requests, aka "HTTP.sys Denial of Service Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8227
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8229.
CVE-2018-8229
A remote code execution vulnerability exists in the way that the Chakra scripting engine handles objects in memory in Microsoft Edge, aka "Chakra Scripting Engine Memory Corruption Vulnerability." This affects Microsoft Edge, ChakraCore. This CVE ID is unique from CVE-2018-8227.
CVE-2018-8231
A remote code execution vulnerability exists when HTTP Protocol Stack (Http.sys) improperly handles objects in memory, aka "HTTP Protocol Stack Remote Code Execution Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8233
An elevation of privilege vulnerability exists in Windows when the Win32k component fails to properly handle objects in memory, aka "Win32k Elevation of Privilege Vulnerability." This affects Windows 10, Windows 10 Servers.
CVE-2018-8234
An information disclosure vulnerability exists when Microsoft Edge improperly handles objects in memory, aka "Microsoft Edge Information Disclosure Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-0871.
CVE-2018-8235
A security feature bypass vulnerability exists when Microsoft Edge improperly handles requests of different origins, aka "Microsoft Edge Security Feature Bypass Vulnerability." This affects Microsoft Edge.
CVE-2018-8236
A remote code execution vulnerability exists when Microsoft Edge improperly accesses objects in memory, aka "Microsoft Edge Memory Corruption Vulnerability." This affects Microsoft Edge. This CVE ID is unique from CVE-2018-8110, CVE-2018-8111.
CVE-2018-8239
An information disclosure vulnerability exists when the Windows GDI component improperly discloses the contents of its memory, aka "Windows GDI Information Disclosure Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers.
CVE-2018-8243
A remote code execution vulnerability exists in the way that the ChakraCore scripting engine handles objects in memory, aka "Scripting Engine Memory Corruption Vulnerability." This affects ChakraCore. This CVE ID is unique from CVE-2018-8267.
CVE-2018-8244
An elevation of privilege vulnerability exists when Microsoft Outlook does not validate attachment headers properly, aka "Microsoft Outlook Elevation of Privilege Vulnerability." This affects Microsoft Office, Microsoft Outlook.
CVE-2018-8245
A remote code execution vulnerability exists when Microsoft Publisher fails to utilize features that lock down the Local Machine zone when instantiating OLE objects, aka "Microsoft Publisher Remote Code Execution Vulnerability." This affects Microsoft Publisher.
CVE-2018-8246
An information disclosure vulnerability exists when Microsoft Excel improperly discloses the contents of its memory, aka "Microsoft Excel Information Disclosure Vulnerability." This affects Microsoft Excel Viewer, Microsoft Office, Microsoft Excel.
CVE-2018-8247
An elevation of privilege vulnerability exists when Office Web Apps Server 2013 and Office Online Server fail to properly handle web requests, aka "Microsoft Office Elevation of Privilege Vulnerability." This affects Microsoft Office, Microsoft Office Online Server. This CVE ID is unique from CVE-2018-8245.
CVE-2018-8248
A remote code execution vulnerability exists in Microsoft Excel software when the software fails to properly handle objects in memory, aka "Microsoft Excel Remote Code Execution Vulnerability." This affects Microsoft Office.
CVE-2018-8249
A remote code execution vulnerability exists when Internet Explorer improperly accesses objects in memory, aka "Internet Explorer Memory Corruption Vulnerability." This affects Internet Explorer 11. This CVE ID is unique from CVE-2018-0978.
CVE-2018-8251
A memory corruption vulnerability exists when Windows Media Foundation improperly handles objects in memory, aka "Media Foundation Memory Corruption Vulnerability." This affects Windows 7, Windows Server 2012 R2, Windows RT 8.1, Windows Server 2012, Windows 8.1, Windows Server 2016, Windows Server 2008 R2, Windows 10, Windows 10 Servers.
CVE-2018-8252
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft SharePoint. This CVE ID is unique from CVE-2018-8254.
CVE-2018-8254
An elevation of privilege vulnerability exists when Microsoft SharePoint Server does not properly sanitize a specially crafted web request to an affected SharePoint server, aka "Microsoft SharePoint Elevation of Privilege Vulnerability." This affects Microsoft Project Server, Microsoft SharePoint. This CVE ID is unique from CVE-2018-8252.
CVE-2018-8267
A remote code execution vulnerability exists in the way that the scripting engine handles objects in memory in Internet Explorer, aka "Scripting Engine Memory Corruption Vulnerability." This affects Internet Explorer 9, Internet Explorer 11, Internet Explorer 10. This CVE ID is unique from CVE-2018-8243.
CVE-2018-8819
An XXE issue was discovered in Automated Logic Corporation (ALC) WebCTRL Versions 6.0, 6.1 and 6.5. An unauthenticated attacker could enter malicious input to WebCTRL and a weakly configured XML parser will allow the application to disclose full file contents from the underlying web server OS via the "X-Wap-Profile" HTTP header.
CVE-2018-8927
Improper authorization vulnerability in SYNO.Cal.Event in Calendar before 2.1.2-0511 allows remote authenticated users to create arbitrary events via the (1) cal_id or (2) original_cal_id parameter.
