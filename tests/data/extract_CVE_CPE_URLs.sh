#!/bin/bash
# $1 = path to yearly CVE file from NIST
# $2 = day to collect
jq -r --arg dt "$2" '.CVE_Items [] | select(.publishedDate | startswith($dt)) | .cve.CVE_data_meta.ID, .configurations.nodes[].cpe_match[]?.cpe23Uri, .configurations.nodes[].children[]?.cpe_match[].cpe23Uri' $1