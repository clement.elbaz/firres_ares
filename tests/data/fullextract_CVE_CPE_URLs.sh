#!/bin/bash
# $1 = path to yearly CVE file from NIST
./extract_publication_dates.sh $1 | while read line; do ./extract_CVE_CPE_URLs.sh $1 $line >> CVE_CPE_URLs/$line; done