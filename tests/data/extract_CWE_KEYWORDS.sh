#!/bin/bash
# $1 = path to zipped CWE file
cat $1 | gunzip | xpath -q -e '/Weakness_Catalog/Weaknesses/Weakness/@Name' | tr -c '[:alnum:][:blank:]' ' ' | tr  ' ' '\n' | tr '[:upper:]' '[:lower:]' | sort | uniq