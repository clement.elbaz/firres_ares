#!/bin/bash

echo "=== ALL YEARS FROM 2007 to 2019 ==="
echo -n 'Number of unique keywords: '
cat CPE_URL_KEYWORDS/200* CPE_URL_KEYWORDS/201* | sort | uniq | wc -l
echo -n 'Number of these keywords missing from the CPE dictionary: '
cat CPE_URL_KEYWORDS/200* CPE_URL_KEYWORDS/201* | sort | uniq | while read -r line; do < official-cpe-dictionary_v2.3.xml grep -c -m 1 -i -F -e "$line" | grep ^0; done | wc -l


./compute_missing_CPE_URIs.sh 2007
./compute_missing_CPE_URIs.sh 2008
./compute_missing_CPE_URIs.sh 2009
./compute_missing_CPE_URIs.sh 2010
./compute_missing_CPE_URIs.sh 2011
./compute_missing_CPE_URIs.sh 2012
./compute_missing_CPE_URIs.sh 2013
./compute_missing_CPE_URIs.sh 2014
./compute_missing_CPE_URIs.sh 2015
./compute_missing_CPE_URIs.sh 2016
./compute_missing_CPE_URIs.sh 2017
./compute_missing_CPE_URIs.sh 2018
./compute_missing_CPE_URIs.sh 2019