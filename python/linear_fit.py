"""
This file is part of Firres.

Copyright (C) 2020  Inria

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import pandas as pd
import sys
from sklearn.linear_model import LinearRegression

X_train_filename = sys.argv[1]
Y_train_filename = sys.argv[2]
X_test_filename = sys.argv[3]
Z_test_filename = sys.argv[4]
W_linear_coefficients_filename = sys.argv[5]

print("Reading input files...")
X_train_file = pd.read_csv(X_train_filename, header = None)
Y_train_file = pd.read_csv(Y_train_filename, header = None)
X_test_file = pd.read_csv(X_test_filename, header = None)

X_train = X_train_file.values
Y_train = Y_train_file.values
X_test = X_test_file.values

print("Fitting linear regression...")
reg = LinearRegression(n_jobs = -1).fit(X_train, Y_train)

print("Computing predicted values...")
Z_test = reg.predict(X_test)

print("Writing output file...")
pd.DataFrame(Z_test).to_csv(Z_test_filename, header= False, index = False)

print("Writing linear weights to file...")
weights = reg.coef_
pd.DataFrame(weights).to_csv(W_linear_coefficients_filename, header= False, index = False)

print("The end.")