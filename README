---------------------------------------------------------------------------------
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

---------------------------------------------------------------------------------

Firres ('FIRst RESponder') is an experimental companion software to the paper ' Fighting N-Day Vulnerabilities with Automated CVSS Vector Prediction at Disclosure ' by Clément Elbaz (Inria [1]), Louis Rilling (DGA, [2]), and Christine Morin (Inria, [1]).
Contact us at
[1] firstname.lastname@inria.fr
[2] firstname.lastname@irisa.fr

Firres is written in Rust. It was tested on Linux. There are no known obstacles to run in on Mac OS X or Windows, but no guarantees either.

How to run :

1. Make sure you have a working Rust developement environment.
See instructions at https://www.rust-lang.org/tools/install

Also make sure you have an up to date version of Python 3.

2. Optionnaly, edit the firres.toml file and modify the configuration.
 - Change the number of threads to the number of physical cores you are willing to use (16 is recommended).
 - Switch from CVSS V2 to CVSS V3 by toggling experiments.cvss/use_cvss_v3.
 - The use of a whitelist can be toggled through the experiments.cvss/use_cpe_whitelist option
 - The whitelist data can be changed from CPE to CWE or CWE+CPE by changing cpe/repository to either "tests/data/CPE_URL_KEYWORDS", "tests/data/CWE_KEYWORDS" or "tests/data/CWE_AND_CPE_KEYWORDS"
 - The use of conditional entropy sorting can be toggled through experiments.cvss/dimension_reduction_entropy and the number of retained dimensions can be set using experiments.cvss/max_number_dimensions. Activating simultaneously whitelist and conditional entropy has not been properly tested.

3. Compile and run Firres in release mode. In shell at the root of this repository, enter :
cargo run --release

4. Choose an experiment name. This will create a directory of the same name in experiments/.

5. The experiment may take some time to complete (multiple hours / days).

6. In a shell, go to the experiments/ directory, then enter :
./xp_summarize.sh <experiment name>

This will display multiple metrics and create additional CSV metrics files. You can compare them to our own results :

						                CVSS V2					                CVSS V3
Conditional entropy sorting (n=100)		20200406_formal_entropy100_cvss_v2		20200407_formal_entropy100_cvss_v3
Conditional entropy sorting (n=500)		20200408_formal_entropy500_cvss_v2		20200407_formal_entropy500_cvss_v3
Conditional entropy sorting (n=1000)	20200408_formal_entropy1000_cvss_v2		20200408_formal_entropy1000_cvss_v3
Conditional entropy sorting (n=5000)	20200408_formal_entropy5000_cvss_v2		20200408_formal_entropy5000_cvss_v3
CWE						                20200406_formal_cwe_cvss_v2			    20200406_formal_cwe_cvss_v3
CPE						                20200413_formal_cpe_cvss_v2			    20200410_formal_cpe_cvss_v3
CWE+CPE					                20200406_formal_cwe_and_cpe_cvss_v2		20200412_formal_cwe_and_cpe_cvss_v3
